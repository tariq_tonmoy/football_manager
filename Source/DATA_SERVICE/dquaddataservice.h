#ifndef DQUADDATASERVICE_H
#define DQUADDATASERVICE_H
#include<vector>
#include<string>
#include<iostream>
#include<DTO/teamdto.h>
#include<DTO/squaddto.h>
#include<fstream>
#include<DTO/profiledto.h>
#include<sstream>
#include<QString>

using namespace std;

class SquadDataService
{
    SquadDto sd;
    vector<string>player_name;
    vector<char>position;
    vector<int>overall,training;

public:
    SquadDataService();
    void writeSquad(SquadDto sd);
    SquadDto readSquad(SquadDto sd);
   // void writeTeam();

};

#endif // DQUADDATASERVICE_H
