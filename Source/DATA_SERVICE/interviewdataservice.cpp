#include "interviewdataservice.h"
#include<QMessageBox>
#include<QString>
InterviewDataService::InterviewDataService()
{
}
InterviewDto InterviewDataService::readQues(){
    QMessageBox qm;
    QString qstr;
    string ques,ans[3];
    srand(time(NULL));
    ifstream fin;
    fin.open("Questions.txt",ios::in);
    Encoder en;
    int lim=rand()%10;
    for(int i=0;i<=lim*4;i++){
        fin>>ques;
        ques=en.str_str(ques);
    }
    for(int i=0;i<3;i++){
        fin>>ans[i];
        ans[i]=en.str_str(ans[i]);


    }
    fin.close();

    InterviewDto id;
    id.setQues(ques,ans);
    return id;
}
