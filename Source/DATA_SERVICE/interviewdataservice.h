#ifndef INTERVIEWDATASERVICE_H
#define INTERVIEWDATASERVICE_H
#include<cstdio>
#include<cstdlib>
#include<iostream>
#include<fstream>
#include<string>
#include<DTO/interviewdto.h>
#include<SERVICE/encoder.h>
#include<ctime>
using namespace std;

class InterviewDataService
{
public:
    InterviewDataService();
    InterviewDto readQues();

};

#endif // INTERVIEWDATASERVICE_H
