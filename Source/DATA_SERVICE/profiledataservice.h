#ifndef PROFILEDATASERVICE_H
#define PROFILEDATASERVICE_H

#include<iostream>
#include<fstream>
#include<QString>
#include<DTO/profiledto.h>
#include<string>
#include<cstring>
#include<SERVICE/encoder.h>
#include<QMessageBox>

using namespace std;

class ProfileDataService
{
    QString *qstr;
    int count;
    Encoder en;
public:
    ProfileDataService();
    bool createFile(ProfileDto d);
    ProfileDto loadFile(ProfileDto d);
    bool isSimilar(QString *str,int n,QString *file);
    void addUserName(QString qstr);
};

#endif // PROFILEDATASERVICE_H
