#include "teamdataservice.h"

TeamDataService::TeamDataService()
{

}
TeamDto TeamDataService::readAllteam(){
    ifstream fin;
    string sponsors,str;
    Encoder en;
    vector<string>vname,vsponsors,vlocation,vcountry;
    vector<int>overall,stadium;

    fin.open("TeamName.txt",ios::in);
    str="";
    while(!fin.eof()){
        getline(fin,str);
        vname.push_back(en.str_str(str));
        str="";
        getline(fin,str);
        vlocation.push_back(en.str_str(str));
        str="";
        getline(fin,str);
        vcountry.push_back(en.str_str(str));
        str="";
        getline(fin,str);
        overall.push_back(en.int_str(str));
        str="";
        getline(fin,str);
        stadium.push_back(en.int_str(str));
        str="";
    }
    TeamDto td;
    fin.close();
    fin.open("Sponsors.txt",ios::in);
    sponsors="";
    while(!fin.eof()){
        getline(fin,sponsors);


        vsponsors.push_back(en.str_str(sponsors));
        sponsors="";
    }
    fin.close();
    td.setNewTeam(vname,vsponsors,vlocation,vcountry,overall,stadium);
    return td;
}

bool TeamDataService::wrirteTeam(SquadDto sd){

    ofstream fout;
    char *file;
    Encoder en;

    QString qstr;
    qstr=sd.get_qstr()[0];
    qstr="MyFile\\"+qstr+"TeamName.txt";

    QByteArray ba;
    ba=qstr.toLatin1();
    file=ba.data();

    fout.open(file,ios::out);



    fout<<en.str_int(sd.getTeamName().size())<<"\n";
    for(int i=0;i<sd.getTeamName().size();i++){
        fout<<en.str_str(sd.getTeamName()[i])<<"\n";
    }
    fout<<en.str_int(sd.getSponsors().size())<<"\n";
    for(int i=0;i<sd.getSponsors().size();i++){
        fout<<en.str_str(sd.getSponsors()[i])<<"\n";
    }
    fout<<en.str_int(sd.getLocation().size())<<"\n";
    for(int i=0;i<sd.getLocation().size();i++){
        fout<<en.str_str(sd.getLocation()[i])<<"\n";
    }
    fout<<en.str_int(sd.getCountry().size())<<"\n";
    for(int i=0;i<sd.getCountry().size();i++){
        fout<<en.str_str(sd.getCountry()[i])<<"\n";
    }


    fout<<en.str_int(sd.getStadium().size())<<"\n";
    for(int i=0;i<sd.getStadium().size();i++){
        fout<<en.str_int(sd.getStadium()[i])<<"\n";
    }

    fout<<en.str_int(sd.getTeamOverall().size())<<"\n";
    for(int i=0;i<sd.getTeamOverall().size();i++){
        fout<<en.str_int(sd.getTeamOverall()[i])<<"\n";
    }

    fout<<en.str_int(sd.getMatchAvgOverall())<<"\n"<<en.str_int(sd.getCurrentExpense())<<"\n"<<en.str_int(sd.getCurrentMoney())<<"\n"<<en.str_int(sd.getTeamIndex())<<"\n"<<en.str_int(sd.getSponsorIndex())<<"\n"<<en.str_int(sd.getStartMoney())<<"\n"<<en.str_int(sd.getMatchMoney())<<"\n"<<en.str_int(sd.getDrawMoney())<<"\n"<<en.str_int(sd.getWinMoney())<<"\n"<<en.str_int(sd.getAvgOverall())<<"\n";
    fout<<en.str_int(sd.getForward())<<"\n"<<en.str_int(sd.getDefence())<<"\n"<<en.str_int(sd.getMid())<<"\n"<<en.str_int(sd.getGk())<<"\n"<<en.str_int(sd.getScout())<<"\n"<<en.str_int(sd.getStadiumUpgrade())<<"\n";
    fout.close();

}


TeamDto TeamDataService::LoadTeam(SquadDto sd){

    QMessageBox qm;

    vector<string>name,sp,loc,con;
    vector<int>vstadium,voverall;
    int matchAvgOverall,currentExpense,currentMoney,teamIndex,sponsorIndex,startMoney,matchMoney,drawMoney,winMoney,avgOverall;
    int forward,defence,mid,gk,scout;
    int stadiumUpgrade;


    ifstream fin;
    char *file;
    QString qstr=sd.get_qstr()[0];
    Encoder en;
    qstr="MyFile\\"+qstr+"TeamName.txt";


    QByteArray ba;
    ba=qstr.toLatin1();
    file=ba.data();

    fin.open(file,ios::in);

    int num;

    TeamDto td;
    string str;
    str="";
    getline(fin,str);
    num=en.int_str(str);
    str="";
    for(int i=0;i<num;i++){
        getline(fin,str);
        name.push_back(en.str_str(str));
        str="";
    }

    getline(fin,str);
    num=en.int_str(str);
    str="";
    for(int i=0;i<num;i++){
        getline(fin,str);

        sp.push_back(en.str_str(str));
        str="";
    }

    getline(fin,str);
    num=en.int_str(str);
    str="";
    for(int i=0;i<num;i++){
        getline(fin,str);

        loc.push_back(en.str_str(str));
        str="";
    }

    getline(fin,str);
    num=en.int_str(str);
    str="";
    for(int i=0;i<num;i++){
        getline(fin,str);

        con.push_back(en.str_str(str));
        str="";
    }

    getline(fin,str);
    num=en.int_str(str);
    str="";
    for(int i=0;i<num;i++){
        getline(fin,str);
        vstadium.push_back(en.int_str(str));

        str="";
    }

    getline(fin,str);
    num=en.int_str(str);
    str="";
    for(int i=0;i<num;i++){
        getline(fin,str);

        voverall.push_back(en.int_str(str));
        str="";
    }

    td.setNewTeam(name,sp,loc,con,voverall,vstadium);


    getline(fin,str);
    matchAvgOverall=en.int_str(str);
    str="";

    getline(fin,str);

    currentExpense=en.int_str(str);
    str="";
    getline(fin,str);
    currentMoney=en.int_str(str);
    str="";
    getline(fin,str);
    teamIndex=en.int_str(str);
    str="";
    getline(fin,str);
    sponsorIndex=en.int_str(str);
    str="";
    getline(fin,str);
    startMoney=en.int_str(str);
    str="";
    getline(fin,str);
    matchMoney=en.int_str(str);
    str="";
    getline(fin,str);
    drawMoney=en.int_str(str);
    str="";
    getline(fin,str);
    winMoney=en.int_str(str);
    str="";
    getline(fin,str);
    avgOverall=en.int_str(str);
    str="";
    getline(fin,str);
    forward=en.int_str(str);
    str="";
    getline(fin,str);
    defence=en.int_str(str);


    str="";
    getline(fin,str);
    mid=en.int_str(str);
    str="";
    getline(fin,str);
    gk=en.int_str(str);
    str="";

    getline(fin,str);
    scout=en.int_str(str);
    str="";

    getline(fin,str);
    stadiumUpgrade=en.int_str(str);
    str="";

    td.setMySponsor(sponsorIndex,startMoney,matchMoney,winMoney,drawMoney);
    td.setMyTeam(teamIndex);
    td.setAvgOverall(avgOverall);
    td.setMatchAvgOverall(matchAvgOverall);
    td.setCoaching(forward,mid,defence,gk,scout);
    td.setCurrentMoney(currentMoney);
    td.setCurrentExpense(currentExpense);
    td.setStadiumUpgrade(stadiumUpgrade);



    fin.close();


    return td;

}











