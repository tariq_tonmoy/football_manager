#ifndef TEAMDATASERVICE_H
#define TEAMDATASERVICE_H
#include<iostream>
#include<fstream>
#include<DTO/teamdto.h>
#include<string>
#include<vector>
#include<DTO/teamdto.h>
#include<SERVICE/encoder.h>
#include<sstream>
#include<DTO/profiledto.h>
#include<QString>
#include<QMessageBox>
#include<DTO/squaddto.h>
using namespace std;

class TeamDataService
{

public:
    TeamDataService();
    TeamDto readAllteam();
    bool wrirteTeam(SquadDto sd);
    TeamDto LoadTeam(SquadDto pd);

};

#endif // TEAMDATASERVICE_H
