#ifndef INTERVIEWDTO_H
#define INTERVIEWDTO_H
#include<string>
using namespace std;

class InterviewDto
{
    string ques,ans[3];
    int res;
public:
    InterviewDto();
    void setQues(string ques,string* ans);
    string getQues();
    string* getAns();
    void setRes(int i){
        this->res=i;
    }

    int getRes(){
        return this->res;
    }
};

#endif // INTERVIEWDTO_H
