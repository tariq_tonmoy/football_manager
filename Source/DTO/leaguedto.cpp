#include "leaguedto.h"

bool LeagueDto::WriteLeague(SquadDto sd){

    char *file;
    string str;
    QString qstr;
    Encoder en;


    qstr="MyFile\\"+sd.get_qstr()[0]+"League.txt";



    QByteArray ba=qstr.toLatin1();
    file=ba.data();

    ofstream fout;
    fout.open(file,ios::out);
    if(!fout)
        return false;

    fout<<en.str_int(this->a.size())<<"\n";
    for(int i=0;i<this->a.size();i++){
        fout<<en.str_int(this->a[i])<<"\n";
    }

    fout<<en.str_int(this->b.size())<<"\n";
    for(int i=0;i<this->b.size();i++){
        fout<<en.str_int(this->b[i])<<"\n";
    }

    fout<<en.str_int(this->goalA.size())<<"\n";
    for(int i=0;i<this->goalA.size();i++){
        fout<<en.str_int(this->goalA[i])<<"\n";
    }

    fout<<en.str_int(this->goalB.size())<<"\n";
    for(int i=0;i<this->goalB.size();i++){
        fout<<en.str_int(this->goalB[i])<<"\n";
    }

    fout<<en.str_int(this->point.size())<<"\n";
    for(int i=0;i<this->point.size();i++){
        fout<<en.str_int(this->point[i])<<"\n";
    }

    fout<<en.str_int(this->goalDiff.size())<<"\n";
    for(int i=0;i<this->goalDiff.size();i++){
        fout<<en.str_int(this->goalDiff[i])<<"\n";
    }


    fout<<en.str_int(this->awayGoal.size())<<"\n";
    for(int i=0;i<this->awayGoal.size();i++){
        fout<<en.str_int(this->awayGoal[i])<<"\n";
    }



    fout<<en.str_int(this->playerName.size())<<"\n";
    for(int i=0;i<this->playerName.size();i++){
        fout<<en.str_str(this->playerName[i])<<"\n";
    }

    fout<<en.str_int(this->teamName.size())<<"\n";
    for(int i=0;i<this->teamName.size();i++){
        fout<<en.str_str(this->teamName[i])<<"\n";
    }

    fout<<en.str_int(this->goalCount.size())<<"\n";
    for(int i=0;i<this->goalCount.size();i++){
        fout<<en.str_int(this->goalCount[i])<<"\n";
    }

    fout<<en.str_int(this->LeaguePosition.size())<<"\n";
    for(int i=0;i<this->LeaguePosition.size();i++){
        fout<<en.str_int(this->LeaguePosition[i])<<"\n";
    }

    fout<<en.str_int(this->SeasonCount)<<"\n"<<en.str_int(this->currentMatch)<<"\n";
    fout.close();


}


bool LeagueDto::readDto(SquadDto sd){
    char *file;
    string str;
    QString qstr;
    Encoder en;


    qstr="MyFile\\"+sd.get_qstr()[0]+"League.txt";

    QByteArray ba=qstr.toLatin1();
    file=ba.data();

    ifstream fin;
    fin.open(file,ios::in);
    if(!fin)
        return false;

    str="";
    str="";
    getline(fin,str);
    int num=en.int_str(str);
    a.clear();
    for(int i=0;i<num;i++){
        str="";
        str="";
        getline(fin,str);
        this->a.push_back(en.int_str(str));
    }

    str="";
    str="";
    getline(fin,str);
    num=en.int_str(str);
    b.clear();
    for(int i=0;i<num;i++){
        str="";
        str="";
        getline(fin,str);
        this->b.push_back(en.int_str(str));
    }
    str="";
    getline(fin,str);
    num=en.int_str(str);
    goalA.clear();
    for(int i=0;i<num;i++){
        str="";
        getline(fin,str);
        this->goalA.push_back(en.int_str(str));
    }
    str="";
    getline(fin,str);
    num=en.int_str(str);
    goalB.clear();
    for(int i=0;i<num;i++){
        str="";
        getline(fin,str);
        this->goalB.push_back(en.int_str(str));
    }
    str="";
    getline(fin,str);
    num=en.int_str(str);
    point.clear();
    for(int i=0;i<num;i++){
        str="";
        getline(fin,str);
        this->point.push_back(en.int_str(str));
    }
    str="";
    getline(fin,str);
    num=en.int_str(str);
    goalDiff.clear();
    for(int i=0;i<num;i++){
        str="";
        getline(fin,str);
        this->goalDiff.push_back(en.int_str(str));
    }
    str="";
    getline(fin,str);
    num=en.int_str(str);
    awayGoal.clear();
    for(int i=0;i<num;i++){
        str="";
        getline(fin,str);
        this->awayGoal.push_back(en.int_str(str));
    }

    str="";
    getline(fin,str);
    num=en.int_str(str);
    playerName.clear();
    for(int i=0;i<num;i++){
        str="";
        getline(fin,str);
        this->playerName.push_back(en.str_str(str));

    }

    str="";
    getline(fin,str);
    num=en.int_str(str);
    teamName.clear();
    for(int i=0;i<num;i++){
        str="";
        getline(fin,str);
        this->teamName.push_back(en.str_str(str));

    }

    str="";
    getline(fin,str);
    num=en.int_str(str);
    goalCount.clear();
    for(int i=0;i<num;i++){
        str="";
        getline(fin,str);
        this->goalCount.push_back(en.int_str(str));
    }

    str="";
    getline(fin,str);
    num=en.int_str(str);
    LeaguePosition.clear();
    for(int i=0;i<num;i++){
        str="";
        getline(fin,str);
        this->LeaguePosition.push_back(en.int_str(str));
    }
    str="";
    getline(fin,str);

    this->SeasonCount=en.int_str(str);
    str="";
    getline(fin,str);
    this->currentMatch=en.int_str(str);

    fin.close();
    return true;

}








