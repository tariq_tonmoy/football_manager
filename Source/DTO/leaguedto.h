#ifndef LEAGUEDTO_H
#define LEAGUEDTO_H
#include<vector>
#include<string>
#include<DTO/squaddto.h>
//#include<SERVICE/leagueservice.h>
#include<DTO/popularitydto.h>
#include<QMessageBox>
#include<QString>
#include<DTO/leaguedto.h>
#include<ctime>
#include<cstdlib>
#include<SERVICE/encoder.h>
#include<fstream>
#include<ios>
#include<iostream>

using namespace std;

class LeagueDto{

    SquadDto sd;
    vector<int>a,b;
    vector<int>goalA,goalB;
    vector<int>point,goalDiff,awayGoal;
    vector<string>playerName,teamName;
    vector<int>goalCount;
    vector<int>LeaguePosition;
    int SeasonCount;
    int currentMatch;
public:
    LeagueDto():SeasonCount(0){;}
    void setSquad(SquadDto sd){
        this->sd=sd;
    }

    SquadDto getSquad(){
        return this->sd;
    }

    void setMatches(){
       // LeagueService ls;
        vector<int>a1(sd.getTeamName().size());
        vector<int>b1(sd.getTeamName().size());
        setMatches(a1,b1);
        this->a=a1;
        this->b=b1;

        vector<int>init(this->sd.getTeamName().size(),0),initGoal(this->a.size(),-1);
        this->point=init;
        this->goalDiff=init;
        this->awayGoal=init;
        this->goalA=initGoal;
        this->goalB=initGoal;
        this->currentMatch=0;

    }

    vector<int> getA(){
        return this->a;
    }

    vector<int> getB(){
        return this->b;
    }


    void setPoint(int point,int pos){
        this->point[pos]=point;
    }

    vector<int> getPoint(){
        return this->point;
    }

    void setGoalDiff(int goal,int pos){
        this->goalDiff[pos]=goal;
    }

    vector<int> getGoalDiff(){
        return this->goalDiff;
    }

    void setAwayGoal(int goal,int pos){
        this->awayGoal[pos]=goal;
    }

    vector<int> getAwayGoal(){
        return this->awayGoal;
    }

    void setLeague(vector<int>point,vector<int>goalDiff,vector<int>awayGoal){
        this->point=point;
        this->goalDiff=goalDiff;
        this->awayGoal=awayGoal;
    }
    void setGoalA(int goal,int pos){
        this->goalA[pos]=goal;
    }

    vector<int> getGoalA(){
        return this->goalA;
    }

    void setGoalB(int goal,int pos){
        this->goalB[pos]=goal;
    }

    vector<int> getGoalB(){
        return this->goalB;
    }

    void setCurrentMatch(int i){
        this->currentMatch=i;
    }

    int getCurrentMatch(){
        return this->currentMatch;
    }

    void setScorer(string name,string team){
        this->playerName.push_back(name);
        this->teamName.push_back(team);
        this->goalCount.push_back(1);
    }

    void setGoalCount(int pos){
        this->goalCount[pos]++;
    }

    vector<string> getPlayerName(){
        return this->playerName;
    }

    vector<string> getTeamName(){
        return this->teamName;


    }

    vector<int> getGoalCount(){
        return this->goalCount;
    }


    void setMatches(vector<int>&a,vector<int>&b){

        vector<int>a1,b1;
        int temp1,temp2;
        bool flag;
        srand(time(NULL));
        for(int i=0;i<a.size();i++){
            for(int j=0;j<b.size();){
                if(i==j){
                    j++;
                    continue;
                }
                temp1=rand()%a.size();
                temp2=rand()%b.size();
                if(temp1==temp2)
                    continue;
                flag=true;
                for(int k=0;k<a1.size();k++){
                    if((temp1==a1[k]&&temp2==b1[k])){
                        flag=false;
                        break;
                    }
                }
                if(flag==false)continue;
                a1.push_back(temp1);
                b1.push_back(temp2);
                j++;

            }
        }
        a=a1;
        b=b1;
    }

    vector<int> getLeaguePosition(){
        return this->LeaguePosition;
    }

    void setLeaguePosition(int pos){
        this->LeaguePosition.push_back(pos);
    }

    void setSeasonCount(int count){
        this->SeasonCount=count;

    }

    int getSeasonCount(){
        return this->SeasonCount;
    }

    void clearAll(){
        this->a.clear();
        this->b.clear();
        this->goalA.clear();
        this->goalB.clear();
        this->awayGoal.clear();
        this->goalCount.clear();
        this->point.clear();
        this->goalDiff.clear();
        this->teamName.clear();
        this->playerName.clear();

    }

    bool WriteLeague(SquadDto sd);
    bool readDto(SquadDto sd);



};


#endif // LEAGUEDTO_H
