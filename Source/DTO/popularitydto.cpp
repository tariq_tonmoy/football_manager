#include "popularitydto.h"
#include<QString>
#include<iostream>
#include<fstream>
using namespace std;


PopularityDto::PopularityDto()
{
    this->interview=0;
    this->ticketPrice=0;
    this->ticket=70;
    this->match=0;
    this->popularity=0;
    this->overall=0;
    this->souvenir=0;
}

void PopularityDto::setPopularityComponents(int interview,int ticket,int match,int overall){
    this->interview=interview;
    this->match=match;
    this->ticket=ticket;
    this->overall=overall;

    this->popularity=pps.countPopularity(this->match,this->interview,this->ticket);
    this->souvenir=pps.countSouvenir(this->overall,this->popularity);
    this->ticketPrice=countTicketPrice();


}


void PopularityDto::setPopularityComponents(int interview,int overall){
    this->interview=interview;
    this->match=0;
    this->ticket=70;
    this->overall=overall;


    this->popularity=pps.countPopularity(this->interview);
    this->souvenir=pps.countSouvenir(this->overall,this->popularity);
    this->ticketPrice=this->countTicketPrice();
}


void PopularityDto::setPopularityComponents(int interview,int ticket,int overall){
    this->interview=interview;
    this->match=0;
    this->ticket=ticket;
    this->overall=overall;


    this->popularity=pps.countPopularity(this->interview,this->ticket);
    this->souvenir=pps.countSouvenir(this->overall,this->popularity);
    this->ticketPrice=this->countTicketPrice();
}



bool PopularityDto::WritePopularity(SquadDto sd){

    QString qstr;
    string str;
    QByteArray ba;
    char* file;
    ofstream fout;
    Encoder en;

    qstr="MyFile\\"+sd.get_qstr()[0]+"Popularity.txt";
    ba=qstr.toLatin1();
    file=ba.data();

    fout.open(file,ios::out);
    if(!fout)
        return false;

    fout<<en.str_int(this->interview)<<"\n";
    fout<<en.str_int(this->ticket)<<"\n";
    fout<<en.str_int(this->match)<<"\n";
    fout<<en.str_int(this->popularity)<<"\n";
    fout<<en.str_int(this->souvenir)<<"\n";
    fout<<en.str_int(this->overall)<<"\n";
    fout<<en.str_int(this->ticketPrice)<<"\n";

    fout.close();

    return true;

}


bool PopularityDto::ReadPopularity(SquadDto sd){

    QString qstr;
    string str;
    QByteArray ba;
    char* file;
    ifstream fin;
    Encoder en;

    qstr="MyFile\\"+sd.get_qstr()[0]+"Popularity.txt";
    ba=qstr.toLatin1();
    file=ba.data();

    fin.open(file,ios::in);

    str="";
    str="";
    getline(fin,str);
    this->interview=en.int_str(str);
    str="";
    str="";
    getline(fin,str);
    this->ticket=en.int_str(str);
    str="";
    str="";
    getline(fin,str);
    this->match=en.int_str(str);
    str="";
    str="";
    getline(fin,str);
    this->popularity=en.int_str(str);
    str="";
    str="";
    getline(fin,str);
    this->souvenir=en.int_str(str);
    str="";
    str="";
    getline(fin,str);
    this->overall=en.int_str(str);
    str="";
    str="";
    getline(fin,str);
    this->ticketPrice=en.int_str(str);

    fin.close();
    return true;
}
