#ifndef POPULARITYDTO_H
#define POPULARITYDTO_H
#include<SERVICE/popularityservice.h>
#include<DTO/squaddto.h>

class PopularityDto
{

    int interview,ticket,match,popularity,souvenir,overall;
    int ticketPrice;
    PopularityService pps;
public:
    PopularityDto();
    void setPopularityComponents(int interview,int ticket,int match,int overall);
    void setPopularityComponents(int interview,int overall);
    void setPopularityComponents(int interview,int ticket,int overall);
    void setPopularity(int popularity){
        this->popularity=popularity;

    }

    void setSouvenir(int souvenir){
        this->souvenir=souvenir;
    }


    int getInterview(){
        return this->interview;
    }
    int getMatch(){
        return this->match;
    }
    int getTicket(){
        return this->ticket;
    }

    int getOverall(){
        return this->overall;
    }

    int getPopularity(){
        return this->popularity;
    }

    int getSouvenir(){
        return this->souvenir;
    }

    int getTicketPrice(){
        return this->ticketPrice;
    }


    int countTicketPrice(){
        float i;
        int ticket_price;
        if(this->ticket==70)
            i=0.0;
        if(this->ticket==75)
            i=-10.0;
        if(this->ticket==80)
            i=-15.0;
        if(this->ticket==85)
            i=-20.0;
        if(this->ticket==55)
            i=10.0;
        if(this->ticket==50)
            i=15.0;
        if(this->ticket==45)
            i=20.0;

        float f;
        f=(0.7*(float)this->overall)+(0.7*(float)this->overall)*i/100.0;
        ticket_price=(int)f;
        return ticket_price;
    }

    bool WritePopularity(SquadDto sd);
    bool ReadPopularity(SquadDto sd);

};

#endif // POPULARITYDTO_H
