#ifndef PROFILEDTO_H
#define PROFILEDTO_H
#include<QString>


class ProfileDto
{
    QString *qstr;
        int count;
    QString userName;
public:
    ProfileDto();
    void setProfiledto(ProfileDto pd){
        *this=pd;
    }
    void set_qstr(QString *qstr,int i);
    int get_count();
    QString* get_qstr();
    void setUser(QString str);
    QString getUser();
};

#endif // PROFILEDTO_H
