#include "squaddto.h"

SquadDto SquadDto::diskIO(){

    ifstream fin;
    Encoder en;
    ofstream fout;
    string pn;
    char pos;
    int ovr;
    char *file;
    QString qstr;

    string sfile=this->TeamDto::getTeamName()[this->TeamDto::getTeamIndex()];
    sfile+=".txt";

    qstr=qstr.fromStdString(sfile);
    QByteArray ba=qstr.toLatin1();
    file=ba.data();

    fin.open(file,ios::in);

    string temp="";
    while(!fin.eof()){
        getline(fin,temp);
        this->SquadDto::player_name.push_back(en.str_str(temp));

        temp="";
        getline(fin,temp);
        this->SquadDto::overall.push_back(en.int_str(temp));

        temp="";
        getline(fin,temp);
        this->SquadDto::position.push_back(en.encode(temp[0]));
        temp="";
        this->SquadDto::train.push_back(0);
    }
    fin.close();


    string un=this->ProfileDto::get_qstr()[0].toStdString();

    sfile="MyFile\\"+un+sfile;
    qstr=qstr.fromStdString(sfile);
    ba=qstr.toLatin1();
    file=ba.data();



    fout.open(file,ios::out);

    for(int i=0;i<player_name.size();i++){
        fout<<en.str_str(this->SquadDto::player_name[i])<<"\n";
        fout<<en.str_int(this->SquadDto::overall[i])<<"\n";
        fout<<en.encode(this->SquadDto::position[i])<<"\n";
        fout<<en.str_int(this->SquadDto::train[i])<<"\n";

    }

    fout.close();
    return *this;
}

SquadDto SquadDto::diskIn(){
    ifstream fin;
    QString qstr;
    char *file;
    int num;
    Encoder en;
    string str="MyFile\\"+this->ProfileDto::get_qstr()[0].toStdString()+this->TeamDto::getTeamName()[this->TeamDto::getTeamIndex()]+".txt";

    qstr=qstr.fromStdString(str);
    QByteArray ba;
    ba=qstr.toLatin1();
    file=ba.data();

    fin.open(file,ios::in);

    while(!fin.eof()){
        fin>>str;
        this->SquadDto::player_name.push_back(en.str_str(str));
        fin>>str;
        this->SquadDto::overall.push_back(en.int_str(str));
        fin>>str;
        this->SquadDto::position.push_back(en.encode(str[0]));
        fin>>str;
        this->SquadDto::train.push_back(en.int_str(str));
    }
    fin.close();
    return *this;



}


bool SquadDto::hasSquad(){
    ifstream fin;
    char *file;
    int num;
    Encoder en;
    QString qstr;

    string str="MyFile\\"+this->ProfileDto::get_qstr()[0].toStdString()+this->TeamDto::getTeamName()[this->TeamDto::getTeamIndex()]+".txt";

    qstr=qstr.fromStdString(str);
    QByteArray ba;
    ba=qstr.toLatin1();
    file=ba.data();

    fin.open(file,ios::in);
    if(fin){
        fin.close();
        return true;
    }
    else return false;

}


void SquadDto::resetRand(){
    {
            QMessageBox qm;
            int temp;
            TransferService trsv;
            bool flag=true;
            for(int i=0;i<10;i++){
                this->rNum[i]=-1;
            }
            if(this->playerCount<12)
                return ;
            srand(time(NULL));
            for(int i=0;i<10;){
                flag=true;
                temp=rand()%playerCount;
                for(int j=0;j<i;j++){
                    if(rNum[j]==temp){
                        flag=false;

                        break;
                    }
                }
                if(flag==false)continue;


                rNum[i]=temp;
                i++;
            }
            for(int i=0;i<10;i++){
                this->price[i]=trsv.calculatePrice(this->overall[rNum[i]]);
            }
        }

}

bool SquadDto::WriteSquad(){


    Encoder en;
    char *file;
    ofstream fout;
    QString qstr;

    qstr="MyFile\\"+this->get_qstr()[0]+QString::fromStdString(this->getTeamName()[this->getTeamIndex()])+".txt";

    QMessageBox qm;
    QByteArray ba=qstr.toLatin1();
    file=ba.data();


    fout.open(file,ios::out);
    if(!fout)
        return false;

    fout<<en.str_int(this->player_name.size())<<"\n";
    for(int i=0;i<this->player_name.size();i++){
        fout<<en.str_str(this->player_name[i])<<"\n";
    }

    fout<<en.str_int(this->player_name.size())<<"\n";
    for(int i=0;i<this->player_name.size();i++){
        fout<<en.encode(this->position[i])<<"\n";
    }

    fout<<en.str_int(this->player_name.size())<<"\n";
    for(int i=0;i<this->player_name.size();i++){
        fout<<en.str_int(this->overall[i])<<"\n";
    }

    fout<<en.str_int(this->player_name.size())<<"\n";
    for(int i=0;i<this->player_name.size();i++){
        fout<<en.str_int(this->train[i])<<"\n";
    }

    fout<<en.str_int(this->playerCount)<<"\n";

    for(int i=0;i<10;i++){
        fout<<en.str_int(this->rNum[i])<<"\n";
    }

    for(int i=0;i<10;i++){
        fout<<en.str_int(this->price[i])<<"\n";
    }


    fout<<en.str_int(this->bonus)<<"\n"<<en.str_int(this->salary)<<"\\n"<<en.str_int(this->totalSalary)<<"\n";


    fout.close();


    return true;
}





bool SquadDto::LoadSqauad(){
    QMessageBox qm;

    char *file;
    string str;

    QString qstr;
    ifstream fin;

    Encoder en;
    qstr="MyFile\\"+this->get_qstr()[0]+QString::fromStdString(this->getTeamName()[this->getTeamIndex()])+".txt";

    QByteArray ba=qstr.toLatin1();
    file=ba.data();
    fin.open(file,ios::in);

    if(!fin)
        return false;
    str="";
    getline(fin,str);

    int num=en.int_str(str);
    for(int i=0;i<num;i++){
        str="";
        getline(fin,str);
        this->player_name.push_back(en.str_str(str));

    }


    str="";
    str="";
    getline(fin,str);
    num=en.int_str(str);
    for(int i=0;i<num;i++){
        str="";
        getline(fin,str);
        this->position.push_back(en.encode(str[0]));

    }

    str="";
    str="";
    getline(fin,str);
    num=en.int_str(str);
    for(int i=0;i<num;i++){
        str="";
        getline(fin,str);
        this->overall.push_back(en.int_str(str));
    }


    str="";
    str="";
    getline(fin,str);
    num=en.int_str(str);
    for(int i=0;i<num;i++){
        str="";
        getline(fin,str);
        this->train.push_back(en.int_str(str));

    }

    str="";
    str="";
    getline(fin,str);
    this->playerCount=en.int_str(str);

    for(int i=0;i<10;i++){
        str="";
        str="";
        getline(fin,str);
        this->rNum[i]=en.int_str(str);
    }

    for(int i=0;i<10;i++){
        str="";
        str="";
        getline(fin,str);
        this->price[i]=en.int_str(str);

    }
    str="";
    str="";
    getline(fin,str);
    this->bonus=en.int_str(str);
    str="";
    getline(fin,str);
    this->salary=en.int_str(str);
    str="";
    getline(fin,str);
    this->totalSalary=en.int_str(str);

    fin.close();
    return true;
}

