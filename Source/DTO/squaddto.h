#ifndef SQUADDTO_H
#define SQUADDTO_H
#include<vector>
#include<string>
#include<iostream>
#include<DTO/teamdto.h>
#include<iostream>
#include<fstream>
#include<SERVICE/encoder.h>
#include<QMessageBox>
#include<QString>
#include<DTO/profiledto.h>
#include<cstdlib>
#include<ctime>
#include<SERVICE/transferservice.h>

using namespace std;

using namespace std;
class SquadDto:public TeamDto,public ProfileDto
{
    vector<string>player_name;
    vector<char>position;
    vector<int>overall,train;
    int playerCount;
    int rNum[10],price[10];
    int bonus,salary,totalSalary;

public:
    SquadDto():playerCount(15),bonus(0),salary(0),totalSalary(0){
        for(int i=0;i<10;i++){
            rNum[i]=0;
            price[i]=0;
        }



    }

    void setPlayer(vector<string>player_name,vector<int>overall,vector<char>position,vector<int>train){
        this->player_name=player_name;
        this->overall=overall;
        this->position=position;
        this->train=train;
    }
    void setTrain(int value,int pos){
        this->train[pos]=value;
    }
    void setPlayer_name(string name,int pos){
        this->player_name[pos]=name;
    }

    void setPlayerOverall(int overall,int pos){
        this->overall[pos]=overall;
    }

    void setPosition(char ch,int pos){
        this->position[pos]=ch;
    }

    vector<string> getPlayerName(){
        return this->player_name;
    }

    vector<char> getPosition(){
        return this->position;
    }

    vector<int> getPlayerOverall(){
        return this->overall;
    }

    vector<int> getTrain(){
        return this->train;
    }

    void setPlayerCount(int i){
        this->playerCount=i;
    }

    int getPlayerCount(){
        return this->playerCount;
    }

    void resetRand();
    int* getRand(){
        return this->rNum;
    }

    int* getPrice(){
        return this->price;
    }

    void setBonus(int bonus){
        this->bonus=bonus;
    }

    int getBonus(){
        return this->bonus;
    }


    void setSalary(int salary){
        this->salary=salary;
    }

    int getSalary(){
        return this->salary;
    }

    void setTotalSalary(int i){
        this->totalSalary=i;
    }

    int getTotalSalary(){
        return this->totalSalary;
    }



    SquadDto diskIO();
    SquadDto diskIn();
    void diskOut();
    bool hasSquad();

    bool WriteSquad();
    bool LoadSqauad();

};

#endif // SQUADDTO_H
