#include "teamdto.h"


void TeamDto::setNewTeam(vector<string>name, vector<string>sponsors, vector<string>location, vector<string>country, vector<int>overall, vector<int>stadium){
    this->vname=name;
    this->vsponsors=sponsors;
    this->vlocation=location;
    this->vcountry=country;
    this->voverall=overall;
    this->vstadium=stadium;


}

vector<string> TeamDto::getTeamName(){
    return this->vname;
}

vector<string> TeamDto::getSponsors(){
    return this->vsponsors;
}


vector<string> TeamDto::getLocation(){
    return this->vlocation;
}

vector<string> TeamDto::getCountry(){
    return this->vcountry;
}

vector<int> TeamDto::getTeamOverall(){
    return this->voverall;
}

vector<int> TeamDto::getStadium(){
    return this->vstadium;
}

void TeamDto::setMyTeam(int teamIndex){
    this->teamIndex=teamIndex;

}

int TeamDto::getTeamIndex(){
    return this->teamIndex;
}

int TeamDto::getSponsorIndex(){
    return this->sponsorIndex;
}

