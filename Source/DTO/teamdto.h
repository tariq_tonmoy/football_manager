#ifndef TEAMDTO_H
#define TEAMDTO_H
#include<string>
#include<vector>
#include<iostream>
using namespace std;
class TeamDto
{
    vector<string>vname,vsponsors,vlocation,vcountry;
    vector<int>vstadium,voverall;
    int matchAvgOverall,currentExpense,currentMoney,teamIndex,sponsorIndex,startMoney,matchMoney,drawMoney,winMoney,avgOverall;
    int forward,defence,mid,gk,scout;
    int stadiumUpgrade;

public:
    TeamDto(){
        currentMoney=0;
        currentExpense=0;
        forward=defence=mid=gk=scout=stadiumUpgrade=0;
    }
    void setTeamDto(TeamDto td){
        *this=td;
    }
    void setNewTeam(vector<string>name,vector<string>sponsors,vector<string>locationo,vector<string>country,vector<int>overall,vector<int>stadium);
    vector<string>getTeamName();
    vector<string>getSponsors();
    vector<string>getLocation();
    vector<string>getCountry();
    vector<int>getTeamOverall();
    vector<int>getStadium();
    void setMyTeam(int teamIndex);
    int getTeamIndex();
    int getSponsorIndex();
    void setMySponsor(int sponsorIndex,int startMoney,int matchMoney,int winMoney,int drawMoney){
        this->sponsorIndex=sponsorIndex;
        this->startMoney=startMoney;
        this->matchMoney=matchMoney;
        this->winMoney=winMoney;
        this->drawMoney=drawMoney;
    }

    void setCoaching(int f,int m,int d,int g,int s){
        this->forward=f;
        this->mid=m;
        this->defence=d;
        this->gk=g;
        this->scout=s;
    }


    void setTeamOverall(int overall,int pos){
        this->voverall[pos]=overall;
    }

    void setForward(int f){
        this->forward=f;

    }
    void setMid(int m){
        this->mid=m;

    }
    void setdefence(int d){
        this->defence=d;

    }

    void setGk(int g){
        this->gk=g;
    }

    void setScout(int s){
        this->scout=s;
    }
    int getForward(){
        return this->forward;
    }

    int getMid(){
        return this->mid;

    }

    int getDefence(){
        return this->defence;
    }

    int getGk(){
        return this->gk;
    }

    int getScout(){
        return this->scout;
    }



    int getStartMoney(){
        return this->startMoney;
    }
    int getMatchMoney(){
        return this->matchMoney;
    }

    int getWinMoney(){
        return this->winMoney;
    }

    int getDrawMoney(){
        return this->drawMoney;
    }
    void setCurrentMoney(int currentMoney){
        this->currentMoney+=currentMoney;
    }

    int getCurrentMoney(){
        return this->currentMoney;
    }

    void setCurrentExpense(int currentExpense){
        this->currentExpense+=currentExpense;
    }

    int getCurrentExpense(){
        return this->currentExpense;
    }

    void setAvgOverall(int avgOverall){
        this->avgOverall=avgOverall;
    }

    int getAvgOverall(){
        return this->avgOverall;
    }
    void setMatchAvgOverall(int n){
        this->matchAvgOverall=n;
    }

    int getMatchAvgOverall(){
        return this->matchAvgOverall;
    }

    void setStadiumUpgrade(int stadiumUpgrade){
        this->stadiumUpgrade=stadiumUpgrade;
    }

    int getStadiumUpgrade(){
        return this->stadiumUpgrade;
    }


};

#endif // TEAMDTO_H
