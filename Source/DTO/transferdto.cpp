#include "transferdto.h"

TransferDto::TransferDto()
{
    this->teamCount=0;

}

TransferDto TransferDto::setOtherTeams(){

    QString qstr;
    QByteArray ba;
    char *file;
    Encoder en;


    int i,j,k=0;
    fstream fin,fout;

    string str;
    for(i=0;i<sd.getTeamName().size();i++){

        if(i==sd.getTeamIndex())
            continue;
        this->teamCount++;
        vector<string>pn;
        vector<int>ovr;
        vector<char>pos;
        str=sd.getTeamName()[i];
        str+=".txt";
        qstr=qstr.fromStdString(str);
        ba=qstr.toLatin1();
        file=ba.data();

        fin.open(file,ios::in);
        str="";
        pn.clear();
        ovr.clear();
        pos.clear();
        while(!fin.eof()){
            getline(fin,str);
            pn.push_back(en.str_str(str));
            str="";
            getline(fin,str);
            ovr.push_back(en.int_str(str));
            str="";
            getline(fin,str);
            pos.push_back(en.encode(str[0]));
            str="";
        }
        this->otherTeamName.push_back(sd.getTeamName()[i]);
        this->otherPlayerName.push_back(pn);
        this->otherPlayerOverall.push_back(ovr);
        this->otherPosition.push_back(pos);
        this->otherPlayerCount.push_back(this->otherPlayerName[k].size());

        fin.close();



        qstr=qstr.fromStdString(sd.getTeamName()[i]);
        qstr="MyFile\\"+this->sd.get_qstr()[0]+qstr+".txt";
        ba=qstr.toLatin1();
        file=ba.data();


        fout.open(file,ios::out);


        for(j=0;j<this->otherPlayerName[k].size();j++){
            fout<<en.str_str(this->otherPlayerName[k][j])<<"\n";
            fout<<en.str_int(this->otherPlayerOverall[k][j])<<"\n";
            fout<<en.encode(this->otherPosition[k][j])<<"\n";


        }
        k++;
        this->teamCount=k;
        fout.close();
    }
    return *this;
}


bool TransferDto::WriteTransfer(SquadDto sd){
    QByteArray ba;
    ofstream fout;
    QString qstr;
    string str;
    char *file;
    Encoder en;
    QMessageBox qm;

    int i;

    for(i=0;i<this->getOtherTeamName().size();i++){
        qstr="MyFile\\"+sd.get_qstr()[0]+QString::fromStdString(this->getOtherTeamName()[i])+".txt";
        ba=qstr.toLatin1();
        file=ba.data();

        fout.open(file,ios::out);
        if(!fout){
            qm.setText("Cannot Create Transfer File");
            qm.exec();
            return false;
        }


        for(int j=0;j<this->otherPlayerCount[i];j++){

            fout<<en.str_str(this->otherPlayerName[i][j])<<"\n";
            fout<<en.str_int(this->otherPlayerOverall[i][j])<<"\n";
            fout<<en.encode(this->otherPosition[i][j])<<"\n";

        }
        fout.close();
    }


    qstr="MyFile\\"+sd.get_qstr()[0]+"Transfer.txt";
    ba=qstr.toLatin1();
    file=ba.data();

    fout.open(file,ios::out);
    if(!fout){
        qm.setText("Cannot Create Transfer File... 2");
        qm.exec();

        return false;
    }

    for (i=0;i<10;i++){
        fout<<en.str_int(this->a[i])<<"\n";
    }
    for( i=0;i<10;i++){
        fout<<en.str_int(b[i])<<"\n";
    }
    for(int i=0;i<10;i++){
        fout<<en.str_int(this->price[i])<<"\n";
    }

    fout.close();


    return true;
}


bool TransferDto::ReadTransfer(SquadDto sd){

    QString qstr;
    char* file;
    QByteArray ba;
    string str;
    Encoder en;
    ifstream fin;
    int num;

    QMessageBox qm;
    for(int i=0;i<sd.getTeamName().size();i++){
        if(sd.getTeamName()[i]!=sd.getTeamName()[sd.getTeamIndex()])
            this->otherTeamName.push_back(sd.getTeamName()[i]);
    }
    this->teamCount=this->otherTeamName.size();

    vector<string> name;
    vector<int>ovr;
    vector<char>pos;

    for(int i=0;i<this->otherTeamName.size();i++){

        qstr="MyFile\\"+sd.get_qstr()[0]+QString::fromStdString(this->getOtherTeamName()[i])+".txt";
        ba=qstr.toLatin1();
        file=ba.data();

        fin.open(file,ios::in);

        while(!fin.eof()){
            str="";
            getline(fin,str);
            if(str!="")
            name.push_back(en.str_str(str));

            str="";
            getline(fin,str);
            if(str!="")
            ovr.push_back(en.int_str(str));

            str="";
            getline(fin,str);
            if(str!="")
            pos.push_back(en.encode(str[0]));

        }

        this->otherPlayerName.push_back(name);
        this->otherPlayerOverall.push_back(ovr);
        this->otherPosition.push_back(pos);
        this->otherPlayerCount.push_back(name.size());

        name.clear();
        pos.clear();
        ovr.clear();
        fin.close();

    }

    qstr="MyFile\\"+sd.get_qstr()[0]+"Transfer.txt";
    ba=qstr.toLatin1();
    file=ba.data();

    fin.open(file,ios::in);
    if(!fin)
        return false;

    for(int i=0;i<10;i++){
        str="";
        getline(fin,str);
        this->a[i]=en.int_str(str);
    }

    for(int i=0;i<10;i++){
        str="";
        getline(fin,str);
        this->b[i]=en.int_str(str);
    }
    for(int i=0;i<10;i++){
        str="";
        getline(fin,str);
        this->price[i]=en.int_str(str);
    }
    fin.close();
    return true;

}
