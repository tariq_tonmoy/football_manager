#ifndef TRANSFERDTO_H
#define TRANSFERDTO_H
#include<DTO/squaddto.h>

#include<vector>
#include<string>
#include<iostream>
#include<fstream>
#include<QString>
#include<QMessageBox>
#include<SERVICE/encoder.h>
#include<SERVICE/transferservice.h>
using namespace std;

class TransferDto
{

    vector < vector<string> >otherPlayerName;
    vector < vector<int> >otherPlayerOverall;
    vector< vector<char> >otherPosition;
    vector<string>otherTeamName;
    SquadDto sd;
    vector<int>otherPlayerCount;
    int a[10],b[10],teamCount,price[10];


public:
    TransferDto();
    TransferDto(SquadDto sd){
        this->teamCount=0;
        this->sd=sd;


    }

    void setSquadDto(SquadDto sd){
        this->sd=sd;
    }


    TransferDto setOtherTeams();
    void resetTransfer(){

        TransferService trsv;
        trsv.randomNumber(this->a,this->b,this->teamCount,this->otherPlayerCount);
        for(int i=0;i<10;i++){
            this->price[i]=trsv.calculatePrice(this->getOtherPlayerOverall()[a[i]][b[i]]);
        }
    }

    void setOtherTeams(vector< vector<string> >name,vector<vector<int> >overall,vector<  vector<char> >position){
        this->otherPlayerName=name;
        this->otherPlayerOverall=overall;
        this->otherPosition=position;
    }

    void setOtherPlayerName(string name,int i,int j){
        this->otherPlayerName[i][j]=name;
    }
    void setOtherPlayerOverall(int num,int i,int j){
        this->otherPlayerOverall[i][j]=num;
    }

    void setOtherPosition(char c,int i,int j){
        this->otherPosition[i][j]=c;
    }

    vector< vector<string> > getOtherPlayerName(){
        return this->otherPlayerName;
    }

    vector< vector<int> > getOtherPlayerOverall(){
        return this->otherPlayerOverall;
    }

    vector< vector<char> > getOtherPosition(){
        return this->otherPosition;
    }

    vector<string> getOtherTeamName(){
        return this->otherTeamName;
    }

    vector<int> getOtherPlayerCount(){
        return this->otherPlayerCount;
    }

    void setOtherPlayerCounnt(int num,int pos){
        this->otherPlayerCount[pos]=num;
    }

    int* getA(){
        return this->a;
    }

    int* getB(){
        return this->b;
    }

    int* getPlayerPrice(){

        return this->price;

    }

    void setTeamCount(int i){
        this->teamCount=i;
    }

    int getTeamCount(){
        return this->teamCount;
    }
    void addNewPlayer(string name,int pos){
        this->otherPlayerName[pos].push_back(name);
    }
    void addNewOverall(int ovr,int pos){
        this->otherPlayerOverall[pos].push_back(ovr);
    }

    void addNewPosition(char ch,int pos){
        this->otherPosition[pos].push_back(ch);
    }

    bool WriteTransfer(SquadDto sd);
    bool ReadTransfer(SquadDto sd);
};

#endif // TRANSFERDTO_H
