# -------------------------------------------------
# Project created by QtCreator 2012-03-19T11:15:39
# -------------------------------------------------
TARGET = Football_manager_Project
TEMPLATE = app
SOURCES += main.cpp \
    mainwindow.cpp \
    newprofile.cpp \
    DTO/profiledto.cpp \
    SERVICE/encoder.cpp \
    DATA_SERVICE/profiledataservice.cpp \
    edit.cpp \
    DTO/teamdto.cpp \
    loadprofile.cpp \
    selectteam.cpp \
    SERVICE/team.cpp \
    DATA_SERVICE/teamdataservice.cpp \
    interview.cpp \
    DATA_SERVICE/interviewdataservice.cpp \
    DTO/interviewdto.cpp \
    SERVICE/interviewservice.cpp \
    DTO/popularitydto.cpp \
    SERVICE/popularityservice.cpp \
    squad.cpp \
    DTO/squaddto.cpp \
    DATA_SERVICE/dquaddataservice.cpp \
    coach.cpp \
    stadium.cpp \
    transfer.cpp \
    DTO/transferdto.cpp \
    SERVICE/transferservice.cpp \
    salary.cpp \
    DTO/leaguedto.cpp \
    SERVICE/leagueservice.cpp \
    leaguetable.cpp \
    match.cpp \
    goal.cpp \
    result.cpp \
    scorer.cpp \
    season.cpp
HEADERS += mainwindow.h \
    newprofile.h \
    DTO/profiledto.h \
    SERVICE/encoder.h \
    DATA_SERVICE/profiledataservice.h \
    edit.h \
    DTO/teamdto.h \
    loadprofile.h \
    selectteam.h \
    SERVICE/team.h \
    DATA_SERVICE/teamdataservice.h \
    interview.h \
    DATA_SERVICE/interviewdataservice.h \
    DTO/interviewdto.h \
    SERVICE/interviewservice.h \
    DTO/popularitydto.h \
    SERVICE/popularityservice.h \
    squad.h \
    DTO/squaddto.h \
    DATA_SERVICE/dquaddataservice.h \
    coach.h \
    stadium.h \
    transfer.h \
    DTO/transferdto.h \
    SERVICE/transferservice.h \
    salary.h \
    DTO/leaguedto.h \
    SERVICE/leagueservice.h \
    leaguetable.h \
    match.h \
    goal.h \
    result.h \
    scorer.h \
    season.h
FORMS += mainwindow.ui \
    newprofile.ui \
    edit.ui \
    loadprofile.ui \
    selectteam.ui \
    interview.ui \
    squad.ui \
    coach.ui \
    stadium.ui \
    transfer.ui \
    salary.ui \
    leaguetable.ui \
    match.ui \
    goal.ui \
    result.ui \
    scorer.ui \
    season.ui
