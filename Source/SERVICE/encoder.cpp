#include "encoder.h"
#include<iostream>
#include<string.h>
#include<sstream>
using namespace std;

Encoder::Encoder()
{
}

char Encoder::encode(char ch){
    unsigned temp;
    EncUnion u;
    u.c=ch;

    temp=u.e.a;
    u.e.a=u.e.b;
    u.e.b=temp;

    temp=u.e.c;
    u.e.c=u.e.d;
    u.e.d=temp;

    temp=u.e.e;
    u.e.e=u.e.f;
    u.e.f=temp;

    temp=u.e.g;
    u.e.g=u.e.h;
    u.e.h=temp;

    return u.c;
}

char* Encoder::char_qstr(QString Qstr){
    char *temp;
    QByteArray ba=Qstr.toLatin1();
    temp=ba.data();
    for(int i=0;i<strlen(temp);i++){
        temp[i]=Encoder::encode(temp[i]);
    }
    return temp;
}

QString Encoder::qstr_char(char *str){
    QString qstr;
    for(int i=0;i<strlen(str);i++){
        str[i]=encode(str[i]);
    }
    qstr=(QString)str;
    return qstr;
}

char*Encoder::char_char(char *str){
    for(int i=0;i<strlen(str);i++){
        str[i]=encode(str[i]);
    }
    return str;
}


string Encoder::str_str(string str){
    string temp=str;
    for(int i=0;i<temp.length();i++){
        temp[i]=Encoder::encode(temp[i]);
    }
    return temp;
}

string Encoder::str_int(int i){
    char str[100];
    itoa(i,str,10);
    for(int j=0;j<strlen(str);j++){
        str[j]=encode(str[j]);
    }
    return (string)str;

}

int Encoder::int_str(string str){
    char ch[100];
    stringstream ss;
    ss.str(str);
    ss>>ch;
    for(int i=0;i<strlen(ch);i++){
        ch[i]=encode(ch[i]);
    }

    return atoi(ch);


}
