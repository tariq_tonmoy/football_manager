#ifndef ENCODER_H
#define ENCODER_H
#include<QString>
#include<string>
#include<cstdlib>
using namespace std;
class Encoder
{
    class Enc{
    public:
        unsigned a:1;
        unsigned b:1;
        unsigned c:1;
        unsigned d:1;
        unsigned e:1;
        unsigned f:1;
        unsigned g:1;
        unsigned h:1;
    };
    union EncUnion{
        char c;
        Enc e;
    };

public:
    Encoder();
    char encode(char ch);
    char* char_qstr(QString Qstr);
    QString qstr_char(char *str);
    char *char_char(char *str);
    string str_str(string str);
    int int_str(string str);
    string str_int(int i);

};

#endif // ENCODER_H
