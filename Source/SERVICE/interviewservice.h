#ifndef INTERVIEWSERVICE_H
#define INTERVIEWSERVICE_H
#include<string>
#include<cstdio>
#include<ctime>
#include<cstdlib>
using namespace std;
class InterviewService
{
    string ans[3],temp[3];
public:
    InterviewService();
    void setAns(string *ans);
    string* getAns();
    int getRes(int index);
};

#endif // INTERVIEWSERVICE_H
