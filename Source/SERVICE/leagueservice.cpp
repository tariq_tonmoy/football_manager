#include "leagueservice.h"
#include<QMessageBox>
#include<algorithm>

LeagueService::LeagueService()
{
}

int LeagueService::countMatchResult(int overall1, int overall2)
{
        int win1,draw1,lose1,win2,draw2,lose2;
        int flag1,flag2,rand1,rand2;

        if(overall1%2)
            overall1++;


        if(overall2%2)
            overall2++;


        win1=overall1/2+5;
        win2=overall2/2;

        draw1=2*(50-(overall1/2));
        draw2=2*(50-(overall2/2));

        lose1=overall1/2-5;
        lose2=overall2/2;

        srand(time(NULL));
        rand1=rand()%100;
        srand(time(NULL));
        rand2=rand()%100;

        if(rand1>=0&&rand1<lose1)
            flag1=3;
        else if(rand1>=lose1&&rand1<(lose1+draw1))
            flag1=2;
        else if(rand1>=(lose1+draw1)&&rand1<=(lose1+draw1+win1))
            flag1=1;


        if(rand2>=0&&rand2<lose2)
            flag2=3;
        else if(rand2>=lose2&&rand2<(lose2+draw2))
            flag2=2;
        else if(rand2>=(lose2+draw2)&&rand2<=(lose2+draw2+win2))
            flag2=1;



        srand(time(NULL));
        int p=rand()%2;




        if(flag1==flag2&&flag1==2){
            return flag1;
        }

        if(flag1==1&&flag2==1){
            if(!p)
                return 1;
            else return 3;
        }


        if(flag1==3&&flag2==3){
            if(!p)
                return 3;
            else return 1;
        }


        if(flag1==1&&flag2==2){
            if(!p)
                return 1;
            else return 2;
        }


        if(flag1==2&&flag2==1){
            if(!p)
                return 2;
            else return 3;
        }


        if(flag1==2&&flag2==3){
            if(!p)
                return 2;
            else return 1;
        }

        if(flag1==3&&flag2==2){
            if(!p)
                return 3;
            else return 2;
        }

        if((flag1==1&&flag2==3)||(flag1==3&&flag2==1)){
            return flag1;
        }
}




int LeagueService::countOverall(SquadDto sd, PopularityDto ppd){
    float avg=0.0;

    float ovr=(float)sd.getTeamOverall()[sd.getTeamIndex()]*40.0/100.0+(float)ppd.getPopularity()*30.0/100.0+((float)sd.getScout()*100.0/10.0)*5.0/100.0+(float)sd.getAvgOverall()*25.0/100.0;


    for(int i=0;i<sd.getTrain().size();i++){
        avg+=(float)sd.getTrain()[i];
    }
    avg=avg/15.0;

    float playerOverall=((float)sd.getMatchAvgOverall()*70.0/100.0)+((((float)sd.getForward()+(float)sd.getMid()+(float)sd.getDefence()+(float)sd.getGk())/4.0))+((avg*100.0/10.0)*15.0/100.0)+(((float)sd.getSalary()*100.0/10.0)*5.0/100.0);


    float p=(playerOverall*55.0/100.0)+ovr*40.0/100.0+(((float)sd.getBonus()*100.0/10.0)*5.0/100.0);

    if(p<50.0)
        p=50.0;
    if(p>=99.0)
        p=99.0;

    return (int)p;
}


void LeagueService::countGoal(int &a, int &b, int res,LeagueDto ld, SquadDto sd){
    int i=0,j=0;
    int home=0,away=0,goal1,goal2;
    int ot;
    int flag1=0,flag2=0,flag3=0,flag4=0;
    int gd,dm,mf;
    int temp1,temp2;

    QMessageBox qm;

    srand(time(NULL));
    i=ld.getA()[ld.getCurrentMatch()];
    j=ld.getB()[ld.getCurrentMatch()];


    if(i==sd.getTeamIndex()){
        ot=sd.getTeamOverall()[ld.getA()[ld.getCurrentMatch()]];
        away=0;
        home=1;
    }
    if(j==sd.getTeamIndex()){
        ot=sd.getTeamOverall()[ld.getB()[ld.getCurrentMatch()]];
        home=0;
        away=1;

    }



    if(home==1||away==1){
        if(ot<=75)
            flag1=1;
        else if(ot<=90)
            flag1=2;
        else flag1=3;

        gd=sd.getGk()+sd.getDefence();
        gd=gd/2;
        dm=sd.getDefence()+sd.getMid();
        dm=dm/2;
        mf=sd.getForward()+sd.getForward();
        mf=mf/2;


        if(gd<=4)
            flag2=1;
        else if(gd<=8)
            flag2=2;
        else flag2=3;


        if(dm<=4)
            flag3=1;
        else if(dm<=8)
            flag3=2;
        else flag3=3;

        //qm.setText("FLAG3: "+QString::number(flag3));
        //qm.exec();


        if(mf<=4)
            flag4=1;
        else if(mf<=8)
            flag4=2;
        else flag4=3;


        if(res==1||res==3){
            if((flag1==1&&flag2==1)||(flag1==2&&flag2==2)||(flag1==3&&flag2==3)||(flag1==1&&flag2==2)||(flag1==2&&flag2==1)||(flag1==3&&flag2==2)||(flag1==2&&flag2==3)){
                if(res==1&&flag1<flag2){
                    swap(flag1,flag2);
                }
                if(res==3&&flag1>flag2){
                    swap(flag1,flag2);
                }


                while(1){
                    temp1=this->randomGoal(flag1);
                    temp2=this->randomGoal(flag2);
                    if(temp1>temp2){
                        goal1=temp1;
                        goal2=temp2;
                        break;
                    }
                    if(temp2>temp1){
                        goal1=temp2;
                        goal2=temp1;
                        break;
                    }
                }
            }

            else if((flag1==1&&flag2==3)||(flag1==3&&flag2==1)){
                while(1){
                    temp1=this->randomGoal(2);
                    temp2=this->randomGoal(2);

                    if(temp1>temp2){
                        goal1=temp1;
                        goal2=temp2;
                        break;
                    }
                    if(temp2>temp1){
                        goal1=temp2;
                        goal2=temp1;
                        break;
                    }
                }
            }
            if(home&&res==3){
                a=goal2;
                b=goal1;
                return;

            }
            else if(away&&res==1){
                a=goal1;
                b=goal2;
                return;

            }

        }

        if(res==1||res==3){
            if((flag1==1&&flag4==1)||(flag1==2&&flag4==2)||(flag1==3&&flag4==3)||(flag1==1&&flag4==2)||(flag1==2&&flag4==1)||(flag1==3&&flag4==2)||(flag1==2&&flag4==3)){
                if(res==1&&flag1<flag4){
                    swap(flag1,flag4);
                }
                if(res==3&&flag1>flag4){
                    swap(flag1,flag4);
                }


                while(1){
                    temp1=this->randomGoal(flag1);
                    temp2=this->randomGoal(flag4);
                    if(temp1>temp2){
                        goal1=temp1;
                        goal2=temp2;
                        break;
                    }
                    if(temp2>temp1){
                        goal1=temp2;
                        goal2=temp1;
                        break;
                    }
                }
            }

            if((flag1==1&&flag4==3)||(flag1==3&&flag4==1)){
                while(1){
                    temp1=this->randomGoal(2);
                    temp2=this->randomGoal(2);

                    if(temp1>temp2){
                        goal1=temp1;
                        goal2=temp2;
                        break;
                    }
                    if(temp2>temp1){
                        goal1=temp2;
                        goal2=temp1;
                        break;
                    }
                }
            }
            if(home&&res==1){
                a=goal1;
                b=goal2;
                return;

            }
            if(away&&res==3){
                a=goal2;
                b=goal1;
                return;

            }

        }

        if(res==2){
            if((flag1==1&&flag3==1)||(flag1==2&&flag3==2)||(flag1==3&&flag3==3)){

                goal1=this->randomGoal(flag1);
            }

           else if((flag1==1&&flag3==3)||(flag1==3&&flag3==1)){
                goal1=this->randomGoal(2);

            }

           else if((flag1==1&&flag3==2)||(flag1==2&&flag3==1)||(flag1==3&&flag3==2)||(flag1==2&&flag3==3)){
                if(home){
                    goal1=goal2=this->randomGoal(flag3);
                }
                if(away){
                    goal1=goal2=this->randomGoal(flag1);
                }
            }
            a=b=goal1;
            return;
        }

    }


    if(home==0&&away==0){
        int ot1=sd.getTeamOverall()[ld.getA()[ld.getCurrentMatch()]];
        int ot2=sd.getTeamOverall()[ld.getB()[ld.getCurrentMatch()]];
        int init,gld;

        if(ot1<=75)
            flag1=1;
        else if(ot1<=90)
            flag1=2;
        else flag1=3;

        if(ot2<=75)
            flag2=1;
        else if(ot2<=90)
            flag2=2;
        else flag2=3;


        if(res==1){
            if(flag1>flag2){
                while(1){

                    init=this->randomGoal(flag1);
                    gld=this->randomGoal(flag2);
                    if(init>gld)break;
                }
                a=init;
                b=gld;
                return;
            }

            if(flag1==flag2){
                while(1){
                    init=this->randomGoal(flag1);
                    gld=this->randomGoal(flag1);
                    if(init>gld)break;
                }
                a=init;
                b=gld;
                return;

            }

            if(flag2>flag1){
                while(1){
                    init=this->randomGoal(2);

                    gld=this->randomGoal(2);
                    if(init>gld)break;
                }
                a=init;
                b=gld;
                return;
            }
        }

        if(res==3){
            if(flag1>flag2){
                while(1){
                    init=this->randomGoal(2);
                    gld=this->randomGoal(2);
                    if(init<gld)break;
                }
                a=init;
                b=gld;
                return;
            }

            if(flag1==flag2){
                while(1){
                    init=this->randomGoal(flag1);
                    gld=this->randomGoal(flag1);
                    if(init<gld)break;
                }
                a=init;
                b=gld;
                return;

            }

            if(flag2>flag1){
                while(1){
                    init=this->randomGoal(flag2);
                    gld=this->randomGoal(flag1);
                    if(init<gld)break;
                }
                a=init;
                b=gld;
                return;
            }
        }

        if(res==2){
            a=b=this->randomGoal(flag1);

            return ;
        }
    }
}




int LeagueService::randomGoal(int flag){

    int i;
    if(flag==1){
        return rand()%3;
    }
    if(flag==2){
        while(1){
            i=rand()%5;
            if(i>=1)break;
        }
        return i;

    }

    if(flag==3){
        while(1){
            i=rand()%6;
            if(i>=1)
                return i;
        }
    }

}

int LeagueService::calculateScorer(SquadDto sd){
    //srand(time(NULL));
    vector<int>vScore;

    int score;
    int pos;
    int scorer;
    while(1){
        scorer=rand()%100;

        for(int i=0;i<11;i++){
            if(sd.getPosition()[i]=='g'||sd.getPosition()[i]=='G')
                pos=10;
            else if(sd.getPosition()[i]=='d'||sd.getPosition()[i]=='D')
                pos=50;
            else if(sd.getPosition()[i]=='m'||sd.getPosition()[i]=='M')
                pos=70;
            else pos=85;
            score=sd.getPlayerOverall()[i]*20/100+pos*70/100+sd.getTrain()[i];
            if(score>=scorer)
                vScore.push_back(i);
        }
        if(!vScore.empty())
            break;
    }
    return vScore[rand()%vScore.size()];

}

int LeagueService::calculateScorer(vector<int>overall,vector<char>position){

    //srand(time(NULL));

    int scorer;
    int score;
    int pos;
    vector<int>vScore;
    while(1){
        scorer=rand()%100;

        for(int i=0;i<11;i++){
            if(position[i]=='g'||position[i]=='G')
                pos=10;
            else if(position[i]=='d'||position[i]=='D')
                pos=50;
            else if(position[i]=='m'||position[i]=='M')
                pos=70;
            else pos=85;
            score=overall[i]*20/100+pos*80/100;
            if(score>=scorer)
                vScore.push_back(i);
        }
        if(!vScore.empty())
            break;
    }
    return vScore[rand()%vScore.size()];




}



int LeagueService::getPosition(vector<int>point, vector<int>goalDiff, vector<int>awayGoal, int pos){
    vector<int>total;
    for(int i=0;i<point.size();i++){
        total.push_back(point[i]+goalDiff[i]+awayGoal[i]);
    }


    int ttl=total[pos];
    vector<int>::iterator p=total.begin();
    sort(p,p+point.size());

    for(int i=0;i<total.size();i++){
        if(total[i]==ttl)
            return total.size()-i;
    }

}





