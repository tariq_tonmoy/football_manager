#ifndef LEAGUESERVICE_H
#define LEAGUESERVICE_H
#include<vector>
#include<cstdlib>
#include<ctime>
#include<DTO/squaddto.h>
#include<QMessageBox>
#include<QString>
#include<DTO/popularitydto.h>
#include<DTO/leaguedto.h>
using namespace std;

class LeagueService
{

public:
    LeagueService();
    int countMatchResult(int overall1,int overall2);
    int countOverall(SquadDto sd,PopularityDto ppd);
    void countGoal(int &a,int &b,int res,LeagueDto ld,SquadDto sd);
    int randomGoal(int flag);
    int calculateScorer(SquadDto sd);
    int calculateScorer(vector<int>overall,vector<char>position);
    int getPosition(vector<int>point,vector<int>goalDiff,vector<int>awayGoal,int pos);



};

#endif // LEAGUESERVICE_H
