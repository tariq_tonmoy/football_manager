#include "popularityservice.h"

PopularityService::PopularityService()
{
    this->popularity=0;
    this->souvenir=0;
}

int PopularityService::countPopularity(int match,int interview,int ticket){
    popularity=((match*40/100)+(interview*30/100)+(ticket*30/100));
    return popularity;
}

int PopularityService::countPopularity(int interview){
    this->popularity=((interview+70)/2);
    return this->popularity;
}

int PopularityService::countPopularity(int interview,int ticket){
    this->popularity=((interview+ticket)/2);
    return this->popularity;
}


int PopularityService::countSouvenir(int overall,int popularity){
        souvenir=((overall*75/100)*(popularity*10/100)*1000)*5/100;
        return souvenir;
}

