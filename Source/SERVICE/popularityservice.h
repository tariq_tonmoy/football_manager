#ifndef POPULARITYSERVICE_H
#define POPULARITYSERVICE_H


class PopularityService{
    int popularity,souvenir;
public:
    PopularityService();
    int countPopularity(int match,int interview,int ticket);
    int countPopularity(int interview);
    int countPopularity(int interview,int ticket);
    int countSouvenir(int overall,int popularity);

};

#endif // POPULARITYSERVICE_H
