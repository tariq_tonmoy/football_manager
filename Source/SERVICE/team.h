#ifndef TEAM_H
#define TEAM_H

#include<string>
#include<iostream>
#include<ctime>
#include<cstdio>
#include<cstdlib>
using namespace std;

class Team
{
    char name[100],sponsor[100];
    int overall,publicity,coaching,stadium,ticket;

public:
    Team();
    bool setSponsor(int ovr,int &start,int &match);
};

#endif // TEAM_H
