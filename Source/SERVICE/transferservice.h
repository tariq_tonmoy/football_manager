#ifndef TRANSFERSERVICE_H
#define TRANSFERSERVICE_H

#include<cstdlib>
#include<ctime>
#include<iostream>
#include<vector>

using namespace std;

class TransferService
{
public:
    TransferService();
    void randomNumber(int *a,int *b,int teamCount,vector<int>playerCount);

    int calculatePrice(int overall){
        int i;
        if(overall<=75){
            i=rand()%1000;
            if(i<500)
                i+=500;
            return i*1000;

        }
        else if(overall>75&&overall<=85){
            i=rand()%3000;
            if(i<1000){
               i+=1000;
            }
            return i*1000;
        }
        else if(i>85){
            i=rand()%5000;
            if(i<2000)
                i+=3000;
            else if(i>=2000&&i<3000)
                i+=1000;
            return i*1000;
        }
    }
};

#endif // TRANSFERSERVICE_H
