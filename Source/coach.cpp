#include "coach.h"
#include "ui_coach.h"

Coach::Coach(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Coach)
{
    ui->setupUi(this);
    ui->lineEdit->setReadOnly(true);
    ui->lineEdit_2->setReadOnly(true);
    flag=false;

    for(int i=0;i<5;i++){
        a[i]=0;
    }


}

Coach::~Coach()
{
    delete ui;
}

void Coach::changeEvent(QEvent *e)
{
    QDialog::changeEvent(e);
    switch (e->type()) {
    case QEvent::LanguageChange:
        ui->retranslateUi(this);
        break;
    default:
        break;
    }
}


void Coach::createCoach(){
    QMessageBox qm;
    QString qstr;

    qstr=QString::number(sd.getCurrentMoney());
    ui->lineEdit->setText(qstr+"$");
    qstr=QString::number(sd.getCurrentExpense());
    ui->lineEdit_2->setText(qstr+"$");

    ui->FverticalSlider->setValue(sd.getForward());
    ui->MverticalSlider->setValue(sd.getMid());
    ui->DverticalSlider->setValue(sd.getDefence());
    ui->GverticalSlider->setValue(sd.getGk());
    ui->SverticalSlider->setValue(sd.getScout());

    a[0]=sd.getForward();
    a[1]=sd.getMid();
    a[2]=sd.getDefence();
    a[3]=sd.getGk();
    a[4]=sd.getScout();

    flag=true;



}


void Coach::on_FverticalSlider_valueChanged(int value)
{
    setSlideBar(0,value);
}

void Coach::on_MverticalSlider_valueChanged(int value)
{
setSlideBar(1,value);
}

void Coach::on_DverticalSlider_valueChanged(int value)
{
setSlideBar(2,value);
}

void Coach::on_GverticalSlider_valueChanged(int value)
{
setSlideBar(3,value);
}

void Coach::on_SverticalSlider_valueChanged(int value)
{
setSlideBar(4,value);
}


void Coach::setSlideBar(int num,int value){
    QString qstr;
    int i;
    if(flag==true){

        if(num>=0&&num<=3)
             i=(sd.getMatchMoney()*15/100)+(ppd.getTicketPrice()*15/100);
        else i=200000;

        if(value>a[num]){
            i=i*(value-a[num]);
            sd.setCurrentExpense(i);
            sd.setCurrentMoney(-i);
            a[num]=value;
        }
        else if(value<a[num]){
            i=i*(a[num]-value);
            sd.setCurrentExpense(-i);
            sd.setCurrentMoney(i);
            a[num]=value;

        }

        if(num==0)
            sd.setForward(value);
        else if(num==1)
            sd.setMid(value);
        else if(num==2)
            sd.setdefence(value);
        else if(num==3)
            sd.setGk(value);
        else if(num==4)
            sd.setScout(value);

    }
    qstr=QString::number(sd.getCurrentMoney());
    ui->lineEdit->setText(qstr+"$");
    qstr=QString::number(sd.getCurrentExpense());
    ui->lineEdit_2->setText(qstr+"$");

}


void Coach::on_pushButton_clicked()
{
    QMessageBox qm;
    if(sd.getCurrentMoney()<0){
        qm.setText("INSUFFICIENT FUND");
        qm.exec();
    }
    else this->hide();
}
