#ifndef COACH_H
#define COACH_H

#include <QDialog>
#include<DTO/squaddto.h>
#include<QString>
#include<QMessageBox>
#include<DTO/popularitydto.h>
namespace Ui {
    class Coach;
}

class Coach : public QDialog {
    Q_OBJECT
public:
    Coach(QWidget *parent = 0);
    void setSquad(SquadDto sd){
        this->sd=sd;
    }
    SquadDto getSquad(){
        return this->sd;
    }

    void setPopularity(PopularityDto ppd){
        this->ppd=ppd;
    }

    void setSlideBar(int num,int value);
    void createCoach();
    ~Coach();

protected:
    void changeEvent(QEvent *e);

private:
    Ui::Coach *ui;
    bool flag;
    PopularityDto ppd;
    SquadDto sd;
    int a[5];

private slots:
    void on_pushButton_clicked();
    void on_SverticalSlider_valueChanged(int value);
    void on_GverticalSlider_valueChanged(int value);
    void on_DverticalSlider_valueChanged(int value);
    void on_MverticalSlider_valueChanged(int value);
    void on_FverticalSlider_valueChanged(int value);
};

#endif // COACH_H
