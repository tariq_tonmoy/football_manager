/****************************************************************************
** Meta object code from reading C++ file 'edit.h'
**
** Created: Thu Apr 19 06:21:37 2012
**      by: The Qt Meta Object Compiler version 62 (Qt 4.6.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../edit.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'edit.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 62
#error "This file was generated using the moc from 4.6.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_Edit[] = {

 // content:
       4,       // revision
       0,       // classname
       0,    0, // classinfo
      16,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
       6,    5,    5,    5, 0x08,
      32,    5,    5,    5, 0x08,
      58,    5,    5,    5, 0x08,
      84,    5,    5,    5, 0x08,
     110,    5,    5,    5, 0x08,
     137,    5,    5,    5, 0x08,
     164,    5,    5,    5, 0x08,
     191,    5,    5,    5, 0x08,
     218,    5,    5,    5, 0x08,
     245,    5,    5,    5, 0x08,
     272,    5,    5,    5, 0x08,
     298,    5,    5,    5, 0x08,
     324,    5,    5,    5, 0x08,
     350,    5,    5,    5, 0x08,
     374,    5,    5,    5, 0x08,
     405,  400,    5,    5, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_Edit[] = {
    "Edit\0\0on_pushButton_8_clicked()\0"
    "on_pushButton_7_clicked()\0"
    "on_pushButton_2_clicked()\0"
    "on_pushButton_9_clicked()\0"
    "on_pushButton_12_clicked()\0"
    "on_pushButton_13_clicked()\0"
    "on_pushButton_11_clicked()\0"
    "on_pushButton_15_clicked()\0"
    "on_pushButton_10_clicked()\0"
    "on_pushButton_14_clicked()\0"
    "on_pushButton_4_clicked()\0"
    "on_pushButton_6_clicked()\0"
    "on_pushButton_5_clicked()\0"
    "on_pushButton_clicked()\0"
    "on_pushButton_3_clicked()\0link\0"
    "on_label_linkActivated(QString)\0"
};

const QMetaObject Edit::staticMetaObject = {
    { &QDialog::staticMetaObject, qt_meta_stringdata_Edit,
      qt_meta_data_Edit, 0 }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &Edit::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *Edit::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *Edit::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_Edit))
        return static_cast<void*>(const_cast< Edit*>(this));
    return QDialog::qt_metacast(_clname);
}

int Edit::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QDialog::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        switch (_id) {
        case 0: on_pushButton_8_clicked(); break;
        case 1: on_pushButton_7_clicked(); break;
        case 2: on_pushButton_2_clicked(); break;
        case 3: on_pushButton_9_clicked(); break;
        case 4: on_pushButton_12_clicked(); break;
        case 5: on_pushButton_13_clicked(); break;
        case 6: on_pushButton_11_clicked(); break;
        case 7: on_pushButton_15_clicked(); break;
        case 8: on_pushButton_10_clicked(); break;
        case 9: on_pushButton_14_clicked(); break;
        case 10: on_pushButton_4_clicked(); break;
        case 11: on_pushButton_6_clicked(); break;
        case 12: on_pushButton_5_clicked(); break;
        case 13: on_pushButton_clicked(); break;
        case 14: on_pushButton_3_clicked(); break;
        case 15: on_label_linkActivated((*reinterpret_cast< QString(*)>(_a[1]))); break;
        default: ;
        }
        _id -= 16;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
