/****************************************************************************
** Meta object code from reading C++ file 'interview.h'
**
** Created: Thu Mar 29 04:44:41 2012
**      by: The Qt Meta Object Compiler version 62 (Qt 4.6.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../interview.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'interview.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 62
#error "This file was generated using the moc from 4.6.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_Interview[] = {

 // content:
       4,       // revision
       0,       // classname
       0,    0, // classinfo
       4,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      11,   10,   10,   10, 0x08,
      35,   10,   10,   10, 0x08,
      59,   10,   10,   10, 0x08,
      83,   10,   10,   10, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_Interview[] = {
    "Interview\0\0on_pushButton_clicked()\0"
    "on_checkBox_3_clicked()\0on_checkBox_2_clicked()\0"
    "on_checkBox_clicked()\0"
};

const QMetaObject Interview::staticMetaObject = {
    { &QDialog::staticMetaObject, qt_meta_stringdata_Interview,
      qt_meta_data_Interview, 0 }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &Interview::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *Interview::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *Interview::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_Interview))
        return static_cast<void*>(const_cast< Interview*>(this));
    return QDialog::qt_metacast(_clname);
}

int Interview::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QDialog::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        switch (_id) {
        case 0: on_pushButton_clicked(); break;
        case 1: on_checkBox_3_clicked(); break;
        case 2: on_checkBox_2_clicked(); break;
        case 3: on_checkBox_clicked(); break;
        default: ;
        }
        _id -= 4;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
