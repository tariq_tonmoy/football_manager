/****************************************************************************
** Meta object code from reading C++ file 'loadprofile.h'
**
** Created: Thu Apr 19 06:21:39 2012
**      by: The Qt Meta Object Compiler version 62 (Qt 4.6.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../loadprofile.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'loadprofile.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 62
#error "This file was generated using the moc from 4.6.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_LoadProfile[] = {

 // content:
       4,       // revision
       0,       // classname
       0,    0, // classinfo
       3,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      19,   13,   12,   12, 0x08,
      64,   57,   12,   12, 0x08,
     105,   12,   12,   12, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_LoadProfile[] = {
    "LoadProfile\0\0value\0"
    "on_horizontalSlider_valueChanged(int)\0"
    "action\0on_horizontalSlider_actionTriggered(int)\0"
    "on_pushButton_clicked()\0"
};

const QMetaObject LoadProfile::staticMetaObject = {
    { &QDialog::staticMetaObject, qt_meta_stringdata_LoadProfile,
      qt_meta_data_LoadProfile, 0 }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &LoadProfile::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *LoadProfile::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *LoadProfile::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_LoadProfile))
        return static_cast<void*>(const_cast< LoadProfile*>(this));
    return QDialog::qt_metacast(_clname);
}

int LoadProfile::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QDialog::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        switch (_id) {
        case 0: on_horizontalSlider_valueChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 1: on_horizontalSlider_actionTriggered((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 2: on_pushButton_clicked(); break;
        default: ;
        }
        _id -= 3;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
