/****************************************************************************
** Meta object code from reading C++ file 'squad.h'
**
** Created: Thu Apr 19 06:21:40 2012
**      by: The Qt Meta Object Compiler version 62 (Qt 4.6.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../squad.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'squad.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 62
#error "This file was generated using the moc from 4.6.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_Squad[] = {

 // content:
       4,       // revision
       0,       // classname
       0,    0, // classinfo
      16,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
       7,    6,    6,    6, 0x08,
      39,   33,    6,    6, 0x08,
      81,   33,    6,    6, 0x08,
     122,   33,    6,    6, 0x08,
     163,   33,    6,    6, 0x08,
     204,   33,    6,    6, 0x08,
     245,   33,    6,    6, 0x08,
     286,   33,    6,    6, 0x08,
     327,   33,    6,    6, 0x08,
     368,   33,    6,    6, 0x08,
     409,   33,    6,    6, 0x08,
     450,   33,    6,    6, 0x08,
     491,   33,    6,    6, 0x08,
     532,   33,    6,    6, 0x08,
     573,   33,    6,    6, 0x08,
     614,   33,    6,    6, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_Squad[] = {
    "Squad\0\0on_pushButton_2_clicked()\0value\0"
    "on_horizontalSlider_100_valueChanged(int)\0"
    "on_horizontalSlider_86_valueChanged(int)\0"
    "on_horizontalSlider_87_valueChanged(int)\0"
    "on_horizontalSlider_88_valueChanged(int)\0"
    "on_horizontalSlider_89_valueChanged(int)\0"
    "on_horizontalSlider_90_valueChanged(int)\0"
    "on_horizontalSlider_91_valueChanged(int)\0"
    "on_horizontalSlider_92_valueChanged(int)\0"
    "on_horizontalSlider_93_valueChanged(int)\0"
    "on_horizontalSlider_94_valueChanged(int)\0"
    "on_horizontalSlider_95_valueChanged(int)\0"
    "on_horizontalSlider_96_valueChanged(int)\0"
    "on_horizontalSlider_97_valueChanged(int)\0"
    "on_horizontalSlider_98_valueChanged(int)\0"
    "on_horizontalSlider_99_valueChanged(int)\0"
};

const QMetaObject Squad::staticMetaObject = {
    { &QDialog::staticMetaObject, qt_meta_stringdata_Squad,
      qt_meta_data_Squad, 0 }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &Squad::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *Squad::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *Squad::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_Squad))
        return static_cast<void*>(const_cast< Squad*>(this));
    return QDialog::qt_metacast(_clname);
}

int Squad::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QDialog::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        switch (_id) {
        case 0: on_pushButton_2_clicked(); break;
        case 1: on_horizontalSlider_100_valueChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 2: on_horizontalSlider_86_valueChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 3: on_horizontalSlider_87_valueChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 4: on_horizontalSlider_88_valueChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 5: on_horizontalSlider_89_valueChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 6: on_horizontalSlider_90_valueChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 7: on_horizontalSlider_91_valueChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 8: on_horizontalSlider_92_valueChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 9: on_horizontalSlider_93_valueChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 10: on_horizontalSlider_94_valueChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 11: on_horizontalSlider_95_valueChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 12: on_horizontalSlider_96_valueChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 13: on_horizontalSlider_97_valueChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 14: on_horizontalSlider_98_valueChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 15: on_horizontalSlider_99_valueChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        default: ;
        }
        _id -= 16;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
