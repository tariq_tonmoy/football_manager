/****************************************************************************
** Meta object code from reading C++ file 'stadium.h'
**
** Created: Thu Apr 19 06:21:42 2012
**      by: The Qt Meta Object Compiler version 62 (Qt 4.6.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../stadium.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'stadium.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 62
#error "This file was generated using the moc from 4.6.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_Stadium[] = {

 // content:
       4,       // revision
       0,       // classname
       0,    0, // classinfo
       5,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      16,    9,    8,    8, 0x08,
      57,    8,    8,    8, 0x08,
      87,   81,    8,    8, 0x08,
     132,  123,    8,    8, 0x08,
     167,   81,    8,    8, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_Stadium[] = {
    "Stadium\0\0action\0"
    "on_verticalSlider_2_actionTriggered(int)\0"
    "on_pushButton_clicked()\0value\0"
    "on_verticalSlider_valueChanged(int)\0"
    "position\0on_verticalSlider_sliderMoved(int)\0"
    "on_verticalSlider_2_valueChanged(int)\0"
};

const QMetaObject Stadium::staticMetaObject = {
    { &QDialog::staticMetaObject, qt_meta_stringdata_Stadium,
      qt_meta_data_Stadium, 0 }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &Stadium::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *Stadium::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *Stadium::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_Stadium))
        return static_cast<void*>(const_cast< Stadium*>(this));
    return QDialog::qt_metacast(_clname);
}

int Stadium::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QDialog::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        switch (_id) {
        case 0: on_verticalSlider_2_actionTriggered((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 1: on_pushButton_clicked(); break;
        case 2: on_verticalSlider_valueChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 3: on_verticalSlider_sliderMoved((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 4: on_verticalSlider_2_valueChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        default: ;
        }
        _id -= 5;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
