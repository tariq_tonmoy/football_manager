/****************************************************************************
** Meta object code from reading C++ file 'transfer.h'
**
** Created: Tue Apr 17 06:12:56 2012
**      by: The Qt Meta Object Compiler version 62 (Qt 4.6.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../transfer.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'transfer.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 62
#error "This file was generated using the moc from 4.6.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_Transfer[] = {

 // content:
       4,       // revision
       0,       // classname
       0,    0, // classinfo
      24,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      10,    9,    9,    9, 0x08,
      36,    9,    9,    9, 0x08,
      62,    9,    9,    9, 0x08,
      94,   86,    9,    9, 0x08,
     124,   86,    9,    9, 0x08,
     153,   86,    9,    9, 0x08,
     182,   86,    9,    9, 0x08,
     211,   86,    9,    9, 0x08,
     240,   86,    9,    9, 0x08,
     269,   86,    9,    9, 0x08,
     298,   86,    9,    9, 0x08,
     327,   86,    9,    9, 0x08,
     356,   86,    9,    9, 0x08,
     385,   86,    9,    9, 0x08,
     415,   86,    9,    9, 0x08,
     444,   86,    9,    9, 0x08,
     473,   86,    9,    9, 0x08,
     502,   86,    9,    9, 0x08,
     531,   86,    9,    9, 0x08,
     560,   86,    9,    9, 0x08,
     589,   86,    9,    9, 0x08,
     618,   86,    9,    9, 0x08,
     647,   86,    9,    9, 0x08,
     682,  676,    9,    9, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_Transfer[] = {
    "Transfer\0\0on_pushButton_4_clicked()\0"
    "on_pushButton_2_clicked()\0"
    "on_pushButton_clicked()\0checked\0"
    "on_checkBox_s10_clicked(bool)\0"
    "on_checkBox_s9_clicked(bool)\0"
    "on_checkBox_s8_clicked(bool)\0"
    "on_checkBox_s7_clicked(bool)\0"
    "on_checkBox_s6_clicked(bool)\0"
    "on_checkBox_s5_clicked(bool)\0"
    "on_checkBox_s4_clicked(bool)\0"
    "on_checkBox_s3_clicked(bool)\0"
    "on_checkBox_s2_clicked(bool)\0"
    "on_checkBox_s1_clicked(bool)\0"
    "on_checkBox_b10_clicked(bool)\0"
    "on_checkBox_b9_clicked(bool)\0"
    "on_checkBox_b8_clicked(bool)\0"
    "on_checkBox_b7_clicked(bool)\0"
    "on_checkBox_b6_clicked(bool)\0"
    "on_checkBox_b5_clicked(bool)\0"
    "on_checkBox_b3_clicked(bool)\0"
    "on_checkBox_b4_clicked(bool)\0"
    "on_checkBox_b2_clicked(bool)\0"
    "on_checkBox_b1_clicked(bool)\0index\0"
    "on_tabWidget_currentChanged(int)\0"
};

const QMetaObject Transfer::staticMetaObject = {
    { &QDialog::staticMetaObject, qt_meta_stringdata_Transfer,
      qt_meta_data_Transfer, 0 }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &Transfer::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *Transfer::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *Transfer::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_Transfer))
        return static_cast<void*>(const_cast< Transfer*>(this));
    return QDialog::qt_metacast(_clname);
}

int Transfer::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QDialog::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        switch (_id) {
        case 0: on_pushButton_4_clicked(); break;
        case 1: on_pushButton_2_clicked(); break;
        case 2: on_pushButton_clicked(); break;
        case 3: on_checkBox_s10_clicked((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 4: on_checkBox_s9_clicked((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 5: on_checkBox_s8_clicked((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 6: on_checkBox_s7_clicked((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 7: on_checkBox_s6_clicked((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 8: on_checkBox_s5_clicked((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 9: on_checkBox_s4_clicked((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 10: on_checkBox_s3_clicked((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 11: on_checkBox_s2_clicked((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 12: on_checkBox_s1_clicked((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 13: on_checkBox_b10_clicked((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 14: on_checkBox_b9_clicked((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 15: on_checkBox_b8_clicked((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 16: on_checkBox_b7_clicked((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 17: on_checkBox_b6_clicked((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 18: on_checkBox_b5_clicked((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 19: on_checkBox_b3_clicked((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 20: on_checkBox_b4_clicked((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 21: on_checkBox_b2_clicked((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 22: on_checkBox_b1_clicked((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 23: on_tabWidget_currentChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        default: ;
        }
        _id -= 24;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
