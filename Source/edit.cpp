#include "edit.h"
#include "ui_edit.h"

#include<cstdio>


Edit::Edit(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Edit)
{
    ui->setupUi(this);
    ui->location_lineEdit->setReadOnly(true);
    ui->country_lineEdit->setReadOnly(true);
    ui->sponsor_lineEdit->setReadOnly(true);

}

Edit::~Edit()
{
    delete ui;
}

void Edit::changeEvent(QEvent *e)
{
    QDialog::changeEvent(e);
    switch (e->type()) {
    case QEvent::LanguageChange:
        ui->retranslateUi(this);
        break;
    default:
        break;
    }
}

void Edit::on_label_linkActivated(QString link)
{

}

void Edit::on_pushButton_3_clicked()
{
    Stadium std;
    std.setSquad(sd);
    std.setPopularity(this->ppd);
    std.createStadium();
    std.exec();
    this->sd=std.getSquad();
    this->ppd=std.getPopularity();
    this->createUI();
}

void Edit::on_pushButton_clicked()
{
    ui->lineEdit->setEnabled(true);
}


void Edit::createUI(){

    QMessageBox qm;
    this->count=sd.get_count();

    qstr=new QString[count];
    for(int i=0;i<4;i++){
        qstr[i]=sd.get_qstr()[i];

    }

    ui->label_18->setText("Season: "+QString::number(ld.getSeasonCount()+1));


    ui->label->setText(qstr[0]);
    ui->lineEdit->setEnabled(false);
    ui->lineEdit->setText(qstr[1]);
    ui->lineEdit_2->setEnabled(false);
    ui->lineEdit_2->setText(qstr[2]);
    ui->lineEdit_3->setEnabled(false);
    ui->lineEdit_3->setText(qstr[3]);
    qstr[count-1]="UserName.txt";






    QString team_name=team_name.fromStdString(sd.getTeamName()[sd.getTeamIndex()]);
    ui->label_4->setText(team_name);

    QString qloc;
    qloc=qloc.fromStdString(sd.getLocation()[sd.getTeamIndex()]);
    ui->location_lineEdit->setText(qloc);


    QString qcon;
    qcon=qcon.fromStdString(sd.getCountry()[sd.getTeamIndex()]);
    ui->country_lineEdit->setText(qcon);


    QString qsp;
    qsp=qsp.fromStdString(sd.getSponsors()[sd.getSponsorIndex()]);
    ui->sponsor_lineEdit->setText(qsp);



    ui->BalanceLcdNumber->display(sd.getCurrentMoney());

    float avg1=0;
    for(int i=0;i<sd.getTrain().size();i++){
        avg1+=(float)sd.getTrain()[i];
    }
    avg1=avg1/15.0;



    int to=(int)((float)sd.getTeamOverall()[sd.getTeamIndex()]*40.0/100.0+(float)ppd.getPopularity()*30.0/100.0+((float)sd.getScout()*100.0/10.0)*5.0/100.0+(float)sd.getAvgOverall()*25.0/100.0);
    if(to<50)
        to=50;
    if(to>=99)
        to=99;
    ui->TeamOverallLcdNumber->display(to);



    int tc=(int)(((float)sd.getMatchAvgOverall()*75.0/100.0)+((((float)sd.getForward()+(float)sd.getMid()+(float)sd.getDefence()+(float)sd.getGk())/4.0))+(((float)avg1*100.0/10.0)*10.0/100.0)+(((float)sd.getSalary()*100.0/10.0)*5.0/100.0));
    if(tc<50)
        tc=50;
    if(tc>=99)
        tc=99;

    ui->TeamChemistryLcdNumber->display(tc);





    ui->lcdNumber_6->display(ppd.getPopularity());
    ui->lcdNumber_2->display(sd.getStadium()[sd.getTeamIndex()]+sd.getStadium()[sd.getTeamIndex()]*sd.getStadiumUpgrade()/10);
    ui->lcdNumber_3->display(ppd.getTicketPrice());






    ui->ForwardLcdNumber->display(sd.getForward());
    ui->MidLcdNumber->display(sd.getMid());
    ui->DefenceLcdNumber->display(sd.getDefence());
    ui->GkLcdNumber->display(sd.getGk());
    ui->ScoutLcdNumber->display(sd.getScout());




    if(tds.wrirteTeam(this->sd)&&sd.WriteSquad()&&ppd.WritePopularity(this->sd)&&ld.WriteLeague(this->sd)&&tsd.WriteTransfer(this->sd)){

    }
    else {
        qm.setText("Update Failed");
        qm.exec();
    }




}

void Edit::on_pushButton_5_clicked()
{
    ui->lineEdit_2->setEnabled(true);
}

void Edit::on_pushButton_6_clicked()
{
    ui->lineEdit_3->setEnabled(true);
}

void Edit::on_pushButton_4_clicked()
{
    QMessageBox qm;
    bool flag=false;
    if (qstr[1]!=ui->lineEdit->text()){
        qstr[1]=ui->lineEdit->text();
        flag=true;
    }
    if (qstr[2]!=ui->lineEdit_2->text()){
        qstr[2]=ui->lineEdit_2->text();
         flag=true;
    }
    if (qstr[3]!=ui->lineEdit_3->text()){
        qstr[3]=ui->lineEdit_3->text();
         flag=true;
    }
    if(!flag){
        qm.setText("No Data Changed");
        qm.exec();
    }
    else {
        qstr[4]="";
        d.set_qstr(qstr,5);
        if(pds.createFile(d)){
            qm.setText("Update Successful");
            qm.exec();
        }
        else {
            qm.setText("Update Failed");
            qm.exec();

        }
    }

    ui->lineEdit->setEnabled(false);
    ui->lineEdit_2->setEnabled(false);
    ui->lineEdit_3->setEnabled(false);
}




void Edit::on_pushButton_14_clicked()
{


    Squad sq;

    sq.setSquadDto(sd);
    sq.createSquad();

    sq.exec();
    this->sd=sq.getSquadDto();
    this->createUI();

}

void Edit::on_pushButton_10_clicked()
{
    Coach ch;
    ch.setSquad(this->sd);
    ch.setPopularity(this->ppd);
    ch.createCoach();

    ch.exec();


    this->sd=ch.getSquad();
    this->createUI();



}

void Edit::on_pushButton_15_clicked()
{
  
    Transfer trs;

    trs.setTransfer(this->tsd);
    trs.setSquad(this->sd);
    trs.createTransfer();
    trs.exec();
    this->sd=trs.getSquad();
    this->tsd=trs.getTransfer();
    this->createUI();

}

void Edit::on_pushButton_11_clicked()
{
    Salary sl;
    sl.setSquad(this->sd);
    sl.createSalary();
    sl.exec();
    this->sd=sl.getSquad();
    this->createUI();
}

void Edit::on_pushButton_13_clicked()
{
    LeagueTable lt;
    lt.setSquad(this->sd);
    lt.setLeague(this->ld);
    lt.createLeague();
    lt.exec();
    this->sd=lt.getSquad();
    this->ld=lt.getLeagueDto();
    this->createUI();

}

void Edit::on_pushButton_12_clicked()
{

    QMessageBox qm;

    bool flag=false,flag1=false,flag2=false,flag3=false;
    int total;

    if(sd.getTeamName()[sd.getTeamIndex()]==sd.getTeamName()[ld.getA()[ld.getCurrentMatch()]]){
        if(!sd.getTotalSalary()){
            total=sd.getMatchAvgOverall()*2500*15;
        }
        else total=sd.getTotalSalary();

        sd.setTotalSalary(total);
        sd.setCurrentMoney(-total);

        if(sd.getCurrentMoney()<0){
            qm.setText("Unable To Pay Salary Of Players Due To Insufficient Fund");
            qm.exec();
            sd.setCurrentMoney(total);
            return;

        }
        else {
            flag=true;
            flag1=true;
            flag3==false;


        }
    }
    else if(sd.getTeamName()[sd.getTeamIndex()]==sd.getTeamName()[ld.getB()[ld.getCurrentMatch()]]){
        if(!sd.getTotalSalary()){
            total=sd.getMatchAvgOverall()*2500*15;
        }
        else total=sd.getTotalSalary();

        sd.setTotalSalary(total);
        sd.setCurrentMoney(-total);
       // sd.setCurrentExpense(total);
        if(sd.getCurrentMoney()<0){
            qm.setText("Unable To Pay Salary Of Players Due To Insufficient Fund");
            qm.exec();
            sd.setCurrentMoney(total);
            return;

        }
        else {
            flag2=true;
            flag=true;
            flag3=false;
        }
    }

    else {
        flag3=true;
        flag=false;
    }
    if(flag==true||flag3==true){
        Interview in;
        InterviewDto id;

        Match mt;
        mt.setSquad(this->sd);
        mt.setLeagueDto(this->ld);
        mt.setTransferDto(this->tsd);
        mt.setPopularityDto(this->ppd);
        mt.createMatch();
        mt.exec();

        this->sd=mt.getSquad();
        this->ld=mt.getLeague();
        this->ppd=mt.getPopularity();

        int match;
        int res=mt.getResult();


        if(flag==true&&flag3==false){
            in.exec();
            id=in.getInterviewDto();

            if(res==1){
                if(flag1==true||flag2==false)
                    match=85;
                else if(flag1==false||flag2==true)
                    match=60;


            }

            else if(res==2){
                match=70;
            }
            else if(res==3){
                if(flag1==true)
                    match=60;
                else if(flag1==false)
                    match=85;

                else if(flag2==true)
                    match==60;
                else if(flag2==false)
                    match=85;
            }




            this->ppd.setPopularityComponents(id.getRes(),ppd.getTicket(),match,sd.TeamDto::getTeamOverall()[sd.getTeamIndex()]);
            this->sd.setCurrentMoney(ppd.getSouvenir());


        }


        else if(flag3==true&&flag==false){
            int ht,at;
            ht=ld.getA()[ld.getCurrentMatch()-1];
            at=ld.getB()[ld.getCurrentMatch()-1];
            if(res==1){
                sd.setTeamOverall(sd.getTeamOverall()[ht]+sd.getTeamOverall()[ht]*3/100,ht);
                sd.setTeamOverall(sd.getTeamOverall()[at]-sd.getTeamOverall()[at]*3/100,at);

            }
            else if(res==2){
                sd.setTeamOverall(sd.getTeamOverall()[ht]+sd.getTeamOverall()[ht]*2/100,ht);
                sd.setTeamOverall(sd.getTeamOverall()[at]+sd.getTeamOverall()[at]*2/100,at);
            }
            else{
                sd.setTeamOverall(sd.getTeamOverall()[ht]-sd.getTeamOverall()[ht]*3/100,ht);
                sd.setTeamOverall(sd.getTeamOverall()[at]+sd.getTeamOverall()[at]*3/100,at);
            }
        }

        if(ld.getCurrentMatch()>=ld.getA().size()){
            LeagueService ls;

            int pos=ls.getPosition(ld.getPoint(),ld.getGoalDiff(),ld.getAwayGoal(),sd.getTeamIndex());
            ld.setSeasonCount(ld.getSeasonCount()+1);
            qm.setText(QString::number(ld.getSeasonCount()));
            qm.exec();

            ld.setLeaguePosition(pos);
            qm.setText(QString::number(ld.getLeaguePosition()[ld.getSeasonCount()-1]));
            qm.exec();



            Season sn;
            sn.setLeagueDto(this->ld);
            sn.setSquadDto(this->sd);
            sn.createSeason();
            sn.exec();
            this->sd=sn.getSquad();
            this->ld=sn.getLeague();

        }


        this->createUI();


    }


    if(tds.wrirteTeam(this->sd)&&sd.WriteSquad()&&ppd.WritePopularity(this->sd)&&ld.WriteLeague(this->sd)&&tsd.WriteTransfer(this->sd)){
        qm.setText("Profile Successfully Updated");
        qm.exec();
    }
    else {
        qm.setText("Update Failed");
        qm.exec();
    }

}





void Edit::on_pushButton_9_clicked()
{
    QMessageBox qm;
    if(tds.wrirteTeam(this->sd)&&sd.WriteSquad()&&ppd.WritePopularity(this->sd)&&ld.WriteLeague(this->sd)&&tsd.WriteTransfer(this->sd)){
        qm.setText("Profile Successfully Updated");
        qm.exec();
    }
    else {
        qm.setText("Update Failed");
        qm.exec();
    }

}

void Edit::on_pushButton_2_clicked()
{
    this->hide();
}

void Edit::on_pushButton_7_clicked()
{
    Season sn;
    sn.setLeagueDto(this->ld);
    sn.setSquadDto(this->sd);
    sn.createSeason();
    sn.exec();
    this->sd=sn.getSquad();
    this->ld=sn.getLeague();
}

void Edit::on_pushButton_8_clicked()
{

    string str;
    vector<string>vname;
    QString qstr;
    char *file;
    Encoder en;
    Scorer sc;
    sc.setText("Do You Really Want To Leave The Job?");
    sc.exec();
    if(!sc.getFlag())
        return;

    str=sd.get_qstr()[0].toStdString();

    qstr="MyFile\\"+sd.get_qstr()[0]+".txt";
    QByteArray ba=qstr.toLatin1();
    file=ba.data();
    remove(file);

    ifstream fin;
    str="";
    fin.open("UserName.txt",ios::in);
    while(!fin.eof()){
        getline(fin,str);
        str=en.str_str(str);
        if(str!=sd.get_qstr()->toStdString()){
            vname.push_back(str);
            str="";
        }
    }
    fin.close();

    ofstream fout;
    fout.open("UserName.txt",ios::out);

    for(int i=0;i<vname.size();i++){
        fout<<en.str_str(vname[i])<<"\n";
    }

    fout.close();

    for(int i=0;i<sd.getTeamName().size();i++){
        qstr="MyFile\\"+sd.get_qstr()[0]+QString::fromStdString(sd.getTeamName()[i])+".txt";
        ba=qstr.toLatin1();
        file=ba.data();
        remove(file);
    }

    qstr="MyFile\\"+sd.get_qstr()[0]+"League.txt";
    ba=qstr.toLatin1();
    file=ba.data();
    remove(file);

    qstr="MyFile\\"+sd.get_qstr()[0]+"TeamName.txt";
    ba=qstr.toLatin1();
    file=ba.data();
    remove(file);

    qstr="MyFile\\"+sd.get_qstr()[0]+"Popularity.txt";
    ba=qstr.toLatin1();
    file=ba.data();
    remove(file);

    qstr="MyFile\\"+sd.get_qstr()[0]+"Transfer.txt";
    ba=qstr.toLatin1();
    file=ba.data();
    remove(file);

    QMessageBox qm;
    qm.setText("Profile Deleted");
    qm.exec();
    this->hide();

}
