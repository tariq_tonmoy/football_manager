#ifndef EDIT_H
#define EDIT_H
#include<DTO/profiledto.h>
#include <QDialog>
#include<QString>
#include<QMessageBox>
#include<DATA_SERVICE/profiledataservice.h>
#include<DTO/teamdto.h>
#include<DTO/interviewdto.h>
#include<DTO/popularitydto.h>
#include<SERVICE/popularityservice.h>
#include"squad.h"
#include<DTO/squaddto.h>
#include<coach.h>
#include"stadium.h"
#include"transfer.h"
#include<DTO/transferdto.h>
#include<salary.h>
#include<DTO/leaguedto.h>
#include<leaguetable.h>
#include<match.h>
#include<interview.h>
#include<DATA_SERVICE/teamdataservice.h>
#include<season.h>
#include<scorer.h>

using namespace std;
#define PA 5

namespace Ui {
    class Edit;
}

class Edit : public QDialog {
    Q_OBJECT
public:
    Edit(QWidget *parent = 0);
    ~Edit();
    void setProfileDto(ProfileDto d){
        this->d=d;
    }
    void setTeamDto(TeamDto td){
        this->td=td;
    }
    void setInterviewDto(InterviewDto id){
       this->id=id;
    }
    void setPopularityDto(PopularityDto ppd){
        this->ppd=ppd;
    }

    void setLeagueDto(LeagueDto ld){
        this->ld=ld;
    }

    void setSquadDto(SquadDto sd){
        this->sd=sd;
    }
    void setTransferDto(TransferDto tsd){
        this->tsd=tsd;
    }


    void createUI();

protected:
    void changeEvent(QEvent *e);

private:
    LeagueDto ld;
    QString *qstr;
    int count;
    PopularityDto ppd;
    PopularityService pps;
    ProfileDataService pds;
    InterviewDto id;
    ProfileDto d;
    SquadDto sd;
    Ui::Edit *ui;
    TeamDto td;
    TransferDto tsd;
    TeamDataService tds;
private slots:
    void on_pushButton_8_clicked();
    void on_pushButton_7_clicked();
    void on_pushButton_2_clicked();
    void on_pushButton_9_clicked();
    void on_pushButton_12_clicked();
    void on_pushButton_13_clicked();
    void on_pushButton_11_clicked();
    void on_pushButton_15_clicked();
    void on_pushButton_10_clicked();
    void on_pushButton_14_clicked();
    void on_pushButton_4_clicked();
    void on_pushButton_6_clicked();
    void on_pushButton_5_clicked();
    void on_pushButton_clicked();
    void on_pushButton_3_clicked();
    void on_label_linkActivated(QString link);
};

#endif // EDIT_H
