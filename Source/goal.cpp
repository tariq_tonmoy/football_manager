#include "goal.h"
#include "ui_goal.h"
#include<string>

Goal::Goal(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Goal)
{
    ui->setupUi(this);
    qstr[0]=" Just Scored A Goal By A Strong Header For ";
    qstr[1]=" Just Scored A Goal From A Wonderful Cross For ";
    qstr[2]="!!! Awesome  One-Man-Show For ";
    qstr[3]="!!! Let's Just Say He Scored A Goal Using X-Box For ";
    qstr[4]=" Just Scored A Goal From A Counter-Attack For ";
    qstr[5]=" Just Scored A Goal From A Penalty For ";
    qstr[6]=" Just Scored A Goal From A Wonderful Free-Kick For ";
    qstr[7]=" !!! What A Goal From A Mesmerising Corner Kick For ";
    qstr[8]=" !!! Finally He Did It From A Long Range Shot For ";
    qstr[9]=" !!! Astonishing Volly From The Corner Kick. Goal For ";
}

Goal::~Goal()
{
    delete ui;
}

void Goal::changeEvent(QEvent *e)
{
    QDialog::changeEvent(e);
    switch (e->type()) {
    case QEvent::LanguageChange:
        ui->retranslateUi(this);
        break;
    default:
        break;
    }
}

void Goal::setUI(string name, string team, int min){

    int i=rand()%10;

    QString qname,qteam;
    qname=qname.fromStdString(name);
    qteam=qteam.fromStdString(team);

    ui->label_2->setText("\""+qname+"\""+qstr[i]+"\""+qteam+"\"");
    ui->label_4->setText(QString::number(min)+"'");

}

void Goal::on_pushButton_clicked()
{
    this->hide();
}
