#ifndef GOAL_H
#define GOAL_H

#include <QDialog>
#include<QString>
#include<ctime>
#include<cstdlib>
#include<string>
using namespace std;
namespace Ui {
    class Goal;
}

class Goal : public QDialog {
    Q_OBJECT
public:
    Goal(QWidget *parent = 0);
     void setUI(string name,string team,int min);
    ~Goal();

protected:
    void changeEvent(QEvent *e);

private:
    Ui::Goal *ui;

    QString qstr[10];

private slots:
    void on_pushButton_clicked();
};

#endif // GOAL_H
