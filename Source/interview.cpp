#include "interview.h"
#include "ui_interview.h"
#include<QMessageBox>
#include<QString>

Interview::Interview(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Interview)
{
    ui->setupUi(this);
    QMessageBox qm;

    QString qques,qans[3],qstr;
    string ques,ans[3];
    id=ids.readQues();
    ques=id.getQues();
    qques=qques.fromStdString(ques);
    for(int i=0;i<3;i++)
        ans[i]=id.getAns()[i];
    for(int i=0;i<3;i++){
        qans[i]=qans[i].fromStdString(ans[i]);

    }

    is.setAns(ans);
    for(int i=0;i<3;i++)
        ans[i]=is.getAns()[i];

    for(int i=0;i<3;i++){
        qans[i]=qans[i].fromStdString(ans[i]);
    }
    ui->lineEdit->setReadOnly(true);
    ui->lineEdit_2->setReadOnly(true);
    ui->lineEdit_3->setReadOnly(true);
    ui->lineEdit_4->setReadOnly(true);

    ui->lineEdit->setText(qques);
    ui->lineEdit_2->setText(qans[0]);
    ui->lineEdit_3->setText(qans[1]);
    ui->lineEdit_4->setText(qans[2]);


}

Interview::~Interview()
{
    delete ui;
}

void Interview::changeEvent(QEvent *e)
{
    QDialog::changeEvent(e);
    switch (e->type()) {
    case QEvent::LanguageChange:
        ui->retranslateUi(this);
        break;
    default:
        break;
    }
}

void Interview::on_checkBox_clicked()
{
    QCheckBox *cb=ui->checkBox_2;
    cb->setChecked(false);
    cb=ui->checkBox_3;
    cb->setChecked(false);

}

void Interview::on_checkBox_2_clicked()
{
    QCheckBox *cb=ui->checkBox;
    cb->setChecked(false);
    cb=ui->checkBox_3;
    cb->setChecked(false);
}

void Interview::on_checkBox_3_clicked()
{
    QCheckBox *cb=ui->checkBox;
    cb->setChecked(false);
    cb=ui->checkBox_2;
    cb->setChecked(false);
}



void Interview::on_pushButton_clicked()
{
    QMessageBox qm;
    QCheckBox *cb=ui->checkBox;
    QCheckBox *cb2=ui->checkBox_2;
    QCheckBox *cb3=ui->checkBox_3;
    if(cb->isChecked()==false&&cb2->isChecked()==false&&cb3->isChecked()==false){
        qm.setText("Select An Answer First");
        qm.exec();
    }
    else{
        int index;
        if(cb->isChecked()==true)
            index=0;
        else if(cb2->isChecked()==true)
            index=1;
        else index=2;

        this->res=is.getRes(index);
        id.setRes(res);
        this->hide();



    }

}
