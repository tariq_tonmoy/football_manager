#ifndef INTERVIEW_H
#define INTERVIEW_H

#include <QDialog>
#include<DTO/interviewdto.h>
#include<DATA_SERVICE/interviewdataservice.h>
#include<SERVICE/interviewservice.h>
#include<QString>
#include<string>


namespace Ui {
    class Interview;
}

class Interview : public QDialog {
    Q_OBJECT
public:
    Interview(QWidget *parent = 0);
    ~Interview();
    InterviewDto getInterviewDto(){
        return this->id;
    }

protected:
    void changeEvent(QEvent *e);

private:
    InterviewDto id;
    InterviewDataService ids;
    InterviewService is;
    Ui::Interview *ui;
    int res;

private slots:
    void on_pushButton_clicked();
    void on_checkBox_3_clicked();
    void on_checkBox_2_clicked();
    void on_checkBox_clicked();
};

#endif // INTERVIEW_H
