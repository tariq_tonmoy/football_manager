#include "leaguetable.h"
#include "ui_leaguetable.h"
#include<QString>
#include<QList>
#include<vector>
#include<string>
#include<algorithm>


LeagueTable::LeagueTable(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::LeagueTable)
{
    ui->setupUi(this);
}

LeagueTable::~LeagueTable()
{
    delete ui;
}

void LeagueTable::changeEvent(QEvent *e)
{
    QDialog::changeEvent(e);
    switch (e->type()) {
    case QEvent::LanguageChange:
        ui->retranslateUi(this);
        break;
    default:
        break;
    }
}

void LeagueTable::createLeague()
{
    int p;
    vector<int>a,b;
    QString qstr;
    a=ld.getA();
    b=ld.getB();
    QTableWidget *table=ui->tableWidget;
    QTableWidgetItem *item1,*item2,*item3,*item4;
    table->setRowCount(a.size());
    table->setColumnCount(4);
    QStringList hl,vl;
    hl.push_back("Goal");
    hl.push_back("Home Team");
    hl.push_back("Away Team");
    hl.push_back("Goal");


    for(int i=0;i<a.size();i++)
        vl.push_back("");

    table->setHorizontalHeaderLabels(hl);
    table->setVerticalHeaderLabels(vl);
    for(int i=0;i<a.size();i++){
        item1=new QTableWidgetItem(QString::fromStdString(sd.getTeamName()[a[i]]));
        item2=new QTableWidgetItem(QString::fromStdString(sd.getTeamName()[b[i]]));
        p=ld.getGoalA()[i];
        if(p==-1)
            qstr="---";
        else qstr=QString::number(p);
        item3=new QTableWidgetItem(qstr);

        p=ld.getGoalB()[i];
        if(p==-1)
            qstr="---";
        else qstr=QString::number(p);
        item4=new QTableWidgetItem(qstr);
        table->setItem(i,1,item1);
        table->setItem(i,2,item2);
        table->setItem(i,0,item3);
        table->setItem(i,3,item4);
    }


    QTableWidget *table2=ui->tableWidget_2;
    table2->setRowCount(sd.getTeamName().size());
    table2->setColumnCount(5);

    QStringList hl2,vl2;
    for(int i=0;i<sd.getTeamName().size();i++){
        vl2.push_back("");
    }

    hl2.push_back("Team Name");
    hl2.push_back("Point");
    hl2.push_back("Goal Difference");
    hl2.push_back("Away Goal");
    hl2.push_back("Total Point");

    table2->setSortingEnabled(false);

    table2->setHorizontalHeaderLabels(hl2);
    table2->setVerticalHeaderLabels(vl2);


    vector<int>total;
    for(int i=0;i<sd.getTeamName().size();i++){
        int num;
        num=ld.getPoint()[i]+ld.getGoalDiff()[i]+ld.getAwayGoal()[i];
        total.push_back(num);
    }

    for(int i=0;i<sd.getTeamName().size();i++){
        item1=new QTableWidgetItem(QString::fromStdString(sd.getTeamName()[i]));
        table2->setItem(i,0,item1);
        item1=new QTableWidgetItem(QString::number(ld.getPoint()[i]));
        table2->setItem(i,1,item1);

        item1=new QTableWidgetItem(QString::number(ld.getGoalDiff()[i]));
        table2->setItem(i,2,item1);

        item1=new QTableWidgetItem(QString::number(ld.getAwayGoal()[i]));
        table2->setItem(i,3,item1);

        item1=new QTableWidgetItem(QString::number(total[i]));
        table2->setItem(i,4,item1);
    }


    QTableWidget *table3=ui->tableWidget_3;
    table3->setColumnCount(9);
    table3->setRowCount(ld.getPlayerName().size());
    QStringList hl3,vl3;

    for(int i=0;i<ld.getPlayerName().size();i++){
        vl3.push_back("");
    }
    hl3.push_back("");
    hl3.push_back("");
    hl3.push_back("");
    hl3.push_back("PLAYER NAME");
    hl3.push_back("TEAM NAME");
    hl3.push_back("GOALS");
    hl3.push_back("");
    hl3.push_back("");
    hl3.push_back("");

    table3->setSortingEnabled(false);
    table3->setHorizontalHeaderLabels(hl3);
    table3->setVerticalHeaderLabels(vl3);
    for(int i=0;i<ld.getPlayerName().size();i++){
        item1=new QTableWidgetItem(QString::fromStdString(ld.getPlayerName()[i]));
        table3->setItem(i,3,item1);

        item1=new QTableWidgetItem(QString::fromStdString(ld.getTeamName()[i]));
        table3->setItem(i,4,item1);

        item1=new QTableWidgetItem(QString::number(ld.getGoalCount()[i]));
        table3->setItem(i,5,item1);

    }

    table2->setSortingEnabled(true);
    table2->sortItems(4,Qt::DescendingOrder);

    table3->setSortingEnabled(true);
    table3->sortItems(5,Qt::DescendingOrder);


}

void LeagueTable::on_pushButton_clicked()
{
    this->hide();
}
