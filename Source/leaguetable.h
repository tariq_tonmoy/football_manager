#ifndef LEAGUETABLE_H
#define LEAGUETABLE_H
#include<DTO/squaddto.h>
#include<DTO/leaguedto.h>
#include <QDialog>

namespace Ui {
    class LeagueTable;
}

class LeagueTable : public QDialog {
    Q_OBJECT
public:
    LeagueTable(QWidget *parent = 0);
    void setSquad(SquadDto sd){
        this->sd=sd;
    }

    SquadDto getSquad(){
        return this->sd;

    }

    void setLeague(LeagueDto ld){
        this->ld=ld;
    }

    LeagueDto getLeagueDto(){
        return this->ld;
    }

    void createLeague();

    ~LeagueTable();

protected:
    void changeEvent(QEvent *e);

private:
    Ui::LeagueTable *ui;
    SquadDto sd;
    LeagueDto ld;

private slots:
    void on_pushButton_clicked();
};

#endif // LEAGUETABLE_H
