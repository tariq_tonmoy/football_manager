#include "loadprofile.h"
#include "ui_loadprofile.h"
#include<vector>
#include<string>
#include<fstream>
#include<iostream>
#include<SERVICE/encoder.h>
using namespace std;

LoadProfile::LoadProfile(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::LoadProfile)
{
    ui->setupUi(this);
}

LoadProfile::~LoadProfile()
{
    delete ui;
}

void LoadProfile::changeEvent(QEvent *e)
{
    QDialog::changeEvent(e);
    switch (e->type()) {
    case QEvent::LanguageChange:
        ui->retranslateUi(this);
        break;
    default:
        break;
    }
}

void LoadProfile::CreateUI(){
    ifstream fin;
    string str;
    Encoder en;

    fin.open("UserName.txt",ios::in);
    str="";
    while(!fin.eof()){
        getline(fin,str);
        str=en.str_str(str);
        name.push_back(str);
        str="";
    }
    fin.close();
    //ui->horizontalSlider->setMaximum(name.size()-2);
    ui->lineEdit->setText(QString::fromStdString(name[0]));

}

void LoadProfile::on_pushButton_clicked()
{
    QMessageBox qm;
    ProfileDataService pds;
    QString username,un,*unp;
    username=ui->lineEdit->text();
    string str;
    char ch;
    str=username.toStdString();
    for(int i=0;i<str.length();i++){
        ch=str[i];
        if(ch=='\\'||ch=='/'||ch=='?'||ch==':'||ch=='*'||ch=='<'||ch=='>'||ch=='\"'||ch=='|'||str==""||str[0]==' '||str[0]=='\t'){

            qm.setText("Invalid Characters");
            qm.exec();
            ui->lineEdit->setText("");
            return ;
        }
    }
    //ProfileDto pd;
    this->pd.setUser(username);
    un="UserName.txt";
    unp=&un;

    if(pds.isSimilar(&username,1,unp)){

        this->pd=pds.loadFile(pd);
        sd.setProfiledto(this->pd);
        TeamDto td;
        td=tds.LoadTeam(this->sd);
        this->sd.setTeamDto(td);

        this->sd.LoadSqauad();

       // qm.setText(QString::number(sd.getPlayerName().size()));
       //  qm.exec();

        for(int i=0;i<sd.getPlayerCount();i++){
           // qm.setText(QString::fromStdString(sd.getPlayerName()[i]));
            //qm.exec();
        }


        this->ppd.ReadPopularity(this->sd);
        this->ld.readDto(this->sd);
        this->tsd.ReadTransfer(this->sd);
        for(int i=0;i<tsd.getOtherTeamName().size();i++){
            qm.setText(QString::number(tsd.getOtherPlayerCount()[i]));
           // qm.exec();

        }

        Edit ed;

        ed.setProfileDto(pd);

        ed.setSquadDto(this->sd);
        ed.setLeagueDto(this->ld);
        ed.setPopularityDto(this->ppd);
        ed.setTransferDto(this->tsd);


        ed.createUI();
        qm.setText("Profile Successfully Loaded");
        qm.exec();
        ed.exec();
        this->hide();

    }
    else{
        qm.setText("No Profile Found");
        qm.exec();
    }

}




void LoadProfile::on_horizontalSlider_actionTriggered(int action)
{

}

void LoadProfile::on_horizontalSlider_valueChanged(int value)
{
     ui->lineEdit->setText(QString::fromStdString(name[value]));
}
