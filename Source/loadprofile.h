#ifndef LOADPROFILE_H
#define LOADPROFILE_H
#include<DTO/profiledto.h>
#include <QDialog>
#include<QString>
#include<DATA_SERVICE/profiledataservice.h>
#include"edit.h"
#include<QMessageBox>
#include<DATA_SERVICE/teamdataservice.h>
#include<DTO/leaguedto.h>
#include<DTO/teamdto.h>
#include<DTO/squaddto.h>
#include<DTO/popularitydto.h>
#include<DTO/transferdto.h>
#include<vector>

namespace Ui {
    class LoadProfile;
}

class LoadProfile : public QDialog {
    Q_OBJECT
public:
    LoadProfile(QWidget *parent = 0);
    void CreateUI();
    ~LoadProfile();

protected:
    void changeEvent(QEvent *e);

private:
    vector<string> name;
    Ui::LoadProfile *ui;
    SquadDto sd;
    TeamDto td;
    PopularityDto ppd;
    LeagueDto ld;
    TransferDto tsd;
    TeamDataService tds;
    ProfileDto pd;


private slots:


    void on_horizontalSlider_valueChanged(int value);
    void on_horizontalSlider_actionTriggered(int action);
    void on_pushButton_clicked();
};

#endif // LOADPROFILE_H
