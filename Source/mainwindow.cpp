#include "mainwindow.h"
#include "ui_mainwindow.h"
#include"newprofile.h"
#include<QDebug>
#include<DATA_SERVICE/teamdataservice.h>
#include"selectteam.h"
#include"interview.h"
#include"squad.h"
#include<DATA_SERVICE/dquaddataservice.h>
#include<SERVICE/encoder.h>
#include<string>
#include<fstream>
#include<iostream>
#include<sstream>
#include<stadium.h>
#include<transfer.h>
#include<scorer.h>
using namespace std;
MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::changeEvent(QEvent *e)
{
    QMainWindow::changeEvent(e);
    switch (e->type()) {
    case QEvent::LanguageChange:
        ui->retranslateUi(this);
        break;
    default:
        break;
    }
}

void MainWindow::on_pushButton_clicked()
{




    NewProfile np;
    this->hide();
    np.exec();
    this->show();

}

void MainWindow::on_pushButton_3_clicked()
{
    Scorer sc;
    sc.setText("Do You Really Want to Quit?");
    sc.exec();
    if(!sc.getFlag())
        return ;
    exit(1);
}

void MainWindow::on_pushButton_2_clicked()
{


    LoadProfile lp;

    this->hide();
    lp.exec();
    this->show();

}

