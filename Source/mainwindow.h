#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include"newprofile.h"
#include"edit.h"
#include<DTO/teamdto.h>
#include"loadprofile.h"
#define PA 5

namespace Ui {
    class MainWindow;
}

class MainWindow : public QMainWindow {
    Q_OBJECT
public:
    MainWindow(QWidget *parent = 0);
    ~MainWindow();

protected:
    void changeEvent(QEvent *e);

private:
   // TeamDto td;
    Ui::MainWindow *ui;

private slots:

    void on_pushButton_2_clicked();
    void on_pushButton_3_clicked();
    void on_pushButton_clicked();
};

#endif // MAINWINDOW_H
