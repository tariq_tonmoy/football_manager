#include "match.h"
#include "ui_match.h"
#include<algorithm>
#include<vector>
Match::Match(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Match)
{
    ui->setupUi(this);
    this->flag=true;
}

Match::~Match()
{
    delete ui;
}

void Match::changeEvent(QEvent *e)
{
    QDialog::changeEvent(e);
    switch (e->type()) {
    case QEvent::LanguageChange:
        ui->retranslateUi(this);
        break;
    default:
        break;
    }
}

void Match::createMatch(){
    ui->progressBar->setValue(0);
    ui->lcdNumber_2->display(0);
    ui->lcdNumber_3->display(0);
    ui->HomeLabel->setText(QString::fromStdString(sd.getTeamName()[ld.getA()[ld.getCurrentMatch()]]));
    ui->AwayLabel->setText(QString::fromStdString((sd.getTeamName()[ld.getB()[ld.getCurrentMatch()]])));
    ui->lineEdit->setText(QString::fromStdString(sd.getLocation()[ld.getA()[ld.getCurrentMatch()]]));
    int i;
    i=sd.getTeamOverall()[ld.getA()[ld.getCurrentMatch()]]-sd.getTeamOverall()[ld.getB()[ld.getCurrentMatch()]];
    if(i<0)
        i=-i;
    int att=((100-i)*sd.getStadium()[ld.getA()[ld.getCurrentMatch()]]/100);
    ui->lineEdit_2->setText(QString::number(att+sd.getStadiumUpgrade()*10*att/100));
    ui->label_2->setText("Match #"+QString::number(ld.getCurrentMatch()+1));

    ui->lineEdit->setReadOnly(true);
    ui->lineEdit_2->setReadOnly(true);
    ui->lineEdit_2->setAlignment(Qt::AlignRight);


}



void Match::on_pushButton_clicked()
{




    ui->pushButton->setEnabled(false);
    int home,away;

    clock_t start,end;

    int a,b;
    QMessageBox qm;
    LeagueService ls;
    int flag1,flag2,flag3;
    flag1=flag2=flag3=0;
    int overall;



    if(flag==true){
        if((ld.getA()[ld.getCurrentMatch()]==sd.getTeamIndex())){

            overall=ls.countOverall(this->sd,this->ppd);

            result=ls.countMatchResult(overall,(sd.getTeamOverall()[ld.getB()[ld.getCurrentMatch()]]));

            ls.countGoal(a,b,result,this->ld,this->sd);



        }

        else if((ld.getB()[ld.getCurrentMatch()])==sd.getTeamIndex()){


            overall=ls.countOverall(sd,ppd);


            result=ls.countMatchResult((sd.getTeamOverall()[ld.getA()[ld.getCurrentMatch()]]),overall);


        }

        else{


            result=ls.countMatchResult(sd.getTeamOverall()[ld.getA()[ld.getCurrentMatch()]],sd.getTeamOverall()[ld.getB()[ld.getCurrentMatch()]]);

        }

        ls.countGoal(home,away,result,ld,sd);

    }


    int scorer;
    string teamName;
    int ttl=home+away;

    qm.setText(QString::number(this->result));
    //qm.exec();

    qm.setText(QString::number(ttl));
    //qm.exec();



    int goalTeam[ttl+1];
    int goalTime[ttl+1],tempGoal;
    //string player[ttl+1],team[ttl+1];
    vector<string> player,team;
    srand(time(NULL));
    for(int n=0;n<ttl;){
        tempGoal=rand()%90;
        flag1=true;
        for(int m=0;m<n;m++){
            if(tempGoal==goalTime[m]){
                flag1=false;
                break;
            }
        }
        if(flag1==true){
            goalTime[n]=tempGoal;
            n++;
        }
    }
    sort(goalTime,goalTime+ttl);

    int hm,aw;
    hm=aw=0;
    for(int n=0;n<ttl;n++){
        if(hm<home&&aw<away){
            goalTeam[n]=rand()%2;
        }
        if(hm<home&&aw>=away)
            goalTeam[n]=0;
        if(aw<away&&hm>=home)
            goalTeam[n]=1;
        if(goalTeam[n]==0){
            teamName=sd.getTeamName()[ld.getA()[ld.getCurrentMatch()]];
            hm++;
        }
        else {
            teamName=sd.getTeamName()[ld.getB()[ld.getCurrentMatch()]];
            aw++;
        }


        if(teamName==sd.getTeamName()[sd.getTeamIndex()]){
            scorer=ls.calculateScorer(this->sd);
            flag2=true;
            for(int p=0;p<ld.getPlayerName().size();p++){

                if(ld.getPlayerName()[p]==sd.getPlayerName()[scorer]&&ld.getTeamName()[p]==sd.getTeamName()[sd.getTeamIndex()]){
                    ld.setGoalCount(p);
                    flag2=false;
                    break;
                }
            }
            if(flag2==true){
                ld.setScorer(sd.getPlayerName()[scorer],sd.getTeamName()[sd.getTeamIndex()]);
            }
            player.push_back(sd.getPlayerName()[scorer]);
            team.push_back(sd.getTeamName()[sd.getTeamIndex()]);


        }



        else {
            int p;
            for(p=0;p<td.getOtherTeamName().size();p++){
                if(teamName==td.getOtherTeamName()[p])
                    break;
            }
            scorer=(ls.calculateScorer(td.getOtherPlayerOverall()[p],td.getOtherPosition()[p]));
            flag2=true;

            for(int q=0;q<ld.getPlayerName().size();q++){

                if((ld.getPlayerName()[q]==td.getOtherPlayerName()[p][scorer])&&(ld.getTeamName()[q]==td.getOtherTeamName()[p])){
                    ld.setGoalCount(q);
                    flag2=false;
                    break;
                }
            }

            if(flag2==true){
                ld.setScorer(td.getOtherPlayerName()[p][scorer],td.getOtherTeamName()[p]);

            }
            player.push_back(td.getOtherPlayerName()[p][scorer]);
            team.push_back(td.getOtherTeamName()[p]);


        }



    }



    ld.setGoalA(home,ld.getCurrentMatch());
    ld.setGoalB(away,ld.getCurrentMatch());
    int p,q;

    p=ld.getA()[ld.getCurrentMatch()];
    q=ld.getB()[ld.getCurrentMatch()];
    if(result==1){
        ld.setPoint(ld.getPoint()[p]+3,p);
    }
    else if(result==2){
        ld.setPoint(ld.getPoint()[p]+1,p);
        ld.setPoint(ld.getPoint()[q]+1,q);

    }
    else
        ld.setPoint(ld.getPoint()[q]+3,q);

    ld.setGoalDiff(ld.getGoalDiff()[p]+home-away,p);
    ld.setGoalDiff(ld.getGoalDiff()[q]+away-home,q);

    ld.setAwayGoal(ld.getAwayGoal()[q]+away,q);




     srand(time(NULL));

    Goal gl;
    int n=0;
    for(int i=0;i<90;i++){
        start=clock();
       while(1){
            end=clock();
            if((((float)end-(float)start)/(float)CLOCKS_PER_SEC)>0.2)
               break;
        }
            ui->progressBar->setValue(ui->progressBar->value()+1);

            if(i>=goalTime[n]&&n<ttl){

                gl.setUI(player[n],team[n],i);

                gl.exec();
                if(goalTeam[n]==0){
                    ui->lcdNumber_2->display(ui->lcdNumber_2->value()+1);
                }
                else if(goalTeam[n]==1){
                    ui->lcdNumber_3->display(ui->lcdNumber_3->value()+1);
                }
                n++;
            }

    }

    string ht,at,mt;
    ht=sd.getTeamName()[ld.getA()[ld.getCurrentMatch()]];
    at=sd.getTeamName()[ld.getB()[ld.getCurrentMatch()]];
    mt=sd.getTeamName()[sd.getTeamIndex()];


    if(ht==mt||at==mt){
        int i;
        i=sd.getTeamOverall()[ld.getA()[ld.getCurrentMatch()]]-sd.getTeamOverall()[ld.getB()[ld.getCurrentMatch()]];
        if(i<0)
            i=-i;
        int tp=((100-i)*sd.getStadium()[ld.getA()[ld.getCurrentMatch()]]/100)*ppd.getTicketPrice();
        this->sd.setCurrentMoney(tp+tp*sd.getStadiumUpgrade()*10/100);
    }



    if(result==1){
        qm.setText("\""+QString::fromStdString(sd.getTeamName()[p])+"\""+"Has Won The Match");
        qm.exec();
    }


    if(result==2){
        qm.setText("The Match Is Drawn");
        qm.exec();
    }


    if(result==3){
        qm.setText("\""+QString::fromStdString(sd.getTeamName()[q])+"\""+" Has Won The Match");
        qm.exec();
    }



    ld.setCurrentMatch(ld.getCurrentMatch()+1);
   /* if(ld.getCurrentMatch()>=ld.getA().size()){
        ld.clearAll();
        ld.setMatches();
        int pos=ls.getPosition(ld.getPoint(),ld.getGoalDiff(),ld.getAwayGoal(),sd.getTeamIndex());
        ld.setSeasonCount(ld.getSeasonCount()+1);
        ld.setLeaguePosition(pos);
        qm.setText(QString::number(pos));
        qm.exec();
    }

*/
    //this->createMatch();

     this->hide();
}
