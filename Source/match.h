#ifndef MATCH_H
#define MATCH_H

#include <QDialog>
#include<DTO/leaguedto.h>
#include<DTO/squaddto.h>
#include<DTO/popularitydto.h>
#include<QString>
#include<QMessageBox>
#include<ctime>
#include<cstdlib>
#include<SERVICE/leagueservice.h>
#include<DTO/transferdto.h>
#include<goal.h>
using namespace std;

namespace Ui {
    class Match;
}

class Match : public QDialog {
    Q_OBJECT
public:
    Match(QWidget *parent = 0);
    void setSquad(SquadDto sd){
        this->sd=sd;
    }

    SquadDto getSquad(){
        return this->sd;
    }

    void setLeagueDto(LeagueDto ld){
        this->ld=ld;
    }

    LeagueDto getLeague(){
       return this->ld;
    }

    void setFlag(bool flag){
        this->flag=flag;
    }

    bool getFlag(){
        return this->flag;
    }

    PopularityDto getPopularity(){
        return this->ppd;
    }

    void setPopularityDto(PopularityDto ppd){
        this->ppd=ppd;
    }

    void setTransferDto(TransferDto td){
        this->td=td;
    }

    TransferDto getTransferDto(){
        return this->td;
    }

    int getResult(){
        return this->result;
    }

    void createMatch();

    ~Match();

protected:
    void changeEvent(QEvent *e);

private:
    Ui::Match *ui;
    bool flag;
    SquadDto sd;
    LeagueDto ld;
    PopularityDto ppd;
    TransferDto td;
    int result;




private slots:
    void on_pushButton_clicked();
};

#endif // MATCH_H
