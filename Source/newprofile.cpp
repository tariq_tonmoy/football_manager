#include "newprofile.h"
#include "ui_newprofile.h"


NewProfile::NewProfile(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::NewProfile)
{
    ui->setupUi(this);
}

NewProfile::~NewProfile()
{
    delete ui;
}

void NewProfile::changeEvent(QEvent *e)
{
    QDialog::changeEvent(e);
    switch (e->type()) {
    case QEvent::LanguageChange:
        ui->retranslateUi(this);
        break;
    default:
        break;
    }
}

void NewProfile::on_pushButton_clicked()
{

    string str;



    QMessageBox qm;
    QString qstr[PA];

    qstr[0]=ui->nicklineEdit->text();
    qstr[1]=ui->namelineEdit->text();
    qstr[2]=ui->agelineEdit->text();
    qstr[3]=ui->nationlineEdit->text();
    qstr[4]="UserName.txt";
    d.set_qstr(qstr,PA);

    char ch;
    str=qstr[0].toStdString();
    for(int i=0;i<str.length();i++){
        ch=str[i];
        if(ch=='\\'||ch=='/'||ch=='?'||ch==':'||ch=='*'||ch=='<'||ch=='>'||ch=='\"'||ch=='|'||str==""||str[0]==' '||str[0]=='\t'){

            qm.setText("Invalid Characters");
            qm.exec();
            ui->nicklineEdit->setText("");
            ui->namelineEdit->setText("");
            ui->agelineEdit->setText("");
            ui->nationlineEdit->setText("");
            return ;
        }
    }

    for(int i=0;i<5;i++){
        if(qstr[i]==""){
            qm.setText("Fill All The Fields First");
            qm.exec();
            return;
        }
    }

    if(pds.createFile(d)){
        pds.addUserName(qstr[0]);
        qm.setText("Profile Created");
        qm.exec();

        st.exec();
        TeamDto td=st.getTeamDto();
        sd.setTeamDto(td);
        sd.setCurrentMoney(sd.getStartMoney());

        in.exec();
        InterviewDto id=in.getInterviewDto();
        PopularityDto ppd;


        ppd.setPopularityComponents(id.getRes(),sd.TeamDto::getTeamOverall()[sd.getTeamIndex()]);
        sd.setCurrentMoney(ppd.getSouvenir());
        sd.setProfiledto(d);
        sd=sds.readSquad(sd);
        sd.resetRand();
        int avg=0;
        for(int i=0;i<sd.SquadDto::getPlayerOverall().size();i++){
            avg+=sd.SquadDto::getPlayerOverall()[i];
        }
        avg=avg/15;
        if(avg%2)avg++;
        sd.setAvgOverall(avg);



        avg=0;
        for(int i=0;i<sd.getPlayerOverall().size();i++){
            if(i>=11)break;
            avg+=sd.SquadDto::getPlayerOverall()[i];
        }
        avg=avg/11;
        if(avg%2)avg++;
        sd.setMatchAvgOverall(avg);



        tsd.setSquadDto(sd);
        tsd.setOtherTeams();
        tsd.resetTransfer();

        this->ld.setSquad(this->sd);
        this->ld.setMatches();


        ed.setPopularityDto(ppd);
        ed.setSquadDto(sd);
        ed.setTransferDto(tsd);
        ed.setLeagueDto(this->ld);
        ed.createUI();
        ed.exec();

        this->hide();

    }
    else {
        qm.setText("Change Profile Name");
        qm.exec();
    }

}

void NewProfile::on_pushButton_2_clicked()
{
    this->hide();
}
