#ifndef NEWPROFILE_H
#define NEWPROFILE_H

#include <QDialog>
#include<QMessageBox>
#include<DTO/profiledto.h>
#include<DATA_SERVICE/profiledataservice.h>
#include"edit.h"
#include"selectteam.h"
#include<DTO/teamdto.h>
#include"interview.h"
#include<DTO/interviewdto.h>
#include<DTO/popularitydto.h>
#include<SERVICE/popularityservice.h>
#include<DTO/squaddto.h>
#include<DATA_SERVICE/dquaddataservice.h>
#include<DTO/transferdto.h>
#include<DTO/leaguedto.h>


namespace Ui {
    class NewProfile;
}

class NewProfile : public QDialog {
    Q_OBJECT
public:
    NewProfile(QWidget *parent = 0);
    ~NewProfile();

protected:
    void changeEvent(QEvent *e);

private:
    LeagueDto ld;
    SquadDataService sds;
    SelectTeam st;
    Interview in;
    Edit ed;
    SquadDto sd;
    ProfileDto d;
    ProfileDataService pds;
    TransferDto tsd;
    Ui::NewProfile *ui;

private slots:
    void on_pushButton_2_clicked();
    void on_pushButton_clicked();
};

#endif // NEWPROFILE_H
