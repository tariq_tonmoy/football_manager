#ifndef RESULT_H
#define RESULT_H

#include <QDialog>

namespace Ui {
    class Result;
}

class Result : public QDialog {
    Q_OBJECT
public:
    Result(QWidget *parent = 0);
    ~Result();

protected:
    void changeEvent(QEvent *e);

private:
    Ui::Result *ui;
};

#endif // RESULT_H
