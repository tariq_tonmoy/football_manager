#include "salary.h"
#include "ui_salary.h"

Salary::Salary(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Salary)
{
    ui->setupUi(this);
}

Salary::~Salary()
{
    delete ui;
}

void Salary::changeEvent(QEvent *e)
{
    QDialog::changeEvent(e);
    switch (e->type()) {
    case QEvent::LanguageChange:
        ui->retranslateUi(this);
        break;
    default:
        break;
    }
}

void Salary::createSalary(){
    flag=false;
    QString qstr;
    QMessageBox qm;
    ui->lineEdit_2->setReadOnly(true);

    qstr=qstr.fromStdString(sd.getPlayerName()[0]);
    //ui->label_85->setText(qstr);
    ui->label_133->setText(qstr);
    qstr=qstr.fromStdString(sd.getPlayerName()[1]);
    //ui->label_86->setText(qstr);
    ui->label_134->setText(qstr);
    qstr=qstr.fromStdString(sd.getPlayerName()[2]);
    //ui->label_87->setText(qstr);
    ui->label_135->setText(qstr);
    qstr=qstr.fromStdString(sd.getPlayerName()[3]);
    //ui->label_88->setText(qstr);
    ui->label_136->setText(qstr);
    qstr=qstr.fromStdString(sd.getPlayerName()[4]);
    //ui->label_89->setText(qstr);
    ui->label_137->setText(qstr);
    qstr=qstr.fromStdString(sd.getPlayerName()[5]);
    //ui->label_90->setText(qstr);
    ui->label_138->setText(qstr);
    qstr=qstr.fromStdString(sd.getPlayerName()[6]);
    //ui->label_91->setText(qstr);
    ui->label_139->setText(qstr);
    qstr=qstr.fromStdString(sd.getPlayerName()[7]);
    //ui->label_92->setText(qstr);
    ui->label_140->setText(qstr);
    qstr=qstr.fromStdString(sd.getPlayerName()[8]);
    //ui->label_93->setText(qstr);
    ui->label_141->setText(qstr);
    qstr=qstr.fromStdString(sd.getPlayerName()[9]);
    //ui->label_94->setText(qstr);
    ui->label_142->setText(qstr);
    qstr=qstr.fromStdString(sd.getPlayerName()[10]);
    //ui->label_95->setText(qstr);
    ui->label_143->setText(qstr);
    qstr=qstr.fromStdString(sd.getPlayerName()[11]);
    //ui->label_96->setText(qstr);
    ui->label_144->setText(qstr);
    qstr=qstr.fromStdString(sd.getPlayerName()[12]);
    //ui->label_97->setText(qstr);
    ui->label_145->setText(qstr);
    qstr=qstr.fromStdString(sd.getPlayerName()[13]);
    //ui->label_98->setText(qstr);
    ui->label_146->setText(qstr);
    qstr=qstr.fromStdString(sd.getPlayerName()[14]);
    //ui->label_99->setText(qstr);
    ui->label_147->setText(qstr);


    qstr=QString::number(sd.SquadDto::getPlayerOverall()[0]);
    //ui->label_52->setText(qstr);
    ui->label_101->setText(qstr);
    qstr=QString::number(sd.SquadDto::getPlayerOverall()[1]);
    //ui->label_53->setText(qstr);
    ui->label_102->setText(qstr);
    qstr=QString::number(sd.SquadDto::getPlayerOverall()[2]);
    //ui->label_54->setText(qstr);
    ui->label_103->setText(qstr);
    qstr=QString::number(sd.SquadDto::getPlayerOverall()[3]);
    //ui->label_55->setText(qstr);
    ui->label_104->setText(qstr);
    qstr=QString::number(sd.SquadDto::getPlayerOverall()[4]);
    //ui->label_56->setText(qstr);
    ui->label_105->setText(qstr);
    qstr=QString::number(sd.SquadDto::getPlayerOverall()[5]);
    //ui->label_57->setText(qstr);
    ui->label_106->setText(qstr);
    qstr=QString::number(sd.SquadDto::getPlayerOverall()[6]);
    //ui->label_58->setText(qstr);
    ui->label_107->setText(qstr);
    qstr=QString::number(sd.SquadDto::getPlayerOverall()[7]);
    //ui->label_59->setText(qstr);
    ui->label_108->setText(qstr);
    qstr=QString::number(sd.SquadDto::getPlayerOverall()[8]);
    //ui->label_60->setText(qstr);
    ui->label_109->setText(qstr);
    qstr=QString::number(sd.SquadDto::getPlayerOverall()[9]);
    //ui->label_61->setText(qstr);
    ui->label_110->setText(qstr);
    qstr=QString::number(sd.SquadDto::getPlayerOverall()[10]);
    //ui->label_62->setText(qstr);
    ui->label_111->setText(qstr);
    qstr=QString::number(sd.SquadDto::getPlayerOverall()[11]);
    //ui->label_63->setText(qstr);
    ui->label_112->setText(qstr);
    qstr=QString::number(sd.SquadDto::getPlayerOverall()[12]);
    //ui->label_64->setText(qstr);
    ui->label_113->setText(qstr);
    qstr=QString::number(sd.SquadDto::getPlayerOverall()[13]);
    //ui->label_65->setText(qstr);
    ui->label_114->setText(qstr);
    qstr=QString::number(sd.SquadDto::getPlayerOverall()[14]);
    //ui->label_66->setText(qstr);
    ui->label_115->setText(qstr);



    qstr=qstr.fromLocal8Bit(&sd.getPosition()[0],1);
    //ui->label_69->setText(qstr);
    ui->label_117->setText(qstr);
    qstr=qstr.fromLocal8Bit(&sd.getPosition()[1],1);
    //ui->label_70->setText(qstr);
    ui->label_118->setText(qstr);
    qstr=qstr.fromLocal8Bit(&sd.getPosition()[2],1);
    //ui->label_71->setText(qstr);
    ui->label_119->setText(qstr);
    qstr=qstr.fromLocal8Bit(&sd.getPosition()[3],1);
    //ui->label_72->setText(qstr);
    ui->label_120->setText(qstr);
    qstr=qstr.fromLocal8Bit(&sd.getPosition()[4],1);
    //ui->label_73->setText(qstr);
    ui->label_121->setText(qstr);
    qstr=qstr.fromLocal8Bit(&sd.getPosition()[5],1);
    //ui->label_74->setText(qstr);
    ui->label_122->setText(qstr);
    qstr=qstr.fromLocal8Bit(&sd.getPosition()[6],1);
    //ui->label_75->setText(qstr);
    ui->label_123->setText(qstr);
    qstr=qstr.fromLocal8Bit(&sd.getPosition()[7],1);
    //ui->label_76->setText(qstr);
    ui->label_124->setText(qstr);
    qstr=qstr.fromLocal8Bit(&sd.getPosition()[8],1);
    //ui->label_77->setText(qstr);
    ui->label_125->setText(qstr);
    qstr=qstr.fromLocal8Bit(&sd.getPosition()[9],1);
    //ui->label_78->setText(qstr);
    ui->label_126->setText(qstr);
    qstr=qstr.fromLocal8Bit(&sd.getPosition()[10],1);
    //ui->label_79->setText(qstr);
    ui->label_127->setText(qstr);
    qstr=qstr.fromLocal8Bit(&sd.getPosition()[11],1);
    //ui->label_80->setText(qstr);
    ui->label_128->setText(qstr);
    qstr=qstr.fromLocal8Bit(&sd.getPosition()[12],1);
    //ui->label_81->setText(qstr);
    ui->label_129->setText(qstr);
    qstr=qstr.fromLocal8Bit(&sd.getPosition()[13],1);
    //ui->label_82->setText(qstr);
    ui->label_130->setText(qstr);
    qstr=qstr.fromLocal8Bit(&sd.getPosition()[14],1);
    //ui->label_83->setText(qstr);
    ui->label_131->setText(qstr);



    if(sd.getPlayerName()[12]=="N/A"){
        //ui->checkBox_30->setEnabled(false);
        ui->label_145->setText("- - -");
        ui->label_113->setText("- - -");
        ui->label_129->setText("- - -");
     }
    if(sd.getPlayerName()[12]=="N/A"){
        ui->label_145->setText("- - -");
        ui->label_113->setText("- - -");
        ui->label_129->setText("- - -");

    }

    if(sd.getPlayerName()[12]=="N/A"){
       // ui->checkBox_30->setEnabled(false);
        ui->label_145->setText("- - -");
        ui->label_113->setText("- - -");
        ui->label_129->setText("- - -");

    }


    ui->lineEdit->setReadOnly(true);
    ui->verticalSlider->setValue(sd.getBonus());
    ui->verticalSlider_2->setValue(sd.getSalary());
    ui->lineEdit_2->setText(QString::number(this->sd.getCurrentMoney()));
    this->calculateSalary();

    flag=true;



}

void Salary::on_verticalSlider_2_valueChanged(int value)
{
    if(flag==true)

    this->calculateSalary();




}
void Salary::calculateSalary(){
    int a[15];
    int total=0;
    sd.setSalary(ui->verticalSlider_2->value());
    sd.setBonus(ui->verticalSlider->value());
    for(int i=0;i<sd.getPlayerCount();i++){
        a[i]=(sd.getPlayerOverall()[i]*2500)+(sd.getPlayerOverall()[i]*2500*ui->spinBox_2->value()/100)+(sd.getMatchMoney()*(ui->spinBox->value())/100);
        total+=a[i];
    }
    total+=(15-sd.getPlayerCount())*(sd.getMatchAvgOverall()*2500);
    ui->lcdNumber->display(a[0]);
    ui->lcdNumber_2->display(a[1]);
    ui->lcdNumber_3->display(a[2]);
    ui->lcdNumber_4->display(a[3]);
    ui->lcdNumber_5->display(a[4]);
    ui->lcdNumber_6->display(a[5]);
    ui->lcdNumber_7->display(a[6]);
    ui->lcdNumber_8->display(a[7]);
    ui->lcdNumber_9->display(a[8]);
    ui->lcdNumber_10->display(a[9]);
    ui->lcdNumber_11->display(a[10]);
    ui->lcdNumber_12->display(a[11]);
    ui->lcdNumber_13->display(a[12]);
    ui->lcdNumber_14->display(a[13]);
    ui->lcdNumber_15->display(a[14]);

    ui->lineEdit->setText(QString::number(total));
    sd.setTotalSalary(total);

}

void Salary::on_verticalSlider_valueChanged(int value)
{
    if(flag==true)
    this->calculateSalary();
}

void Salary::on_pushButton_clicked()
{


    this->hide();
}
