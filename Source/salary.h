#ifndef SALARY_H
#define SALARY_H
#include<DTO/squaddto.h>
#include <QDialog>
#include<QString>
namespace Ui {
    class Salary;
}

class Salary : public QDialog {
    Q_OBJECT
public:
    Salary(QWidget *parent = 0);
    void createSalary();
    void setSquad(SquadDto sd){
        this->sd=sd;
    }
    SquadDto getSquad(){
        return this->sd;
    }
    void calculateSalary();

    ~Salary();

protected:
    void changeEvent(QEvent *e);

private:
    SquadDto sd;
    bool flag;
    Ui::Salary *ui;

private slots:
    void on_pushButton_clicked();
    void on_verticalSlider_valueChanged(int value);
    void on_verticalSlider_2_valueChanged(int value);
};

#endif // SALARY_H
