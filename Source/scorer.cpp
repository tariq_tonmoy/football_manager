#include "scorer.h"
#include "ui_scorer.h"

Scorer::Scorer(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Scorer)
{
    ui->setupUi(this);
}

Scorer::~Scorer()
{
    delete ui;
}

void Scorer::changeEvent(QEvent *e)
{
    QDialog::changeEvent(e);
    switch (e->type()) {
    case QEvent::LanguageChange:
        ui->retranslateUi(this);
        break;
    default:
        break;
    }
}

void Scorer::setText(QString qstr){
    ui->label->setText(qstr);
}

void Scorer::on_buttonBox_accepted()
{
    this->flag=true;
    this->hide();
}

void Scorer::on_buttonBox_rejected()
{
    this->flag=false;
    this->hide();
}
