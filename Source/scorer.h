#ifndef SCORER_H
#define SCORER_H

#include <QDialog>

namespace Ui {
    class Scorer;
}

class Scorer : public QDialog {
    Q_OBJECT
public:
    Scorer(QWidget *parent = 0);
    bool getFlag(){
        return this->flag;
    }
    void setText(QString qstr);



    ~Scorer();

protected:
    void changeEvent(QEvent *e);

private:
    Ui::Scorer *ui;
    bool flag;

private slots:
    void on_buttonBox_rejected();
    void on_buttonBox_accepted();
};

#endif // SCORER_H
