#include "season.h"
#include "ui_season.h"
#include<QString>
#include<string>
#include<QList>

Season::Season(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Season)
{
    ui->setupUi(this);

}

Season::~Season()
{
    delete ui;
}

void Season::changeEvent(QEvent *e)
{
    QDialog::changeEvent(e);
    switch (e->type()) {
    case QEvent::LanguageChange:
        ui->retranslateUi(this);
        break;
    default:
        break;
    }
}

void Season::createSeason(){

    if(ld.getCurrentMatch()==ld.getA().size()){
        ui->pushButton_2->setEnabled(true);
    }
    else ui->pushButton_2->setEnabled(false);

    if(ui->pushButton_2->isEnabled()==true)
        ui->pushButton_3->setEnabled(false);
    else ui->pushButton_3->setEnabled(true);

    QTableWidget *table=ui->tableWidget;
    QTableWidgetItem *item;

    QStringList hl,vl;

    hl.push_back("");
    hl.push_back("Season");
    hl.push_back("Position");

    for(int i=0;i<ld.getSeasonCount();i++){
        vl.push_back("");
    }

    table->setColumnCount(3);
    table->setRowCount(ld.getSeasonCount());

    table->setHorizontalHeaderLabels(hl);
    table->setVerticalHeaderLabels(vl);

    for(int i=0;i<ld.getSeasonCount();i++){
        item=new QTableWidgetItem("Season: "+QString::number(i+1));
        table->setItem(i,1,item);

        item=new QTableWidgetItem(QString::number(ld.getLeaguePosition()[i]));
        table->setItem(i,2,item);
    }

    ui->label_2->setText(sd.get_qstr()[0]);
    ui->label_3->setText(QString::fromStdString(sd.getTeamName()[sd.getTeamIndex()]));




}

void Season::on_Season_accepted()
{

}

void Season::on_pushButton_clicked()
{
    LeagueTable ls;
    ls.setSquad(this->sd);
    ls.setLeague(this->ld);
    ls.createLeague();
    ls.exec();


}

void Season::on_pushButton_3_clicked()
{
    this->hide();
}

void Season::on_pushButton_2_clicked()
{
    ld.setSquad(this->sd);
    ld.clearAll();
    ld.setMatches();
    this->createSeason();
}
