#ifndef SEASON_H
#define SEASON_H

#include <QDialog>
#include<vector>
#include<DTO/squaddto.h>
#include<DTO/leaguedto.h>
#include<leaguetable.h>

namespace Ui {
    class Season;
}

class Season : public QDialog {
    Q_OBJECT
public:
    Season(QWidget *parent = 0);
    void setSquadDto(SquadDto sd){
        this->sd=sd;
    }
    void createSeason();


    void setLeagueDto(LeagueDto ld){
        this->ld=ld;
    }
    SquadDto getSquad(){
        return this->sd;
    }

    LeagueDto getLeague(){
        return this->ld;
    }

    ~Season();

protected:
    void changeEvent(QEvent *e);

private:
    SquadDto sd;
    LeagueDto ld;

    Ui::Season *ui;

private slots:
    void on_pushButton_2_clicked();
    void on_pushButton_3_clicked();
    void on_pushButton_clicked();
    void on_Season_accepted();
};

#endif // SEASON_H
