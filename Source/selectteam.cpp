#include "selectteam.h"
#include "ui_selectteam.h"

SelectTeam::SelectTeam(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::SelectTeam)
{

    ui->setupUi(this);

    for(int i=0;i<100;i++)
        a[i]=-1;


    ui->overall_lineEdit->setReadOnly(true);
    ui->location_lineEdit->setReadOnly(true);
    ui->Country_lineEdit->setReadOnly(true);
    ui->stadium_lineEdit->setReadOnly(true);
    ui->money_lineEdit->setReadOnly(true);
    ui->match_lineEdit->setReadOnly(true);
    ui->win_lineEdit->setReadOnly(true);
    ui->draw_lineEdit->setReadOnly(true);
    ui->lose_lineEdit->setReadOnly(true);

    td=tds.readAllteam();
    QString qname[(td.getTeamName()).size()],qsponsors[(td.getSponsors()).size()],qoverall,qstadium;
    QComboBox *cb=ui->comboBox;
    for(int i=0;i<td.getTeamName().size();i++){
        qname[i]=qname[i].fromStdString(td.getTeamName()[i]);
        cb->addItem(qname[i]);
    }
    cb=ui->comboBox_2;
    for(int i=0;i<td.getSponsors().size();i++){
        qsponsors[i]=qsponsors[i].fromStdString(td.getSponsors()[i]);
        cb->addItem(qsponsors[i]);
    }
    cb->setEnabled(false);

}

SelectTeam::~SelectTeam()
{
    delete ui;
}

void SelectTeam::changeEvent(QEvent *e)
{
    QDialog::changeEvent(e);
    switch (e->type()) {
    case QEvent::LanguageChange:
        ui->retranslateUi(this);
        break;
    default:
        break;
    }
}

void SelectTeam::on_comboBox_activated(QString )
{
    QComboBox *cb=ui->comboBox;
    int i=cb->currentIndex();
    teamIndex=i;
    QString qcountry=qcountry.fromStdString(td.getCountry()[i]);
    QString qlocation=qlocation.fromStdString(td.getLocation()[i]);
    QString qstadium;
    qstadium=QString::number(td.getStadium()[i],10);
    QString qoverall=QString::number(td.getTeamOverall()[i],10);
    ui->overall_lineEdit->setText(qoverall);
    ui->location_lineEdit->setText(qlocation);
    ui->Country_lineEdit->setText(qcountry);
    ui->stadium_lineEdit->setText(qstadium);

}

void SelectTeam::on_comboBox_2_activated(QString )
{
    QString qstr;
    int j,start, match,win,draw,lose;
    QComboBox *cb=ui->comboBox_2;
    int i=sponsorIndex=cb->currentIndex();
    if(a[i]==-1){
        j=t.setSponsor(td.getTeamOverall()[teamIndex],start,match);
        a[i]=j;
    }
    else{
        j=t.setSponsor(td.getTeamOverall()[teamIndex],start,match);
        while(j!=a[i]){
            j=t.setSponsor(td.getTeamOverall()[teamIndex],start,match);
        }
    }
    if(j){
        sType=j;
        win=25*match/100;
        draw=15*match/100;
        lose=0;
    }
    else{
        sType=j;
        win=15*match/100;
        draw=0;
        lose=0;
    }
    startMoney=start;
    matchMoney=match;
    winMoney=win;
    drawMoney=draw;
    qstr=QString::number(start,10);
    ui->money_lineEdit->setText(qstr+"$");
    qstr=QString::number(match,10);
    ui->match_lineEdit->setText(qstr+"$");
    qstr=QString::number(win,10);
    ui->win_lineEdit->setText(qstr+"$");
    qstr=QString::number(draw,10);
    ui->draw_lineEdit->setText(qstr+"$");
    ui->lose_lineEdit->setText("0$");

}

void SelectTeam::on_pushButton_clicked()
{
    if(ui->overall_lineEdit->text()==""){
        QMessageBox qm;
        qm.setText("Select A Team First");
        qm.exec();
    }
    else{
        QComboBox *cb=ui->comboBox;
        cb->setEnabled(false);
        cb=ui->comboBox_2;
        cb->setEnabled(true);

    }
}

void SelectTeam::on_pushButton_6_clicked()
{
    QMessageBox qm;
    QString qstr;
    if(ui->match_lineEdit->text()==""){
                qm.setText("Select A Sponsor First");
        qm.exec();
    }
    else{
        td.setMyTeam(teamIndex);
        td.setMySponsor(sponsorIndex,startMoney,matchMoney,winMoney,drawMoney);

        this->hide();

    }

}

void SelectTeam::on_comboBox_currentIndexChanged(QString )
{

}
