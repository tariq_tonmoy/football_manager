#ifndef SELECTTEAM_H
#define SELECTTEAM_H
#include<DTO/teamdto.h>
#include<DATA_SERVICE/teamdataservice.h>
#include<QString>
#include <QDialog>
#include<vector>
#include<string>
#include<SERVICE/team.h>
#include<QMessageBox>



namespace Ui {
    class SelectTeam;
}

class SelectTeam : public QDialog {
    Q_OBJECT
public:
    SelectTeam(QWidget *parent = 0);
    ~SelectTeam();
    TeamDto getTeamDto(){
        return this->td;
    }

protected:
    void changeEvent(QEvent *e);

private:
    int teamIndex,sponsorIndex,sType,myOverall,count;
    Team t;
    TeamDto td;
    TeamDataService tds;
    string myTeam,sponsor;
    int a[100],startMoney,matchMoney,winMoney,drawMoney;



    Ui::SelectTeam *ui;

private slots:
    void on_comboBox_currentIndexChanged(QString );
    void on_pushButton_6_clicked();
    void on_pushButton_clicked();
    void on_comboBox_2_activated(QString );
    void on_comboBox_activated(QString );
};

#endif // SELECTTEAM_H
