#include "squad.h"
#include "ui_squad.h"
Squad::Squad(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Squad)
{
    ui->setupUi(this);
    vector<int>::iterator p=v.begin();
    v.insert(p,15,0);
    ui->spinBox_16->setVisible(false);
    ui->checkBox_17->setCheckable(false);
    ui->lineEdit_2->setEnabled(false);
    ui->lineEdit_3->setEnabled(false);
}

Squad::~Squad()
{
    delete ui;
}

void Squad::changeEvent(QEvent *e)
{
    QDialog::changeEvent(e);
    switch (e->type()) {
    case QEvent::LanguageChange:
        ui->retranslateUi(this);
        break;
    default:
        break;
    }
}

void Squad::createSquad(){



    QString qstr;
    flag=false;
    qstr=qstr.fromStdString(sd.TeamDto::getTeamName()[sd.getTeamIndex()]);
    ui->label_4->setText(qstr);
    ui->label_50->setText(qstr);




    qstr=qstr.fromStdString(sd.getPlayerName()[0]);
    ui->label_85->setText(qstr);
    ui->label_133->setText(qstr);
    qstr=qstr.fromStdString(sd.getPlayerName()[1]);
    ui->label_86->setText(qstr);
    ui->label_134->setText(qstr);
    qstr=qstr.fromStdString(sd.getPlayerName()[2]);
    ui->label_87->setText(qstr);
    ui->label_135->setText(qstr);
    qstr=qstr.fromStdString(sd.getPlayerName()[3]);
    ui->label_88->setText(qstr);
    ui->label_136->setText(qstr);
    qstr=qstr.fromStdString(sd.getPlayerName()[4]);
    ui->label_89->setText(qstr);
    ui->label_137->setText(qstr);
    qstr=qstr.fromStdString(sd.getPlayerName()[5]);
    ui->label_90->setText(qstr);
    ui->label_138->setText(qstr);
    qstr=qstr.fromStdString(sd.getPlayerName()[6]);
    ui->label_91->setText(qstr);
    ui->label_139->setText(qstr);
    qstr=qstr.fromStdString(sd.getPlayerName()[7]);
    ui->label_92->setText(qstr);
    ui->label_140->setText(qstr);
    qstr=qstr.fromStdString(sd.getPlayerName()[8]);
    ui->label_93->setText(qstr);
    ui->label_141->setText(qstr);
    qstr=qstr.fromStdString(sd.getPlayerName()[9]);
    ui->label_94->setText(qstr);
    ui->label_142->setText(qstr);
    qstr=qstr.fromStdString(sd.getPlayerName()[10]);
    ui->label_95->setText(qstr);
    ui->label_143->setText(qstr);
    qstr=qstr.fromStdString(sd.getPlayerName()[11]);
    ui->label_96->setText(qstr);
    ui->label_144->setText(qstr);
    qstr=qstr.fromStdString(sd.getPlayerName()[12]);
    ui->label_97->setText(qstr);
    ui->label_145->setText(qstr);
    qstr=qstr.fromStdString(sd.getPlayerName()[13]);
    ui->label_98->setText(qstr);
    ui->label_146->setText(qstr);
    qstr=qstr.fromStdString(sd.getPlayerName()[14]);
    ui->label_99->setText(qstr);
    ui->label_147->setText(qstr);


    qstr=QString::number(sd.SquadDto::getPlayerOverall()[0]);
    ui->label_52->setText(qstr);
    ui->label_101->setText(qstr);
    qstr=QString::number(sd.SquadDto::getPlayerOverall()[1]);
    ui->label_53->setText(qstr);
    ui->label_102->setText(qstr);
    qstr=QString::number(sd.SquadDto::getPlayerOverall()[2]);
    ui->label_54->setText(qstr);
    ui->label_103->setText(qstr);
    qstr=QString::number(sd.SquadDto::getPlayerOverall()[3]);
    ui->label_55->setText(qstr);
    ui->label_104->setText(qstr);
    qstr=QString::number(sd.SquadDto::getPlayerOverall()[4]);
    ui->label_56->setText(qstr);
    ui->label_105->setText(qstr);
    qstr=QString::number(sd.SquadDto::getPlayerOverall()[5]);
    ui->label_57->setText(qstr);
    ui->label_106->setText(qstr);
    qstr=QString::number(sd.SquadDto::getPlayerOverall()[6]);
    ui->label_58->setText(qstr);
    ui->label_107->setText(qstr);
    qstr=QString::number(sd.SquadDto::getPlayerOverall()[7]);
    ui->label_59->setText(qstr);
    ui->label_108->setText(qstr);
    qstr=QString::number(sd.SquadDto::getPlayerOverall()[8]);
    ui->label_60->setText(qstr);
    ui->label_109->setText(qstr);
    qstr=QString::number(sd.SquadDto::getPlayerOverall()[9]);
    ui->label_61->setText(qstr);
    ui->label_110->setText(qstr);
    qstr=QString::number(sd.SquadDto::getPlayerOverall()[10]);
    ui->label_62->setText(qstr);
    ui->label_111->setText(qstr);
    qstr=QString::number(sd.SquadDto::getPlayerOverall()[11]);
    ui->label_63->setText(qstr);
    ui->label_112->setText(qstr);
    qstr=QString::number(sd.SquadDto::getPlayerOverall()[12]);
    ui->label_64->setText(qstr);
    ui->label_113->setText(qstr);
    qstr=QString::number(sd.SquadDto::getPlayerOverall()[13]);
    ui->label_65->setText(qstr);
    ui->label_114->setText(qstr);
    qstr=QString::number(sd.SquadDto::getPlayerOverall()[14]);
    ui->label_66->setText(qstr);
    ui->label_115->setText(qstr);



    qstr=qstr.fromLocal8Bit(&sd.getPosition()[0],1);
    ui->label_69->setText(qstr);
    ui->label_117->setText(qstr);
    qstr=qstr.fromLocal8Bit(&sd.getPosition()[1],1);
    ui->label_70->setText(qstr);
    ui->label_118->setText(qstr);
    qstr=qstr.fromLocal8Bit(&sd.getPosition()[2],1);
    ui->label_71->setText(qstr);
    ui->label_119->setText(qstr);
    qstr=qstr.fromLocal8Bit(&sd.getPosition()[3],1);
    ui->label_72->setText(qstr);
    ui->label_120->setText(qstr);
    qstr=qstr.fromLocal8Bit(&sd.getPosition()[4],1);
    ui->label_73->setText(qstr);
    ui->label_121->setText(qstr);
    qstr=qstr.fromLocal8Bit(&sd.getPosition()[5],1);
    ui->label_74->setText(qstr);
    ui->label_122->setText(qstr);
    qstr=qstr.fromLocal8Bit(&sd.getPosition()[6],1);
    ui->label_75->setText(qstr);
    ui->label_123->setText(qstr);
    qstr=qstr.fromLocal8Bit(&sd.getPosition()[7],1);
    ui->label_76->setText(qstr);
    ui->label_124->setText(qstr);
    qstr=qstr.fromLocal8Bit(&sd.getPosition()[8],1);
    ui->label_77->setText(qstr);
    ui->label_125->setText(qstr);
    qstr=qstr.fromLocal8Bit(&sd.getPosition()[9],1);
    ui->label_78->setText(qstr);
    ui->label_126->setText(qstr);
    qstr=qstr.fromLocal8Bit(&sd.getPosition()[10],1);
    ui->label_79->setText(qstr);
    ui->label_127->setText(qstr);
    qstr=qstr.fromLocal8Bit(&sd.getPosition()[11],1);
    ui->label_80->setText(qstr);
    ui->label_128->setText(qstr);
    qstr=qstr.fromLocal8Bit(&sd.getPosition()[12],1);
    ui->label_81->setText(qstr);
    ui->label_129->setText(qstr);
    qstr=qstr.fromLocal8Bit(&sd.getPosition()[13],1);
    ui->label_82->setText(qstr);
    ui->label_130->setText(qstr);
    qstr=qstr.fromLocal8Bit(&sd.getPosition()[14],1);
    ui->label_83->setText(qstr);
    ui->label_131->setText(qstr);

    ui->horizontalSlider_86->setValue(sd.getTrain()[0]);
    ui->horizontalSlider_87->setValue(sd.getTrain()[1]);
    ui->horizontalSlider_88->setValue(sd.getTrain()[2]);
    ui->horizontalSlider_89->setValue(sd.getTrain()[3]);
    ui->horizontalSlider_90->setValue(sd.getTrain()[4]);
    ui->horizontalSlider_91->setValue(sd.getTrain()[5]);
    ui->horizontalSlider_92->setValue(sd.getTrain()[6]);
    ui->horizontalSlider_93->setValue(sd.getTrain()[7]);
    ui->horizontalSlider_94->setValue(sd.getTrain()[8]);
    ui->horizontalSlider_95->setValue(sd.getTrain()[9]);
    ui->horizontalSlider_96->setValue(sd.getTrain()[10]);
    ui->horizontalSlider_97->setValue(sd.getTrain()[11]);
    ui->horizontalSlider_98->setValue(sd.getTrain()[12]);
    ui->horizontalSlider_99->setValue(sd.getTrain()[13]);
    ui->horizontalSlider_100->setValue(sd.getTrain()[14]);


    for(int j=0;j<15;j++){

        v[j]=sd.getTrain()[j];
    }
    flag=true;

    qstr=QString::number(sd.getCurrentExpense());
    ui->lineEdit_2->setText(qstr+"$");
    qstr=QString::number(sd.getCurrentMoney());
    ui->lineEdit_3->setText(qstr+"$");

    ui->checkBox_18->setChecked(true);
    ui->checkBox_19->setChecked(true);
    ui->checkBox_20->setChecked(true);
    ui->checkBox_21->setChecked(true);
    ui->checkBox_22->setChecked(true);
    ui->checkBox_23->setChecked(true);
    ui->checkBox_24->setChecked(true);
    ui->checkBox_25->setChecked(true);
    ui->checkBox_26->setChecked(true);
    ui->checkBox_27->setChecked(true);
    ui->checkBox_28->setChecked(true);


    if(sd.getPlayerName()[12]=="N/A"){
        ui->checkBox_30->setEnabled(false);
        ui->label_145->setText("- - -");
        ui->label_113->setText("- - -");
        ui->label_129->setText("- - -");
        ui->label_97->setText("- - -");
        ui->label_64->setText("- - -");
        ui->label_81->setText("- - -");
        ui->horizontalSlider_98->setEnabled(false);
    }
    else{
        ui->checkBox_30->setEnabled(true);
        ui->horizontalSlider_98->setEnabled(true);

    }

    if(sd.getPlayerName()[13]=="N/A"){
        ui->checkBox_31->setEnabled(false);
        ui->label_146->setText("- - -");
        ui->label_114->setText("- - -");
        ui->label_130->setText("- - -");
        ui->label_98->setText("- - -");
        ui->label_65->setText("- - -");
        ui->label_82->setText("- - -");
        ui->horizontalSlider_99->setEnabled(false);
    }

    else{
        ui->checkBox_31->setEnabled(true);
        ui->horizontalSlider_99->setEnabled(true);

    }
    if(sd.getPlayerName()[14]=="N/A"){
        ui->checkBox_32->setEnabled(false);
        ui->label_147->setText("- - -");
        ui->label_115->setText("- - -");
        ui->label_131->setText("- - -");
        ui->label_99->setText("- - -");
        ui->label_66->setText("- - -");
        ui->label_83->setText("- - -");
        ui->horizontalSlider_100->setEnabled(false);
    }

    else{
        ui->checkBox_32->setEnabled(true);
        ui->horizontalSlider_100->setEnabled(true);

    }
    int avg=0;
    for(int i=0;i<sd.getPlayerCount();i++)
        avg+=sd.SquadDto::getPlayerOverall()[i];
    avg=avg/15;
    if(avg%2)
        avg+=1;
    sd.setAvgOverall(avg);
}

void Squad::on_horizontalSlider_86_valueChanged(int value)
{
    setSlideBar(0,value);


}
void Squad::on_horizontalSlider_87_valueChanged(int value)
{
    setSlideBar(1,value);

}
void Squad::on_horizontalSlider_88_valueChanged(int value)
{
    setSlideBar(2,value);

}
void Squad::on_horizontalSlider_89_valueChanged(int value)
{
    setSlideBar(3,value);

}
void Squad::on_horizontalSlider_90_valueChanged(int value)
{
    setSlideBar(4,value);

}
void Squad::on_horizontalSlider_91_valueChanged(int value)
{
    setSlideBar(5,value);

}
void Squad::on_horizontalSlider_92_valueChanged(int value)
{
    setSlideBar(6,value);

}
void Squad::on_horizontalSlider_93_valueChanged(int value)
{
    setSlideBar(7,value);

}
void Squad::on_horizontalSlider_94_valueChanged(int value)
{
    setSlideBar(8,value);

}
void Squad::on_horizontalSlider_95_valueChanged(int value)
{
    setSlideBar(9,value);

}
void Squad::on_horizontalSlider_96_valueChanged(int value)
{
    setSlideBar(10,value);
}
void Squad::on_horizontalSlider_97_valueChanged(int value)
{
    setSlideBar(11,value);

}
void Squad::on_horizontalSlider_98_valueChanged(int value)
{
    setSlideBar(12,value);

}
void Squad::on_horizontalSlider_99_valueChanged(int value)
{
    setSlideBar(13,value);

}
void Squad::on_horizontalSlider_100_valueChanged(int value)
{
    setSlideBar(14,value);

}



void Squad::setSlideBar(int num, int value){
    if(flag==true){
        int i;
        QString qstr;

        if(value>v[num]){
            i=(sd.SquadDto::getPlayerOverall()[num]*2000*(value-v[num]))*10/100;

            sd.setCurrentMoney(-i);
            sd.setCurrentExpense(i);

        }
        else if(value<v[num]){
            i=(sd.SquadDto::getPlayerOverall()[num]*2000*(v[num]-value))*10/100;
            sd.setCurrentMoney(i);
            sd.setCurrentExpense(-i);
        }
    setValue(num);
        qstr=QString::number(sd.getCurrentMoney());
        ui->lineEdit_3->setText(qstr+"$");
        qstr=QString::number(sd.getCurrentExpense());
        ui->lineEdit_2->setText(qstr+"$");
        v[num]=value;
    }
}

void Squad::setValue(int j)
{
    QMessageBox qm;
    QString qstr;

    if(j==0)sd.SquadDto::setTrain(ui->horizontalSlider_86->value(),0);
    if(j==1)sd.SquadDto::setTrain(ui->horizontalSlider_87->value(),1);
    if(j==2)sd.SquadDto::setTrain(ui->horizontalSlider_88->value(),2);
    if(j==3)sd.SquadDto::setTrain(ui->horizontalSlider_89->value(),3);
    if(j==4)sd.SquadDto::setTrain(ui->horizontalSlider_90->value(),4);
    if(j==5)sd.SquadDto::setTrain(ui->horizontalSlider_91->value(),5);
    if(j==6)sd.SquadDto::setTrain(ui->horizontalSlider_92->value(),6);
    if(j==7)sd.SquadDto::setTrain(ui->horizontalSlider_93->value(),7);
    if(j==8)sd.SquadDto::setTrain(ui->horizontalSlider_94->value(),8);
    if(j==9)sd.SquadDto::setTrain(ui->horizontalSlider_95->value(),9);
    if(j==10)sd.SquadDto::setTrain(ui->horizontalSlider_96->value(),10);
    if(j==11)sd.SquadDto::setTrain(ui->horizontalSlider_97->value(),11);
    if(j==12)sd.SquadDto::setTrain(ui->horizontalSlider_98->value(),12);
    if(j==13)sd.SquadDto::setTrain(ui->horizontalSlider_99->value(),13);
    if(j==14)sd.SquadDto::setTrain(ui->horizontalSlider_100->value(),14);


}

void Squad::on_pushButton_2_clicked()
{
    string name;
    int ovr,trn;
    vector<int>a(15,0);
    char pos;
    int matchAvg=0;

    QMessageBox qm;
    int i=0;
    if(ui->checkBox_18->checkState()){
        i++;
        a[0]=1;
        matchAvg+=sd.SquadDto::getPlayerOverall()[0];

    }
    if(ui->checkBox_19->checkState()){
        i++;
        a[1]=1;
        matchAvg+=sd.SquadDto::getPlayerOverall()[1];
    }
    if(ui->checkBox_20->checkState()){
        i++;
        a[2]=1;
        matchAvg+=sd.SquadDto::getPlayerOverall()[2];
    }
    if(ui->checkBox_21->checkState()){
        a[3]=1;
        i++;
        matchAvg+=sd.SquadDto::getPlayerOverall()[3];
    }
    if(ui->checkBox_22->checkState()){
        i++;
        a[4]=1;
        matchAvg+=sd.SquadDto::getPlayerOverall()[4];
    }
    if(ui->checkBox_23->checkState()){
        a[5]=1;
        i++;
        matchAvg+=sd.SquadDto::getPlayerOverall()[5];
    }
    if(ui->checkBox_24->checkState()){
        a[6]=1;
        i++;
        matchAvg+=sd.SquadDto::getPlayerOverall()[6];
    }
    if(ui->checkBox_25->checkState()){
        a[7]=1;
        i++;
        matchAvg+=sd.SquadDto::getPlayerOverall()[7];
    }
    if(ui->checkBox_26->checkState()){
        a[8]=1;
        i++;
        matchAvg+=sd.SquadDto::getPlayerOverall()[8];
    }
    if(ui->checkBox_27->checkState()){
        a[9]=1;
        i++;
        matchAvg+=sd.SquadDto::getPlayerOverall()[9];
    }
    if(ui->checkBox_28->checkState()){
        a[10]=1;
        i++;
        matchAvg+=sd.SquadDto::getPlayerOverall()[10];
    }
    if(ui->checkBox_29->checkState()){
        a[11]=1;
        i++;
        matchAvg+=sd.SquadDto::getPlayerOverall()[11];
    }
    if(ui->checkBox_30->checkState()){
        a[12]=1;
        i++;
        matchAvg+=sd.SquadDto::getPlayerOverall()[12];
    }
    if(ui->checkBox_31->checkState()){
        a[13]=1;
        i++;
        matchAvg+=sd.SquadDto::getPlayerOverall()[13];
    }
    if(ui->checkBox_32->checkState()){
        a[14]=1;
        i++;
        matchAvg+=sd.SquadDto::getPlayerOverall()[14];
    }

    if(sd.getCurrentMoney()<0){
        qm.setText("INSUFFICIENT FUND");
        qm.exec();
    }
    else if(i!=11){
        qm.setText("SELECT PROPER SQUAD");
        qm.exec();
    }
    else{
        matchAvg=matchAvg/11;
        if(matchAvg%2)
            matchAvg+=1;

        sd.setMatchAvgOverall(matchAvg);
        int i,j;
        for(i=0;i<15;i++){
            if(a[i]==0){
                for(j=i+1;j<15;j++){
                    if(a[j]==1){

                        name=sd.getPlayerName()[i];
                        ovr=sd.SquadDto::getPlayerOverall()[i];
                        pos=sd.getPosition()[i];
                        trn=sd.getTrain()[i];

                        sd.setPlayer_name(sd.getPlayerName()[j],i);
                        sd.setPlayerOverall(sd.SquadDto::getPlayerOverall()[j],i);
                        sd.setPosition(sd.getPosition()[j],i);
                        sd.setTrain(sd.getTrain()[j],i);

                        sd.setPlayer_name(name,j);
                        sd.setPlayerOverall(ovr,j);
                        sd.setPosition(pos,j);
                        sd.setTrain(trn,j);
                        a[i]=1;
                        a[j]=0;
                        break;
                    }
                }
            }
        }
        this->hide();
    }
}
