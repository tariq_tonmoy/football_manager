#ifndef SQUAD_H
#define SQUAD_H

#include <QDialog>
#include<DTO/squaddto.h>
#include<DATA_SERVICE/dquaddataservice.h>
#include<QMessageBox>
#include<QString>
#include<QMessageBox>
#include<vector>
#include"edit.h"

namespace Ui {
    class Squad;
}

class Squad : public QDialog {
    Q_OBJECT
public:
    Squad(QWidget *parent = 0);
    void setSquadDto(SquadDto sd){

        this->sd=sd;

    }

    SquadDto getSquadDto(){
        return this->sd;
    }
    void createSquad();
    ~Squad();

protected:
    void changeEvent(QEvent *e);

private:
    vector<int>v;
    SquadDataService sds;
    SquadDto sd;
    bool flag;
    Ui::Squad *ui;
    void setSlideBar(int num,int value);
    void setValue(int j);

private slots:
    void on_pushButton_2_clicked();
    void on_horizontalSlider_100_valueChanged(int value);
    void on_horizontalSlider_86_valueChanged(int value);
    void on_horizontalSlider_87_valueChanged(int value);
    void on_horizontalSlider_88_valueChanged(int value);
    void on_horizontalSlider_89_valueChanged(int value);
    void on_horizontalSlider_90_valueChanged(int value);
    void on_horizontalSlider_91_valueChanged(int value);
    void on_horizontalSlider_92_valueChanged(int value);
    void on_horizontalSlider_93_valueChanged(int value);
    void on_horizontalSlider_94_valueChanged(int value);
    void on_horizontalSlider_95_valueChanged(int value);
    void on_horizontalSlider_96_valueChanged(int value);
    void on_horizontalSlider_97_valueChanged(int value);
    void on_horizontalSlider_98_valueChanged(int value);
    void on_horizontalSlider_99_valueChanged(int value);


};

#endif // SQUAD_H
