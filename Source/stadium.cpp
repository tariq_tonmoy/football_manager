#include "stadium.h"
#include "ui_stadium.h"

Stadium::Stadium(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Stadium)
{
    ui->setupUi(this);

    QString qstr;
    qstr=QString::number(ui->verticalSlider->value());
    ui->label_6->setText(qstr+"%");

    ui->spinBox_2->setReadOnly(true);

    ui->lineEdit->setReadOnly(true);
    ui->lineEdit_5->setReadOnly(true);
    ui->lineEdit_2->setReadOnly(true);
    ui->lineEdit_3->setReadOnly(true);
    ui->lineEdit_4->setReadOnly(true);

    flag=0;

}

Stadium::~Stadium()
{
    delete ui;
}

void Stadium::changeEvent(QEvent *e)
{
    QDialog::changeEvent(e);
    switch (e->type()) {
    case QEvent::LanguageChange:
        ui->retranslateUi(this);
        break;
    default:
        break;
    }
}


void Stadium::createStadium(){

    QSlider *vs;

    ui->lineEdit->setReadOnly(true);
    ui->lineEdit_2->setReadOnly(true);
    ui->lineEdit->setText(QString::number(sd.getCurrentMoney()));
    ui->lineEdit_2->setText(QString::number(sd.getCurrentExpense()));
    ui->lineEdit_3->setText(QString::number(sd.getStadium()[sd.getTeamIndex()]));
    ui->lineEdit_4->setText(QString::number(ppd.getTicketPrice()));
    ui->lineEdit_5->setText(QString::number(ppd.getPopularity()));





    if(ppd.getTicket()==70)
        ui->verticalSlider->setValue(0);
    else if(ppd.getTicket()==75)
        ui->verticalSlider->setValue(-1);
    else if(ppd.getTicket()==80)
        ui->verticalSlider->setValue(-2);
    else if(ppd.getTicket()==85)
        ui->verticalSlider->setValue(-3);
    else if(ppd.getTicket()==45)
        ui->verticalSlider->setValue(1);
    else if(ppd.getTicket()==50)
        ui->verticalSlider->setValue(2);
    else if(ppd.getTicket()==55)
        ui->verticalSlider->setValue(3);
    a=ui->verticalSlider->value();

    this->initStd=sd.getStadiumUpgrade();
    vs=ui->verticalSlider_2;
    vs->setMinimum(sd.getStadiumUpgrade());
    vs->setMaximum(5);
    vs->setValue(sd.getStadiumUpgrade());
    b=sd.getStadiumUpgrade();

    flag=1;

}

void Stadium::on_verticalSlider_2_valueChanged(int value)
{
    ui->spinBox_2->setValue(ui->verticalSlider_2->value()*10);
    if(flag==1){

        sd.setStadiumUpgrade(value);
        int i=(sd.getTeamOverall()[sd.getTeamIndex()])*125000;

        if(value>b){
            sd.setCurrentMoney(-i);
            sd.setCurrentExpense(i);
        }

        else if(value<b){
            sd.setCurrentMoney(+i);
            sd.setCurrentExpense(-i);


        }

        b=value;

        ui->lineEdit->setText(QString::number(sd.getCurrentMoney())+"$");
        ui->lineEdit_2->setText(QString::number(sd.getCurrentExpense())+"$");
        ui->lineEdit_3->setText(QString::number(sd.getStadium()[sd.getTeamIndex()]+sd.getStadium()[sd.getTeamIndex()]*value/10));


    }
}


void Stadium::on_verticalSlider_sliderMoved(int position)
{


}

void Stadium::on_verticalSlider_valueChanged(int value)
{
    int num;
    if(ui->verticalSlider->value()==0){
        ui->label_6->setText("0%");
        num=70;
    }
    else if(ui->verticalSlider->value()==1){
        ui->label_6->setText("+10%");
        num=45;
    }

    else if(ui->verticalSlider->value()==2){
        ui->label_6->setText("+15%");
        num=50;
    }

    else if(ui->verticalSlider->value()==3){
        ui->label_6->setText("+20%");
        num=55;
    }

    else if(ui->verticalSlider->value()==-1){
        ui->label_6->setText("-10%");
        num=75;
    }

    else if(ui->verticalSlider->value()==-2){
        ui->label_6->setText("-15%");
        num=80;
    }

    else if(ui->verticalSlider->value()==-3){
        ui->label_6->setText("-20%");
        num=85;
    }

    setTicketSlideBar(num);

}



void Stadium::setTicketSlideBar(int num){
    if(!ppd.getMatch())
        this->ppd.setPopularityComponents(ppd.getInterview(),num,sd.getTeamOverall()[sd.getTeamIndex()]);
    else this->ppd.setPopularityComponents(ppd.getInterview(),num,ppd.getMatch(),sd.getTeamOverall()[sd.getTeamIndex()]);

    ui->lineEdit_4->setText(QString::number(ppd.getTicketPrice()));
    ui->lineEdit_5->setText(QString::number(ppd.getPopularity()));





}
void Stadium::setStadiumSlideBar(int num, int value){

}

void Stadium::on_pushButton_clicked()
{
    QMessageBox qm;
    qm.setText("INSUFFICIENT FUND");
    if(sd.getCurrentMoney()<0)
        qm.exec();
    else {
        if(ui->verticalSlider_2->value()>this->initStd){
            int i=(sd.getTeamOverall()[sd.getTeamIndex()]*125000)*(ui->verticalSlider_2->value()-this->initStd);
            sd.setCurrentExpense(-i);
        }
        this->hide();
    }
}

void Stadium::on_verticalSlider_2_actionTriggered(int action)
{

}
