#ifndef STADIUM_H
#define STADIUM_H
#include<DTO/squaddto.h>
#include<DTO/popularitydto.h>
#include <QDialog>
#include<QString>
#include<QMessageBox>
#include<QString>
#include<QMessageBox>
#include<string>
#include<QSlider>

namespace Ui {
    class Stadium;
}

class Stadium : public QDialog {
    Q_OBJECT
public:
    Stadium(QWidget *parent = 0);
    void setSquad(SquadDto sd){
        this->sd=sd;
    }

    void setPopularity(PopularityDto ppd){
        this->ppd=ppd;
    }

    SquadDto getSquad(){
        return this->sd;
    }

    PopularityDto getPopularity(){
        return this->ppd;
    }

    void createStadium();
    void setTicketSlideBar(int num);
    void setStadiumSlideBar(int num,int value);
    ~Stadium();

protected:
    void changeEvent(QEvent *e);

private:
    int flag;

    int a,b;
    Ui::Stadium *ui;
    SquadDto sd;
    PopularityDto ppd;
    //int a[2];

    int initStd;

private slots:
    void on_verticalSlider_2_actionTriggered(int action);
    void on_pushButton_clicked();
    void on_verticalSlider_valueChanged(int value);
    void on_verticalSlider_sliderMoved(int position);
    void on_verticalSlider_2_valueChanged(int value);
};

#endif // STADIUM_H
