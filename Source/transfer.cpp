#include "transfer.h"
#include "ui_transfer.h"

Transfer::Transfer(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Transfer)
{
    ui->setupUi(this);
    ui->lineEdit->setReadOnly(true);
    ui->lineEdit_2->setReadOnly(true);
    ui->checkBox->setCheckable(false);
    ui->checkBox_12->setCheckable(false);
    int index=ui->tabWidget->currentIndex();
    if(index==0){
        ui->pushButton->setEnabled(true);
        ui->pushButton_2->setEnabled(false);
    }
    else if(index==1){
        ui->pushButton->setEnabled(false);
        ui->pushButton_2->setEnabled(true);
    }

    for(int i=0;i<10;i++){
        brr[i]=0;
        srr[i]=0;
    }

    sell=0;
    buy=0;
}

Transfer::~Transfer()
{
    delete ui;
}

void Transfer::changeEvent(QEvent *e)
{
    QDialog::changeEvent(e);
    switch (e->type()) {
    case QEvent::LanguageChange:
        ui->retranslateUi(this);
        break;
    default:
        break;
    }
}

void Transfer::createTransfer(){

    this->a=this->trd.getA();
    this->b=this->trd.getB();
    this->c=this->sd.getRand();

    ui->lineEdit->setText(QString::number(sd.getCurrentMoney())+"$");
    ui->lineEdit_2->setText(QString::number(sd.getCurrentExpense())+"$");
    ui->tabWidget->setCurrentIndex(0);
    if(sd.getPlayerCount()>=15){
        ui->tab->setEnabled(false);
        ui->pushButton->setEnabled(false);
    }
    else{

        ui->tab->setEnabled(true);
        ui->pushButton->setEnabled(true);

        ui->label_pn1->setText(QString::fromStdString(trd.getOtherPlayerName()[a[0]][b[0]]));
        ui->label_pn2->setText(QString::fromStdString(trd.getOtherPlayerName()[a[1]][b[1]]));
        ui->label_pn3->setText(QString::fromStdString(trd.getOtherPlayerName()[a[2]][b[2]]));
        ui->label_pn4->setText(QString::fromStdString(trd.getOtherPlayerName()[a[3]][b[3]]));
        ui->label_pn5->setText(QString::fromStdString(trd.getOtherPlayerName()[a[4]][b[4]]));
        ui->label_pn6->setText(QString::fromStdString(trd.getOtherPlayerName()[a[5]][b[5]]));
        ui->label_pn7->setText(QString::fromStdString(trd.getOtherPlayerName()[a[6]][b[6]]));
        ui->label_pn8->setText(QString::fromStdString(trd.getOtherPlayerName()[a[7]][b[7]]));
        ui->label_pn9->setText(QString::fromStdString(trd.getOtherPlayerName()[a[8]][b[8]]));
        ui->label_pn10->setText(QString::fromStdString(trd.getOtherPlayerName()[a[9]][b[9]]));

        ui->label_o1->setText(QString::number(trd.getOtherPlayerOverall()[a[0]][b[0]]));
        ui->label_o2->setText(QString::number(trd.getOtherPlayerOverall()[a[1]][b[1]]));
        ui->label_o3->setText(QString::number(trd.getOtherPlayerOverall()[a[2]][b[2]]));
        ui->label_o4->setText(QString::number(trd.getOtherPlayerOverall()[a[3]][b[3]]));
        ui->label_o5->setText(QString::number(trd.getOtherPlayerOverall()[a[4]][b[4]]));
        ui->label_o6->setText(QString::number(trd.getOtherPlayerOverall()[a[5]][b[5]]));
        ui->label_o7->setText(QString::number(trd.getOtherPlayerOverall()[a[6]][b[6]]));
        ui->label_o8->setText(QString::number(trd.getOtherPlayerOverall()[a[7]][b[7]]));
        ui->label_o9->setText(QString::number(trd.getOtherPlayerOverall()[a[8]][b[8]]));
        ui->label_o10->setText(QString::number(trd.getOtherPlayerOverall()[a[9]][b[9]]));

        ui->label_p1->setText(QString::fromLocal8Bit(&trd.getOtherPosition()[a[0]][b[0]],1));
        ui->label_p2->setText(QString::fromLocal8Bit(&trd.getOtherPosition()[a[1]][b[1]],1));
        ui->label_p3->setText(QString::fromLocal8Bit(&trd.getOtherPosition()[a[2]][b[2]],1));
        ui->label_p4->setText(QString::fromLocal8Bit(&trd.getOtherPosition()[a[3]][b[3]],1));
        ui->label_p5->setText(QString::fromLocal8Bit(&trd.getOtherPosition()[a[4]][b[4]],1));
        ui->label_p6->setText(QString::fromLocal8Bit(&trd.getOtherPosition()[a[5]][b[5]],1));
        ui->label_p7->setText(QString::fromLocal8Bit(&trd.getOtherPosition()[a[6]][b[6]],1));
        ui->label_p8->setText(QString::fromLocal8Bit(&trd.getOtherPosition()[a[7]][b[7]],1));
        ui->label_p9->setText(QString::fromLocal8Bit(&trd.getOtherPosition()[a[8]][b[8]],1));
        ui->label_p10->setText(QString::fromLocal8Bit(&trd.getOtherPosition()[a[9]][b[9]],1));

        ui->label_t1->setText(QString::fromStdString(trd.getOtherTeamName()[a[0]]));
        ui->label_t2->setText(QString::fromStdString(trd.getOtherTeamName()[a[1]]));
        ui->label_t3->setText(QString::fromStdString(trd.getOtherTeamName()[a[2]]));
        ui->label_t4->setText(QString::fromStdString(trd.getOtherTeamName()[a[3]]));
        ui->label_t5->setText(QString::fromStdString(trd.getOtherTeamName()[a[4]]));
        ui->label_t6->setText(QString::fromStdString(trd.getOtherTeamName()[a[5]]));
        ui->label_t7->setText(QString::fromStdString(trd.getOtherTeamName()[a[6]]));
        ui->label_t8->setText(QString::fromStdString(trd.getOtherTeamName()[a[7]]));
        ui->label_t9->setText(QString::fromStdString(trd.getOtherTeamName()[a[8]]));
        ui->label_t10->setText(QString::fromStdString(trd.getOtherTeamName()[a[9]]));

        ui->label_pr1->setText(QString::number(trd.getPlayerPrice()[0])+"$");
        ui->label_pr2->setText(QString::number(trd.getPlayerPrice()[1])+"$");
        ui->label_pr3->setText(QString::number(trd.getPlayerPrice()[2])+"$");
        ui->label_pr4->setText(QString::number(trd.getPlayerPrice()[3])+"$");
        ui->label_pr5->setText(QString::number(trd.getPlayerPrice()[4])+"$");
        ui->label_pr6->setText(QString::number(trd.getPlayerPrice()[5])+"$");
        ui->label_pr7->setText(QString::number(trd.getPlayerPrice()[6])+"$");
        ui->label_pr8->setText(QString::number(trd.getPlayerPrice()[7])+"$");
        ui->label_pr9->setText(QString::number(trd.getPlayerPrice()[8])+"$");
        ui->label_pr10->setText(QString::number(trd.getPlayerPrice()[9])+"$");

   }

    if(sd.getPlayerCount()<12){

        ui->pushButton->setEnabled(false);
        ui->tab_2->setEnabled(false);
    }
    else{
        ui->pushButton->setEnabled(true);
        ui->tab_2->setEnabled(true);

        ui->label_pn11->setText(QString::fromStdString(sd.getPlayerName()[c[0]]));
        ui->label_pn12->setText(QString::fromStdString(sd.getPlayerName()[c[1]]));
        ui->label_pn13->setText(QString::fromStdString(sd.getPlayerName()[c[2]]));
        ui->label_pn14->setText(QString::fromStdString(sd.getPlayerName()[c[3]]));
        ui->label_pn15->setText(QString::fromStdString(sd.getPlayerName()[c[4]]));
        ui->label_pn16->setText(QString::fromStdString(sd.getPlayerName()[c[5]]));
        ui->label_pn17->setText(QString::fromStdString(sd.getPlayerName()[c[6]]));
        ui->label_pn18->setText(QString::fromStdString(sd.getPlayerName()[c[7]]));
        ui->label_pn19->setText(QString::fromStdString(sd.getPlayerName()[c[8]]));
        ui->label_pn20->setText(QString::fromStdString(sd.getPlayerName()[c[9]]));

        ui->label_o11->setText(QString::number(sd.getPlayerOverall()[c[0]]));
        ui->label_o12->setText(QString::number(sd.getPlayerOverall()[c[1]]));
        ui->label_o13->setText(QString::number(sd.getPlayerOverall()[c[2]]));
        ui->label_o14->setText(QString::number(sd.getPlayerOverall()[c[3]]));
        ui->label_o15->setText(QString::number(sd.getPlayerOverall()[c[4]]));
        ui->label_o16->setText(QString::number(sd.getPlayerOverall()[c[5]]));
        ui->label_o17->setText(QString::number(sd.getPlayerOverall()[c[6]]));
        ui->label_o18->setText(QString::number(sd.getPlayerOverall()[c[7]]));
        ui->label_o19->setText(QString::number(sd.getPlayerOverall()[c[8]]));
        ui->label_o20->setText(QString::number(sd.getPlayerOverall()[c[9]]));

        ui->label_p11->setText(QString::fromLocal8Bit(&sd.getPosition()[c[0]],1));
        ui->label_p12->setText(QString::fromLocal8Bit(&sd.getPosition()[c[1]],1));
        ui->label_p13->setText(QString::fromLocal8Bit(&sd.getPosition()[c[2]],1));
        ui->label_p14->setText(QString::fromLocal8Bit(&sd.getPosition()[c[3]],1));
        ui->label_p15->setText(QString::fromLocal8Bit(&sd.getPosition()[c[4]],1));
        ui->label_p16->setText(QString::fromLocal8Bit(&sd.getPosition()[c[5]],1));
        ui->label_p17->setText(QString::fromLocal8Bit(&sd.getPosition()[c[6]],1));
        ui->label_p18->setText(QString::fromLocal8Bit(&sd.getPosition()[c[7]],1));
        ui->label_p19->setText(QString::fromLocal8Bit(&sd.getPosition()[c[8]],1));
        ui->label_p20->setText(QString::fromLocal8Bit(&sd.getPosition()[c[9]],1));

        ui->label_pr11->setText(QString::number(sd.getPrice()[0])+"$");
        ui->label_pr12->setText(QString::number(sd.getPrice()[1])+"$");
        ui->label_pr13->setText(QString::number(sd.getPrice()[2])+"$");
        ui->label_pr14->setText(QString::number(sd.getPrice()[3])+"$");
        ui->label_pr15->setText(QString::number(sd.getPrice()[4])+"$");
        ui->label_pr16->setText(QString::number(sd.getPrice()[5])+"$");
        ui->label_pr17->setText(QString::number(sd.getPrice()[6])+"$");
        ui->label_pr18->setText(QString::number(sd.getPrice()[7])+"$");
        ui->label_pr19->setText(QString::number(sd.getPrice()[8])+"$");
        ui->label_pr20->setText(QString::number(sd.getPrice()[9])+"$");

        ui->checkBox_s1->setChecked(false);
        ui->checkBox_s2->setChecked(false);
        ui->checkBox_s3->setChecked(false);
        ui->checkBox_s4->setChecked(false);
        ui->checkBox_s5->setChecked(false);
        ui->checkBox_s6->setChecked(false);
        ui->checkBox_s7->setChecked(false);
        ui->checkBox_s8->setChecked(false);
        ui->checkBox_s9->setChecked(false);
        ui->checkBox_s10->setChecked(false);

        ui->checkBox_b1->setChecked(false);
        ui->checkBox_b2->setChecked(false);
        ui->checkBox_b3->setChecked(false);
        ui->checkBox_b4->setChecked(false);
        ui->checkBox_b5->setChecked(false);
        ui->checkBox_b6->setChecked(false);
        ui->checkBox_b7->setChecked(false);
        ui->checkBox_b8->setChecked(false);
        ui->checkBox_b9->setChecked(false);
        ui->checkBox_b10->setChecked(false);


    }

}





void Transfer::on_tabWidget_currentChanged(int index)
{
    if(index==0){
        if(sd.getPlayerCount()<15)
            ui->pushButton->setEnabled(true);
            ui->pushButton_2->setEnabled(false);
    }
    else if(index==1){
        ui->pushButton->setEnabled(false);
        if(sd.getPlayerCount()>12)
            ui->pushButton_2->setEnabled(true);

    }
}

void Transfer::on_checkBox_b1_clicked(bool checked)
{

    this->setCheckBox(checked,0);

}

void Transfer::on_checkBox_b2_clicked(bool checked)
{
    this->setCheckBox(checked,1);

}

void Transfer::on_checkBox_b4_clicked(bool checked)
{

    this->setCheckBox(checked,3);

}

void Transfer::on_checkBox_b3_clicked(bool checked)
{

    this->setCheckBox(checked,2);

}


void Transfer::on_checkBox_b5_clicked(bool checked)
{

    this->setCheckBox(checked,4);

}

void Transfer::on_checkBox_b6_clicked(bool checked)
{

    this->setCheckBox(checked,5);

}

void Transfer::on_checkBox_b7_clicked(bool checked)
{

    this->setCheckBox(checked,6);

}

void Transfer::on_checkBox_b8_clicked(bool checked)
{

    this->setCheckBox(checked,7);

}

void Transfer::on_checkBox_b9_clicked(bool checked)
{

    this->setCheckBox(checked,8);
}

void Transfer::on_checkBox_b10_clicked(bool checked)
{

    this->setCheckBox(checked,9);

}

void Transfer::on_checkBox_s1_clicked(bool checked)
{

    this->setCheckBox(checked,10);

}

void Transfer::on_checkBox_s2_clicked(bool checked)
{


    this->setCheckBox(checked,11);

}

void Transfer::on_checkBox_s3_clicked(bool checked)
{

    this->setCheckBox(checked,12);

}

void Transfer::on_checkBox_s4_clicked(bool checked)
{

    this->setCheckBox(checked,13);
}

void Transfer::on_checkBox_s5_clicked(bool checked)
{

    this->setCheckBox(checked,14);

}

void Transfer::on_checkBox_s6_clicked(bool checked)
{

    this->setCheckBox(checked,15);

}

void Transfer::on_checkBox_s7_clicked(bool checked)
{

    this->setCheckBox(checked,16);

}

void Transfer::on_checkBox_s8_clicked(bool checked)
{


    this->setCheckBox(checked,17);
}


void Transfer::on_checkBox_s9_clicked(bool checked)
{

    this->setCheckBox(checked,18);

}

void Transfer::on_checkBox_s10_clicked(bool checked)
{

    this->setCheckBox(checked,19);

}





void Transfer::setCheckBox(bool checked, int i){
    if(i<10){
        if(checked==true){
            ui->pushButton_4->setEnabled(false);
            sd.setCurrentMoney(-trd.getPlayerPrice()[i]);
            sd.setCurrentExpense(trd.getPlayerPrice()[i]);
            sd.setPlayerCount(sd.getPlayerCount()+1);
            brr[i]=1;
        }
        else if(checked==false){

            sd.setCurrentMoney(trd.getPlayerPrice()[i]);
            sd.setCurrentExpense(-trd.getPlayerPrice()[i]);
            sd.setPlayerCount(sd.getPlayerCount()-1);
            brr[i]=0;
        }

        ui->lineEdit->setText(QString::number(sd.getCurrentMoney())+"$");
        ui->lineEdit_2->setText(QString::number(sd.getCurrentExpense())+"$");

        if(sd.getPlayerCount()<=15){
            ui->pushButton->setEnabled(true);

        }
        else if(sd.getPlayerCount()>15||sd.getCurrentMoney()<0){
            ui->pushButton->setEnabled(false);
        }
    }
    else if(i>=10&&i<=19){
        if(checked==true){
            ui->pushButton_4->setEnabled(false);
            sd.setCurrentMoney(sd.getPrice()[i-10]);
            sd.setPlayerCount(sd.getPlayerCount()-1);
            srr[i-10]=1;
        }
        else if(checked==false){
            sd.setCurrentMoney(-sd.getPrice()[i-10]);
            sd.setPlayerCount(sd.getPlayerCount()+1);
            srr[i-10]=0;
        }
        ui->lineEdit->setText(QString::number(sd.getCurrentMoney())+"$");

        if(sd.getPlayerCount()>=12){
            ui->pushButton_2->setEnabled(true);
        }
        else if(sd.getPlayerCount()<12){
            ui->pushButton_2->setEnabled(false);
        }
    }
    bool flag=true;

    for(int n=0;n<10;n++){
        if(brr[n]==1||srr[n]==1){
            flag=false;
            break;
        }
    }
    if(flag==true)
        ui->pushButton_4->setEnabled(true);

}

void Transfer::on_pushButton_clicked()
{

    int flag1;
    flag1=0;
    for(int i=0;i<10;i++){
        if(brr[i]==1){
            flag1=1;
            break;
        }
    }

    if(flag1==1){
        vector< vector<string> >name;
        vector< vector<int> >overall;
        vector< vector<char> >pos;

        name=trd.getOtherPlayerName();
        overall=trd.getOtherPlayerOverall();
        pos=trd.getOtherPosition();

        vector< string >::iterator p;
        vector< int >::iterator p1;
        vector< char >::iterator p2;


        for(int i=0;i<10;i++){
            if(brr[i]==1){

                for(int j=0;j<15;j++){
                    if(sd.getPlayerName()[j]=="N/A"){
                        sd.setPlayer_name(name[a[i]][b[i]],j);
                        sd.setPlayerOverall(overall[a[i]][b[i]],j);
                        sd.setPosition(pos[a[i]][b[i]],j);
                  //      sd.setPlayerCount(sd.getPlayerCount()+1);
                        break;
                    }
                }

                p=name[a[i]].begin();
                name[a[i]].erase(p+b[i],p+b[i]+1);
                p1=overall[a[i]].begin();
                overall[a[i]].erase(p1+b[i],p1+b[i]+1);
                p2=pos[a[i]].begin();
                pos[a[i]].erase(p2+b[i],p2+b[i]+1);
                trd.setOtherPlayerCounnt(trd.getOtherPlayerCount()[a[i]]-1,a[i]);
            }
        }
        trd.setOtherTeams(name,overall,pos);
        bool flag=true;
        for(int i=0;i<10;i++){
            brr[i]=0;
            if(srr[i]==1)
                flag=false;
        }
        if(flag==true)
            ui->pushButton_4->setEnabled(true);
        this->trd.resetTransfer();
        this->createTransfer();

    }


}


void Transfer::on_pushButton_2_clicked()
{
    bool flag=false;
    int min;
    int j;
    string name;
    int overall;
    char position;
    int train;
    for(int i=0;i<10;i++){
        if(srr[i]==1){
            flag=true;
            for(j=0;j<trd.getTeamCount();j++){
                if(j==0)min=j;
                if(trd.getOtherPlayerCount()[j]<trd.getOtherPlayerCount()[min])
                    min=j;
            }
            trd.addNewPlayer(sd.getPlayerName()[c[i]],min);
            trd.addNewOverall(sd.getPlayerOverall()[c[i]],min);
            trd.addNewPosition(sd.getPosition()[c[i]],min);
            trd.setOtherPlayerCounnt(trd.getOtherPlayerCount()[min]+1,min);


            int n=sd.SquadDto::getPlayerOverall()[c[i]]*2000*(sd.getTrain()[c[i]])*10/100;
            sd.setCurrentMoney(n);
            sd.setCurrentExpense(-n);
            sd.setPlayer_name("N/A",c[i]);
            sd.setPlayerOverall(0,c[i]);
            sd.setPosition('-',c[i]);
            sd.setTrain(0,c[i]);



        }
    }
    if(flag==false)
        return ;

    for(int i=0;i<15;i++){
        if(sd.getPlayerName()[i]=="N/A"){
            for(int j=i+1;j<15;j++){
                if(sd.getPlayerName()[j]!="N/A"){


                    name=sd.getPlayerName()[i];
                    overall=sd.getPlayerOverall()[i];
                    position=sd.getPosition()[i];
                    train=sd.getTrain()[i];

                    sd.setPlayer_name(sd.getPlayerName()[j],i);
                    sd.setPlayerOverall(sd.getPlayerOverall()[j],i);
                    sd.setPosition(sd.getPosition()[j],i);
                    sd.setTrain(sd.getTrain()[j],i);

                    sd.setPlayer_name(name,j);
                    sd.setPlayerOverall(overall,j);
                    sd.setPosition(position,j);
                    sd.setTrain(train,j);
                    break;

                }
            }
        }

        bool flag=true;
        for(int i=0;i<10;i++){
            srr[i]=0;
            if(brr[i]==1)
                flag=false;
        }
        if(flag==true)
            ui->pushButton_4->setEnabled(true);
        sd.resetRand();
        this->createTransfer();
    }
}

void Transfer::on_pushButton_4_clicked()
{
    this->hide();
}
