#ifndef TRANSFER_H
#define TRANSFER_H

#include <QDialog>
#include<DTO/transferdto.h>
#include<DTO/squaddto.h>
#include<QString>
#include<QMessageBox>

namespace Ui {
    class Transfer;
}

class Transfer : public QDialog {
    Q_OBJECT
public:
    Transfer(QWidget *parent = 0);
    void setSquad(SquadDto sd){
        this->sd=sd;
    }

    SquadDto getSquad(){
        return this->sd;
    }

    void setTransfer(TransferDto trd){

        this->trd=trd;

    }

    TransferDto getTransfer(){
        return this->trd;
    }


    void createTransfer();
    void setCheckBox(bool checked,int i);

    ~Transfer();

protected:
    void changeEvent(QEvent *e);

private:
    Ui::Transfer *ui;
    SquadDto sd;
    TransferDto trd;
    int brr[10],srr[10],sell,buy,*a,*b,*c;

private slots:
    void on_pushButton_4_clicked();
    void on_pushButton_2_clicked();
    void on_pushButton_clicked();
    void on_checkBox_s10_clicked(bool checked);
    void on_checkBox_s9_clicked(bool checked);
    void on_checkBox_s8_clicked(bool checked);
    void on_checkBox_s7_clicked(bool checked);
    void on_checkBox_s6_clicked(bool checked);
    void on_checkBox_s5_clicked(bool checked);
    void on_checkBox_s4_clicked(bool checked);
    void on_checkBox_s3_clicked(bool checked);
    void on_checkBox_s2_clicked(bool checked);
    void on_checkBox_s1_clicked(bool checked);
    void on_checkBox_b10_clicked(bool checked);
    void on_checkBox_b9_clicked(bool checked);
    void on_checkBox_b8_clicked(bool checked);
    void on_checkBox_b7_clicked(bool checked);
    void on_checkBox_b6_clicked(bool checked);
    void on_checkBox_b5_clicked(bool checked);
    void on_checkBox_b3_clicked(bool checked);
    void on_checkBox_b4_clicked(bool checked);
    void on_checkBox_b2_clicked(bool checked);
    void on_checkBox_b1_clicked(bool checked);
    void on_tabWidget_currentChanged(int index);
};

#endif // TRANSFER_H
