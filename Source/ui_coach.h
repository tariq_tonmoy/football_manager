/********************************************************************************
** Form generated from reading UI file 'coach.ui'
**
** Created: Thu Apr 19 02:12:48 2012
**      by: Qt User Interface Compiler version 4.6.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_COACH_H
#define UI_COACH_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QDialog>
#include <QtGui/QFormLayout>
#include <QtGui/QGridLayout>
#include <QtGui/QGroupBox>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLCDNumber>
#include <QtGui/QLabel>
#include <QtGui/QLineEdit>
#include <QtGui/QPushButton>
#include <QtGui/QSlider>
#include <QtGui/QSpacerItem>
#include <QtGui/QSplitter>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_Coach
{
public:
    QVBoxLayout *verticalLayout_2;
    QLabel *label_8;
    QSplitter *splitter;
    QGroupBox *groupBox_3;
    QVBoxLayout *verticalLayout;
    QFormLayout *formLayout_2;
    QSpacerItem *horizontalSpacer_2;
    QLabel *label;
    QLineEdit *lineEdit;
    QFormLayout *formLayout_3;
    QLineEdit *lineEdit_2;
    QLabel *label_7;
    QSpacerItem *horizontalSpacer_3;
    QWidget *layoutWidget;
    QGridLayout *gridLayout_9;
    QGroupBox *groupBox_2;
    QGridLayout *gridLayout_8;
    QGridLayout *gridLayout_7;
    QLCDNumber *lcdNumber_7;
    QSlider *SverticalSlider;
    QLabel *label_6;
    QGroupBox *groupBox;
    QGridLayout *gridLayout_6;
    QHBoxLayout *horizontalLayout;
    QGridLayout *gridLayout;
    QLabel *label_4;
    QLCDNumber *lcdNumber_5;
    QSlider *FverticalSlider;
    QGridLayout *gridLayout_2;
    QLabel *label_2;
    QLCDNumber *lcdNumber_2;
    QSlider *MverticalSlider;
    QGridLayout *gridLayout_5;
    QLabel *label_5;
    QLCDNumber *lcdNumber_6;
    QSlider *DverticalSlider;
    QGridLayout *gridLayout_3;
    QLabel *label_3;
    QLCDNumber *lcdNumber_3;
    QSlider *GverticalSlider;
    QGroupBox *groupBox_4;
    QGridLayout *gridLayout_10;
    QPushButton *pushButton;
    QSpacerItem *horizontalSpacer;

    void setupUi(QDialog *Coach)
    {
        if (Coach->objectName().isEmpty())
            Coach->setObjectName(QString::fromUtf8("Coach"));
        Coach->resize(508, 475);
        verticalLayout_2 = new QVBoxLayout(Coach);
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        label_8 = new QLabel(Coach);
        label_8->setObjectName(QString::fromUtf8("label_8"));
        QFont font;
        font.setFamily(QString::fromUtf8("Stencil"));
        font.setPointSize(26);
        label_8->setFont(font);
        label_8->setAlignment(Qt::AlignCenter);

        verticalLayout_2->addWidget(label_8);

        splitter = new QSplitter(Coach);
        splitter->setObjectName(QString::fromUtf8("splitter"));
        splitter->setOrientation(Qt::Vertical);
        groupBox_3 = new QGroupBox(splitter);
        groupBox_3->setObjectName(QString::fromUtf8("groupBox_3"));
        verticalLayout = new QVBoxLayout(groupBox_3);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        formLayout_2 = new QFormLayout();
        formLayout_2->setObjectName(QString::fromUtf8("formLayout_2"));
        formLayout_2->setFieldGrowthPolicy(QFormLayout::AllNonFixedFieldsGrow);
        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Maximum, QSizePolicy::Minimum);

        formLayout_2->setItem(0, QFormLayout::LabelRole, horizontalSpacer_2);

        label = new QLabel(groupBox_3);
        label->setObjectName(QString::fromUtf8("label"));
        QFont font1;
        font1.setFamily(QString::fromUtf8("Copperplate Gothic Light"));
        font1.setPointSize(12);
        label->setFont(font1);

        formLayout_2->setWidget(0, QFormLayout::FieldRole, label);

        lineEdit = new QLineEdit(groupBox_3);
        lineEdit->setObjectName(QString::fromUtf8("lineEdit"));
        QFont font2;
        font2.setFamily(QString::fromUtf8("Century Gothic"));
        lineEdit->setFont(font2);

        formLayout_2->setWidget(1, QFormLayout::FieldRole, lineEdit);


        verticalLayout->addLayout(formLayout_2);

        formLayout_3 = new QFormLayout();
        formLayout_3->setObjectName(QString::fromUtf8("formLayout_3"));
        lineEdit_2 = new QLineEdit(groupBox_3);
        lineEdit_2->setObjectName(QString::fromUtf8("lineEdit_2"));
        lineEdit_2->setFont(font2);

        formLayout_3->setWidget(1, QFormLayout::FieldRole, lineEdit_2);

        label_7 = new QLabel(groupBox_3);
        label_7->setObjectName(QString::fromUtf8("label_7"));
        label_7->setFont(font1);

        formLayout_3->setWidget(0, QFormLayout::FieldRole, label_7);

        horizontalSpacer_3 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        formLayout_3->setItem(0, QFormLayout::LabelRole, horizontalSpacer_3);


        verticalLayout->addLayout(formLayout_3);

        splitter->addWidget(groupBox_3);
        layoutWidget = new QWidget(splitter);
        layoutWidget->setObjectName(QString::fromUtf8("layoutWidget"));
        gridLayout_9 = new QGridLayout(layoutWidget);
        gridLayout_9->setObjectName(QString::fromUtf8("gridLayout_9"));
        gridLayout_9->setContentsMargins(0, 0, 0, 0);
        groupBox_2 = new QGroupBox(layoutWidget);
        groupBox_2->setObjectName(QString::fromUtf8("groupBox_2"));
        QFont font3;
        font3.setFamily(QString::fromUtf8("Copperplate Gothic Light"));
        groupBox_2->setFont(font3);
        gridLayout_8 = new QGridLayout(groupBox_2);
        gridLayout_8->setObjectName(QString::fromUtf8("gridLayout_8"));
        gridLayout_7 = new QGridLayout();
        gridLayout_7->setObjectName(QString::fromUtf8("gridLayout_7"));
        lcdNumber_7 = new QLCDNumber(groupBox_2);
        lcdNumber_7->setObjectName(QString::fromUtf8("lcdNumber_7"));
        lcdNumber_7->setSegmentStyle(QLCDNumber::Flat);

        gridLayout_7->addWidget(lcdNumber_7, 2, 0, 1, 1);

        SverticalSlider = new QSlider(groupBox_2);
        SverticalSlider->setObjectName(QString::fromUtf8("SverticalSlider"));
        SverticalSlider->setCursor(QCursor(Qt::PointingHandCursor));
        SverticalSlider->setMaximum(10);
        SverticalSlider->setOrientation(Qt::Vertical);

        gridLayout_7->addWidget(SverticalSlider, 3, 0, 1, 1);

        label_6 = new QLabel(groupBox_2);
        label_6->setObjectName(QString::fromUtf8("label_6"));

        gridLayout_7->addWidget(label_6, 1, 0, 1, 1);


        gridLayout_8->addLayout(gridLayout_7, 0, 0, 1, 1);


        gridLayout_9->addWidget(groupBox_2, 0, 0, 1, 1);

        groupBox = new QGroupBox(layoutWidget);
        groupBox->setObjectName(QString::fromUtf8("groupBox"));
        groupBox->setFont(font3);
        gridLayout_6 = new QGridLayout(groupBox);
        gridLayout_6->setObjectName(QString::fromUtf8("gridLayout_6"));
        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        gridLayout = new QGridLayout();
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        label_4 = new QLabel(groupBox);
        label_4->setObjectName(QString::fromUtf8("label_4"));

        gridLayout->addWidget(label_4, 0, 0, 1, 1);

        lcdNumber_5 = new QLCDNumber(groupBox);
        lcdNumber_5->setObjectName(QString::fromUtf8("lcdNumber_5"));
        lcdNumber_5->setSegmentStyle(QLCDNumber::Flat);

        gridLayout->addWidget(lcdNumber_5, 1, 0, 1, 1);

        FverticalSlider = new QSlider(groupBox);
        FverticalSlider->setObjectName(QString::fromUtf8("FverticalSlider"));
        FverticalSlider->setCursor(QCursor(Qt::PointingHandCursor));
        FverticalSlider->setMaximum(10);
        FverticalSlider->setOrientation(Qt::Vertical);

        gridLayout->addWidget(FverticalSlider, 2, 0, 1, 1);


        horizontalLayout->addLayout(gridLayout);

        gridLayout_2 = new QGridLayout();
        gridLayout_2->setObjectName(QString::fromUtf8("gridLayout_2"));
        label_2 = new QLabel(groupBox);
        label_2->setObjectName(QString::fromUtf8("label_2"));

        gridLayout_2->addWidget(label_2, 0, 0, 1, 1);

        lcdNumber_2 = new QLCDNumber(groupBox);
        lcdNumber_2->setObjectName(QString::fromUtf8("lcdNumber_2"));
        lcdNumber_2->setSegmentStyle(QLCDNumber::Flat);

        gridLayout_2->addWidget(lcdNumber_2, 1, 0, 1, 1);

        MverticalSlider = new QSlider(groupBox);
        MverticalSlider->setObjectName(QString::fromUtf8("MverticalSlider"));
        MverticalSlider->setCursor(QCursor(Qt::PointingHandCursor));
        MverticalSlider->setMaximum(10);
        MverticalSlider->setOrientation(Qt::Vertical);

        gridLayout_2->addWidget(MverticalSlider, 2, 0, 1, 1);


        horizontalLayout->addLayout(gridLayout_2);

        gridLayout_5 = new QGridLayout();
        gridLayout_5->setObjectName(QString::fromUtf8("gridLayout_5"));
        label_5 = new QLabel(groupBox);
        label_5->setObjectName(QString::fromUtf8("label_5"));

        gridLayout_5->addWidget(label_5, 0, 0, 1, 1);

        lcdNumber_6 = new QLCDNumber(groupBox);
        lcdNumber_6->setObjectName(QString::fromUtf8("lcdNumber_6"));
        lcdNumber_6->setSegmentStyle(QLCDNumber::Flat);

        gridLayout_5->addWidget(lcdNumber_6, 1, 0, 1, 1);

        DverticalSlider = new QSlider(groupBox);
        DverticalSlider->setObjectName(QString::fromUtf8("DverticalSlider"));
        DverticalSlider->setCursor(QCursor(Qt::PointingHandCursor));
        DverticalSlider->setMaximum(10);
        DverticalSlider->setOrientation(Qt::Vertical);

        gridLayout_5->addWidget(DverticalSlider, 2, 0, 1, 1);


        horizontalLayout->addLayout(gridLayout_5);

        gridLayout_3 = new QGridLayout();
        gridLayout_3->setObjectName(QString::fromUtf8("gridLayout_3"));
        label_3 = new QLabel(groupBox);
        label_3->setObjectName(QString::fromUtf8("label_3"));

        gridLayout_3->addWidget(label_3, 0, 0, 1, 1);

        lcdNumber_3 = new QLCDNumber(groupBox);
        lcdNumber_3->setObjectName(QString::fromUtf8("lcdNumber_3"));
        lcdNumber_3->setSegmentStyle(QLCDNumber::Flat);

        gridLayout_3->addWidget(lcdNumber_3, 1, 0, 1, 1);

        GverticalSlider = new QSlider(groupBox);
        GverticalSlider->setObjectName(QString::fromUtf8("GverticalSlider"));
        GverticalSlider->setCursor(QCursor(Qt::PointingHandCursor));
        GverticalSlider->setMaximum(10);
        GverticalSlider->setOrientation(Qt::Vertical);

        gridLayout_3->addWidget(GverticalSlider, 2, 0, 1, 1);


        horizontalLayout->addLayout(gridLayout_3);


        gridLayout_6->addLayout(horizontalLayout, 0, 0, 1, 1);


        gridLayout_9->addWidget(groupBox, 0, 1, 1, 1);

        splitter->addWidget(layoutWidget);

        verticalLayout_2->addWidget(splitter);

        groupBox_4 = new QGroupBox(Coach);
        groupBox_4->setObjectName(QString::fromUtf8("groupBox_4"));
        gridLayout_10 = new QGridLayout(groupBox_4);
        gridLayout_10->setObjectName(QString::fromUtf8("gridLayout_10"));
        pushButton = new QPushButton(groupBox_4);
        pushButton->setObjectName(QString::fromUtf8("pushButton"));
        QFont font4;
        font4.setFamily(QString::fromUtf8("Century Gothic"));
        font4.setBold(true);
        font4.setWeight(75);
        pushButton->setFont(font4);
        pushButton->setCursor(QCursor(Qt::PointingHandCursor));

        gridLayout_10->addWidget(pushButton, 0, 0, 1, 1);

        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_10->addItem(horizontalSpacer, 0, 1, 1, 1);


        verticalLayout_2->addWidget(groupBox_4);


        retranslateUi(Coach);
        QObject::connect(FverticalSlider, SIGNAL(valueChanged(int)), lcdNumber_5, SLOT(display(int)));
        QObject::connect(MverticalSlider, SIGNAL(valueChanged(int)), lcdNumber_2, SLOT(display(int)));
        QObject::connect(DverticalSlider, SIGNAL(valueChanged(int)), lcdNumber_6, SLOT(display(int)));
        QObject::connect(GverticalSlider, SIGNAL(valueChanged(int)), lcdNumber_3, SLOT(display(int)));
        QObject::connect(SverticalSlider, SIGNAL(valueChanged(int)), lcdNumber_7, SLOT(display(int)));

        QMetaObject::connectSlotsByName(Coach);
    } // setupUi

    void retranslateUi(QDialog *Coach)
    {
        Coach->setWindowTitle(QApplication::translate("Coach", "Dialog", 0, QApplication::UnicodeUTF8));
        label_8->setText(QApplication::translate("Coach", "STAFF UPGRADE", 0, QApplication::UnicodeUTF8));
        groupBox_3->setTitle(QString());
        label->setText(QApplication::translate("Coach", "CURRENT BALANCE", 0, QApplication::UnicodeUTF8));
        label_7->setText(QApplication::translate("Coach", "EXPENDITURE", 0, QApplication::UnicodeUTF8));
        groupBox_2->setTitle(QApplication::translate("Coach", "Scout", 0, QApplication::UnicodeUTF8));
        label_6->setText(QApplication::translate("Coach", "SCOUTING", 0, QApplication::UnicodeUTF8));
        groupBox->setTitle(QApplication::translate("Coach", "Coaching", 0, QApplication::UnicodeUTF8));
        label_4->setText(QApplication::translate("Coach", "FORWARD", 0, QApplication::UnicodeUTF8));
        label_2->setText(QApplication::translate("Coach", "MID FIELD", 0, QApplication::UnicodeUTF8));
        label_5->setText(QApplication::translate("Coach", "DEFENCE", 0, QApplication::UnicodeUTF8));
        label_3->setText(QApplication::translate("Coach", "GOAL KEEPING", 0, QApplication::UnicodeUTF8));
        groupBox_4->setTitle(QString());
        pushButton->setText(QApplication::translate("Coach", "Back", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class Coach: public Ui_Coach {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_COACH_H
