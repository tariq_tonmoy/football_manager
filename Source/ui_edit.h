/********************************************************************************
** Form generated from reading UI file 'edit.ui'
**
** Created: Thu Apr 19 05:59:42 2012
**      by: Qt User Interface Compiler version 4.6.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_EDIT_H
#define UI_EDIT_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QDialog>
#include <QtGui/QGridLayout>
#include <QtGui/QGroupBox>
#include <QtGui/QHeaderView>
#include <QtGui/QLCDNumber>
#include <QtGui/QLabel>
#include <QtGui/QLineEdit>
#include <QtGui/QPushButton>
#include <QtGui/QSpacerItem>
#include <QtGui/QSplitter>
#include <QtGui/QTabWidget>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_Edit
{
public:
    QGridLayout *gridLayout_22;
    QSplitter *splitter_4;
    QLabel *label_17;
    QSplitter *splitter_2;
    QWidget *layoutWidget;
    QGridLayout *gridLayout_4;
    QPushButton *pushButton_14;
    QPushButton *pushButton_10;
    QPushButton *pushButton_11;
    QPushButton *pushButton_15;
    QPushButton *pushButton_3;
    QPushButton *pushButton_12;
    QSpacerItem *verticalSpacer;
    QPushButton *pushButton_13;
    QSpacerItem *verticalSpacer_2;
    QPushButton *pushButton_7;
    QTabWidget *tabWidget;
    QWidget *tab;
    QGridLayout *gridLayout_16;
    QSplitter *splitter_3;
    QLabel *label;
    QSplitter *splitter;
    QWidget *layoutWidget1;
    QGridLayout *gridLayout_3;
    QGridLayout *gridLayout;
    QPushButton *pushButton;
    QPushButton *pushButton_5;
    QPushButton *pushButton_6;
    QGridLayout *gridLayout_2;
    QLineEdit *lineEdit;
    QLineEdit *lineEdit_2;
    QLineEdit *lineEdit_3;
    QSpacerItem *verticalSpacer_6;
    QLabel *label_18;
    QWidget *layoutWidget2;
    QGridLayout *gridLayout_21;
    QPushButton *pushButton_2;
    QPushButton *pushButton_9;
    QPushButton *pushButton_4;
    QSpacerItem *verticalSpacer_3;
    QSpacerItem *verticalSpacer_4;
    QSpacerItem *verticalSpacer_5;
    QPushButton *pushButton_8;
    QSpacerItem *horizontalSpacer_6;
    QSpacerItem *horizontalSpacer_7;
    QSpacerItem *horizontalSpacer_8;
    QWidget *tab_4;
    QGridLayout *gridLayout_8;
    QLabel *label_4;
    QSpacerItem *horizontalSpacer_2;
    QSpacerItem *horizontalSpacer;
    QGroupBox *groupBox_2;
    QGridLayout *gridLayout_17;
    QGridLayout *gridLayout_15;
    QGridLayout *gridLayout_7;
    QLabel *label_10;
    QLabel *label_16;
    QLabel *label_9;
    QLabel *label_8;
    QGridLayout *gridLayout_10;
    QLCDNumber *TeamOverallLcdNumber;
    QLCDNumber *TeamChemistryLcdNumber;
    QLCDNumber *BalanceLcdNumber;
    QLCDNumber *lcdNumber_6;
    QGroupBox *groupBox;
    QGridLayout *gridLayout_20;
    QGridLayout *gridLayout_19;
    QGridLayout *gridLayout_6;
    QLabel *label_3;
    QLabel *label_6;
    QLabel *label_7;
    QGridLayout *gridLayout_18;
    QLineEdit *location_lineEdit;
    QLineEdit *country_lineEdit;
    QLineEdit *sponsor_lineEdit;
    QSpacerItem *horizontalSpacer_3;
    QSpacerItem *horizontalSpacer_4;
    QGroupBox *groupBox_4;
    QGridLayout *gridLayout_14;
    QGridLayout *gridLayout_13;
    QLabel *label_2;
    QLabel *label_11;
    QGridLayout *gridLayout_9;
    QLCDNumber *lcdNumber_2;
    QLCDNumber *lcdNumber_3;
    QGroupBox *groupBox_3;
    QGridLayout *gridLayout_12;
    QGridLayout *gridLayout_11;
    QLabel *label_12;
    QLabel *label_13;
    QLabel *label_14;
    QLabel *label_15;
    QLabel *label_5;
    QGridLayout *gridLayout_5;
    QLCDNumber *ForwardLcdNumber;
    QLCDNumber *MidLcdNumber;
    QLCDNumber *DefenceLcdNumber;
    QLCDNumber *GkLcdNumber;
    QLCDNumber *ScoutLcdNumber;
    QSpacerItem *horizontalSpacer_5;

    void setupUi(QDialog *Edit)
    {
        if (Edit->objectName().isEmpty())
            Edit->setObjectName(QString::fromUtf8("Edit"));
        Edit->resize(769, 714);
        gridLayout_22 = new QGridLayout(Edit);
        gridLayout_22->setObjectName(QString::fromUtf8("gridLayout_22"));
        splitter_4 = new QSplitter(Edit);
        splitter_4->setObjectName(QString::fromUtf8("splitter_4"));
        splitter_4->setOrientation(Qt::Vertical);
        label_17 = new QLabel(splitter_4);
        label_17->setObjectName(QString::fromUtf8("label_17"));
        QFont font;
        font.setFamily(QString::fromUtf8("Stencil"));
        font.setPointSize(26);
        label_17->setFont(font);
        label_17->setAlignment(Qt::AlignCenter);
        splitter_4->addWidget(label_17);
        splitter_2 = new QSplitter(splitter_4);
        splitter_2->setObjectName(QString::fromUtf8("splitter_2"));
        splitter_2->setOrientation(Qt::Horizontal);
        layoutWidget = new QWidget(splitter_2);
        layoutWidget->setObjectName(QString::fromUtf8("layoutWidget"));
        gridLayout_4 = new QGridLayout(layoutWidget);
        gridLayout_4->setObjectName(QString::fromUtf8("gridLayout_4"));
        gridLayout_4->setContentsMargins(0, 0, 0, 0);
        pushButton_14 = new QPushButton(layoutWidget);
        pushButton_14->setObjectName(QString::fromUtf8("pushButton_14"));
        QFont font1;
        font1.setFamily(QString::fromUtf8("Century Gothic"));
        font1.setBold(true);
        font1.setWeight(75);
        pushButton_14->setFont(font1);
        pushButton_14->setCursor(QCursor(Qt::PointingHandCursor));

        gridLayout_4->addWidget(pushButton_14, 4, 0, 1, 1);

        pushButton_10 = new QPushButton(layoutWidget);
        pushButton_10->setObjectName(QString::fromUtf8("pushButton_10"));
        pushButton_10->setFont(font1);
        pushButton_10->setCursor(QCursor(Qt::PointingHandCursor));

        gridLayout_4->addWidget(pushButton_10, 5, 0, 1, 1);

        pushButton_11 = new QPushButton(layoutWidget);
        pushButton_11->setObjectName(QString::fromUtf8("pushButton_11"));
        pushButton_11->setFont(font1);
        pushButton_11->setCursor(QCursor(Qt::PointingHandCursor));

        gridLayout_4->addWidget(pushButton_11, 6, 0, 1, 1);

        pushButton_15 = new QPushButton(layoutWidget);
        pushButton_15->setObjectName(QString::fromUtf8("pushButton_15"));
        pushButton_15->setFont(font1);
        pushButton_15->setCursor(QCursor(Qt::PointingHandCursor));

        gridLayout_4->addWidget(pushButton_15, 7, 0, 1, 1);

        pushButton_3 = new QPushButton(layoutWidget);
        pushButton_3->setObjectName(QString::fromUtf8("pushButton_3"));
        pushButton_3->setFont(font1);
        pushButton_3->setCursor(QCursor(Qt::PointingHandCursor));

        gridLayout_4->addWidget(pushButton_3, 8, 0, 1, 1);

        pushButton_12 = new QPushButton(layoutWidget);
        pushButton_12->setObjectName(QString::fromUtf8("pushButton_12"));
        pushButton_12->setFont(font1);
        pushButton_12->setCursor(QCursor(Qt::PointingHandCursor));

        gridLayout_4->addWidget(pushButton_12, 11, 0, 1, 1);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Fixed);

        gridLayout_4->addItem(verticalSpacer, 0, 0, 1, 1);

        pushButton_13 = new QPushButton(layoutWidget);
        pushButton_13->setObjectName(QString::fromUtf8("pushButton_13"));
        pushButton_13->setFont(font1);
        pushButton_13->setCursor(QCursor(Qt::PointingHandCursor));

        gridLayout_4->addWidget(pushButton_13, 10, 0, 1, 1);

        verticalSpacer_2 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Fixed);

        gridLayout_4->addItem(verticalSpacer_2, 12, 0, 1, 1);

        pushButton_7 = new QPushButton(layoutWidget);
        pushButton_7->setObjectName(QString::fromUtf8("pushButton_7"));
        pushButton_7->setFont(font1);
        pushButton_7->setCursor(QCursor(Qt::PointingHandCursor));

        gridLayout_4->addWidget(pushButton_7, 9, 0, 1, 1);

        splitter_2->addWidget(layoutWidget);
        tabWidget = new QTabWidget(splitter_2);
        tabWidget->setObjectName(QString::fromUtf8("tabWidget"));
        tabWidget->setFont(font1);
        tabWidget->setAutoFillBackground(false);
        tabWidget->setMovable(false);
        tab = new QWidget();
        tab->setObjectName(QString::fromUtf8("tab"));
        gridLayout_16 = new QGridLayout(tab);
        gridLayout_16->setObjectName(QString::fromUtf8("gridLayout_16"));
        splitter_3 = new QSplitter(tab);
        splitter_3->setObjectName(QString::fromUtf8("splitter_3"));
        splitter_3->setOrientation(Qt::Vertical);
        label = new QLabel(splitter_3);
        label->setObjectName(QString::fromUtf8("label"));
        QFont font2;
        font2.setFamily(QString::fromUtf8("Copperplate Gothic Light"));
        font2.setPointSize(26);
        font2.setBold(true);
        font2.setWeight(75);
        label->setFont(font2);
        label->setAlignment(Qt::AlignCenter);
        splitter_3->addWidget(label);
        splitter = new QSplitter(splitter_3);
        splitter->setObjectName(QString::fromUtf8("splitter"));
        splitter->setOrientation(Qt::Vertical);
        layoutWidget1 = new QWidget(splitter);
        layoutWidget1->setObjectName(QString::fromUtf8("layoutWidget1"));
        gridLayout_3 = new QGridLayout(layoutWidget1);
        gridLayout_3->setObjectName(QString::fromUtf8("gridLayout_3"));
        gridLayout_3->setContentsMargins(0, 0, 0, 0);
        gridLayout = new QGridLayout();
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        pushButton = new QPushButton(layoutWidget1);
        pushButton->setObjectName(QString::fromUtf8("pushButton"));
        pushButton->setFont(font1);
        pushButton->setCursor(QCursor(Qt::PointingHandCursor));

        gridLayout->addWidget(pushButton, 0, 0, 1, 1);

        pushButton_5 = new QPushButton(layoutWidget1);
        pushButton_5->setObjectName(QString::fromUtf8("pushButton_5"));
        pushButton_5->setFont(font1);
        pushButton_5->setCursor(QCursor(Qt::PointingHandCursor));

        gridLayout->addWidget(pushButton_5, 1, 0, 1, 1);

        pushButton_6 = new QPushButton(layoutWidget1);
        pushButton_6->setObjectName(QString::fromUtf8("pushButton_6"));
        pushButton_6->setFont(font1);
        pushButton_6->setCursor(QCursor(Qt::PointingHandCursor));

        gridLayout->addWidget(pushButton_6, 2, 0, 1, 1);


        gridLayout_3->addLayout(gridLayout, 1, 0, 1, 1);

        gridLayout_2 = new QGridLayout();
        gridLayout_2->setObjectName(QString::fromUtf8("gridLayout_2"));
        lineEdit = new QLineEdit(layoutWidget1);
        lineEdit->setObjectName(QString::fromUtf8("lineEdit"));
        lineEdit->setEnabled(true);

        gridLayout_2->addWidget(lineEdit, 0, 0, 1, 1);

        lineEdit_2 = new QLineEdit(layoutWidget1);
        lineEdit_2->setObjectName(QString::fromUtf8("lineEdit_2"));
        lineEdit_2->setEnabled(true);

        gridLayout_2->addWidget(lineEdit_2, 1, 0, 1, 1);

        lineEdit_3 = new QLineEdit(layoutWidget1);
        lineEdit_3->setObjectName(QString::fromUtf8("lineEdit_3"));
        lineEdit_3->setEnabled(true);

        gridLayout_2->addWidget(lineEdit_3, 2, 0, 1, 1);


        gridLayout_3->addLayout(gridLayout_2, 1, 1, 1, 1);

        verticalSpacer_6 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Minimum);

        gridLayout_3->addItem(verticalSpacer_6, 0, 1, 1, 1);

        label_18 = new QLabel(layoutWidget1);
        label_18->setObjectName(QString::fromUtf8("label_18"));
        label_18->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignTop);

        gridLayout_3->addWidget(label_18, 0, 0, 1, 1);

        splitter->addWidget(layoutWidget1);
        layoutWidget2 = new QWidget(splitter);
        layoutWidget2->setObjectName(QString::fromUtf8("layoutWidget2"));
        gridLayout_21 = new QGridLayout(layoutWidget2);
        gridLayout_21->setObjectName(QString::fromUtf8("gridLayout_21"));
        gridLayout_21->setContentsMargins(0, 0, 0, 0);
        pushButton_2 = new QPushButton(layoutWidget2);
        pushButton_2->setObjectName(QString::fromUtf8("pushButton_2"));
        pushButton_2->setFont(font1);
        pushButton_2->setCursor(QCursor(Qt::PointingHandCursor));

        gridLayout_21->addWidget(pushButton_2, 2, 0, 1, 1);

        pushButton_9 = new QPushButton(layoutWidget2);
        pushButton_9->setObjectName(QString::fromUtf8("pushButton_9"));
        pushButton_9->setFont(font1);
        pushButton_9->setCursor(QCursor(Qt::PointingHandCursor));

        gridLayout_21->addWidget(pushButton_9, 2, 4, 1, 1);

        pushButton_4 = new QPushButton(layoutWidget2);
        pushButton_4->setObjectName(QString::fromUtf8("pushButton_4"));
        pushButton_4->setFont(font1);
        pushButton_4->setCursor(QCursor(Qt::PointingHandCursor));

        gridLayout_21->addWidget(pushButton_4, 2, 6, 1, 1);

        verticalSpacer_3 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout_21->addItem(verticalSpacer_3, 0, 0, 1, 1);

        verticalSpacer_4 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout_21->addItem(verticalSpacer_4, 0, 4, 1, 1);

        verticalSpacer_5 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        gridLayout_21->addItem(verticalSpacer_5, 0, 6, 1, 1);

        pushButton_8 = new QPushButton(layoutWidget2);
        pushButton_8->setObjectName(QString::fromUtf8("pushButton_8"));
        pushButton_8->setFont(font1);
        pushButton_8->setCursor(QCursor(Qt::PointingHandCursor));

        gridLayout_21->addWidget(pushButton_8, 2, 2, 1, 1);

        horizontalSpacer_6 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_21->addItem(horizontalSpacer_6, 2, 1, 1, 1);

        horizontalSpacer_7 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_21->addItem(horizontalSpacer_7, 2, 3, 1, 1);

        horizontalSpacer_8 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_21->addItem(horizontalSpacer_8, 2, 5, 1, 1);

        splitter->addWidget(layoutWidget2);
        splitter_3->addWidget(splitter);

        gridLayout_16->addWidget(splitter_3, 0, 0, 1, 1);

        tabWidget->addTab(tab, QString());
        tab_4 = new QWidget();
        tab_4->setObjectName(QString::fromUtf8("tab_4"));
        gridLayout_8 = new QGridLayout(tab_4);
        gridLayout_8->setObjectName(QString::fromUtf8("gridLayout_8"));
        label_4 = new QLabel(tab_4);
        label_4->setObjectName(QString::fromUtf8("label_4"));
        label_4->setFont(font2);

        gridLayout_8->addWidget(label_4, 0, 0, 1, 1);

        horizontalSpacer_2 = new QSpacerItem(230, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_8->addItem(horizontalSpacer_2, 0, 3, 1, 1);

        horizontalSpacer = new QSpacerItem(187, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_8->addItem(horizontalSpacer, 1, 0, 1, 1);

        groupBox_2 = new QGroupBox(tab_4);
        groupBox_2->setObjectName(QString::fromUtf8("groupBox_2"));
        QFont font3;
        font3.setFamily(QString::fromUtf8("Copperplate Gothic Light"));
        font3.setPointSize(12);
        font3.setBold(false);
        font3.setWeight(50);
        groupBox_2->setFont(font3);
        gridLayout_17 = new QGridLayout(groupBox_2);
        gridLayout_17->setObjectName(QString::fromUtf8("gridLayout_17"));
        gridLayout_15 = new QGridLayout();
        gridLayout_15->setObjectName(QString::fromUtf8("gridLayout_15"));
        gridLayout_7 = new QGridLayout();
        gridLayout_7->setObjectName(QString::fromUtf8("gridLayout_7"));
        label_10 = new QLabel(groupBox_2);
        label_10->setObjectName(QString::fromUtf8("label_10"));
        QFont font4;
        font4.setFamily(QString::fromUtf8("Copperplate Gothic Light"));
        font4.setPointSize(10);
        label_10->setFont(font4);
        label_10->setAlignment(Qt::AlignCenter);

        gridLayout_7->addWidget(label_10, 0, 0, 1, 1);

        label_16 = new QLabel(groupBox_2);
        label_16->setObjectName(QString::fromUtf8("label_16"));
        label_16->setFont(font4);
        label_16->setAlignment(Qt::AlignCenter);

        gridLayout_7->addWidget(label_16, 1, 0, 1, 1);

        label_9 = new QLabel(groupBox_2);
        label_9->setObjectName(QString::fromUtf8("label_9"));
        label_9->setFont(font4);
        label_9->setAlignment(Qt::AlignCenter);

        gridLayout_7->addWidget(label_9, 2, 0, 1, 1);

        label_8 = new QLabel(groupBox_2);
        label_8->setObjectName(QString::fromUtf8("label_8"));
        label_8->setFont(font4);
        label_8->setAlignment(Qt::AlignCenter);

        gridLayout_7->addWidget(label_8, 3, 0, 1, 1);


        gridLayout_15->addLayout(gridLayout_7, 0, 0, 1, 1);

        gridLayout_10 = new QGridLayout();
        gridLayout_10->setObjectName(QString::fromUtf8("gridLayout_10"));
        TeamOverallLcdNumber = new QLCDNumber(groupBox_2);
        TeamOverallLcdNumber->setObjectName(QString::fromUtf8("TeamOverallLcdNumber"));
        TeamOverallLcdNumber->setStyleSheet(QString::fromUtf8("background-color: rgb(0, 0, 0);"));
        TeamOverallLcdNumber->setNumDigits(8);
        TeamOverallLcdNumber->setDigitCount(8);
        TeamOverallLcdNumber->setSegmentStyle(QLCDNumber::Filled);

        gridLayout_10->addWidget(TeamOverallLcdNumber, 0, 0, 1, 1);

        TeamChemistryLcdNumber = new QLCDNumber(groupBox_2);
        TeamChemistryLcdNumber->setObjectName(QString::fromUtf8("TeamChemistryLcdNumber"));
        TeamChemistryLcdNumber->setStyleSheet(QString::fromUtf8("background-color: rgb(0, 0, 0);"));
        TeamChemistryLcdNumber->setNumDigits(8);
        TeamChemistryLcdNumber->setDigitCount(8);
        TeamChemistryLcdNumber->setSegmentStyle(QLCDNumber::Filled);

        gridLayout_10->addWidget(TeamChemistryLcdNumber, 1, 0, 1, 1);

        BalanceLcdNumber = new QLCDNumber(groupBox_2);
        BalanceLcdNumber->setObjectName(QString::fromUtf8("BalanceLcdNumber"));
        BalanceLcdNumber->setStyleSheet(QString::fromUtf8("background-color: rgb(0, 0, 0);"));
        BalanceLcdNumber->setNumDigits(8);
        BalanceLcdNumber->setDigitCount(8);
        BalanceLcdNumber->setSegmentStyle(QLCDNumber::Filled);

        gridLayout_10->addWidget(BalanceLcdNumber, 2, 0, 1, 1);

        lcdNumber_6 = new QLCDNumber(groupBox_2);
        lcdNumber_6->setObjectName(QString::fromUtf8("lcdNumber_6"));
        lcdNumber_6->setStyleSheet(QString::fromUtf8("background-color: rgb(0, 0, 0);"));
        lcdNumber_6->setNumDigits(8);
        lcdNumber_6->setDigitCount(8);
        lcdNumber_6->setSegmentStyle(QLCDNumber::Filled);

        gridLayout_10->addWidget(lcdNumber_6, 3, 0, 1, 1);


        gridLayout_15->addLayout(gridLayout_10, 0, 1, 1, 1);


        gridLayout_17->addLayout(gridLayout_15, 0, 0, 1, 1);


        gridLayout_8->addWidget(groupBox_2, 1, 1, 1, 3);

        groupBox = new QGroupBox(tab_4);
        groupBox->setObjectName(QString::fromUtf8("groupBox"));
        groupBox->setFont(font3);
        gridLayout_20 = new QGridLayout(groupBox);
        gridLayout_20->setObjectName(QString::fromUtf8("gridLayout_20"));
        gridLayout_19 = new QGridLayout();
        gridLayout_19->setObjectName(QString::fromUtf8("gridLayout_19"));
        gridLayout_6 = new QGridLayout();
        gridLayout_6->setObjectName(QString::fromUtf8("gridLayout_6"));
        label_3 = new QLabel(groupBox);
        label_3->setObjectName(QString::fromUtf8("label_3"));
        label_3->setFont(font4);
        label_3->setAlignment(Qt::AlignCenter);

        gridLayout_6->addWidget(label_3, 0, 0, 1, 1);

        label_6 = new QLabel(groupBox);
        label_6->setObjectName(QString::fromUtf8("label_6"));
        label_6->setFont(font4);
        label_6->setAlignment(Qt::AlignCenter);

        gridLayout_6->addWidget(label_6, 1, 0, 1, 1);

        label_7 = new QLabel(groupBox);
        label_7->setObjectName(QString::fromUtf8("label_7"));
        label_7->setFont(font4);
        label_7->setAlignment(Qt::AlignCenter);

        gridLayout_6->addWidget(label_7, 2, 0, 1, 1);


        gridLayout_19->addLayout(gridLayout_6, 0, 0, 1, 1);

        gridLayout_18 = new QGridLayout();
        gridLayout_18->setObjectName(QString::fromUtf8("gridLayout_18"));
        location_lineEdit = new QLineEdit(groupBox);
        location_lineEdit->setObjectName(QString::fromUtf8("location_lineEdit"));
        QFont font5;
        font5.setFamily(QString::fromUtf8("Century Gothic"));
        font5.setPointSize(8);
        location_lineEdit->setFont(font5);

        gridLayout_18->addWidget(location_lineEdit, 0, 0, 1, 1);

        country_lineEdit = new QLineEdit(groupBox);
        country_lineEdit->setObjectName(QString::fromUtf8("country_lineEdit"));
        country_lineEdit->setFont(font5);

        gridLayout_18->addWidget(country_lineEdit, 1, 0, 1, 1);

        sponsor_lineEdit = new QLineEdit(groupBox);
        sponsor_lineEdit->setObjectName(QString::fromUtf8("sponsor_lineEdit"));
        sponsor_lineEdit->setFont(font5);

        gridLayout_18->addWidget(sponsor_lineEdit, 2, 0, 1, 1);


        gridLayout_19->addLayout(gridLayout_18, 0, 1, 1, 1);


        gridLayout_20->addLayout(gridLayout_19, 0, 0, 1, 1);


        gridLayout_8->addWidget(groupBox, 2, 0, 1, 2);

        horizontalSpacer_3 = new QSpacerItem(230, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_8->addItem(horizontalSpacer_3, 2, 3, 1, 1);

        horizontalSpacer_4 = new QSpacerItem(187, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_8->addItem(horizontalSpacer_4, 3, 0, 1, 1);

        groupBox_4 = new QGroupBox(tab_4);
        groupBox_4->setObjectName(QString::fromUtf8("groupBox_4"));
        groupBox_4->setFont(font3);
        gridLayout_14 = new QGridLayout(groupBox_4);
        gridLayout_14->setObjectName(QString::fromUtf8("gridLayout_14"));
        gridLayout_13 = new QGridLayout();
        gridLayout_13->setObjectName(QString::fromUtf8("gridLayout_13"));
        label_2 = new QLabel(groupBox_4);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        label_2->setFont(font4);
        label_2->setAlignment(Qt::AlignCenter);

        gridLayout_13->addWidget(label_2, 0, 0, 1, 1);

        label_11 = new QLabel(groupBox_4);
        label_11->setObjectName(QString::fromUtf8("label_11"));
        label_11->setFont(font4);
        label_11->setAlignment(Qt::AlignCenter);

        gridLayout_13->addWidget(label_11, 1, 0, 1, 1);


        gridLayout_14->addLayout(gridLayout_13, 0, 0, 1, 1);

        gridLayout_9 = new QGridLayout();
        gridLayout_9->setObjectName(QString::fromUtf8("gridLayout_9"));
        lcdNumber_2 = new QLCDNumber(groupBox_4);
        lcdNumber_2->setObjectName(QString::fromUtf8("lcdNumber_2"));
        lcdNumber_2->setStyleSheet(QString::fromUtf8("background-color: rgb(0, 0, 0);"));
        lcdNumber_2->setNumDigits(7);
        lcdNumber_2->setSegmentStyle(QLCDNumber::Filled);

        gridLayout_9->addWidget(lcdNumber_2, 0, 0, 1, 1);

        lcdNumber_3 = new QLCDNumber(groupBox_4);
        lcdNumber_3->setObjectName(QString::fromUtf8("lcdNumber_3"));
        lcdNumber_3->setStyleSheet(QString::fromUtf8("background-color: rgb(0, 0, 0);"));
        lcdNumber_3->setNumDigits(7);
        lcdNumber_3->setSegmentStyle(QLCDNumber::Filled);

        gridLayout_9->addWidget(lcdNumber_3, 1, 0, 1, 1);


        gridLayout_14->addLayout(gridLayout_9, 0, 1, 1, 1);


        gridLayout_8->addWidget(groupBox_4, 3, 2, 1, 2);

        groupBox_3 = new QGroupBox(tab_4);
        groupBox_3->setObjectName(QString::fromUtf8("groupBox_3"));
        groupBox_3->setFont(font3);
        gridLayout_12 = new QGridLayout(groupBox_3);
        gridLayout_12->setObjectName(QString::fromUtf8("gridLayout_12"));
        gridLayout_11 = new QGridLayout();
        gridLayout_11->setObjectName(QString::fromUtf8("gridLayout_11"));
        label_12 = new QLabel(groupBox_3);
        label_12->setObjectName(QString::fromUtf8("label_12"));
        label_12->setFont(font4);
        label_12->setAlignment(Qt::AlignCenter);

        gridLayout_11->addWidget(label_12, 0, 0, 1, 1);

        label_13 = new QLabel(groupBox_3);
        label_13->setObjectName(QString::fromUtf8("label_13"));
        label_13->setFont(font4);
        label_13->setAlignment(Qt::AlignCenter);

        gridLayout_11->addWidget(label_13, 1, 0, 1, 1);

        label_14 = new QLabel(groupBox_3);
        label_14->setObjectName(QString::fromUtf8("label_14"));
        label_14->setFont(font4);
        label_14->setAlignment(Qt::AlignCenter);

        gridLayout_11->addWidget(label_14, 2, 0, 1, 1);

        label_15 = new QLabel(groupBox_3);
        label_15->setObjectName(QString::fromUtf8("label_15"));
        label_15->setFont(font4);
        label_15->setAlignment(Qt::AlignCenter);

        gridLayout_11->addWidget(label_15, 3, 0, 1, 1);

        label_5 = new QLabel(groupBox_3);
        label_5->setObjectName(QString::fromUtf8("label_5"));
        label_5->setFont(font4);
        label_5->setAlignment(Qt::AlignCenter);

        gridLayout_11->addWidget(label_5, 4, 0, 1, 1);


        gridLayout_12->addLayout(gridLayout_11, 0, 0, 1, 1);

        gridLayout_5 = new QGridLayout();
        gridLayout_5->setObjectName(QString::fromUtf8("gridLayout_5"));
        ForwardLcdNumber = new QLCDNumber(groupBox_3);
        ForwardLcdNumber->setObjectName(QString::fromUtf8("ForwardLcdNumber"));
        ForwardLcdNumber->setStyleSheet(QString::fromUtf8("background-color: rgb(0, 0, 0);"));
        ForwardLcdNumber->setSegmentStyle(QLCDNumber::Filled);

        gridLayout_5->addWidget(ForwardLcdNumber, 0, 0, 1, 1);

        MidLcdNumber = new QLCDNumber(groupBox_3);
        MidLcdNumber->setObjectName(QString::fromUtf8("MidLcdNumber"));
        MidLcdNumber->setStyleSheet(QString::fromUtf8("background-color: rgb(0, 0, 0);"));
        MidLcdNumber->setSegmentStyle(QLCDNumber::Filled);

        gridLayout_5->addWidget(MidLcdNumber, 1, 0, 1, 1);

        DefenceLcdNumber = new QLCDNumber(groupBox_3);
        DefenceLcdNumber->setObjectName(QString::fromUtf8("DefenceLcdNumber"));
        DefenceLcdNumber->setStyleSheet(QString::fromUtf8("background-color: rgb(0, 0, 0);"));
        DefenceLcdNumber->setSegmentStyle(QLCDNumber::Filled);

        gridLayout_5->addWidget(DefenceLcdNumber, 2, 0, 1, 1);

        GkLcdNumber = new QLCDNumber(groupBox_3);
        GkLcdNumber->setObjectName(QString::fromUtf8("GkLcdNumber"));
        GkLcdNumber->setStyleSheet(QString::fromUtf8("background-color: rgb(0, 0, 0);"));
        GkLcdNumber->setSegmentStyle(QLCDNumber::Filled);

        gridLayout_5->addWidget(GkLcdNumber, 3, 0, 1, 1);

        ScoutLcdNumber = new QLCDNumber(groupBox_3);
        ScoutLcdNumber->setObjectName(QString::fromUtf8("ScoutLcdNumber"));
        ScoutLcdNumber->setStyleSheet(QString::fromUtf8("background-color: rgb(0, 0, 0);"));
        ScoutLcdNumber->setSegmentStyle(QLCDNumber::Filled);

        gridLayout_5->addWidget(ScoutLcdNumber, 4, 0, 1, 1);


        gridLayout_12->addLayout(gridLayout_5, 0, 1, 1, 1);


        gridLayout_8->addWidget(groupBox_3, 4, 0, 1, 3);

        horizontalSpacer_5 = new QSpacerItem(230, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_8->addItem(horizontalSpacer_5, 4, 3, 1, 1);

        tabWidget->addTab(tab_4, QString());
        splitter_2->addWidget(tabWidget);
        splitter_4->addWidget(splitter_2);

        gridLayout_22->addWidget(splitter_4, 0, 0, 1, 1);


        retranslateUi(Edit);

        tabWidget->setCurrentIndex(0);


        QMetaObject::connectSlotsByName(Edit);
    } // setupUi

    void retranslateUi(QDialog *Edit)
    {
        Edit->setWindowTitle(QApplication::translate("Edit", "Dialog", 0, QApplication::UnicodeUTF8));
        label_17->setText(QApplication::translate("Edit", "Management station", 0, QApplication::UnicodeUTF8));
        pushButton_14->setText(QApplication::translate("Edit", "Squad", 0, QApplication::UnicodeUTF8));
        pushButton_10->setText(QApplication::translate("Edit", "Staff Upgrade", 0, QApplication::UnicodeUTF8));
        pushButton_11->setText(QApplication::translate("Edit", "Set Increments", 0, QApplication::UnicodeUTF8));
        pushButton_15->setText(QApplication::translate("Edit", "Transfer Window", 0, QApplication::UnicodeUTF8));
        pushButton_3->setText(QApplication::translate("Edit", "Update Stadium", 0, QApplication::UnicodeUTF8));
        pushButton_12->setText(QApplication::translate("Edit", "Next Match", 0, QApplication::UnicodeUTF8));
        pushButton_13->setText(QApplication::translate("Edit", "League Table", 0, QApplication::UnicodeUTF8));
        pushButton_7->setText(QApplication::translate("Edit", "Season History", 0, QApplication::UnicodeUTF8));
        label->setText(QApplication::translate("Edit", "TextLabel", 0, QApplication::UnicodeUTF8));
        pushButton->setText(QApplication::translate("Edit", "Change Name", 0, QApplication::UnicodeUTF8));
        pushButton_5->setText(QApplication::translate("Edit", "Change Age", 0, QApplication::UnicodeUTF8));
        pushButton_6->setText(QApplication::translate("Edit", "Change Nationality", 0, QApplication::UnicodeUTF8));
        label_18->setText(QApplication::translate("Edit", "TextLabel", 0, QApplication::UnicodeUTF8));
        pushButton_2->setText(QApplication::translate("Edit", "Exit", 0, QApplication::UnicodeUTF8));
        pushButton_9->setText(QApplication::translate("Edit", "Save Game", 0, QApplication::UnicodeUTF8));
        pushButton_4->setText(QApplication::translate("Edit", "Update Profile", 0, QApplication::UnicodeUTF8));
        pushButton_8->setText(QApplication::translate("Edit", "Leave Job", 0, QApplication::UnicodeUTF8));
        tabWidget->setTabText(tabWidget->indexOf(tab), QApplication::translate("Edit", "Profile", 0, QApplication::UnicodeUTF8));
        label_4->setText(QApplication::translate("Edit", "TextLabel", 0, QApplication::UnicodeUTF8));
        groupBox_2->setTitle(QApplication::translate("Edit", "Statistics Overview", 0, QApplication::UnicodeUTF8));
        label_10->setText(QApplication::translate("Edit", "Team Overall", 0, QApplication::UnicodeUTF8));
        label_16->setText(QApplication::translate("Edit", "Team Chemistry", 0, QApplication::UnicodeUTF8));
        label_9->setText(QApplication::translate("Edit", "Current Balance", 0, QApplication::UnicodeUTF8));
        label_8->setText(QApplication::translate("Edit", "Popularity", 0, QApplication::UnicodeUTF8));
        groupBox->setTitle(QApplication::translate("Edit", "Team Details", 0, QApplication::UnicodeUTF8));
        label_3->setText(QApplication::translate("Edit", "Location", 0, QApplication::UnicodeUTF8));
        label_6->setText(QApplication::translate("Edit", "Country", 0, QApplication::UnicodeUTF8));
        label_7->setText(QApplication::translate("Edit", "Sponsor", 0, QApplication::UnicodeUTF8));
        groupBox_4->setTitle(QApplication::translate("Edit", "Stadium", 0, QApplication::UnicodeUTF8));
        label_2->setText(QApplication::translate("Edit", "Stadium capacity", 0, QApplication::UnicodeUTF8));
        label_11->setText(QApplication::translate("Edit", "Ticket Price", 0, QApplication::UnicodeUTF8));
        groupBox_3->setTitle(QApplication::translate("Edit", "Coaching", 0, QApplication::UnicodeUTF8));
        label_12->setText(QApplication::translate("Edit", "Forward", 0, QApplication::UnicodeUTF8));
        label_13->setText(QApplication::translate("Edit", "Mid Field", 0, QApplication::UnicodeUTF8));
        label_14->setText(QApplication::translate("Edit", "Defence", 0, QApplication::UnicodeUTF8));
        label_15->setText(QApplication::translate("Edit", "Goal Keeping", 0, QApplication::UnicodeUTF8));
        label_5->setText(QApplication::translate("Edit", "Scouting", 0, QApplication::UnicodeUTF8));
        tabWidget->setTabText(tabWidget->indexOf(tab_4), QApplication::translate("Edit", "Team Details", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class Edit: public Ui_Edit {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_EDIT_H
