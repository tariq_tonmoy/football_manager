/********************************************************************************
** Form generated from reading UI file 'goal.ui'
**
** Created: Thu Apr 19 02:12:48 2012
**      by: Qt User Interface Compiler version 4.6.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_GOAL_H
#define UI_GOAL_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QDialog>
#include <QtGui/QGridLayout>
#include <QtGui/QGroupBox>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QPushButton>
#include <QtGui/QSpacerItem>
#include <QtGui/QSplitter>

QT_BEGIN_NAMESPACE

class Ui_Goal
{
public:
    QGridLayout *gridLayout_2;
    QSplitter *splitter;
    QLabel *label;
    QGroupBox *groupBox;
    QGridLayout *gridLayout;
    QLabel *label_2;
    QLabel *label_4;
    QSpacerItem *horizontalSpacer_2;
    QPushButton *pushButton;
    QSpacerItem *horizontalSpacer;

    void setupUi(QDialog *Goal)
    {
        if (Goal->objectName().isEmpty())
            Goal->setObjectName(QString::fromUtf8("Goal"));
        Goal->resize(546, 369);
        gridLayout_2 = new QGridLayout(Goal);
        gridLayout_2->setObjectName(QString::fromUtf8("gridLayout_2"));
        splitter = new QSplitter(Goal);
        splitter->setObjectName(QString::fromUtf8("splitter"));
        splitter->setOrientation(Qt::Vertical);
        label = new QLabel(splitter);
        label->setObjectName(QString::fromUtf8("label"));
        QFont font;
        font.setFamily(QString::fromUtf8("Stencil"));
        font.setPointSize(36);
        label->setFont(font);
        label->setAlignment(Qt::AlignCenter);
        splitter->addWidget(label);
        groupBox = new QGroupBox(splitter);
        groupBox->setObjectName(QString::fromUtf8("groupBox"));
        QFont font1;
        font1.setFamily(QString::fromUtf8("Copperplate Gothic Bold"));
        font1.setPointSize(10);
        groupBox->setFont(font1);
        gridLayout = new QGridLayout(groupBox);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        label_2 = new QLabel(groupBox);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        QFont font2;
        font2.setFamily(QString::fromUtf8("Copperplate Gothic Bold"));
        font2.setPointSize(14);
        font2.setBold(true);
        font2.setWeight(75);
        label_2->setFont(font2);
        label_2->setAlignment(Qt::AlignCenter);

        gridLayout->addWidget(label_2, 0, 0, 1, 1);

        label_4 = new QLabel(groupBox);
        label_4->setObjectName(QString::fromUtf8("label_4"));
        QFont font3;
        font3.setFamily(QString::fromUtf8("Copperplate Gothic Light"));
        font3.setPointSize(12);
        font3.setItalic(true);
        label_4->setFont(font3);
        label_4->setAlignment(Qt::AlignRight|Qt::AlignTop|Qt::AlignTrailing);

        gridLayout->addWidget(label_4, 1, 0, 1, 1);

        splitter->addWidget(groupBox);

        gridLayout_2->addWidget(splitter, 0, 0, 1, 3);

        horizontalSpacer_2 = new QSpacerItem(218, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_2->addItem(horizontalSpacer_2, 1, 0, 1, 1);

        pushButton = new QPushButton(Goal);
        pushButton->setObjectName(QString::fromUtf8("pushButton"));
        QFont font4;
        font4.setFamily(QString::fromUtf8("Century Gothic"));
        font4.setBold(true);
        font4.setWeight(75);
        pushButton->setFont(font4);
        pushButton->setCursor(QCursor(Qt::PointingHandCursor));

        gridLayout_2->addWidget(pushButton, 1, 1, 1, 1);

        horizontalSpacer = new QSpacerItem(217, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_2->addItem(horizontalSpacer, 1, 2, 1, 1);


        retranslateUi(Goal);

        QMetaObject::connectSlotsByName(Goal);
    } // setupUi

    void retranslateUi(QDialog *Goal)
    {
        Goal->setWindowTitle(QApplication::translate("Goal", "Dialog", 0, QApplication::UnicodeUTF8));
        label->setText(QApplication::translate("Goal", "GOAL!!!!", 0, QApplication::UnicodeUTF8));
        groupBox->setTitle(QApplication::translate("Goal", "SCORER", 0, QApplication::UnicodeUTF8));
        label_2->setText(QApplication::translate("Goal", "TextLabel", 0, QApplication::UnicodeUTF8));
        label_4->setText(QApplication::translate("Goal", "TextLabel", 0, QApplication::UnicodeUTF8));
        pushButton->setText(QApplication::translate("Goal", "OK", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class Goal: public Ui_Goal {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_GOAL_H
