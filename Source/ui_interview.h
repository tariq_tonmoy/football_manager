/********************************************************************************
** Form generated from reading UI file 'interview.ui'
**
** Created: Thu Apr 19 02:24:59 2012
**      by: Qt User Interface Compiler version 4.6.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_INTERVIEW_H
#define UI_INTERVIEW_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QCheckBox>
#include <QtGui/QDialog>
#include <QtGui/QFormLayout>
#include <QtGui/QGridLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QLineEdit>
#include <QtGui/QPushButton>
#include <QtGui/QSpacerItem>
#include <QtGui/QSplitter>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_Interview
{
public:
    QGridLayout *gridLayout_3;
    QGridLayout *gridLayout_2;
    QSplitter *splitter;
    QWidget *layoutWidget;
    QFormLayout *formLayout;
    QLabel *label;
    QLineEdit *lineEdit;
    QWidget *layoutWidget1;
    QGridLayout *gridLayout;
    QFormLayout *formLayout_3;
    QCheckBox *checkBox_2;
    QLineEdit *lineEdit_3;
    QFormLayout *formLayout_4;
    QCheckBox *checkBox_3;
    QLineEdit *lineEdit_4;
    QFormLayout *formLayout_2;
    QCheckBox *checkBox;
    QLineEdit *lineEdit_2;
    QPushButton *pushButton;
    QLabel *label_2;
    QSpacerItem *verticalSpacer;
    QSpacerItem *verticalSpacer_2;

    void setupUi(QDialog *Interview)
    {
        if (Interview->objectName().isEmpty())
            Interview->setObjectName(QString::fromUtf8("Interview"));
        Interview->resize(602, 467);
        gridLayout_3 = new QGridLayout(Interview);
        gridLayout_3->setObjectName(QString::fromUtf8("gridLayout_3"));
        gridLayout_2 = new QGridLayout();
        gridLayout_2->setObjectName(QString::fromUtf8("gridLayout_2"));
        splitter = new QSplitter(Interview);
        splitter->setObjectName(QString::fromUtf8("splitter"));
        splitter->setOrientation(Qt::Vertical);
        layoutWidget = new QWidget(splitter);
        layoutWidget->setObjectName(QString::fromUtf8("layoutWidget"));
        formLayout = new QFormLayout(layoutWidget);
        formLayout->setObjectName(QString::fromUtf8("formLayout"));
        formLayout->setFieldGrowthPolicy(QFormLayout::AllNonFixedFieldsGrow);
        formLayout->setContentsMargins(0, 0, 0, 0);
        label = new QLabel(layoutWidget);
        label->setObjectName(QString::fromUtf8("label"));
        QFont font;
        font.setFamily(QString::fromUtf8("Copperplate Gothic Light"));
        label->setFont(font);

        formLayout->setWidget(0, QFormLayout::LabelRole, label);

        lineEdit = new QLineEdit(layoutWidget);
        lineEdit->setObjectName(QString::fromUtf8("lineEdit"));
        QFont font1;
        font1.setFamily(QString::fromUtf8("Century Gothic"));
        font1.setBold(true);
        font1.setWeight(75);
        lineEdit->setFont(font1);

        formLayout->setWidget(0, QFormLayout::FieldRole, lineEdit);

        splitter->addWidget(layoutWidget);
        layoutWidget1 = new QWidget(splitter);
        layoutWidget1->setObjectName(QString::fromUtf8("layoutWidget1"));
        gridLayout = new QGridLayout(layoutWidget1);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        gridLayout->setContentsMargins(0, 0, 0, 0);
        formLayout_3 = new QFormLayout();
        formLayout_3->setObjectName(QString::fromUtf8("formLayout_3"));
        formLayout_3->setFieldGrowthPolicy(QFormLayout::AllNonFixedFieldsGrow);
        checkBox_2 = new QCheckBox(layoutWidget1);
        checkBox_2->setObjectName(QString::fromUtf8("checkBox_2"));
        checkBox_2->setFont(font);

        formLayout_3->setWidget(0, QFormLayout::LabelRole, checkBox_2);

        lineEdit_3 = new QLineEdit(layoutWidget1);
        lineEdit_3->setObjectName(QString::fromUtf8("lineEdit_3"));
        lineEdit_3->setFont(font1);

        formLayout_3->setWidget(0, QFormLayout::FieldRole, lineEdit_3);


        gridLayout->addLayout(formLayout_3, 1, 0, 1, 1);

        formLayout_4 = new QFormLayout();
        formLayout_4->setObjectName(QString::fromUtf8("formLayout_4"));
        checkBox_3 = new QCheckBox(layoutWidget1);
        checkBox_3->setObjectName(QString::fromUtf8("checkBox_3"));
        checkBox_3->setFont(font);

        formLayout_4->setWidget(0, QFormLayout::LabelRole, checkBox_3);

        lineEdit_4 = new QLineEdit(layoutWidget1);
        lineEdit_4->setObjectName(QString::fromUtf8("lineEdit_4"));
        lineEdit_4->setFont(font1);
        lineEdit_4->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignTop);

        formLayout_4->setWidget(0, QFormLayout::FieldRole, lineEdit_4);


        gridLayout->addLayout(formLayout_4, 2, 0, 1, 1);

        formLayout_2 = new QFormLayout();
        formLayout_2->setObjectName(QString::fromUtf8("formLayout_2"));
        checkBox = new QCheckBox(layoutWidget1);
        checkBox->setObjectName(QString::fromUtf8("checkBox"));
        checkBox->setFont(font);

        formLayout_2->setWidget(0, QFormLayout::LabelRole, checkBox);

        lineEdit_2 = new QLineEdit(layoutWidget1);
        lineEdit_2->setObjectName(QString::fromUtf8("lineEdit_2"));
        lineEdit_2->setFont(font1);

        formLayout_2->setWidget(0, QFormLayout::FieldRole, lineEdit_2);


        gridLayout->addLayout(formLayout_2, 0, 0, 1, 1);

        splitter->addWidget(layoutWidget1);

        gridLayout_2->addWidget(splitter, 3, 0, 1, 3);

        pushButton = new QPushButton(Interview);
        pushButton->setObjectName(QString::fromUtf8("pushButton"));
        pushButton->setFont(font1);
        pushButton->setCursor(QCursor(Qt::PointingHandCursor));

        gridLayout_2->addWidget(pushButton, 4, 2, 1, 1);

        label_2 = new QLabel(Interview);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        QFont font2;
        font2.setFamily(QString::fromUtf8("Stencil"));
        font2.setPointSize(26);
        font2.setBold(false);
        font2.setWeight(50);
        label_2->setFont(font2);
        label_2->setFrameShape(QFrame::Box);
        label_2->setFrameShadow(QFrame::Sunken);
        label_2->setAlignment(Qt::AlignCenter);

        gridLayout_2->addWidget(label_2, 1, 1, 1, 1);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Fixed);

        gridLayout_2->addItem(verticalSpacer, 2, 1, 1, 1);

        verticalSpacer_2 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Maximum);

        gridLayout_2->addItem(verticalSpacer_2, 4, 1, 1, 1);


        gridLayout_3->addLayout(gridLayout_2, 0, 0, 1, 1);


        retranslateUi(Interview);

        QMetaObject::connectSlotsByName(Interview);
    } // setupUi

    void retranslateUi(QDialog *Interview)
    {
        Interview->setWindowTitle(QApplication::translate("Interview", "Dialog", 0, QApplication::UnicodeUTF8));
        label->setText(QApplication::translate("Interview", "QUESTION", 0, QApplication::UnicodeUTF8));
        checkBox_2->setText(QApplication::translate("Interview", "Ans2", 0, QApplication::UnicodeUTF8));
        checkBox_3->setText(QApplication::translate("Interview", "Ans3", 0, QApplication::UnicodeUTF8));
        checkBox->setText(QApplication::translate("Interview", "Ans1", 0, QApplication::UnicodeUTF8));
        pushButton->setText(QApplication::translate("Interview", "Submit", 0, QApplication::UnicodeUTF8));
        label_2->setText(QApplication::translate("Interview", "THE DAILY NEWSPAPER", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class Interview: public Ui_Interview {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_INTERVIEW_H
