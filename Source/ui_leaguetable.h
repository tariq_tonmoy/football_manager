/********************************************************************************
** Form generated from reading UI file 'leaguetable.ui'
**
** Created: Thu Apr 19 05:59:43 2012
**      by: Qt User Interface Compiler version 4.6.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_LEAGUETABLE_H
#define UI_LEAGUETABLE_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QDialog>
#include <QtGui/QGridLayout>
#include <QtGui/QGroupBox>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QPushButton>
#include <QtGui/QSpacerItem>
#include <QtGui/QSplitter>
#include <QtGui/QTabWidget>
#include <QtGui/QTableWidget>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_LeagueTable
{
public:
    QGridLayout *gridLayout_7;
    QTabWidget *tabWidget;
    QWidget *tab;
    QGridLayout *gridLayout_4;
    QSplitter *splitter;
    QLabel *label;
    QWidget *layoutWidget;
    QGridLayout *gridLayout_3;
    QGroupBox *groupBox_2;
    QGridLayout *gridLayout;
    QTableWidget *tableWidget;
    QGroupBox *groupBox;
    QGridLayout *gridLayout_2;
    QTableWidget *tableWidget_2;
    QWidget *tab_2;
    QGridLayout *gridLayout_6;
    QSplitter *splitter_2;
    QLabel *label_2;
    QGroupBox *groupBox_3;
    QGridLayout *gridLayout_5;
    QTableWidget *tableWidget_3;
    QSpacerItem *horizontalSpacer_2;
    QSpacerItem *horizontalSpacer_3;
    QPushButton *pushButton;
    QSpacerItem *horizontalSpacer;

    void setupUi(QDialog *LeagueTable)
    {
        if (LeagueTable->objectName().isEmpty())
            LeagueTable->setObjectName(QString::fromUtf8("LeagueTable"));
        LeagueTable->resize(967, 474);
        gridLayout_7 = new QGridLayout(LeagueTable);
        gridLayout_7->setObjectName(QString::fromUtf8("gridLayout_7"));
        tabWidget = new QTabWidget(LeagueTable);
        tabWidget->setObjectName(QString::fromUtf8("tabWidget"));
        tab = new QWidget();
        tab->setObjectName(QString::fromUtf8("tab"));
        gridLayout_4 = new QGridLayout(tab);
        gridLayout_4->setObjectName(QString::fromUtf8("gridLayout_4"));
        splitter = new QSplitter(tab);
        splitter->setObjectName(QString::fromUtf8("splitter"));
        splitter->setOrientation(Qt::Vertical);
        label = new QLabel(splitter);
        label->setObjectName(QString::fromUtf8("label"));
        QFont font;
        font.setFamily(QString::fromUtf8("Stencil"));
        font.setPointSize(22);
        label->setFont(font);
        label->setFrameShape(QFrame::StyledPanel);
        label->setTextFormat(Qt::AutoText);
        label->setAlignment(Qt::AlignCenter);
        splitter->addWidget(label);
        layoutWidget = new QWidget(splitter);
        layoutWidget->setObjectName(QString::fromUtf8("layoutWidget"));
        gridLayout_3 = new QGridLayout(layoutWidget);
        gridLayout_3->setObjectName(QString::fromUtf8("gridLayout_3"));
        gridLayout_3->setContentsMargins(0, 0, 0, 0);
        groupBox_2 = new QGroupBox(layoutWidget);
        groupBox_2->setObjectName(QString::fromUtf8("groupBox_2"));
        QFont font1;
        font1.setFamily(QString::fromUtf8("Copperplate Gothic Light"));
        font1.setPointSize(12);
        groupBox_2->setFont(font1);
        gridLayout = new QGridLayout(groupBox_2);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        tableWidget = new QTableWidget(groupBox_2);
        tableWidget->setObjectName(QString::fromUtf8("tableWidget"));

        gridLayout->addWidget(tableWidget, 0, 0, 1, 1);


        gridLayout_3->addWidget(groupBox_2, 0, 0, 1, 1);

        groupBox = new QGroupBox(layoutWidget);
        groupBox->setObjectName(QString::fromUtf8("groupBox"));
        groupBox->setFont(font1);
        gridLayout_2 = new QGridLayout(groupBox);
        gridLayout_2->setObjectName(QString::fromUtf8("gridLayout_2"));
        tableWidget_2 = new QTableWidget(groupBox);
        tableWidget_2->setObjectName(QString::fromUtf8("tableWidget_2"));

        gridLayout_2->addWidget(tableWidget_2, 0, 0, 1, 1);


        gridLayout_3->addWidget(groupBox, 0, 1, 1, 1);

        splitter->addWidget(layoutWidget);

        gridLayout_4->addWidget(splitter, 0, 0, 1, 1);

        tabWidget->addTab(tab, QString());
        tab_2 = new QWidget();
        tab_2->setObjectName(QString::fromUtf8("tab_2"));
        gridLayout_6 = new QGridLayout(tab_2);
        gridLayout_6->setObjectName(QString::fromUtf8("gridLayout_6"));
        splitter_2 = new QSplitter(tab_2);
        splitter_2->setObjectName(QString::fromUtf8("splitter_2"));
        splitter_2->setOrientation(Qt::Vertical);
        label_2 = new QLabel(splitter_2);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        label_2->setFont(font);
        label_2->setFrameShape(QFrame::StyledPanel);
        label_2->setAlignment(Qt::AlignCenter);
        splitter_2->addWidget(label_2);
        groupBox_3 = new QGroupBox(splitter_2);
        groupBox_3->setObjectName(QString::fromUtf8("groupBox_3"));
        gridLayout_5 = new QGridLayout(groupBox_3);
        gridLayout_5->setObjectName(QString::fromUtf8("gridLayout_5"));
        tableWidget_3 = new QTableWidget(groupBox_3);
        tableWidget_3->setObjectName(QString::fromUtf8("tableWidget_3"));

        gridLayout_5->addWidget(tableWidget_3, 0, 1, 1, 1);

        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Minimum, QSizePolicy::Minimum);

        gridLayout_5->addItem(horizontalSpacer_2, 0, 0, 1, 1);

        horizontalSpacer_3 = new QSpacerItem(40, 20, QSizePolicy::Minimum, QSizePolicy::Minimum);

        gridLayout_5->addItem(horizontalSpacer_3, 0, 2, 1, 1);

        splitter_2->addWidget(groupBox_3);

        gridLayout_6->addWidget(splitter_2, 0, 0, 1, 1);

        tabWidget->addTab(tab_2, QString());

        gridLayout_7->addWidget(tabWidget, 0, 0, 2, 3);

        pushButton = new QPushButton(LeagueTable);
        pushButton->setObjectName(QString::fromUtf8("pushButton"));
        QFont font2;
        font2.setFamily(QString::fromUtf8("Century Gothic"));
        font2.setBold(true);
        font2.setWeight(75);
        pushButton->setFont(font2);
        pushButton->setCursor(QCursor(Qt::PointingHandCursor));

        gridLayout_7->addWidget(pushButton, 1, 1, 1, 1);

        horizontalSpacer = new QSpacerItem(606, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_7->addItem(horizontalSpacer, 1, 2, 1, 1);


        retranslateUi(LeagueTable);

        tabWidget->setCurrentIndex(0);


        QMetaObject::connectSlotsByName(LeagueTable);
    } // setupUi

    void retranslateUi(QDialog *LeagueTable)
    {
        LeagueTable->setWindowTitle(QApplication::translate("LeagueTable", "Dialog", 0, QApplication::UnicodeUTF8));
        label->setText(QApplication::translate("LeagueTable", "LEAGUE TABLE", 0, QApplication::UnicodeUTF8));
        groupBox_2->setTitle(QApplication::translate("LeagueTable", "MATCH RESULTS", 0, QApplication::UnicodeUTF8));
        groupBox->setTitle(QApplication::translate("LeagueTable", "POINT TABLE", 0, QApplication::UnicodeUTF8));
        tabWidget->setTabText(tabWidget->indexOf(tab), QApplication::translate("LeagueTable", "LEAGUE TABLE", 0, QApplication::UnicodeUTF8));
        label_2->setText(QApplication::translate("LeagueTable", "TOP SCORERS", 0, QApplication::UnicodeUTF8));
        groupBox_3->setTitle(QString());
        tabWidget->setTabText(tabWidget->indexOf(tab_2), QApplication::translate("LeagueTable", "TOP SCORERS", 0, QApplication::UnicodeUTF8));
        pushButton->setText(QApplication::translate("LeagueTable", "BACK", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class LeagueTable: public Ui_LeagueTable {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_LEAGUETABLE_H
