/********************************************************************************
** Form generated from reading UI file 'match.ui'
**
** Created: Thu Apr 19 02:12:48 2012
**      by: Qt User Interface Compiler version 4.6.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MATCH_H
#define UI_MATCH_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QDialog>
#include <QtGui/QGridLayout>
#include <QtGui/QGroupBox>
#include <QtGui/QHeaderView>
#include <QtGui/QLCDNumber>
#include <QtGui/QLabel>
#include <QtGui/QLineEdit>
#include <QtGui/QProgressBar>
#include <QtGui/QPushButton>
#include <QtGui/QSpacerItem>
#include <QtGui/QSplitter>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_Match
{
public:
    QGridLayout *gridLayout_13;
    QSplitter *splitter_4;
    QLabel *label_2;
    QSplitter *splitter_3;
    QWidget *layoutWidget;
    QGridLayout *gridLayout_12;
    QSplitter *splitter;
    QLabel *label_4;
    QWidget *layoutWidget1;
    QGridLayout *gridLayout_8;
    QGridLayout *gridLayout_5;
    QLCDNumber *lcdNumber_2;
    QLabel *label_3;
    QGridLayout *gridLayout_7;
    QLabel *HomeLabel;
    QLineEdit *lineEdit;
    QSplitter *splitter_2;
    QLabel *label_5;
    QWidget *layoutWidget2;
    QGridLayout *gridLayout_11;
    QGridLayout *gridLayout_9;
    QLabel *AwayLabel;
    QLineEdit *lineEdit_2;
    QGridLayout *gridLayout_10;
    QLCDNumber *lcdNumber_3;
    QLabel *label_6;
    QSpacerItem *horizontalSpacer;
    QWidget *layoutWidget3;
    QGridLayout *gridLayout_6;
    QSpacerItem *horizontalSpacer_2;
    QSpacerItem *horizontalSpacer_3;
    QGroupBox *groupBox;
    QGridLayout *gridLayout_2;
    QGridLayout *gridLayout;
    QLCDNumber *lcdNumber;
    QLabel *label;
    QProgressBar *progressBar;
    QSpacerItem *horizontalSpacer_4;
    QPushButton *pushButton;
    QSpacerItem *horizontalSpacer_5;

    void setupUi(QDialog *Match)
    {
        if (Match->objectName().isEmpty())
            Match->setObjectName(QString::fromUtf8("Match"));
        Match->resize(539, 317);
        gridLayout_13 = new QGridLayout(Match);
        gridLayout_13->setObjectName(QString::fromUtf8("gridLayout_13"));
        splitter_4 = new QSplitter(Match);
        splitter_4->setObjectName(QString::fromUtf8("splitter_4"));
        splitter_4->setOrientation(Qt::Vertical);
        label_2 = new QLabel(splitter_4);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        QFont font;
        font.setFamily(QString::fromUtf8("Stencil"));
        font.setPointSize(22);
        font.setBold(false);
        font.setWeight(50);
        label_2->setFont(font);
        label_2->setAlignment(Qt::AlignCenter);
        splitter_4->addWidget(label_2);
        splitter_3 = new QSplitter(splitter_4);
        splitter_3->setObjectName(QString::fromUtf8("splitter_3"));
        splitter_3->setOrientation(Qt::Vertical);
        layoutWidget = new QWidget(splitter_3);
        layoutWidget->setObjectName(QString::fromUtf8("layoutWidget"));
        gridLayout_12 = new QGridLayout(layoutWidget);
        gridLayout_12->setObjectName(QString::fromUtf8("gridLayout_12"));
        gridLayout_12->setContentsMargins(0, 0, 0, 0);
        splitter = new QSplitter(layoutWidget);
        splitter->setObjectName(QString::fromUtf8("splitter"));
        splitter->setOrientation(Qt::Vertical);
        label_4 = new QLabel(splitter);
        label_4->setObjectName(QString::fromUtf8("label_4"));
        QFont font1;
        font1.setFamily(QString::fromUtf8("Stencil"));
        font1.setPointSize(14);
        label_4->setFont(font1);
        label_4->setAlignment(Qt::AlignCenter);
        splitter->addWidget(label_4);
        layoutWidget1 = new QWidget(splitter);
        layoutWidget1->setObjectName(QString::fromUtf8("layoutWidget1"));
        gridLayout_8 = new QGridLayout(layoutWidget1);
        gridLayout_8->setObjectName(QString::fromUtf8("gridLayout_8"));
        gridLayout_8->setContentsMargins(0, 0, 0, 0);
        gridLayout_5 = new QGridLayout();
        gridLayout_5->setObjectName(QString::fromUtf8("gridLayout_5"));
        lcdNumber_2 = new QLCDNumber(layoutWidget1);
        lcdNumber_2->setObjectName(QString::fromUtf8("lcdNumber_2"));
        lcdNumber_2->setSegmentStyle(QLCDNumber::Flat);

        gridLayout_5->addWidget(lcdNumber_2, 0, 0, 1, 1);

        label_3 = new QLabel(layoutWidget1);
        label_3->setObjectName(QString::fromUtf8("label_3"));
        QFont font2;
        font2.setFamily(QString::fromUtf8("Copperplate Gothic Light"));
        font2.setPointSize(10);
        label_3->setFont(font2);
        label_3->setAlignment(Qt::AlignCenter);

        gridLayout_5->addWidget(label_3, 1, 0, 1, 1);


        gridLayout_8->addLayout(gridLayout_5, 0, 0, 1, 1);

        gridLayout_7 = new QGridLayout();
        gridLayout_7->setObjectName(QString::fromUtf8("gridLayout_7"));
        HomeLabel = new QLabel(layoutWidget1);
        HomeLabel->setObjectName(QString::fromUtf8("HomeLabel"));
        QFont font3;
        font3.setFamily(QString::fromUtf8("Copperplate Gothic Light"));
        font3.setPointSize(14);
        HomeLabel->setFont(font3);
        HomeLabel->setAlignment(Qt::AlignCenter);

        gridLayout_7->addWidget(HomeLabel, 0, 0, 1, 1);

        lineEdit = new QLineEdit(layoutWidget1);
        lineEdit->setObjectName(QString::fromUtf8("lineEdit"));

        gridLayout_7->addWidget(lineEdit, 1, 0, 1, 1);


        gridLayout_8->addLayout(gridLayout_7, 0, 1, 1, 1);

        splitter->addWidget(layoutWidget1);

        gridLayout_12->addWidget(splitter, 0, 0, 1, 1);

        splitter_2 = new QSplitter(layoutWidget);
        splitter_2->setObjectName(QString::fromUtf8("splitter_2"));
        splitter_2->setOrientation(Qt::Vertical);
        label_5 = new QLabel(splitter_2);
        label_5->setObjectName(QString::fromUtf8("label_5"));
        label_5->setFont(font1);
        label_5->setAlignment(Qt::AlignCenter);
        splitter_2->addWidget(label_5);
        layoutWidget2 = new QWidget(splitter_2);
        layoutWidget2->setObjectName(QString::fromUtf8("layoutWidget2"));
        gridLayout_11 = new QGridLayout(layoutWidget2);
        gridLayout_11->setObjectName(QString::fromUtf8("gridLayout_11"));
        gridLayout_11->setContentsMargins(0, 0, 0, 0);
        gridLayout_9 = new QGridLayout();
        gridLayout_9->setObjectName(QString::fromUtf8("gridLayout_9"));
        AwayLabel = new QLabel(layoutWidget2);
        AwayLabel->setObjectName(QString::fromUtf8("AwayLabel"));
        AwayLabel->setFont(font3);
        AwayLabel->setAlignment(Qt::AlignCenter);

        gridLayout_9->addWidget(AwayLabel, 0, 0, 1, 1);

        lineEdit_2 = new QLineEdit(layoutWidget2);
        lineEdit_2->setObjectName(QString::fromUtf8("lineEdit_2"));

        gridLayout_9->addWidget(lineEdit_2, 1, 0, 1, 1);


        gridLayout_11->addLayout(gridLayout_9, 0, 0, 1, 1);

        gridLayout_10 = new QGridLayout();
        gridLayout_10->setObjectName(QString::fromUtf8("gridLayout_10"));
        lcdNumber_3 = new QLCDNumber(layoutWidget2);
        lcdNumber_3->setObjectName(QString::fromUtf8("lcdNumber_3"));
        lcdNumber_3->setSegmentStyle(QLCDNumber::Flat);

        gridLayout_10->addWidget(lcdNumber_3, 0, 0, 1, 1);

        label_6 = new QLabel(layoutWidget2);
        label_6->setObjectName(QString::fromUtf8("label_6"));
        label_6->setFont(font2);
        label_6->setAlignment(Qt::AlignCenter);

        gridLayout_10->addWidget(label_6, 1, 0, 1, 1);


        gridLayout_11->addLayout(gridLayout_10, 0, 1, 1, 1);

        splitter_2->addWidget(layoutWidget2);

        gridLayout_12->addWidget(splitter_2, 0, 2, 1, 1);

        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Fixed, QSizePolicy::Minimum);

        gridLayout_12->addItem(horizontalSpacer, 0, 1, 1, 1);

        splitter_3->addWidget(layoutWidget);
        layoutWidget3 = new QWidget(splitter_3);
        layoutWidget3->setObjectName(QString::fromUtf8("layoutWidget3"));
        gridLayout_6 = new QGridLayout(layoutWidget3);
        gridLayout_6->setObjectName(QString::fromUtf8("gridLayout_6"));
        gridLayout_6->setContentsMargins(0, 0, 0, 0);
        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Minimum, QSizePolicy::Minimum);

        gridLayout_6->addItem(horizontalSpacer_2, 0, 0, 1, 1);

        horizontalSpacer_3 = new QSpacerItem(40, 20, QSizePolicy::Minimum, QSizePolicy::Minimum);

        gridLayout_6->addItem(horizontalSpacer_3, 0, 3, 1, 1);

        groupBox = new QGroupBox(layoutWidget3);
        groupBox->setObjectName(QString::fromUtf8("groupBox"));
        QFont font4;
        font4.setFamily(QString::fromUtf8("Copperplate Gothic Bold"));
        font4.setPointSize(16);
        groupBox->setFont(font4);
        gridLayout_2 = new QGridLayout(groupBox);
        gridLayout_2->setObjectName(QString::fromUtf8("gridLayout_2"));
        gridLayout = new QGridLayout();
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        lcdNumber = new QLCDNumber(groupBox);
        lcdNumber->setObjectName(QString::fromUtf8("lcdNumber"));
        lcdNumber->setSegmentStyle(QLCDNumber::Flat);

        gridLayout->addWidget(lcdNumber, 0, 0, 1, 1);

        label = new QLabel(groupBox);
        label->setObjectName(QString::fromUtf8("label"));
        label->setEnabled(true);
        QFont font5;
        font5.setFamily(QString::fromUtf8("Copperplate Gothic Light"));
        font5.setPointSize(16);
        label->setFont(font5);
        label->setAlignment(Qt::AlignCenter);

        gridLayout->addWidget(label, 2, 0, 1, 1);

        progressBar = new QProgressBar(groupBox);
        progressBar->setObjectName(QString::fromUtf8("progressBar"));
        progressBar->setMaximum(90);
        progressBar->setValue(0);
        progressBar->setAlignment(Qt::AlignCenter);
        progressBar->setFormat(QString::fromUtf8(""));

        gridLayout->addWidget(progressBar, 1, 0, 1, 1);


        gridLayout_2->addLayout(gridLayout, 0, 0, 1, 1);


        gridLayout_6->addWidget(groupBox, 0, 1, 1, 1);

        splitter_3->addWidget(layoutWidget3);
        splitter_4->addWidget(splitter_3);

        gridLayout_13->addWidget(splitter_4, 0, 0, 1, 3);

        horizontalSpacer_4 = new QSpacerItem(214, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_13->addItem(horizontalSpacer_4, 1, 0, 1, 1);

        pushButton = new QPushButton(Match);
        pushButton->setObjectName(QString::fromUtf8("pushButton"));
        QFont font6;
        font6.setFamily(QString::fromUtf8("Century Gothic"));
        font6.setBold(true);
        font6.setWeight(75);
        pushButton->setFont(font6);
        pushButton->setCursor(QCursor(Qt::PointingHandCursor));

        gridLayout_13->addWidget(pushButton, 1, 1, 1, 1);

        horizontalSpacer_5 = new QSpacerItem(214, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_13->addItem(horizontalSpacer_5, 1, 2, 1, 1);


        retranslateUi(Match);
        QObject::connect(progressBar, SIGNAL(valueChanged(int)), lcdNumber, SLOT(display(int)));

        QMetaObject::connectSlotsByName(Match);
    } // setupUi

    void retranslateUi(QDialog *Match)
    {
        Match->setWindowTitle(QApplication::translate("Match", "Dialog", 0, QApplication::UnicodeUTF8));
        label_2->setText(QApplication::translate("Match", "TextLabel", 0, QApplication::UnicodeUTF8));
        label_4->setText(QApplication::translate("Match", "HOME", 0, QApplication::UnicodeUTF8));
        label_3->setText(QApplication::translate("Match", "Vanue", 0, QApplication::UnicodeUTF8));
        HomeLabel->setText(QApplication::translate("Match", "TextLabel", 0, QApplication::UnicodeUTF8));
        label_5->setText(QApplication::translate("Match", "AWAY", 0, QApplication::UnicodeUTF8));
        AwayLabel->setText(QApplication::translate("Match", "TextLabel", 0, QApplication::UnicodeUTF8));
        label_6->setText(QApplication::translate("Match", "Attendance", 0, QApplication::UnicodeUTF8));
        groupBox->setTitle(QApplication::translate("Match", "MATCH", 0, QApplication::UnicodeUTF8));
        label->setText(QApplication::translate("Match", "MINUTES", 0, QApplication::UnicodeUTF8));
        pushButton->setText(QApplication::translate("Match", "Start Match", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class Match: public Ui_Match {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MATCH_H
