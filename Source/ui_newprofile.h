/********************************************************************************
** Form generated from reading UI file 'newprofile.ui'
**
** Created: Thu Apr 19 05:14:49 2012
**      by: Qt User Interface Compiler version 4.6.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_NEWPROFILE_H
#define UI_NEWPROFILE_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QDialog>
#include <QtGui/QGridLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QLineEdit>
#include <QtGui/QPushButton>
#include <QtGui/QSpacerItem>
#include <QtGui/QSplitter>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_NewProfile
{
public:
    QGridLayout *gridLayout_4;
    QSplitter *splitter_2;
    QLabel *label_4;
    QSplitter *splitter;
    QWidget *widget;
    QGridLayout *gridLayout_2;
    QLabel *label_2;
    QLabel *label_3;
    QLabel *label_5;
    QLabel *label;
    QWidget *widget1;
    QGridLayout *gridLayout_3;
    QLineEdit *namelineEdit;
    QLineEdit *nicklineEdit;
    QLineEdit *agelineEdit;
    QLineEdit *nationlineEdit;
    QWidget *widget2;
    QGridLayout *gridLayout;
    QPushButton *pushButton;
    QPushButton *pushButton_2;
    QSpacerItem *horizontalSpacer;
    QSpacerItem *horizontalSpacer_2;
    QSpacerItem *verticalSpacer;

    void setupUi(QDialog *NewProfile)
    {
        if (NewProfile->objectName().isEmpty())
            NewProfile->setObjectName(QString::fromUtf8("NewProfile"));
        NewProfile->resize(412, 381);
        gridLayout_4 = new QGridLayout(NewProfile);
        gridLayout_4->setObjectName(QString::fromUtf8("gridLayout_4"));
        splitter_2 = new QSplitter(NewProfile);
        splitter_2->setObjectName(QString::fromUtf8("splitter_2"));
        splitter_2->setOrientation(Qt::Vertical);
        label_4 = new QLabel(splitter_2);
        label_4->setObjectName(QString::fromUtf8("label_4"));
        QFont font;
        font.setFamily(QString::fromUtf8("Stencil"));
        font.setPointSize(26);
        label_4->setFont(font);
        splitter_2->addWidget(label_4);
        splitter = new QSplitter(splitter_2);
        splitter->setObjectName(QString::fromUtf8("splitter"));
        splitter->setOrientation(Qt::Horizontal);
        widget = new QWidget(splitter);
        widget->setObjectName(QString::fromUtf8("widget"));
        gridLayout_2 = new QGridLayout(widget);
        gridLayout_2->setObjectName(QString::fromUtf8("gridLayout_2"));
        gridLayout_2->setContentsMargins(0, 0, 0, 0);
        label_2 = new QLabel(widget);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        QFont font1;
        font1.setFamily(QString::fromUtf8("Copperplate Gothic Light"));
        font1.setPointSize(12);
        font1.setBold(true);
        font1.setWeight(75);
        label_2->setFont(font1);
        label_2->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout_2->addWidget(label_2, 1, 0, 1, 1);

        label_3 = new QLabel(widget);
        label_3->setObjectName(QString::fromUtf8("label_3"));
        label_3->setFont(font1);
        label_3->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout_2->addWidget(label_3, 2, 0, 1, 1);

        label_5 = new QLabel(widget);
        label_5->setObjectName(QString::fromUtf8("label_5"));
        label_5->setFont(font1);
        label_5->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout_2->addWidget(label_5, 3, 0, 1, 1);

        label = new QLabel(widget);
        label->setObjectName(QString::fromUtf8("label"));
        label->setFont(font1);
        label->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout_2->addWidget(label, 0, 0, 1, 1);

        splitter->addWidget(widget);
        widget1 = new QWidget(splitter);
        widget1->setObjectName(QString::fromUtf8("widget1"));
        gridLayout_3 = new QGridLayout(widget1);
        gridLayout_3->setObjectName(QString::fromUtf8("gridLayout_3"));
        gridLayout_3->setContentsMargins(0, 0, 0, 0);
        namelineEdit = new QLineEdit(widget1);
        namelineEdit->setObjectName(QString::fromUtf8("namelineEdit"));
        QFont font2;
        font2.setFamily(QString::fromUtf8("Century Gothic"));
        font2.setBold(true);
        font2.setWeight(75);
        namelineEdit->setFont(font2);

        gridLayout_3->addWidget(namelineEdit, 0, 0, 1, 1);

        nicklineEdit = new QLineEdit(widget1);
        nicklineEdit->setObjectName(QString::fromUtf8("nicklineEdit"));
        nicklineEdit->setFont(font2);

        gridLayout_3->addWidget(nicklineEdit, 1, 0, 1, 1);

        agelineEdit = new QLineEdit(widget1);
        agelineEdit->setObjectName(QString::fromUtf8("agelineEdit"));
        agelineEdit->setFont(font2);

        gridLayout_3->addWidget(agelineEdit, 2, 0, 1, 1);

        nationlineEdit = new QLineEdit(widget1);
        nationlineEdit->setObjectName(QString::fromUtf8("nationlineEdit"));
        nationlineEdit->setFont(font2);

        gridLayout_3->addWidget(nationlineEdit, 3, 0, 1, 1);

        splitter->addWidget(widget1);
        splitter_2->addWidget(splitter);
        widget2 = new QWidget(splitter_2);
        widget2->setObjectName(QString::fromUtf8("widget2"));
        gridLayout = new QGridLayout(widget2);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        gridLayout->setContentsMargins(0, 0, 0, 0);
        pushButton = new QPushButton(widget2);
        pushButton->setObjectName(QString::fromUtf8("pushButton"));
        pushButton->setFont(font2);
        pushButton->setCursor(QCursor(Qt::PointingHandCursor));

        gridLayout->addWidget(pushButton, 0, 1, 1, 1);

        pushButton_2 = new QPushButton(widget2);
        pushButton_2->setObjectName(QString::fromUtf8("pushButton_2"));
        pushButton_2->setFont(font2);
        pushButton_2->setCursor(QCursor(Qt::PointingHandCursor));

        gridLayout->addWidget(pushButton_2, 1, 1, 1, 1);

        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout->addItem(horizontalSpacer, 0, 0, 1, 1);

        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout->addItem(horizontalSpacer_2, 0, 2, 1, 1);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Maximum);

        gridLayout->addItem(verticalSpacer, 2, 1, 1, 1);

        splitter_2->addWidget(widget2);

        gridLayout_4->addWidget(splitter_2, 0, 0, 1, 1);


        retranslateUi(NewProfile);

        QMetaObject::connectSlotsByName(NewProfile);
    } // setupUi

    void retranslateUi(QDialog *NewProfile)
    {
        NewProfile->setWindowTitle(QApplication::translate("NewProfile", "Dialog", 0, QApplication::UnicodeUTF8));
        label_4->setText(QApplication::translate("NewProfile", "New Profile", 0, QApplication::UnicodeUTF8));
        label_2->setText(QApplication::translate("NewProfile", "Profile Name", 0, QApplication::UnicodeUTF8));
        label_3->setText(QApplication::translate("NewProfile", "Age", 0, QApplication::UnicodeUTF8));
        label_5->setText(QApplication::translate("NewProfile", "Nationality", 0, QApplication::UnicodeUTF8));
        label->setText(QApplication::translate("NewProfile", "Name", 0, QApplication::UnicodeUTF8));
        pushButton->setText(QApplication::translate("NewProfile", "Create", 0, QApplication::UnicodeUTF8));
        pushButton_2->setText(QApplication::translate("NewProfile", "Cancel", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class NewProfile: public Ui_NewProfile {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_NEWPROFILE_H
