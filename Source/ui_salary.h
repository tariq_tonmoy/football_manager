/********************************************************************************
** Form generated from reading UI file 'salary.ui'
**
** Created: Thu Apr 19 10:27:40 2012
**      by: Qt User Interface Compiler version 4.6.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_SALARY_H
#define UI_SALARY_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QDialog>
#include <QtGui/QFormLayout>
#include <QtGui/QGridLayout>
#include <QtGui/QGroupBox>
#include <QtGui/QHeaderView>
#include <QtGui/QLCDNumber>
#include <QtGui/QLabel>
#include <QtGui/QLineEdit>
#include <QtGui/QPushButton>
#include <QtGui/QSlider>
#include <QtGui/QSpacerItem>
#include <QtGui/QSpinBox>
#include <QtGui/QSplitter>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_Salary
{
public:
    QGridLayout *gridLayout_9;
    QSplitter *splitter;
    QLabel *label_2;
    QWidget *layoutWidget;
    QGridLayout *gridLayout_8;
    QGroupBox *groupBox_2;
    QGridLayout *gridLayout_7;
    QFormLayout *formLayout;
    QLabel *label;
    QLineEdit *lineEdit;
    QFormLayout *formLayout_2;
    QLabel *label_6;
    QLineEdit *lineEdit_2;
    QGridLayout *gridLayout_5;
    QLabel *label_3;
    QSpinBox *spinBox;
    QSlider *verticalSlider;
    QGridLayout *gridLayout_6;
    QLabel *label_4;
    QSpinBox *spinBox_2;
    QSlider *verticalSlider_2;
    QPushButton *pushButton;
    QSpacerItem *horizontalSpacer;
    QGroupBox *groupBox;
    QGridLayout *gridLayout_4;
    QGridLayout *gridLayout_3;
    QGridLayout *gridLayout;
    QGridLayout *gridLayout_14;
    QLabel *label_133;
    QLabel *label_134;
    QLabel *label_135;
    QLabel *label_136;
    QLabel *label_137;
    QLabel *label_138;
    QLabel *label_139;
    QLabel *label_140;
    QLabel *label_141;
    QLabel *label_142;
    QLabel *label_143;
    QLabel *label_144;
    QLabel *label_145;
    QLabel *label_146;
    QLabel *label_147;
    QLabel *label_132;
    QGridLayout *gridLayout_12;
    QLabel *label_100;
    QLabel *label_101;
    QLabel *label_102;
    QLabel *label_103;
    QLabel *label_104;
    QLabel *label_105;
    QLabel *label_106;
    QLabel *label_107;
    QLabel *label_108;
    QLabel *label_109;
    QLabel *label_110;
    QLabel *label_111;
    QLabel *label_112;
    QLabel *label_113;
    QLabel *label_114;
    QLabel *label_115;
    QGridLayout *gridLayout_13;
    QLabel *label_116;
    QLabel *label_117;
    QLabel *label_118;
    QLabel *label_119;
    QLabel *label_120;
    QLabel *label_121;
    QLabel *label_122;
    QLabel *label_123;
    QLabel *label_124;
    QLabel *label_125;
    QLabel *label_126;
    QLabel *label_127;
    QLabel *label_128;
    QLabel *label_129;
    QLabel *label_130;
    QLabel *label_131;
    QGridLayout *gridLayout_2;
    QLCDNumber *lcdNumber;
    QLCDNumber *lcdNumber_2;
    QLCDNumber *lcdNumber_3;
    QLCDNumber *lcdNumber_4;
    QLCDNumber *lcdNumber_5;
    QLCDNumber *lcdNumber_6;
    QLCDNumber *lcdNumber_7;
    QLCDNumber *lcdNumber_8;
    QLCDNumber *lcdNumber_9;
    QLCDNumber *lcdNumber_10;
    QLCDNumber *lcdNumber_11;
    QLCDNumber *lcdNumber_12;
    QLCDNumber *lcdNumber_13;
    QLCDNumber *lcdNumber_14;
    QLCDNumber *lcdNumber_15;
    QLabel *label_5;

    void setupUi(QDialog *Salary)
    {
        if (Salary->objectName().isEmpty())
            Salary->setObjectName(QString::fromUtf8("Salary"));
        Salary->resize(702, 574);
        gridLayout_9 = new QGridLayout(Salary);
        gridLayout_9->setObjectName(QString::fromUtf8("gridLayout_9"));
        splitter = new QSplitter(Salary);
        splitter->setObjectName(QString::fromUtf8("splitter"));
        splitter->setOrientation(Qt::Vertical);
        label_2 = new QLabel(splitter);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        QFont font;
        font.setFamily(QString::fromUtf8("Stencil"));
        font.setPointSize(22);
        label_2->setFont(font);
        label_2->setAlignment(Qt::AlignCenter);
        splitter->addWidget(label_2);
        layoutWidget = new QWidget(splitter);
        layoutWidget->setObjectName(QString::fromUtf8("layoutWidget"));
        gridLayout_8 = new QGridLayout(layoutWidget);
        gridLayout_8->setObjectName(QString::fromUtf8("gridLayout_8"));
        gridLayout_8->setContentsMargins(0, 0, 0, 0);
        groupBox_2 = new QGroupBox(layoutWidget);
        groupBox_2->setObjectName(QString::fromUtf8("groupBox_2"));
        QFont font1;
        font1.setFamily(QString::fromUtf8("Stencil"));
        font1.setPointSize(10);
        groupBox_2->setFont(font1);
        gridLayout_7 = new QGridLayout(groupBox_2);
        gridLayout_7->setObjectName(QString::fromUtf8("gridLayout_7"));
        formLayout = new QFormLayout();
        formLayout->setObjectName(QString::fromUtf8("formLayout"));
        label = new QLabel(groupBox_2);
        label->setObjectName(QString::fromUtf8("label"));
        QFont font2;
        font2.setFamily(QString::fromUtf8("Copperplate Gothic Light"));
        font2.setPointSize(8);
        label->setFont(font2);

        formLayout->setWidget(0, QFormLayout::LabelRole, label);

        lineEdit = new QLineEdit(groupBox_2);
        lineEdit->setObjectName(QString::fromUtf8("lineEdit"));
        QFont font3;
        font3.setFamily(QString::fromUtf8("Century Gothic"));
        font3.setPointSize(8);
        font3.setBold(true);
        font3.setWeight(75);
        lineEdit->setFont(font3);

        formLayout->setWidget(0, QFormLayout::FieldRole, lineEdit);


        gridLayout_7->addLayout(formLayout, 0, 0, 1, 2);

        formLayout_2 = new QFormLayout();
        formLayout_2->setObjectName(QString::fromUtf8("formLayout_2"));
        label_6 = new QLabel(groupBox_2);
        label_6->setObjectName(QString::fromUtf8("label_6"));
        label_6->setFont(font2);

        formLayout_2->setWidget(0, QFormLayout::LabelRole, label_6);

        lineEdit_2 = new QLineEdit(groupBox_2);
        lineEdit_2->setObjectName(QString::fromUtf8("lineEdit_2"));
        lineEdit_2->setFont(font3);

        formLayout_2->setWidget(0, QFormLayout::FieldRole, lineEdit_2);


        gridLayout_7->addLayout(formLayout_2, 1, 0, 1, 2);

        gridLayout_5 = new QGridLayout();
        gridLayout_5->setObjectName(QString::fromUtf8("gridLayout_5"));
        label_3 = new QLabel(groupBox_2);
        label_3->setObjectName(QString::fromUtf8("label_3"));
        QFont font4;
        font4.setFamily(QString::fromUtf8("Copperplate Gothic Light"));
        font4.setPointSize(10);
        label_3->setFont(font4);

        gridLayout_5->addWidget(label_3, 0, 0, 1, 1);

        spinBox = new QSpinBox(groupBox_2);
        spinBox->setObjectName(QString::fromUtf8("spinBox"));
        QFont font5;
        font5.setFamily(QString::fromUtf8("Century Gothic"));
        font5.setPointSize(8);
        spinBox->setFont(font5);

        gridLayout_5->addWidget(spinBox, 1, 0, 1, 1);

        verticalSlider = new QSlider(groupBox_2);
        verticalSlider->setObjectName(QString::fromUtf8("verticalSlider"));
        verticalSlider->setCursor(QCursor(Qt::PointingHandCursor));
        verticalSlider->setMaximum(10);
        verticalSlider->setOrientation(Qt::Vertical);

        gridLayout_5->addWidget(verticalSlider, 2, 0, 1, 1);


        gridLayout_7->addLayout(gridLayout_5, 2, 0, 1, 1);

        gridLayout_6 = new QGridLayout();
        gridLayout_6->setObjectName(QString::fromUtf8("gridLayout_6"));
        label_4 = new QLabel(groupBox_2);
        label_4->setObjectName(QString::fromUtf8("label_4"));
        label_4->setFont(font4);

        gridLayout_6->addWidget(label_4, 0, 0, 1, 1);

        spinBox_2 = new QSpinBox(groupBox_2);
        spinBox_2->setObjectName(QString::fromUtf8("spinBox_2"));
        spinBox_2->setFont(font5);
        spinBox_2->setMaximum(10);
        spinBox_2->setSingleStep(1);

        gridLayout_6->addWidget(spinBox_2, 1, 0, 1, 1);

        verticalSlider_2 = new QSlider(groupBox_2);
        verticalSlider_2->setObjectName(QString::fromUtf8("verticalSlider_2"));
        verticalSlider_2->setCursor(QCursor(Qt::PointingHandCursor));
        verticalSlider_2->setMaximum(10);
        verticalSlider_2->setPageStep(10);
        verticalSlider_2->setOrientation(Qt::Vertical);

        gridLayout_6->addWidget(verticalSlider_2, 2, 0, 1, 1);


        gridLayout_7->addLayout(gridLayout_6, 2, 1, 1, 1);

        pushButton = new QPushButton(groupBox_2);
        pushButton->setObjectName(QString::fromUtf8("pushButton"));
        pushButton->setFont(font3);
        pushButton->setCursor(QCursor(Qt::PointingHandCursor));

        gridLayout_7->addWidget(pushButton, 3, 0, 1, 1);

        horizontalSpacer = new QSpacerItem(179, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_7->addItem(horizontalSpacer, 3, 1, 1, 1);


        gridLayout_8->addWidget(groupBox_2, 0, 0, 1, 1);

        groupBox = new QGroupBox(layoutWidget);
        groupBox->setObjectName(QString::fromUtf8("groupBox"));
        groupBox->setFont(font1);
        gridLayout_4 = new QGridLayout(groupBox);
        gridLayout_4->setObjectName(QString::fromUtf8("gridLayout_4"));
        gridLayout_3 = new QGridLayout();
        gridLayout_3->setObjectName(QString::fromUtf8("gridLayout_3"));
        gridLayout = new QGridLayout();
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        gridLayout_14 = new QGridLayout();
        gridLayout_14->setObjectName(QString::fromUtf8("gridLayout_14"));
        label_133 = new QLabel(groupBox);
        label_133->setObjectName(QString::fromUtf8("label_133"));
        QFont font6;
        font6.setFamily(QString::fromUtf8("Century Gothic"));
        font6.setPointSize(10);
        label_133->setFont(font6);
        label_133->setAlignment(Qt::AlignCenter);

        gridLayout_14->addWidget(label_133, 1, 0, 1, 1);

        label_134 = new QLabel(groupBox);
        label_134->setObjectName(QString::fromUtf8("label_134"));
        label_134->setFont(font6);
        label_134->setAlignment(Qt::AlignCenter);

        gridLayout_14->addWidget(label_134, 2, 0, 1, 1);

        label_135 = new QLabel(groupBox);
        label_135->setObjectName(QString::fromUtf8("label_135"));
        label_135->setFont(font6);
        label_135->setAlignment(Qt::AlignCenter);

        gridLayout_14->addWidget(label_135, 3, 0, 1, 1);

        label_136 = new QLabel(groupBox);
        label_136->setObjectName(QString::fromUtf8("label_136"));
        label_136->setFont(font6);
        label_136->setAlignment(Qt::AlignCenter);

        gridLayout_14->addWidget(label_136, 4, 0, 1, 1);

        label_137 = new QLabel(groupBox);
        label_137->setObjectName(QString::fromUtf8("label_137"));
        label_137->setFont(font6);
        label_137->setAlignment(Qt::AlignCenter);

        gridLayout_14->addWidget(label_137, 5, 0, 1, 1);

        label_138 = new QLabel(groupBox);
        label_138->setObjectName(QString::fromUtf8("label_138"));
        label_138->setFont(font6);
        label_138->setAlignment(Qt::AlignCenter);

        gridLayout_14->addWidget(label_138, 6, 0, 1, 1);

        label_139 = new QLabel(groupBox);
        label_139->setObjectName(QString::fromUtf8("label_139"));
        label_139->setFont(font6);
        label_139->setAlignment(Qt::AlignCenter);

        gridLayout_14->addWidget(label_139, 7, 0, 1, 1);

        label_140 = new QLabel(groupBox);
        label_140->setObjectName(QString::fromUtf8("label_140"));
        label_140->setFont(font6);
        label_140->setAlignment(Qt::AlignCenter);

        gridLayout_14->addWidget(label_140, 8, 0, 1, 1);

        label_141 = new QLabel(groupBox);
        label_141->setObjectName(QString::fromUtf8("label_141"));
        label_141->setFont(font6);
        label_141->setAlignment(Qt::AlignCenter);

        gridLayout_14->addWidget(label_141, 9, 0, 1, 1);

        label_142 = new QLabel(groupBox);
        label_142->setObjectName(QString::fromUtf8("label_142"));
        label_142->setFont(font6);
        label_142->setAlignment(Qt::AlignCenter);

        gridLayout_14->addWidget(label_142, 10, 0, 1, 1);

        label_143 = new QLabel(groupBox);
        label_143->setObjectName(QString::fromUtf8("label_143"));
        label_143->setFont(font6);
        label_143->setAlignment(Qt::AlignCenter);

        gridLayout_14->addWidget(label_143, 11, 0, 1, 1);

        label_144 = new QLabel(groupBox);
        label_144->setObjectName(QString::fromUtf8("label_144"));
        label_144->setFont(font6);
        label_144->setAlignment(Qt::AlignCenter);

        gridLayout_14->addWidget(label_144, 12, 0, 1, 1);

        label_145 = new QLabel(groupBox);
        label_145->setObjectName(QString::fromUtf8("label_145"));
        label_145->setFont(font6);
        label_145->setAlignment(Qt::AlignCenter);

        gridLayout_14->addWidget(label_145, 13, 0, 1, 1);

        label_146 = new QLabel(groupBox);
        label_146->setObjectName(QString::fromUtf8("label_146"));
        label_146->setFont(font6);
        label_146->setAlignment(Qt::AlignCenter);

        gridLayout_14->addWidget(label_146, 14, 0, 1, 1);

        label_147 = new QLabel(groupBox);
        label_147->setObjectName(QString::fromUtf8("label_147"));
        label_147->setFont(font6);
        label_147->setAlignment(Qt::AlignCenter);

        gridLayout_14->addWidget(label_147, 15, 0, 1, 1);

        label_132 = new QLabel(groupBox);
        label_132->setObjectName(QString::fromUtf8("label_132"));
        QFont font7;
        font7.setFamily(QString::fromUtf8("Copperplate Gothic Light"));
        font7.setPointSize(12);
        font7.setBold(true);
        font7.setWeight(75);
        label_132->setFont(font7);
        label_132->setAlignment(Qt::AlignCenter);

        gridLayout_14->addWidget(label_132, 0, 0, 1, 1);


        gridLayout->addLayout(gridLayout_14, 0, 0, 1, 1);

        gridLayout_12 = new QGridLayout();
        gridLayout_12->setObjectName(QString::fromUtf8("gridLayout_12"));
        label_100 = new QLabel(groupBox);
        label_100->setObjectName(QString::fromUtf8("label_100"));
        QFont font8;
        font8.setFamily(QString::fromUtf8("Copperplate Gothic Light"));
        font8.setPointSize(10);
        font8.setBold(true);
        font8.setWeight(75);
        label_100->setFont(font8);
        label_100->setAlignment(Qt::AlignCenter);

        gridLayout_12->addWidget(label_100, 0, 0, 1, 1);

        label_101 = new QLabel(groupBox);
        label_101->setObjectName(QString::fromUtf8("label_101"));
        label_101->setFont(font6);
        label_101->setAlignment(Qt::AlignCenter);

        gridLayout_12->addWidget(label_101, 1, 0, 1, 1);

        label_102 = new QLabel(groupBox);
        label_102->setObjectName(QString::fromUtf8("label_102"));
        label_102->setFont(font6);
        label_102->setAlignment(Qt::AlignCenter);

        gridLayout_12->addWidget(label_102, 2, 0, 1, 1);

        label_103 = new QLabel(groupBox);
        label_103->setObjectName(QString::fromUtf8("label_103"));
        label_103->setFont(font6);
        label_103->setAlignment(Qt::AlignCenter);

        gridLayout_12->addWidget(label_103, 3, 0, 1, 1);

        label_104 = new QLabel(groupBox);
        label_104->setObjectName(QString::fromUtf8("label_104"));
        label_104->setFont(font6);
        label_104->setAlignment(Qt::AlignCenter);

        gridLayout_12->addWidget(label_104, 4, 0, 1, 1);

        label_105 = new QLabel(groupBox);
        label_105->setObjectName(QString::fromUtf8("label_105"));
        label_105->setFont(font6);
        label_105->setAlignment(Qt::AlignCenter);

        gridLayout_12->addWidget(label_105, 5, 0, 1, 1);

        label_106 = new QLabel(groupBox);
        label_106->setObjectName(QString::fromUtf8("label_106"));
        label_106->setFont(font6);
        label_106->setAlignment(Qt::AlignCenter);

        gridLayout_12->addWidget(label_106, 6, 0, 1, 1);

        label_107 = new QLabel(groupBox);
        label_107->setObjectName(QString::fromUtf8("label_107"));
        label_107->setFont(font6);
        label_107->setAlignment(Qt::AlignCenter);

        gridLayout_12->addWidget(label_107, 7, 0, 1, 1);

        label_108 = new QLabel(groupBox);
        label_108->setObjectName(QString::fromUtf8("label_108"));
        label_108->setFont(font6);
        label_108->setAlignment(Qt::AlignCenter);

        gridLayout_12->addWidget(label_108, 8, 0, 1, 1);

        label_109 = new QLabel(groupBox);
        label_109->setObjectName(QString::fromUtf8("label_109"));
        label_109->setFont(font6);
        label_109->setAlignment(Qt::AlignCenter);

        gridLayout_12->addWidget(label_109, 9, 0, 1, 1);

        label_110 = new QLabel(groupBox);
        label_110->setObjectName(QString::fromUtf8("label_110"));
        label_110->setFont(font6);
        label_110->setAlignment(Qt::AlignCenter);

        gridLayout_12->addWidget(label_110, 10, 0, 1, 1);

        label_111 = new QLabel(groupBox);
        label_111->setObjectName(QString::fromUtf8("label_111"));
        label_111->setFont(font6);
        label_111->setAlignment(Qt::AlignCenter);

        gridLayout_12->addWidget(label_111, 11, 0, 1, 1);

        label_112 = new QLabel(groupBox);
        label_112->setObjectName(QString::fromUtf8("label_112"));
        label_112->setFont(font6);
        label_112->setAlignment(Qt::AlignCenter);

        gridLayout_12->addWidget(label_112, 12, 0, 1, 1);

        label_113 = new QLabel(groupBox);
        label_113->setObjectName(QString::fromUtf8("label_113"));
        label_113->setFont(font6);
        label_113->setAlignment(Qt::AlignCenter);

        gridLayout_12->addWidget(label_113, 13, 0, 1, 1);

        label_114 = new QLabel(groupBox);
        label_114->setObjectName(QString::fromUtf8("label_114"));
        label_114->setFont(font6);
        label_114->setAlignment(Qt::AlignCenter);

        gridLayout_12->addWidget(label_114, 14, 0, 1, 1);

        label_115 = new QLabel(groupBox);
        label_115->setObjectName(QString::fromUtf8("label_115"));
        label_115->setFont(font6);
        label_115->setAlignment(Qt::AlignCenter);

        gridLayout_12->addWidget(label_115, 15, 0, 1, 1);


        gridLayout->addLayout(gridLayout_12, 0, 1, 1, 1);

        gridLayout_13 = new QGridLayout();
        gridLayout_13->setObjectName(QString::fromUtf8("gridLayout_13"));
        label_116 = new QLabel(groupBox);
        label_116->setObjectName(QString::fromUtf8("label_116"));
        label_116->setFont(font8);
        label_116->setAlignment(Qt::AlignCenter);

        gridLayout_13->addWidget(label_116, 0, 0, 1, 1);

        label_117 = new QLabel(groupBox);
        label_117->setObjectName(QString::fromUtf8("label_117"));
        label_117->setFont(font6);
        label_117->setAlignment(Qt::AlignCenter);

        gridLayout_13->addWidget(label_117, 1, 0, 1, 1);

        label_118 = new QLabel(groupBox);
        label_118->setObjectName(QString::fromUtf8("label_118"));
        label_118->setFont(font6);
        label_118->setAlignment(Qt::AlignCenter);

        gridLayout_13->addWidget(label_118, 2, 0, 1, 1);

        label_119 = new QLabel(groupBox);
        label_119->setObjectName(QString::fromUtf8("label_119"));
        label_119->setFont(font6);
        label_119->setAlignment(Qt::AlignCenter);

        gridLayout_13->addWidget(label_119, 3, 0, 1, 1);

        label_120 = new QLabel(groupBox);
        label_120->setObjectName(QString::fromUtf8("label_120"));
        label_120->setFont(font6);
        label_120->setAlignment(Qt::AlignCenter);

        gridLayout_13->addWidget(label_120, 4, 0, 1, 1);

        label_121 = new QLabel(groupBox);
        label_121->setObjectName(QString::fromUtf8("label_121"));
        label_121->setFont(font6);
        label_121->setAlignment(Qt::AlignCenter);

        gridLayout_13->addWidget(label_121, 5, 0, 1, 1);

        label_122 = new QLabel(groupBox);
        label_122->setObjectName(QString::fromUtf8("label_122"));
        label_122->setFont(font6);
        label_122->setAlignment(Qt::AlignCenter);

        gridLayout_13->addWidget(label_122, 6, 0, 1, 1);

        label_123 = new QLabel(groupBox);
        label_123->setObjectName(QString::fromUtf8("label_123"));
        label_123->setFont(font6);
        label_123->setAlignment(Qt::AlignCenter);

        gridLayout_13->addWidget(label_123, 7, 0, 1, 1);

        label_124 = new QLabel(groupBox);
        label_124->setObjectName(QString::fromUtf8("label_124"));
        label_124->setFont(font6);
        label_124->setAlignment(Qt::AlignCenter);

        gridLayout_13->addWidget(label_124, 8, 0, 1, 1);

        label_125 = new QLabel(groupBox);
        label_125->setObjectName(QString::fromUtf8("label_125"));
        label_125->setFont(font6);
        label_125->setAlignment(Qt::AlignCenter);

        gridLayout_13->addWidget(label_125, 9, 0, 1, 1);

        label_126 = new QLabel(groupBox);
        label_126->setObjectName(QString::fromUtf8("label_126"));
        label_126->setFont(font6);
        label_126->setAlignment(Qt::AlignCenter);

        gridLayout_13->addWidget(label_126, 10, 0, 1, 1);

        label_127 = new QLabel(groupBox);
        label_127->setObjectName(QString::fromUtf8("label_127"));
        label_127->setFont(font6);
        label_127->setAlignment(Qt::AlignCenter);

        gridLayout_13->addWidget(label_127, 11, 0, 1, 1);

        label_128 = new QLabel(groupBox);
        label_128->setObjectName(QString::fromUtf8("label_128"));
        label_128->setFont(font6);
        label_128->setAlignment(Qt::AlignCenter);

        gridLayout_13->addWidget(label_128, 12, 0, 1, 1);

        label_129 = new QLabel(groupBox);
        label_129->setObjectName(QString::fromUtf8("label_129"));
        label_129->setFont(font6);
        label_129->setAlignment(Qt::AlignCenter);

        gridLayout_13->addWidget(label_129, 13, 0, 1, 1);

        label_130 = new QLabel(groupBox);
        label_130->setObjectName(QString::fromUtf8("label_130"));
        label_130->setFont(font6);
        label_130->setAlignment(Qt::AlignCenter);

        gridLayout_13->addWidget(label_130, 14, 0, 1, 1);

        label_131 = new QLabel(groupBox);
        label_131->setObjectName(QString::fromUtf8("label_131"));
        label_131->setFont(font6);
        label_131->setAlignment(Qt::AlignCenter);

        gridLayout_13->addWidget(label_131, 15, 0, 1, 1);


        gridLayout->addLayout(gridLayout_13, 0, 2, 1, 1);


        gridLayout_3->addLayout(gridLayout, 0, 0, 1, 1);

        gridLayout_2 = new QGridLayout();
        gridLayout_2->setObjectName(QString::fromUtf8("gridLayout_2"));
        lcdNumber = new QLCDNumber(groupBox);
        lcdNumber->setObjectName(QString::fromUtf8("lcdNumber"));
        lcdNumber->setLayoutDirection(Qt::LeftToRight);
        lcdNumber->setStyleSheet(QString::fromUtf8("border-color: qradialgradient(spread:repeat, cx:0.5, cy:0.5, radius:0.077, fx:0.5, fy:0.5, stop:0 rgba(0, 169, 255, 147), stop:0.497326 rgba(0, 0, 0, 147), stop:1 rgba(0, 169, 255, 147));"));
        lcdNumber->setFrameShape(QFrame::Box);
        lcdNumber->setFrameShadow(QFrame::Sunken);
        lcdNumber->setDigitCount(7);
        lcdNumber->setSegmentStyle(QLCDNumber::Flat);

        gridLayout_2->addWidget(lcdNumber, 1, 0, 1, 1);

        lcdNumber_2 = new QLCDNumber(groupBox);
        lcdNumber_2->setObjectName(QString::fromUtf8("lcdNumber_2"));
        lcdNumber_2->setLayoutDirection(Qt::LeftToRight);
        lcdNumber_2->setStyleSheet(QString::fromUtf8("border-color: qradialgradient(spread:repeat, cx:0.5, cy:0.5, radius:0.077, fx:0.5, fy:0.5, stop:0 rgba(0, 169, 255, 147), stop:0.497326 rgba(0, 0, 0, 147), stop:1 rgba(0, 169, 255, 147));"));
        lcdNumber_2->setFrameShape(QFrame::Box);
        lcdNumber_2->setFrameShadow(QFrame::Sunken);
        lcdNumber_2->setDigitCount(7);
        lcdNumber_2->setSegmentStyle(QLCDNumber::Flat);

        gridLayout_2->addWidget(lcdNumber_2, 2, 0, 1, 1);

        lcdNumber_3 = new QLCDNumber(groupBox);
        lcdNumber_3->setObjectName(QString::fromUtf8("lcdNumber_3"));
        lcdNumber_3->setLayoutDirection(Qt::LeftToRight);
        lcdNumber_3->setStyleSheet(QString::fromUtf8("border-color: qradialgradient(spread:repeat, cx:0.5, cy:0.5, radius:0.077, fx:0.5, fy:0.5, stop:0 rgba(0, 169, 255, 147), stop:0.497326 rgba(0, 0, 0, 147), stop:1 rgba(0, 169, 255, 147));"));
        lcdNumber_3->setFrameShape(QFrame::Box);
        lcdNumber_3->setFrameShadow(QFrame::Sunken);
        lcdNumber_3->setDigitCount(7);
        lcdNumber_3->setSegmentStyle(QLCDNumber::Flat);

        gridLayout_2->addWidget(lcdNumber_3, 3, 0, 1, 1);

        lcdNumber_4 = new QLCDNumber(groupBox);
        lcdNumber_4->setObjectName(QString::fromUtf8("lcdNumber_4"));
        lcdNumber_4->setLayoutDirection(Qt::LeftToRight);
        lcdNumber_4->setStyleSheet(QString::fromUtf8("border-color: qradialgradient(spread:repeat, cx:0.5, cy:0.5, radius:0.077, fx:0.5, fy:0.5, stop:0 rgba(0, 169, 255, 147), stop:0.497326 rgba(0, 0, 0, 147), stop:1 rgba(0, 169, 255, 147));"));
        lcdNumber_4->setFrameShape(QFrame::Box);
        lcdNumber_4->setFrameShadow(QFrame::Sunken);
        lcdNumber_4->setDigitCount(7);
        lcdNumber_4->setSegmentStyle(QLCDNumber::Flat);

        gridLayout_2->addWidget(lcdNumber_4, 4, 0, 1, 1);

        lcdNumber_5 = new QLCDNumber(groupBox);
        lcdNumber_5->setObjectName(QString::fromUtf8("lcdNumber_5"));
        lcdNumber_5->setLayoutDirection(Qt::LeftToRight);
        lcdNumber_5->setStyleSheet(QString::fromUtf8("border-color: qradialgradient(spread:repeat, cx:0.5, cy:0.5, radius:0.077, fx:0.5, fy:0.5, stop:0 rgba(0, 169, 255, 147), stop:0.497326 rgba(0, 0, 0, 147), stop:1 rgba(0, 169, 255, 147));"));
        lcdNumber_5->setFrameShape(QFrame::Box);
        lcdNumber_5->setFrameShadow(QFrame::Sunken);
        lcdNumber_5->setDigitCount(7);
        lcdNumber_5->setSegmentStyle(QLCDNumber::Flat);

        gridLayout_2->addWidget(lcdNumber_5, 5, 0, 1, 1);

        lcdNumber_6 = new QLCDNumber(groupBox);
        lcdNumber_6->setObjectName(QString::fromUtf8("lcdNumber_6"));
        lcdNumber_6->setLayoutDirection(Qt::LeftToRight);
        lcdNumber_6->setStyleSheet(QString::fromUtf8("border-color: qradialgradient(spread:repeat, cx:0.5, cy:0.5, radius:0.077, fx:0.5, fy:0.5, stop:0 rgba(0, 169, 255, 147), stop:0.497326 rgba(0, 0, 0, 147), stop:1 rgba(0, 169, 255, 147));"));
        lcdNumber_6->setFrameShape(QFrame::Box);
        lcdNumber_6->setFrameShadow(QFrame::Sunken);
        lcdNumber_6->setDigitCount(7);
        lcdNumber_6->setSegmentStyle(QLCDNumber::Flat);

        gridLayout_2->addWidget(lcdNumber_6, 6, 0, 1, 1);

        lcdNumber_7 = new QLCDNumber(groupBox);
        lcdNumber_7->setObjectName(QString::fromUtf8("lcdNumber_7"));
        lcdNumber_7->setLayoutDirection(Qt::LeftToRight);
        lcdNumber_7->setStyleSheet(QString::fromUtf8("border-color: qradialgradient(spread:repeat, cx:0.5, cy:0.5, radius:0.077, fx:0.5, fy:0.5, stop:0 rgba(0, 169, 255, 147), stop:0.497326 rgba(0, 0, 0, 147), stop:1 rgba(0, 169, 255, 147));"));
        lcdNumber_7->setFrameShape(QFrame::Box);
        lcdNumber_7->setFrameShadow(QFrame::Sunken);
        lcdNumber_7->setDigitCount(7);
        lcdNumber_7->setSegmentStyle(QLCDNumber::Flat);

        gridLayout_2->addWidget(lcdNumber_7, 7, 0, 1, 1);

        lcdNumber_8 = new QLCDNumber(groupBox);
        lcdNumber_8->setObjectName(QString::fromUtf8("lcdNumber_8"));
        lcdNumber_8->setLayoutDirection(Qt::LeftToRight);
        lcdNumber_8->setStyleSheet(QString::fromUtf8("border-color: qradialgradient(spread:repeat, cx:0.5, cy:0.5, radius:0.077, fx:0.5, fy:0.5, stop:0 rgba(0, 169, 255, 147), stop:0.497326 rgba(0, 0, 0, 147), stop:1 rgba(0, 169, 255, 147));"));
        lcdNumber_8->setFrameShape(QFrame::Box);
        lcdNumber_8->setFrameShadow(QFrame::Sunken);
        lcdNumber_8->setDigitCount(7);
        lcdNumber_8->setSegmentStyle(QLCDNumber::Flat);

        gridLayout_2->addWidget(lcdNumber_8, 8, 0, 1, 1);

        lcdNumber_9 = new QLCDNumber(groupBox);
        lcdNumber_9->setObjectName(QString::fromUtf8("lcdNumber_9"));
        lcdNumber_9->setLayoutDirection(Qt::LeftToRight);
        lcdNumber_9->setStyleSheet(QString::fromUtf8("border-color: qradialgradient(spread:repeat, cx:0.5, cy:0.5, radius:0.077, fx:0.5, fy:0.5, stop:0 rgba(0, 169, 255, 147), stop:0.497326 rgba(0, 0, 0, 147), stop:1 rgba(0, 169, 255, 147));"));
        lcdNumber_9->setFrameShape(QFrame::Box);
        lcdNumber_9->setFrameShadow(QFrame::Sunken);
        lcdNumber_9->setDigitCount(7);
        lcdNumber_9->setSegmentStyle(QLCDNumber::Flat);

        gridLayout_2->addWidget(lcdNumber_9, 9, 0, 1, 1);

        lcdNumber_10 = new QLCDNumber(groupBox);
        lcdNumber_10->setObjectName(QString::fromUtf8("lcdNumber_10"));
        lcdNumber_10->setLayoutDirection(Qt::LeftToRight);
        lcdNumber_10->setStyleSheet(QString::fromUtf8("border-color: qradialgradient(spread:repeat, cx:0.5, cy:0.5, radius:0.077, fx:0.5, fy:0.5, stop:0 rgba(0, 169, 255, 147), stop:0.497326 rgba(0, 0, 0, 147), stop:1 rgba(0, 169, 255, 147));"));
        lcdNumber_10->setFrameShape(QFrame::Box);
        lcdNumber_10->setFrameShadow(QFrame::Sunken);
        lcdNumber_10->setDigitCount(7);
        lcdNumber_10->setSegmentStyle(QLCDNumber::Flat);

        gridLayout_2->addWidget(lcdNumber_10, 10, 0, 1, 1);

        lcdNumber_11 = new QLCDNumber(groupBox);
        lcdNumber_11->setObjectName(QString::fromUtf8("lcdNumber_11"));
        lcdNumber_11->setLayoutDirection(Qt::LeftToRight);
        lcdNumber_11->setStyleSheet(QString::fromUtf8("border-color: qradialgradient(spread:repeat, cx:0.5, cy:0.5, radius:0.077, fx:0.5, fy:0.5, stop:0 rgba(0, 169, 255, 147), stop:0.497326 rgba(0, 0, 0, 147), stop:1 rgba(0, 169, 255, 147));"));
        lcdNumber_11->setFrameShape(QFrame::Box);
        lcdNumber_11->setFrameShadow(QFrame::Sunken);
        lcdNumber_11->setDigitCount(7);
        lcdNumber_11->setSegmentStyle(QLCDNumber::Flat);

        gridLayout_2->addWidget(lcdNumber_11, 11, 0, 1, 1);

        lcdNumber_12 = new QLCDNumber(groupBox);
        lcdNumber_12->setObjectName(QString::fromUtf8("lcdNumber_12"));
        lcdNumber_12->setLayoutDirection(Qt::LeftToRight);
        lcdNumber_12->setStyleSheet(QString::fromUtf8("border-color: qradialgradient(spread:repeat, cx:0.5, cy:0.5, radius:0.077, fx:0.5, fy:0.5, stop:0 rgba(0, 169, 255, 147), stop:0.497326 rgba(0, 0, 0, 147), stop:1 rgba(0, 169, 255, 147));"));
        lcdNumber_12->setFrameShape(QFrame::Box);
        lcdNumber_12->setFrameShadow(QFrame::Sunken);
        lcdNumber_12->setDigitCount(7);
        lcdNumber_12->setSegmentStyle(QLCDNumber::Flat);

        gridLayout_2->addWidget(lcdNumber_12, 12, 0, 1, 1);

        lcdNumber_13 = new QLCDNumber(groupBox);
        lcdNumber_13->setObjectName(QString::fromUtf8("lcdNumber_13"));
        lcdNumber_13->setLayoutDirection(Qt::LeftToRight);
        lcdNumber_13->setStyleSheet(QString::fromUtf8("border-color: qradialgradient(spread:repeat, cx:0.5, cy:0.5, radius:0.077, fx:0.5, fy:0.5, stop:0 rgba(0, 169, 255, 147), stop:0.497326 rgba(0, 0, 0, 147), stop:1 rgba(0, 169, 255, 147));"));
        lcdNumber_13->setFrameShape(QFrame::Box);
        lcdNumber_13->setFrameShadow(QFrame::Sunken);
        lcdNumber_13->setDigitCount(7);
        lcdNumber_13->setSegmentStyle(QLCDNumber::Flat);

        gridLayout_2->addWidget(lcdNumber_13, 13, 0, 1, 1);

        lcdNumber_14 = new QLCDNumber(groupBox);
        lcdNumber_14->setObjectName(QString::fromUtf8("lcdNumber_14"));
        lcdNumber_14->setLayoutDirection(Qt::RightToLeft);
        lcdNumber_14->setStyleSheet(QString::fromUtf8("border-color: qradialgradient(spread:repeat, cx:0.5, cy:0.5, radius:0.077, fx:0.5, fy:0.5, stop:0 rgba(0, 169, 255, 147), stop:0.497326 rgba(0, 0, 0, 147), stop:1 rgba(0, 169, 255, 147));"));
        lcdNumber_14->setFrameShape(QFrame::Box);
        lcdNumber_14->setFrameShadow(QFrame::Sunken);
        lcdNumber_14->setDigitCount(7);
        lcdNumber_14->setSegmentStyle(QLCDNumber::Flat);

        gridLayout_2->addWidget(lcdNumber_14, 14, 0, 1, 1);

        lcdNumber_15 = new QLCDNumber(groupBox);
        lcdNumber_15->setObjectName(QString::fromUtf8("lcdNumber_15"));
        lcdNumber_15->setLayoutDirection(Qt::LeftToRight);
        lcdNumber_15->setStyleSheet(QString::fromUtf8("border-color: qradialgradient(spread:repeat, cx:0.5, cy:0.5, radius:0.077, fx:0.5, fy:0.5, stop:0 rgba(0, 169, 255, 147), stop:0.497326 rgba(0, 0, 0, 147), stop:1 rgba(0, 169, 255, 147));"));
        lcdNumber_15->setFrameShape(QFrame::Box);
        lcdNumber_15->setFrameShadow(QFrame::Sunken);
        lcdNumber_15->setDigitCount(7);
        lcdNumber_15->setSegmentStyle(QLCDNumber::Flat);

        gridLayout_2->addWidget(lcdNumber_15, 15, 0, 1, 1);

        label_5 = new QLabel(groupBox);
        label_5->setObjectName(QString::fromUtf8("label_5"));
        QSizePolicy sizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::Maximum);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(label_5->sizePolicy().hasHeightForWidth());
        label_5->setSizePolicy(sizePolicy);
        QFont font9;
        font9.setFamily(QString::fromUtf8("Copperplate Gothic Light"));
        font9.setPointSize(14);
        font9.setBold(true);
        font9.setWeight(75);
        label_5->setFont(font9);
        label_5->setAlignment(Qt::AlignCenter);

        gridLayout_2->addWidget(label_5, 0, 0, 1, 1);


        gridLayout_3->addLayout(gridLayout_2, 0, 1, 1, 1);


        gridLayout_4->addLayout(gridLayout_3, 0, 0, 1, 1);


        gridLayout_8->addWidget(groupBox, 0, 1, 1, 1);

        splitter->addWidget(layoutWidget);

        gridLayout_9->addWidget(splitter, 0, 0, 1, 1);


        retranslateUi(Salary);
        QObject::connect(verticalSlider_2, SIGNAL(valueChanged(int)), spinBox_2, SLOT(setValue(int)));
        QObject::connect(spinBox_2, SIGNAL(valueChanged(int)), verticalSlider_2, SLOT(setValue(int)));
        QObject::connect(verticalSlider, SIGNAL(valueChanged(int)), spinBox, SLOT(setValue(int)));
        QObject::connect(spinBox, SIGNAL(valueChanged(int)), verticalSlider, SLOT(setValue(int)));

        QMetaObject::connectSlotsByName(Salary);
    } // setupUi

    void retranslateUi(QDialog *Salary)
    {
        Salary->setWindowTitle(QApplication::translate("Salary", "Dialog", 0, QApplication::UnicodeUTF8));
        label_2->setText(QApplication::translate("Salary", "increments", 0, QApplication::UnicodeUTF8));
        groupBox_2->setTitle(QApplication::translate("Salary", "Salary And Bonus", 0, QApplication::UnicodeUTF8));
        label->setText(QApplication::translate("Salary", "TOTAL SALARY", 0, QApplication::UnicodeUTF8));
        label_6->setText(QApplication::translate("Salary", "Current Balance", 0, QApplication::UnicodeUTF8));
        label_3->setText(QApplication::translate("Salary", "BONUS(%)", 0, QApplication::UnicodeUTF8));
        label_4->setText(QApplication::translate("Salary", "SALARY INCREASE(%)", 0, QApplication::UnicodeUTF8));
        pushButton->setText(QApplication::translate("Salary", "BACK", 0, QApplication::UnicodeUTF8));
        groupBox->setTitle(QApplication::translate("Salary", "PLAYERS", 0, QApplication::UnicodeUTF8));
        label_133->setText(QApplication::translate("Salary", "TextLabel", 0, QApplication::UnicodeUTF8));
        label_134->setText(QApplication::translate("Salary", "TextLabel", 0, QApplication::UnicodeUTF8));
        label_135->setText(QApplication::translate("Salary", "TextLabel", 0, QApplication::UnicodeUTF8));
        label_136->setText(QApplication::translate("Salary", "TextLabel", 0, QApplication::UnicodeUTF8));
        label_137->setText(QApplication::translate("Salary", "TextLabel", 0, QApplication::UnicodeUTF8));
        label_138->setText(QApplication::translate("Salary", "TextLabel", 0, QApplication::UnicodeUTF8));
        label_139->setText(QApplication::translate("Salary", "TextLabel", 0, QApplication::UnicodeUTF8));
        label_140->setText(QApplication::translate("Salary", "TextLabel", 0, QApplication::UnicodeUTF8));
        label_141->setText(QApplication::translate("Salary", "TextLabel", 0, QApplication::UnicodeUTF8));
        label_142->setText(QApplication::translate("Salary", "TextLabel", 0, QApplication::UnicodeUTF8));
        label_143->setText(QApplication::translate("Salary", "TextLabel", 0, QApplication::UnicodeUTF8));
        label_144->setText(QApplication::translate("Salary", "TextLabel", 0, QApplication::UnicodeUTF8));
        label_145->setText(QApplication::translate("Salary", "TextLabel", 0, QApplication::UnicodeUTF8));
        label_146->setText(QApplication::translate("Salary", "TextLabel", 0, QApplication::UnicodeUTF8));
        label_147->setText(QApplication::translate("Salary", "TextLabel", 0, QApplication::UnicodeUTF8));
        label_132->setText(QApplication::translate("Salary", "NAME", 0, QApplication::UnicodeUTF8));
        label_100->setText(QApplication::translate("Salary", "OVERALL", 0, QApplication::UnicodeUTF8));
        label_101->setText(QApplication::translate("Salary", "TextLabel", 0, QApplication::UnicodeUTF8));
        label_102->setText(QApplication::translate("Salary", "TextLabel", 0, QApplication::UnicodeUTF8));
        label_103->setText(QApplication::translate("Salary", "TextLabel", 0, QApplication::UnicodeUTF8));
        label_104->setText(QApplication::translate("Salary", "TextLabel", 0, QApplication::UnicodeUTF8));
        label_105->setText(QApplication::translate("Salary", "TextLabel", 0, QApplication::UnicodeUTF8));
        label_106->setText(QApplication::translate("Salary", "TextLabel", 0, QApplication::UnicodeUTF8));
        label_107->setText(QApplication::translate("Salary", "TextLabel", 0, QApplication::UnicodeUTF8));
        label_108->setText(QApplication::translate("Salary", "TextLabel", 0, QApplication::UnicodeUTF8));
        label_109->setText(QApplication::translate("Salary", "TextLabel", 0, QApplication::UnicodeUTF8));
        label_110->setText(QApplication::translate("Salary", "TextLabel", 0, QApplication::UnicodeUTF8));
        label_111->setText(QApplication::translate("Salary", "TextLabel", 0, QApplication::UnicodeUTF8));
        label_112->setText(QApplication::translate("Salary", "TextLabel", 0, QApplication::UnicodeUTF8));
        label_113->setText(QApplication::translate("Salary", "TextLabel", 0, QApplication::UnicodeUTF8));
        label_114->setText(QApplication::translate("Salary", "TextLabel", 0, QApplication::UnicodeUTF8));
        label_115->setText(QApplication::translate("Salary", "TextLabel", 0, QApplication::UnicodeUTF8));
        label_116->setText(QApplication::translate("Salary", "POSITION", 0, QApplication::UnicodeUTF8));
        label_117->setText(QApplication::translate("Salary", "TextLabel", 0, QApplication::UnicodeUTF8));
        label_118->setText(QApplication::translate("Salary", "TextLabel", 0, QApplication::UnicodeUTF8));
        label_119->setText(QApplication::translate("Salary", "TextLabel", 0, QApplication::UnicodeUTF8));
        label_120->setText(QApplication::translate("Salary", "TextLabel", 0, QApplication::UnicodeUTF8));
        label_121->setText(QApplication::translate("Salary", "TextLabel", 0, QApplication::UnicodeUTF8));
        label_122->setText(QApplication::translate("Salary", "TextLabel", 0, QApplication::UnicodeUTF8));
        label_123->setText(QApplication::translate("Salary", "TextLabel", 0, QApplication::UnicodeUTF8));
        label_124->setText(QApplication::translate("Salary", "TextLabel", 0, QApplication::UnicodeUTF8));
        label_125->setText(QApplication::translate("Salary", "TextLabel", 0, QApplication::UnicodeUTF8));
        label_126->setText(QApplication::translate("Salary", "TextLabel", 0, QApplication::UnicodeUTF8));
        label_127->setText(QApplication::translate("Salary", "TextLabel", 0, QApplication::UnicodeUTF8));
        label_128->setText(QApplication::translate("Salary", "TextLabel", 0, QApplication::UnicodeUTF8));
        label_129->setText(QApplication::translate("Salary", "TextLabel", 0, QApplication::UnicodeUTF8));
        label_130->setText(QApplication::translate("Salary", "TextLabel", 0, QApplication::UnicodeUTF8));
        label_131->setText(QApplication::translate("Salary", "TextLabel", 0, QApplication::UnicodeUTF8));
        label_5->setText(QApplication::translate("Salary", "SALARY", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class Salary: public Ui_Salary {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_SALARY_H
