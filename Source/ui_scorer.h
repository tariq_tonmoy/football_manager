/********************************************************************************
** Form generated from reading UI file 'scorer.ui'
**
** Created: Thu Apr 19 02:12:48 2012
**      by: Qt User Interface Compiler version 4.6.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_SCORER_H
#define UI_SCORER_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QDialog>
#include <QtGui/QDialogButtonBox>
#include <QtGui/QGridLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QSplitter>

QT_BEGIN_NAMESPACE

class Ui_Scorer
{
public:
    QGridLayout *gridLayout;
    QSplitter *splitter;
    QLabel *label;
    QDialogButtonBox *buttonBox;

    void setupUi(QDialog *Scorer)
    {
        if (Scorer->objectName().isEmpty())
            Scorer->setObjectName(QString::fromUtf8("Scorer"));
        Scorer->resize(612, 311);
        gridLayout = new QGridLayout(Scorer);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        splitter = new QSplitter(Scorer);
        splitter->setObjectName(QString::fromUtf8("splitter"));
        splitter->setOrientation(Qt::Vertical);
        label = new QLabel(splitter);
        label->setObjectName(QString::fromUtf8("label"));
        QFont font;
        font.setFamily(QString::fromUtf8("Copperplate Gothic Light"));
        font.setPointSize(12);
        font.setBold(true);
        font.setWeight(75);
        label->setFont(font);
        label->setFrameShape(QFrame::Box);
        label->setAlignment(Qt::AlignCenter);
        splitter->addWidget(label);
        buttonBox = new QDialogButtonBox(splitter);
        buttonBox->setObjectName(QString::fromUtf8("buttonBox"));
        QFont font1;
        font1.setFamily(QString::fromUtf8("Copperplate Gothic Light"));
        font1.setBold(true);
        font1.setWeight(75);
        buttonBox->setFont(font1);
        buttonBox->setCursor(QCursor(Qt::PointingHandCursor));
        buttonBox->setOrientation(Qt::Horizontal);
        buttonBox->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::Ok);
        buttonBox->setCenterButtons(true);
        splitter->addWidget(buttonBox);

        gridLayout->addWidget(splitter, 0, 0, 1, 1);


        retranslateUi(Scorer);

        QMetaObject::connectSlotsByName(Scorer);
    } // setupUi

    void retranslateUi(QDialog *Scorer)
    {
        Scorer->setWindowTitle(QApplication::translate("Scorer", "Dialog", 0, QApplication::UnicodeUTF8));
        label->setText(QApplication::translate("Scorer", "TextLabel", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class Scorer: public Ui_Scorer {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_SCORER_H
