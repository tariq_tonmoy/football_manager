/********************************************************************************
** Form generated from reading UI file 'season.ui'
**
** Created: Thu Apr 19 02:12:48 2012
**      by: Qt User Interface Compiler version 4.6.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_SEASON_H
#define UI_SEASON_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QDialog>
#include <QtGui/QGridLayout>
#include <QtGui/QGroupBox>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QPushButton>
#include <QtGui/QSpacerItem>
#include <QtGui/QTableWidget>

QT_BEGIN_NAMESPACE

class Ui_Season
{
public:
    QGridLayout *gridLayout_3;
    QGridLayout *gridLayout_2;
    QLabel *label;
    QSpacerItem *horizontalSpacer;
    QGroupBox *groupBox;
    QGridLayout *gridLayout;
    QTableWidget *tableWidget;
    QLabel *label_2;
    QLabel *label_3;
    QSpacerItem *horizontalSpacer_2;
    QPushButton *pushButton_3;
    QPushButton *pushButton;
    QPushButton *pushButton_2;
    QSpacerItem *horizontalSpacer_3;
    QSpacerItem *horizontalSpacer_4;

    void setupUi(QDialog *Season)
    {
        if (Season->objectName().isEmpty())
            Season->setObjectName(QString::fromUtf8("Season"));
        Season->resize(558, 747);
        gridLayout_3 = new QGridLayout(Season);
        gridLayout_3->setObjectName(QString::fromUtf8("gridLayout_3"));
        gridLayout_2 = new QGridLayout();
        gridLayout_2->setObjectName(QString::fromUtf8("gridLayout_2"));
        label = new QLabel(Season);
        label->setObjectName(QString::fromUtf8("label"));
        QFont font;
        font.setFamily(QString::fromUtf8("Stencil"));
        font.setPointSize(28);
        label->setFont(font);
        label->setFrameShape(QFrame::Box);

        gridLayout_2->addWidget(label, 0, 3, 1, 1);

        horizontalSpacer = new QSpacerItem(119, 544, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_2->addItem(horizontalSpacer, 1, 0, 1, 1);

        groupBox = new QGroupBox(Season);
        groupBox->setObjectName(QString::fromUtf8("groupBox"));
        gridLayout = new QGridLayout(groupBox);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        tableWidget = new QTableWidget(groupBox);
        tableWidget->setObjectName(QString::fromUtf8("tableWidget"));

        gridLayout->addWidget(tableWidget, 2, 0, 1, 1);

        label_2 = new QLabel(groupBox);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        QFont font1;
        font1.setFamily(QString::fromUtf8("Copperplate Gothic Light"));
        font1.setPointSize(18);
        font1.setBold(true);
        font1.setWeight(75);
        label_2->setFont(font1);

        gridLayout->addWidget(label_2, 0, 0, 1, 1);

        label_3 = new QLabel(groupBox);
        label_3->setObjectName(QString::fromUtf8("label_3"));
        QFont font2;
        font2.setFamily(QString::fromUtf8("Copperplate Gothic Light"));
        font2.setPointSize(12);
        font2.setBold(true);
        font2.setWeight(75);
        label_3->setFont(font2);

        gridLayout->addWidget(label_3, 1, 0, 1, 1);


        gridLayout_2->addWidget(groupBox, 1, 1, 1, 5);

        horizontalSpacer_2 = new QSpacerItem(119, 544, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_2->addItem(horizontalSpacer_2, 1, 6, 1, 1);

        pushButton_3 = new QPushButton(Season);
        pushButton_3->setObjectName(QString::fromUtf8("pushButton_3"));
        QFont font3;
        font3.setFamily(QString::fromUtf8("Century Gothic"));
        font3.setBold(true);
        font3.setWeight(75);
        pushButton_3->setFont(font3);
        pushButton_3->setCursor(QCursor(Qt::PointingHandCursor));

        gridLayout_2->addWidget(pushButton_3, 2, 1, 1, 1);

        pushButton = new QPushButton(Season);
        pushButton->setObjectName(QString::fromUtf8("pushButton"));
        pushButton->setFont(font3);
        pushButton->setCursor(QCursor(Qt::PointingHandCursor));

        gridLayout_2->addWidget(pushButton, 2, 3, 1, 1);

        pushButton_2 = new QPushButton(Season);
        pushButton_2->setObjectName(QString::fromUtf8("pushButton_2"));
        pushButton_2->setFont(font3);
        pushButton_2->setCursor(QCursor(Qt::PointingHandCursor));

        gridLayout_2->addWidget(pushButton_2, 2, 5, 1, 1);

        horizontalSpacer_3 = new QSpacerItem(40, 20, QSizePolicy::Minimum, QSizePolicy::Minimum);

        gridLayout_2->addItem(horizontalSpacer_3, 2, 2, 1, 1);

        horizontalSpacer_4 = new QSpacerItem(40, 20, QSizePolicy::Minimum, QSizePolicy::Minimum);

        gridLayout_2->addItem(horizontalSpacer_4, 2, 4, 1, 1);


        gridLayout_3->addLayout(gridLayout_2, 0, 0, 1, 1);


        retranslateUi(Season);

        QMetaObject::connectSlotsByName(Season);
    } // setupUi

    void retranslateUi(QDialog *Season)
    {
        Season->setWindowTitle(QApplication::translate("Season", "Dialog", 0, QApplication::UnicodeUTF8));
        label->setText(QApplication::translate("Season", "History", 0, QApplication::UnicodeUTF8));
        groupBox->setTitle(QString());
        label_2->setText(QApplication::translate("Season", "TextLabel", 0, QApplication::UnicodeUTF8));
        label_3->setText(QApplication::translate("Season", "TextLabel", 0, QApplication::UnicodeUTF8));
        pushButton_3->setText(QApplication::translate("Season", "Back", 0, QApplication::UnicodeUTF8));
        pushButton->setText(QApplication::translate("Season", "League Table", 0, QApplication::UnicodeUTF8));
        pushButton_2->setText(QApplication::translate("Season", "Start New Season", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class Season: public Ui_Season {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_SEASON_H
