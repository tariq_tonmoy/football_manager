/********************************************************************************
** Form generated from reading UI file 'selectteam.ui'
**
** Created: Thu Apr 19 06:15:36 2012
**      by: Qt User Interface Compiler version 4.6.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_SELECTTEAM_H
#define UI_SELECTTEAM_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QComboBox>
#include <QtGui/QDialog>
#include <QtGui/QFormLayout>
#include <QtGui/QGridLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QLineEdit>
#include <QtGui/QPushButton>
#include <QtGui/QSpacerItem>
#include <QtGui/QSplitter>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_SelectTeam
{
public:
    QGridLayout *gridLayout_6;
    QLabel *label_5;
    QSplitter *splitter;
    QWidget *widget;
    QGridLayout *gridLayout_4;
    QFormLayout *formLayout_6;
    QLabel *label_3;
    QComboBox *comboBox;
    QFormLayout *formLayout_5;
    QLabel *label_4;
    QComboBox *comboBox_2;
    QSpacerItem *horizontalSpacer_3;
    QWidget *widget1;
    QGridLayout *gridLayout_3;
    QGridLayout *gridLayout_2;
    QFormLayout *formLayout;
    QLabel *label;
    QLineEdit *overall_lineEdit;
    QFormLayout *formLayout_2;
    QLabel *label_11;
    QLineEdit *location_lineEdit;
    QFormLayout *formLayout_3;
    QLabel *label_26;
    QLineEdit *Country_lineEdit;
    QFormLayout *formLayout_4;
    QLabel *label_27;
    QLineEdit *stadium_lineEdit;
    QGridLayout *gridLayout;
    QFormLayout *formLayout_28;
    QLabel *label_28;
    QLineEdit *money_lineEdit;
    QFormLayout *formLayout_29;
    QLabel *label_29;
    QLineEdit *match_lineEdit;
    QFormLayout *formLayout_30;
    QLabel *label_2;
    QLineEdit *win_lineEdit;
    QFormLayout *formLayout_31;
    QLabel *label_31;
    QLineEdit *draw_lineEdit;
    QFormLayout *formLayout_32;
    QLineEdit *lose_lineEdit;
    QLabel *label_32;
    QSpacerItem *horizontalSpacer_2;
    QGridLayout *gridLayout_5;
    QPushButton *pushButton;
    QPushButton *pushButton_6;
    QSpacerItem *horizontalSpacer;
    QSpacerItem *verticalSpacer;

    void setupUi(QDialog *SelectTeam)
    {
        if (SelectTeam->objectName().isEmpty())
            SelectTeam->setObjectName(QString::fromUtf8("SelectTeam"));
        SelectTeam->resize(738, 773);
        gridLayout_6 = new QGridLayout(SelectTeam);
        gridLayout_6->setObjectName(QString::fromUtf8("gridLayout_6"));
        label_5 = new QLabel(SelectTeam);
        label_5->setObjectName(QString::fromUtf8("label_5"));
        QFont font;
        font.setFamily(QString::fromUtf8("Stencil"));
        font.setPointSize(26);
        label_5->setFont(font);
        label_5->setAlignment(Qt::AlignCenter);

        gridLayout_6->addWidget(label_5, 0, 0, 1, 1);

        splitter = new QSplitter(SelectTeam);
        splitter->setObjectName(QString::fromUtf8("splitter"));
        splitter->setOrientation(Qt::Vertical);
        widget = new QWidget(splitter);
        widget->setObjectName(QString::fromUtf8("widget"));
        gridLayout_4 = new QGridLayout(widget);
        gridLayout_4->setObjectName(QString::fromUtf8("gridLayout_4"));
        gridLayout_4->setContentsMargins(0, 0, 0, 0);
        formLayout_6 = new QFormLayout();
        formLayout_6->setObjectName(QString::fromUtf8("formLayout_6"));
        label_3 = new QLabel(widget);
        label_3->setObjectName(QString::fromUtf8("label_3"));
        QFont font1;
        font1.setFamily(QString::fromUtf8("Copperplate Gothic Light"));
        font1.setPointSize(12);
        label_3->setFont(font1);

        formLayout_6->setWidget(0, QFormLayout::LabelRole, label_3);

        comboBox = new QComboBox(widget);
        comboBox->setObjectName(QString::fromUtf8("comboBox"));
        QFont font2;
        font2.setFamily(QString::fromUtf8("Century Gothic"));
        font2.setBold(true);
        font2.setWeight(75);
        comboBox->setFont(font2);
        comboBox->setCursor(QCursor(Qt::PointingHandCursor));

        formLayout_6->setWidget(0, QFormLayout::FieldRole, comboBox);


        gridLayout_4->addLayout(formLayout_6, 0, 0, 1, 1);

        formLayout_5 = new QFormLayout();
        formLayout_5->setObjectName(QString::fromUtf8("formLayout_5"));
        label_4 = new QLabel(widget);
        label_4->setObjectName(QString::fromUtf8("label_4"));
        label_4->setFont(font1);

        formLayout_5->setWidget(0, QFormLayout::LabelRole, label_4);

        comboBox_2 = new QComboBox(widget);
        comboBox_2->setObjectName(QString::fromUtf8("comboBox_2"));
        comboBox_2->setFont(font2);
        comboBox_2->setCursor(QCursor(Qt::PointingHandCursor));

        formLayout_5->setWidget(0, QFormLayout::FieldRole, comboBox_2);


        gridLayout_4->addLayout(formLayout_5, 0, 2, 1, 1);

        horizontalSpacer_3 = new QSpacerItem(40, 20, QSizePolicy::Minimum, QSizePolicy::Minimum);

        gridLayout_4->addItem(horizontalSpacer_3, 0, 1, 1, 1);

        splitter->addWidget(widget);
        widget1 = new QWidget(splitter);
        widget1->setObjectName(QString::fromUtf8("widget1"));
        gridLayout_3 = new QGridLayout(widget1);
        gridLayout_3->setObjectName(QString::fromUtf8("gridLayout_3"));
        gridLayout_3->setContentsMargins(0, 0, 0, 0);
        gridLayout_2 = new QGridLayout();
        gridLayout_2->setObjectName(QString::fromUtf8("gridLayout_2"));
        formLayout = new QFormLayout();
        formLayout->setObjectName(QString::fromUtf8("formLayout"));
        formLayout->setFieldGrowthPolicy(QFormLayout::AllNonFixedFieldsGrow);
        label = new QLabel(widget1);
        label->setObjectName(QString::fromUtf8("label"));
        label->setFont(font1);

        formLayout->setWidget(0, QFormLayout::LabelRole, label);

        overall_lineEdit = new QLineEdit(widget1);
        overall_lineEdit->setObjectName(QString::fromUtf8("overall_lineEdit"));

        formLayout->setWidget(0, QFormLayout::FieldRole, overall_lineEdit);


        gridLayout_2->addLayout(formLayout, 0, 0, 1, 1);

        formLayout_2 = new QFormLayout();
        formLayout_2->setObjectName(QString::fromUtf8("formLayout_2"));
        label_11 = new QLabel(widget1);
        label_11->setObjectName(QString::fromUtf8("label_11"));
        label_11->setFont(font1);

        formLayout_2->setWidget(0, QFormLayout::LabelRole, label_11);

        location_lineEdit = new QLineEdit(widget1);
        location_lineEdit->setObjectName(QString::fromUtf8("location_lineEdit"));

        formLayout_2->setWidget(0, QFormLayout::FieldRole, location_lineEdit);


        gridLayout_2->addLayout(formLayout_2, 1, 0, 1, 1);

        formLayout_3 = new QFormLayout();
        formLayout_3->setObjectName(QString::fromUtf8("formLayout_3"));
        label_26 = new QLabel(widget1);
        label_26->setObjectName(QString::fromUtf8("label_26"));
        label_26->setFont(font1);

        formLayout_3->setWidget(0, QFormLayout::LabelRole, label_26);

        Country_lineEdit = new QLineEdit(widget1);
        Country_lineEdit->setObjectName(QString::fromUtf8("Country_lineEdit"));

        formLayout_3->setWidget(0, QFormLayout::FieldRole, Country_lineEdit);


        gridLayout_2->addLayout(formLayout_3, 2, 0, 1, 1);

        formLayout_4 = new QFormLayout();
        formLayout_4->setObjectName(QString::fromUtf8("formLayout_4"));
        label_27 = new QLabel(widget1);
        label_27->setObjectName(QString::fromUtf8("label_27"));
        label_27->setFont(font1);

        formLayout_4->setWidget(0, QFormLayout::LabelRole, label_27);

        stadium_lineEdit = new QLineEdit(widget1);
        stadium_lineEdit->setObjectName(QString::fromUtf8("stadium_lineEdit"));

        formLayout_4->setWidget(0, QFormLayout::FieldRole, stadium_lineEdit);


        gridLayout_2->addLayout(formLayout_4, 3, 0, 1, 1);


        gridLayout_3->addLayout(gridLayout_2, 0, 0, 1, 1);

        gridLayout = new QGridLayout();
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        formLayout_28 = new QFormLayout();
        formLayout_28->setObjectName(QString::fromUtf8("formLayout_28"));
        formLayout_28->setFieldGrowthPolicy(QFormLayout::AllNonFixedFieldsGrow);
        label_28 = new QLabel(widget1);
        label_28->setObjectName(QString::fromUtf8("label_28"));
        label_28->setFont(font1);

        formLayout_28->setWidget(0, QFormLayout::LabelRole, label_28);

        money_lineEdit = new QLineEdit(widget1);
        money_lineEdit->setObjectName(QString::fromUtf8("money_lineEdit"));

        formLayout_28->setWidget(0, QFormLayout::FieldRole, money_lineEdit);


        gridLayout->addLayout(formLayout_28, 0, 0, 1, 1);

        formLayout_29 = new QFormLayout();
        formLayout_29->setObjectName(QString::fromUtf8("formLayout_29"));
        label_29 = new QLabel(widget1);
        label_29->setObjectName(QString::fromUtf8("label_29"));
        label_29->setFont(font1);

        formLayout_29->setWidget(0, QFormLayout::LabelRole, label_29);

        match_lineEdit = new QLineEdit(widget1);
        match_lineEdit->setObjectName(QString::fromUtf8("match_lineEdit"));

        formLayout_29->setWidget(0, QFormLayout::FieldRole, match_lineEdit);


        gridLayout->addLayout(formLayout_29, 1, 0, 1, 1);

        formLayout_30 = new QFormLayout();
        formLayout_30->setObjectName(QString::fromUtf8("formLayout_30"));
        label_2 = new QLabel(widget1);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        label_2->setFont(font1);

        formLayout_30->setWidget(0, QFormLayout::LabelRole, label_2);

        win_lineEdit = new QLineEdit(widget1);
        win_lineEdit->setObjectName(QString::fromUtf8("win_lineEdit"));

        formLayout_30->setWidget(0, QFormLayout::FieldRole, win_lineEdit);


        gridLayout->addLayout(formLayout_30, 2, 0, 1, 1);

        formLayout_31 = new QFormLayout();
        formLayout_31->setObjectName(QString::fromUtf8("formLayout_31"));
        label_31 = new QLabel(widget1);
        label_31->setObjectName(QString::fromUtf8("label_31"));
        label_31->setFont(font1);

        formLayout_31->setWidget(0, QFormLayout::LabelRole, label_31);

        draw_lineEdit = new QLineEdit(widget1);
        draw_lineEdit->setObjectName(QString::fromUtf8("draw_lineEdit"));

        formLayout_31->setWidget(0, QFormLayout::FieldRole, draw_lineEdit);


        gridLayout->addLayout(formLayout_31, 3, 0, 1, 1);

        formLayout_32 = new QFormLayout();
        formLayout_32->setObjectName(QString::fromUtf8("formLayout_32"));
        lose_lineEdit = new QLineEdit(widget1);
        lose_lineEdit->setObjectName(QString::fromUtf8("lose_lineEdit"));

        formLayout_32->setWidget(0, QFormLayout::FieldRole, lose_lineEdit);

        label_32 = new QLabel(widget1);
        label_32->setObjectName(QString::fromUtf8("label_32"));
        label_32->setFont(font1);

        formLayout_32->setWidget(0, QFormLayout::LabelRole, label_32);


        gridLayout->addLayout(formLayout_32, 4, 0, 1, 1);


        gridLayout_3->addLayout(gridLayout, 0, 2, 1, 1);

        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::MinimumExpanding, QSizePolicy::Minimum);

        gridLayout_3->addItem(horizontalSpacer_2, 0, 1, 1, 1);

        splitter->addWidget(widget1);

        gridLayout_6->addWidget(splitter, 2, 0, 1, 1);

        gridLayout_5 = new QGridLayout();
        gridLayout_5->setObjectName(QString::fromUtf8("gridLayout_5"));
        pushButton = new QPushButton(SelectTeam);
        pushButton->setObjectName(QString::fromUtf8("pushButton"));
        pushButton->setFont(font2);
        pushButton->setCursor(QCursor(Qt::PointingHandCursor));

        gridLayout_5->addWidget(pushButton, 0, 0, 1, 1);

        pushButton_6 = new QPushButton(SelectTeam);
        pushButton_6->setObjectName(QString::fromUtf8("pushButton_6"));
        pushButton_6->setFont(font2);
        pushButton_6->setCursor(QCursor(Qt::PointingHandCursor));

        gridLayout_5->addWidget(pushButton_6, 0, 2, 1, 1);

        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_5->addItem(horizontalSpacer, 0, 1, 1, 1);


        gridLayout_6->addLayout(gridLayout_5, 3, 0, 1, 1);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Maximum);

        gridLayout_6->addItem(verticalSpacer, 1, 0, 1, 1);


        retranslateUi(SelectTeam);

        QMetaObject::connectSlotsByName(SelectTeam);
    } // setupUi

    void retranslateUi(QDialog *SelectTeam)
    {
        SelectTeam->setWindowTitle(QApplication::translate("SelectTeam", "Dialog", 0, QApplication::UnicodeUTF8));
        label_5->setText(QApplication::translate("SelectTeam", "Select A New Job", 0, QApplication::UnicodeUTF8));
        label_3->setText(QApplication::translate("SelectTeam", "Team Name", 0, QApplication::UnicodeUTF8));
        label_4->setText(QApplication::translate("SelectTeam", "Sponsor's Name", 0, QApplication::UnicodeUTF8));
        label->setText(QApplication::translate("SelectTeam", "Overall", 0, QApplication::UnicodeUTF8));
        overall_lineEdit->setText(QString());
        label_11->setText(QApplication::translate("SelectTeam", "Location", 0, QApplication::UnicodeUTF8));
        location_lineEdit->setText(QString());
        label_26->setText(QApplication::translate("SelectTeam", "Country", 0, QApplication::UnicodeUTF8));
        Country_lineEdit->setText(QString());
        label_27->setText(QApplication::translate("SelectTeam", "stadium Capacity", 0, QApplication::UnicodeUTF8));
        stadium_lineEdit->setText(QString());
        label_28->setText(QApplication::translate("SelectTeam", "Starting Money", 0, QApplication::UnicodeUTF8));
        money_lineEdit->setText(QString());
        label_29->setText(QApplication::translate("SelectTeam", "Per Match Payment", 0, QApplication::UnicodeUTF8));
        match_lineEdit->setText(QString());
        label_2->setText(QApplication::translate("SelectTeam", "Win Bonus", 0, QApplication::UnicodeUTF8));
        win_lineEdit->setText(QString());
        label_31->setText(QApplication::translate("SelectTeam", "Draw Bonus", 0, QApplication::UnicodeUTF8));
        draw_lineEdit->setText(QString());
        lose_lineEdit->setText(QString());
        label_32->setText(QApplication::translate("SelectTeam", "Lose Bonus", 0, QApplication::UnicodeUTF8));
        pushButton->setText(QApplication::translate("SelectTeam", "Confirm Team", 0, QApplication::UnicodeUTF8));
        pushButton_6->setText(QApplication::translate("SelectTeam", "Proceed", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class SelectTeam: public Ui_SelectTeam {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_SELECTTEAM_H
