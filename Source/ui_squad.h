/********************************************************************************
** Form generated from reading UI file 'squad.ui'
**
** Created: Thu Apr 19 10:25:04 2012
**      by: Qt User Interface Compiler version 4.6.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_SQUAD_H
#define UI_SQUAD_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QCheckBox>
#include <QtGui/QDialog>
#include <QtGui/QFormLayout>
#include <QtGui/QGridLayout>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QLineEdit>
#include <QtGui/QPushButton>
#include <QtGui/QSlider>
#include <QtGui/QSpacerItem>
#include <QtGui/QSpinBox>
#include <QtGui/QSplitter>
#include <QtGui/QTabWidget>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_Squad
{
public:
    QGridLayout *gridLayout_17;
    QSplitter *splitter_2;
    QTabWidget *tabWidget;
    QWidget *tab;
    QGridLayout *gridLayout_15;
    QGridLayout *gridLayout_4;
    QFormLayout *formLayout;
    QSpacerItem *horizontalSpacer_3;
    QFormLayout *formLayout_2;
    QLineEdit *lineEdit_2;
    QLabel *label_2;
    QSpacerItem *horizontalSpacer_4;
    QFormLayout *formLayout_3;
    QLineEdit *lineEdit_3;
    QLabel *label_3;
    QGridLayout *gridLayout_3;
    QGridLayout *gridLayout_10;
    QLabel *label_84;
    QLabel *label_85;
    QLabel *label_86;
    QLabel *label_87;
    QLabel *label_88;
    QLabel *label_89;
    QLabel *label_90;
    QLabel *label_91;
    QLabel *label_92;
    QLabel *label_93;
    QLabel *label_94;
    QLabel *label_95;
    QLabel *label_96;
    QLabel *label_97;
    QLabel *label_98;
    QLabel *label_99;
    QGridLayout *gridLayout_7;
    QLabel *label_51;
    QLabel *label_52;
    QLabel *label_53;
    QLabel *label_54;
    QLabel *label_55;
    QLabel *label_56;
    QLabel *label_57;
    QLabel *label_58;
    QLabel *label_59;
    QLabel *label_60;
    QLabel *label_61;
    QLabel *label_62;
    QLabel *label_63;
    QLabel *label_64;
    QLabel *label_65;
    QLabel *label_66;
    QGridLayout *gridLayout_9;
    QLabel *label_68;
    QLabel *label_69;
    QLabel *label_70;
    QLabel *label_71;
    QLabel *label_72;
    QLabel *label_73;
    QLabel *label_74;
    QLabel *label_75;
    QLabel *label_76;
    QLabel *label_77;
    QLabel *label_78;
    QLabel *label_79;
    QLabel *label_80;
    QLabel *label_81;
    QLabel *label_82;
    QLabel *label_83;
    QGridLayout *gridLayout_2;
    QHBoxLayout *horizontalLayout_16;
    QLabel *label_5;
    QSpinBox *spinBox_16;
    QHBoxLayout *horizontalLayout;
    QSlider *horizontalSlider_86;
    QSpinBox *spinBox;
    QHBoxLayout *horizontalLayout_2;
    QSlider *horizontalSlider_87;
    QSpinBox *spinBox_2;
    QHBoxLayout *horizontalLayout_3;
    QSlider *horizontalSlider_88;
    QSpinBox *spinBox_3;
    QHBoxLayout *horizontalLayout_4;
    QSlider *horizontalSlider_89;
    QSpinBox *spinBox_4;
    QHBoxLayout *horizontalLayout_5;
    QSlider *horizontalSlider_90;
    QSpinBox *spinBox_5;
    QHBoxLayout *horizontalLayout_13;
    QSlider *horizontalSlider_91;
    QSpinBox *spinBox_13;
    QHBoxLayout *horizontalLayout_12;
    QSlider *horizontalSlider_92;
    QSpinBox *spinBox_12;
    QHBoxLayout *horizontalLayout_15;
    QSlider *horizontalSlider_93;
    QSpinBox *spinBox_15;
    QHBoxLayout *horizontalLayout_14;
    QSlider *horizontalSlider_94;
    QSpinBox *spinBox_14;
    QHBoxLayout *horizontalLayout_11;
    QSlider *horizontalSlider_95;
    QSpinBox *spinBox_11;
    QHBoxLayout *horizontalLayout_8;
    QSlider *horizontalSlider_96;
    QSpinBox *spinBox_8;
    QHBoxLayout *horizontalLayout_7;
    QSlider *horizontalSlider_97;
    QSpinBox *spinBox_7;
    QHBoxLayout *horizontalLayout_10;
    QSlider *horizontalSlider_98;
    QSpinBox *spinBox_10;
    QHBoxLayout *horizontalLayout_9;
    QSlider *horizontalSlider_99;
    QSpinBox *spinBox_9;
    QHBoxLayout *horizontalLayout_6;
    QSlider *horizontalSlider_100;
    QSpinBox *spinBox_6;
    QGridLayout *gridLayout_5;
    QSpacerItem *horizontalSpacer;
    QPushButton *pushButton_2;
    QLabel *label_50;
    QWidget *tab_2;
    QGridLayout *gridLayout_6;
    QSplitter *splitter;
    QLabel *label_4;
    QWidget *layoutWidget;
    QGridLayout *gridLayout;
    QGridLayout *gridLayout_11;
    QCheckBox *checkBox_18;
    QCheckBox *checkBox_19;
    QCheckBox *checkBox_20;
    QCheckBox *checkBox_21;
    QCheckBox *checkBox_22;
    QCheckBox *checkBox_23;
    QCheckBox *checkBox_24;
    QCheckBox *checkBox_25;
    QCheckBox *checkBox_26;
    QCheckBox *checkBox_27;
    QCheckBox *checkBox_28;
    QCheckBox *checkBox_29;
    QCheckBox *checkBox_30;
    QCheckBox *checkBox_31;
    QCheckBox *checkBox_32;
    QCheckBox *checkBox_17;
    QGridLayout *gridLayout_14;
    QLabel *label_132;
    QLabel *label_133;
    QLabel *label_134;
    QLabel *label_135;
    QLabel *label_136;
    QLabel *label_137;
    QLabel *label_138;
    QLabel *label_139;
    QLabel *label_140;
    QLabel *label_141;
    QLabel *label_142;
    QLabel *label_143;
    QLabel *label_144;
    QLabel *label_145;
    QLabel *label_146;
    QLabel *label_147;
    QGridLayout *gridLayout_12;
    QLabel *label_100;
    QLabel *label_101;
    QLabel *label_102;
    QLabel *label_103;
    QLabel *label_104;
    QLabel *label_105;
    QLabel *label_106;
    QLabel *label_107;
    QLabel *label_108;
    QLabel *label_109;
    QLabel *label_110;
    QLabel *label_111;
    QLabel *label_112;
    QLabel *label_113;
    QLabel *label_114;
    QLabel *label_115;
    QGridLayout *gridLayout_13;
    QLabel *label_116;
    QLabel *label_117;
    QLabel *label_118;
    QLabel *label_119;
    QLabel *label_120;
    QLabel *label_121;
    QLabel *label_122;
    QLabel *label_123;
    QLabel *label_124;
    QLabel *label_125;
    QLabel *label_126;
    QLabel *label_127;
    QLabel *label_128;
    QLabel *label_129;
    QLabel *label_130;
    QLabel *label_131;
    QSpacerItem *verticalSpacer_3;
    QSpacerItem *verticalSpacer;
    QGridLayout *gridLayout_8;
    QSpacerItem *horizontalSpacer_5;

    void setupUi(QDialog *Squad)
    {
        if (Squad->objectName().isEmpty())
            Squad->setObjectName(QString::fromUtf8("Squad"));
        Squad->resize(667, 753);
        gridLayout_17 = new QGridLayout(Squad);
        gridLayout_17->setObjectName(QString::fromUtf8("gridLayout_17"));
        splitter_2 = new QSplitter(Squad);
        splitter_2->setObjectName(QString::fromUtf8("splitter_2"));
        splitter_2->setOrientation(Qt::Vertical);
        tabWidget = new QTabWidget(splitter_2);
        tabWidget->setObjectName(QString::fromUtf8("tabWidget"));
        QFont font;
        font.setFamily(QString::fromUtf8("Century Gothic"));
        font.setPointSize(11);
        tabWidget->setFont(font);
        tab = new QWidget();
        tab->setObjectName(QString::fromUtf8("tab"));
        gridLayout_15 = new QGridLayout(tab);
        gridLayout_15->setObjectName(QString::fromUtf8("gridLayout_15"));
        gridLayout_4 = new QGridLayout();
        gridLayout_4->setObjectName(QString::fromUtf8("gridLayout_4"));
        formLayout = new QFormLayout();
        formLayout->setObjectName(QString::fromUtf8("formLayout"));
        formLayout->setFieldGrowthPolicy(QFormLayout::AllNonFixedFieldsGrow);

        gridLayout_4->addLayout(formLayout, 1, 1, 1, 1);

        horizontalSpacer_3 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_4->addItem(horizontalSpacer_3, 2, 0, 1, 1);

        formLayout_2 = new QFormLayout();
        formLayout_2->setObjectName(QString::fromUtf8("formLayout_2"));
        lineEdit_2 = new QLineEdit(tab);
        lineEdit_2->setObjectName(QString::fromUtf8("lineEdit_2"));

        formLayout_2->setWidget(1, QFormLayout::FieldRole, lineEdit_2);

        label_2 = new QLabel(tab);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        QFont font1;
        font1.setFamily(QString::fromUtf8("Copperplate Gothic Light"));
        font1.setPointSize(10);
        label_2->setFont(font1);

        formLayout_2->setWidget(0, QFormLayout::FieldRole, label_2);


        gridLayout_4->addLayout(formLayout_2, 2, 1, 1, 1);

        horizontalSpacer_4 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_4->addItem(horizontalSpacer_4, 3, 0, 1, 1);

        formLayout_3 = new QFormLayout();
        formLayout_3->setObjectName(QString::fromUtf8("formLayout_3"));
        lineEdit_3 = new QLineEdit(tab);
        lineEdit_3->setObjectName(QString::fromUtf8("lineEdit_3"));

        formLayout_3->setWidget(1, QFormLayout::FieldRole, lineEdit_3);

        label_3 = new QLabel(tab);
        label_3->setObjectName(QString::fromUtf8("label_3"));
        label_3->setFont(font1);

        formLayout_3->setWidget(0, QFormLayout::FieldRole, label_3);


        gridLayout_4->addLayout(formLayout_3, 3, 1, 1, 1);

        gridLayout_3 = new QGridLayout();
        gridLayout_3->setObjectName(QString::fromUtf8("gridLayout_3"));
        gridLayout_10 = new QGridLayout();
        gridLayout_10->setObjectName(QString::fromUtf8("gridLayout_10"));
        label_84 = new QLabel(tab);
        label_84->setObjectName(QString::fromUtf8("label_84"));
        QFont font2;
        font2.setPointSize(11);
        label_84->setFont(font2);
        label_84->setAlignment(Qt::AlignCenter);

        gridLayout_10->addWidget(label_84, 0, 0, 1, 1);

        label_85 = new QLabel(tab);
        label_85->setObjectName(QString::fromUtf8("label_85"));
        QFont font3;
        font3.setPointSize(10);
        label_85->setFont(font3);

        gridLayout_10->addWidget(label_85, 1, 0, 1, 1);

        label_86 = new QLabel(tab);
        label_86->setObjectName(QString::fromUtf8("label_86"));
        label_86->setFont(font3);

        gridLayout_10->addWidget(label_86, 2, 0, 1, 1);

        label_87 = new QLabel(tab);
        label_87->setObjectName(QString::fromUtf8("label_87"));
        label_87->setFont(font3);

        gridLayout_10->addWidget(label_87, 3, 0, 1, 1);

        label_88 = new QLabel(tab);
        label_88->setObjectName(QString::fromUtf8("label_88"));
        label_88->setFont(font3);

        gridLayout_10->addWidget(label_88, 4, 0, 1, 1);

        label_89 = new QLabel(tab);
        label_89->setObjectName(QString::fromUtf8("label_89"));
        label_89->setFont(font3);

        gridLayout_10->addWidget(label_89, 5, 0, 1, 1);

        label_90 = new QLabel(tab);
        label_90->setObjectName(QString::fromUtf8("label_90"));
        label_90->setFont(font3);

        gridLayout_10->addWidget(label_90, 6, 0, 1, 1);

        label_91 = new QLabel(tab);
        label_91->setObjectName(QString::fromUtf8("label_91"));
        label_91->setFont(font3);

        gridLayout_10->addWidget(label_91, 7, 0, 1, 1);

        label_92 = new QLabel(tab);
        label_92->setObjectName(QString::fromUtf8("label_92"));
        label_92->setFont(font3);

        gridLayout_10->addWidget(label_92, 8, 0, 1, 1);

        label_93 = new QLabel(tab);
        label_93->setObjectName(QString::fromUtf8("label_93"));
        label_93->setFont(font3);

        gridLayout_10->addWidget(label_93, 9, 0, 1, 1);

        label_94 = new QLabel(tab);
        label_94->setObjectName(QString::fromUtf8("label_94"));
        label_94->setFont(font3);

        gridLayout_10->addWidget(label_94, 10, 0, 1, 1);

        label_95 = new QLabel(tab);
        label_95->setObjectName(QString::fromUtf8("label_95"));
        label_95->setFont(font3);

        gridLayout_10->addWidget(label_95, 11, 0, 1, 1);

        label_96 = new QLabel(tab);
        label_96->setObjectName(QString::fromUtf8("label_96"));
        label_96->setFont(font3);

        gridLayout_10->addWidget(label_96, 12, 0, 1, 1);

        label_97 = new QLabel(tab);
        label_97->setObjectName(QString::fromUtf8("label_97"));
        label_97->setFont(font3);

        gridLayout_10->addWidget(label_97, 13, 0, 1, 1);

        label_98 = new QLabel(tab);
        label_98->setObjectName(QString::fromUtf8("label_98"));
        label_98->setFont(font3);

        gridLayout_10->addWidget(label_98, 14, 0, 1, 1);

        label_99 = new QLabel(tab);
        label_99->setObjectName(QString::fromUtf8("label_99"));
        label_99->setFont(font3);

        gridLayout_10->addWidget(label_99, 15, 0, 1, 1);


        gridLayout_3->addLayout(gridLayout_10, 0, 0, 1, 1);

        gridLayout_7 = new QGridLayout();
        gridLayout_7->setObjectName(QString::fromUtf8("gridLayout_7"));
        label_51 = new QLabel(tab);
        label_51->setObjectName(QString::fromUtf8("label_51"));
        label_51->setFont(font2);
        label_51->setAlignment(Qt::AlignCenter);

        gridLayout_7->addWidget(label_51, 0, 0, 1, 1);

        label_52 = new QLabel(tab);
        label_52->setObjectName(QString::fromUtf8("label_52"));
        label_52->setFont(font3);
        label_52->setAlignment(Qt::AlignCenter);

        gridLayout_7->addWidget(label_52, 1, 0, 1, 1);

        label_53 = new QLabel(tab);
        label_53->setObjectName(QString::fromUtf8("label_53"));
        label_53->setFont(font3);
        label_53->setAlignment(Qt::AlignCenter);

        gridLayout_7->addWidget(label_53, 2, 0, 1, 1);

        label_54 = new QLabel(tab);
        label_54->setObjectName(QString::fromUtf8("label_54"));
        label_54->setFont(font3);
        label_54->setAlignment(Qt::AlignCenter);

        gridLayout_7->addWidget(label_54, 3, 0, 1, 1);

        label_55 = new QLabel(tab);
        label_55->setObjectName(QString::fromUtf8("label_55"));
        label_55->setFont(font3);
        label_55->setAlignment(Qt::AlignCenter);

        gridLayout_7->addWidget(label_55, 4, 0, 1, 1);

        label_56 = new QLabel(tab);
        label_56->setObjectName(QString::fromUtf8("label_56"));
        label_56->setFont(font3);
        label_56->setAlignment(Qt::AlignCenter);

        gridLayout_7->addWidget(label_56, 5, 0, 1, 1);

        label_57 = new QLabel(tab);
        label_57->setObjectName(QString::fromUtf8("label_57"));
        label_57->setFont(font3);
        label_57->setAlignment(Qt::AlignCenter);

        gridLayout_7->addWidget(label_57, 6, 0, 1, 1);

        label_58 = new QLabel(tab);
        label_58->setObjectName(QString::fromUtf8("label_58"));
        label_58->setFont(font3);
        label_58->setAlignment(Qt::AlignCenter);

        gridLayout_7->addWidget(label_58, 7, 0, 1, 1);

        label_59 = new QLabel(tab);
        label_59->setObjectName(QString::fromUtf8("label_59"));
        label_59->setFont(font3);
        label_59->setAlignment(Qt::AlignCenter);

        gridLayout_7->addWidget(label_59, 8, 0, 1, 1);

        label_60 = new QLabel(tab);
        label_60->setObjectName(QString::fromUtf8("label_60"));
        label_60->setFont(font3);
        label_60->setAlignment(Qt::AlignCenter);

        gridLayout_7->addWidget(label_60, 9, 0, 1, 1);

        label_61 = new QLabel(tab);
        label_61->setObjectName(QString::fromUtf8("label_61"));
        label_61->setFont(font3);
        label_61->setAlignment(Qt::AlignCenter);

        gridLayout_7->addWidget(label_61, 10, 0, 1, 1);

        label_62 = new QLabel(tab);
        label_62->setObjectName(QString::fromUtf8("label_62"));
        label_62->setFont(font3);
        label_62->setAlignment(Qt::AlignCenter);

        gridLayout_7->addWidget(label_62, 11, 0, 1, 1);

        label_63 = new QLabel(tab);
        label_63->setObjectName(QString::fromUtf8("label_63"));
        label_63->setFont(font3);
        label_63->setAlignment(Qt::AlignCenter);

        gridLayout_7->addWidget(label_63, 12, 0, 1, 1);

        label_64 = new QLabel(tab);
        label_64->setObjectName(QString::fromUtf8("label_64"));
        label_64->setFont(font3);
        label_64->setAlignment(Qt::AlignCenter);

        gridLayout_7->addWidget(label_64, 13, 0, 1, 1);

        label_65 = new QLabel(tab);
        label_65->setObjectName(QString::fromUtf8("label_65"));
        label_65->setFont(font3);
        label_65->setAlignment(Qt::AlignCenter);

        gridLayout_7->addWidget(label_65, 14, 0, 1, 1);

        label_66 = new QLabel(tab);
        label_66->setObjectName(QString::fromUtf8("label_66"));
        label_66->setFont(font3);
        label_66->setAlignment(Qt::AlignCenter);

        gridLayout_7->addWidget(label_66, 15, 0, 1, 1);


        gridLayout_3->addLayout(gridLayout_7, 0, 1, 1, 1);

        gridLayout_9 = new QGridLayout();
        gridLayout_9->setObjectName(QString::fromUtf8("gridLayout_9"));
        label_68 = new QLabel(tab);
        label_68->setObjectName(QString::fromUtf8("label_68"));
        label_68->setFont(font2);
        label_68->setAlignment(Qt::AlignCenter);

        gridLayout_9->addWidget(label_68, 0, 0, 1, 1);

        label_69 = new QLabel(tab);
        label_69->setObjectName(QString::fromUtf8("label_69"));
        label_69->setFont(font3);
        label_69->setAlignment(Qt::AlignCenter);

        gridLayout_9->addWidget(label_69, 1, 0, 1, 1);

        label_70 = new QLabel(tab);
        label_70->setObjectName(QString::fromUtf8("label_70"));
        label_70->setFont(font3);
        label_70->setAlignment(Qt::AlignCenter);

        gridLayout_9->addWidget(label_70, 2, 0, 1, 1);

        label_71 = new QLabel(tab);
        label_71->setObjectName(QString::fromUtf8("label_71"));
        label_71->setFont(font3);
        label_71->setAlignment(Qt::AlignCenter);

        gridLayout_9->addWidget(label_71, 3, 0, 1, 1);

        label_72 = new QLabel(tab);
        label_72->setObjectName(QString::fromUtf8("label_72"));
        label_72->setFont(font3);
        label_72->setAlignment(Qt::AlignCenter);

        gridLayout_9->addWidget(label_72, 4, 0, 1, 1);

        label_73 = new QLabel(tab);
        label_73->setObjectName(QString::fromUtf8("label_73"));
        label_73->setFont(font3);
        label_73->setAlignment(Qt::AlignCenter);

        gridLayout_9->addWidget(label_73, 5, 0, 1, 1);

        label_74 = new QLabel(tab);
        label_74->setObjectName(QString::fromUtf8("label_74"));
        label_74->setFont(font3);
        label_74->setAlignment(Qt::AlignCenter);

        gridLayout_9->addWidget(label_74, 6, 0, 1, 1);

        label_75 = new QLabel(tab);
        label_75->setObjectName(QString::fromUtf8("label_75"));
        label_75->setFont(font3);
        label_75->setAlignment(Qt::AlignCenter);

        gridLayout_9->addWidget(label_75, 7, 0, 1, 1);

        label_76 = new QLabel(tab);
        label_76->setObjectName(QString::fromUtf8("label_76"));
        label_76->setFont(font3);
        label_76->setAlignment(Qt::AlignCenter);

        gridLayout_9->addWidget(label_76, 8, 0, 1, 1);

        label_77 = new QLabel(tab);
        label_77->setObjectName(QString::fromUtf8("label_77"));
        label_77->setFont(font3);
        label_77->setAlignment(Qt::AlignCenter);

        gridLayout_9->addWidget(label_77, 9, 0, 1, 1);

        label_78 = new QLabel(tab);
        label_78->setObjectName(QString::fromUtf8("label_78"));
        label_78->setFont(font3);
        label_78->setAlignment(Qt::AlignCenter);

        gridLayout_9->addWidget(label_78, 10, 0, 1, 1);

        label_79 = new QLabel(tab);
        label_79->setObjectName(QString::fromUtf8("label_79"));
        label_79->setFont(font3);
        label_79->setAlignment(Qt::AlignCenter);

        gridLayout_9->addWidget(label_79, 11, 0, 1, 1);

        label_80 = new QLabel(tab);
        label_80->setObjectName(QString::fromUtf8("label_80"));
        label_80->setFont(font3);
        label_80->setAlignment(Qt::AlignCenter);

        gridLayout_9->addWidget(label_80, 12, 0, 1, 1);

        label_81 = new QLabel(tab);
        label_81->setObjectName(QString::fromUtf8("label_81"));
        label_81->setFont(font3);
        label_81->setAlignment(Qt::AlignCenter);

        gridLayout_9->addWidget(label_81, 13, 0, 1, 1);

        label_82 = new QLabel(tab);
        label_82->setObjectName(QString::fromUtf8("label_82"));
        label_82->setFont(font3);
        label_82->setAlignment(Qt::AlignCenter);

        gridLayout_9->addWidget(label_82, 14, 0, 1, 1);

        label_83 = new QLabel(tab);
        label_83->setObjectName(QString::fromUtf8("label_83"));
        label_83->setFont(font3);
        label_83->setAlignment(Qt::AlignCenter);

        gridLayout_9->addWidget(label_83, 15, 0, 1, 1);


        gridLayout_3->addLayout(gridLayout_9, 0, 2, 1, 1);

        gridLayout_2 = new QGridLayout();
        gridLayout_2->setObjectName(QString::fromUtf8("gridLayout_2"));
        horizontalLayout_16 = new QHBoxLayout();
        horizontalLayout_16->setObjectName(QString::fromUtf8("horizontalLayout_16"));
        label_5 = new QLabel(tab);
        label_5->setObjectName(QString::fromUtf8("label_5"));
        label_5->setFont(font2);
        label_5->setAlignment(Qt::AlignCenter);

        horizontalLayout_16->addWidget(label_5);

        spinBox_16 = new QSpinBox(tab);
        spinBox_16->setObjectName(QString::fromUtf8("spinBox_16"));

        horizontalLayout_16->addWidget(spinBox_16);


        gridLayout_2->addLayout(horizontalLayout_16, 0, 0, 1, 1);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        horizontalSlider_86 = new QSlider(tab);
        horizontalSlider_86->setObjectName(QString::fromUtf8("horizontalSlider_86"));
        horizontalSlider_86->setCursor(QCursor(Qt::PointingHandCursor));
        horizontalSlider_86->setMaximum(10);
        horizontalSlider_86->setOrientation(Qt::Horizontal);

        horizontalLayout->addWidget(horizontalSlider_86);

        spinBox = new QSpinBox(tab);
        spinBox->setObjectName(QString::fromUtf8("spinBox"));
        spinBox->setMaximum(10);

        horizontalLayout->addWidget(spinBox);


        gridLayout_2->addLayout(horizontalLayout, 1, 0, 1, 1);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        horizontalSlider_87 = new QSlider(tab);
        horizontalSlider_87->setObjectName(QString::fromUtf8("horizontalSlider_87"));
        horizontalSlider_87->setCursor(QCursor(Qt::PointingHandCursor));
        horizontalSlider_87->setMaximum(10);
        horizontalSlider_87->setOrientation(Qt::Horizontal);

        horizontalLayout_2->addWidget(horizontalSlider_87);

        spinBox_2 = new QSpinBox(tab);
        spinBox_2->setObjectName(QString::fromUtf8("spinBox_2"));
        spinBox_2->setMaximum(10);

        horizontalLayout_2->addWidget(spinBox_2);


        gridLayout_2->addLayout(horizontalLayout_2, 2, 0, 1, 1);

        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        horizontalSlider_88 = new QSlider(tab);
        horizontalSlider_88->setObjectName(QString::fromUtf8("horizontalSlider_88"));
        horizontalSlider_88->setCursor(QCursor(Qt::PointingHandCursor));
        horizontalSlider_88->setMaximum(10);
        horizontalSlider_88->setOrientation(Qt::Horizontal);

        horizontalLayout_3->addWidget(horizontalSlider_88);

        spinBox_3 = new QSpinBox(tab);
        spinBox_3->setObjectName(QString::fromUtf8("spinBox_3"));
        spinBox_3->setMaximum(10);

        horizontalLayout_3->addWidget(spinBox_3);


        gridLayout_2->addLayout(horizontalLayout_3, 3, 0, 1, 1);

        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4->setObjectName(QString::fromUtf8("horizontalLayout_4"));
        horizontalSlider_89 = new QSlider(tab);
        horizontalSlider_89->setObjectName(QString::fromUtf8("horizontalSlider_89"));
        horizontalSlider_89->setCursor(QCursor(Qt::PointingHandCursor));
        horizontalSlider_89->setMaximum(10);
        horizontalSlider_89->setOrientation(Qt::Horizontal);

        horizontalLayout_4->addWidget(horizontalSlider_89);

        spinBox_4 = new QSpinBox(tab);
        spinBox_4->setObjectName(QString::fromUtf8("spinBox_4"));
        spinBox_4->setMaximum(10);

        horizontalLayout_4->addWidget(spinBox_4);


        gridLayout_2->addLayout(horizontalLayout_4, 4, 0, 1, 1);

        horizontalLayout_5 = new QHBoxLayout();
        horizontalLayout_5->setObjectName(QString::fromUtf8("horizontalLayout_5"));
        horizontalSlider_90 = new QSlider(tab);
        horizontalSlider_90->setObjectName(QString::fromUtf8("horizontalSlider_90"));
        horizontalSlider_90->setCursor(QCursor(Qt::PointingHandCursor));
        horizontalSlider_90->setMaximum(10);
        horizontalSlider_90->setOrientation(Qt::Horizontal);

        horizontalLayout_5->addWidget(horizontalSlider_90);

        spinBox_5 = new QSpinBox(tab);
        spinBox_5->setObjectName(QString::fromUtf8("spinBox_5"));
        spinBox_5->setMaximum(10);

        horizontalLayout_5->addWidget(spinBox_5);


        gridLayout_2->addLayout(horizontalLayout_5, 5, 0, 1, 1);

        horizontalLayout_13 = new QHBoxLayout();
        horizontalLayout_13->setObjectName(QString::fromUtf8("horizontalLayout_13"));
        horizontalSlider_91 = new QSlider(tab);
        horizontalSlider_91->setObjectName(QString::fromUtf8("horizontalSlider_91"));
        horizontalSlider_91->setCursor(QCursor(Qt::PointingHandCursor));
        horizontalSlider_91->setMaximum(10);
        horizontalSlider_91->setOrientation(Qt::Horizontal);

        horizontalLayout_13->addWidget(horizontalSlider_91);

        spinBox_13 = new QSpinBox(tab);
        spinBox_13->setObjectName(QString::fromUtf8("spinBox_13"));
        spinBox_13->setMaximum(10);

        horizontalLayout_13->addWidget(spinBox_13);


        gridLayout_2->addLayout(horizontalLayout_13, 6, 0, 1, 1);

        horizontalLayout_12 = new QHBoxLayout();
        horizontalLayout_12->setObjectName(QString::fromUtf8("horizontalLayout_12"));
        horizontalSlider_92 = new QSlider(tab);
        horizontalSlider_92->setObjectName(QString::fromUtf8("horizontalSlider_92"));
        horizontalSlider_92->setCursor(QCursor(Qt::PointingHandCursor));
        horizontalSlider_92->setMaximum(10);
        horizontalSlider_92->setOrientation(Qt::Horizontal);

        horizontalLayout_12->addWidget(horizontalSlider_92);

        spinBox_12 = new QSpinBox(tab);
        spinBox_12->setObjectName(QString::fromUtf8("spinBox_12"));
        spinBox_12->setMaximum(10);

        horizontalLayout_12->addWidget(spinBox_12);


        gridLayout_2->addLayout(horizontalLayout_12, 7, 0, 1, 1);

        horizontalLayout_15 = new QHBoxLayout();
        horizontalLayout_15->setObjectName(QString::fromUtf8("horizontalLayout_15"));
        horizontalSlider_93 = new QSlider(tab);
        horizontalSlider_93->setObjectName(QString::fromUtf8("horizontalSlider_93"));
        horizontalSlider_93->setCursor(QCursor(Qt::PointingHandCursor));
        horizontalSlider_93->setMaximum(10);
        horizontalSlider_93->setOrientation(Qt::Horizontal);

        horizontalLayout_15->addWidget(horizontalSlider_93);

        spinBox_15 = new QSpinBox(tab);
        spinBox_15->setObjectName(QString::fromUtf8("spinBox_15"));
        spinBox_15->setMaximum(10);

        horizontalLayout_15->addWidget(spinBox_15);


        gridLayout_2->addLayout(horizontalLayout_15, 8, 0, 1, 1);

        horizontalLayout_14 = new QHBoxLayout();
        horizontalLayout_14->setObjectName(QString::fromUtf8("horizontalLayout_14"));
        horizontalSlider_94 = new QSlider(tab);
        horizontalSlider_94->setObjectName(QString::fromUtf8("horizontalSlider_94"));
        horizontalSlider_94->setCursor(QCursor(Qt::PointingHandCursor));
        horizontalSlider_94->setMaximum(10);
        horizontalSlider_94->setOrientation(Qt::Horizontal);

        horizontalLayout_14->addWidget(horizontalSlider_94);

        spinBox_14 = new QSpinBox(tab);
        spinBox_14->setObjectName(QString::fromUtf8("spinBox_14"));
        spinBox_14->setMaximum(10);

        horizontalLayout_14->addWidget(spinBox_14);


        gridLayout_2->addLayout(horizontalLayout_14, 9, 0, 1, 1);

        horizontalLayout_11 = new QHBoxLayout();
        horizontalLayout_11->setObjectName(QString::fromUtf8("horizontalLayout_11"));
        horizontalSlider_95 = new QSlider(tab);
        horizontalSlider_95->setObjectName(QString::fromUtf8("horizontalSlider_95"));
        horizontalSlider_95->setCursor(QCursor(Qt::PointingHandCursor));
        horizontalSlider_95->setMaximum(10);
        horizontalSlider_95->setOrientation(Qt::Horizontal);

        horizontalLayout_11->addWidget(horizontalSlider_95);

        spinBox_11 = new QSpinBox(tab);
        spinBox_11->setObjectName(QString::fromUtf8("spinBox_11"));
        spinBox_11->setMaximum(10);

        horizontalLayout_11->addWidget(spinBox_11);


        gridLayout_2->addLayout(horizontalLayout_11, 10, 0, 1, 1);

        horizontalLayout_8 = new QHBoxLayout();
        horizontalLayout_8->setObjectName(QString::fromUtf8("horizontalLayout_8"));
        horizontalSlider_96 = new QSlider(tab);
        horizontalSlider_96->setObjectName(QString::fromUtf8("horizontalSlider_96"));
        horizontalSlider_96->setCursor(QCursor(Qt::PointingHandCursor));
        horizontalSlider_96->setMaximum(10);
        horizontalSlider_96->setOrientation(Qt::Horizontal);

        horizontalLayout_8->addWidget(horizontalSlider_96);

        spinBox_8 = new QSpinBox(tab);
        spinBox_8->setObjectName(QString::fromUtf8("spinBox_8"));
        spinBox_8->setMaximum(10);

        horizontalLayout_8->addWidget(spinBox_8);


        gridLayout_2->addLayout(horizontalLayout_8, 11, 0, 1, 1);

        horizontalLayout_7 = new QHBoxLayout();
        horizontalLayout_7->setObjectName(QString::fromUtf8("horizontalLayout_7"));
        horizontalSlider_97 = new QSlider(tab);
        horizontalSlider_97->setObjectName(QString::fromUtf8("horizontalSlider_97"));
        horizontalSlider_97->setCursor(QCursor(Qt::PointingHandCursor));
        horizontalSlider_97->setMaximum(10);
        horizontalSlider_97->setOrientation(Qt::Horizontal);

        horizontalLayout_7->addWidget(horizontalSlider_97);

        spinBox_7 = new QSpinBox(tab);
        spinBox_7->setObjectName(QString::fromUtf8("spinBox_7"));
        spinBox_7->setMaximum(10);

        horizontalLayout_7->addWidget(spinBox_7);


        gridLayout_2->addLayout(horizontalLayout_7, 12, 0, 1, 1);

        horizontalLayout_10 = new QHBoxLayout();
        horizontalLayout_10->setObjectName(QString::fromUtf8("horizontalLayout_10"));
        horizontalSlider_98 = new QSlider(tab);
        horizontalSlider_98->setObjectName(QString::fromUtf8("horizontalSlider_98"));
        horizontalSlider_98->setCursor(QCursor(Qt::PointingHandCursor));
        horizontalSlider_98->setMaximum(10);
        horizontalSlider_98->setOrientation(Qt::Horizontal);

        horizontalLayout_10->addWidget(horizontalSlider_98);

        spinBox_10 = new QSpinBox(tab);
        spinBox_10->setObjectName(QString::fromUtf8("spinBox_10"));
        spinBox_10->setMaximum(10);

        horizontalLayout_10->addWidget(spinBox_10);


        gridLayout_2->addLayout(horizontalLayout_10, 13, 0, 1, 1);

        horizontalLayout_9 = new QHBoxLayout();
        horizontalLayout_9->setObjectName(QString::fromUtf8("horizontalLayout_9"));
        horizontalSlider_99 = new QSlider(tab);
        horizontalSlider_99->setObjectName(QString::fromUtf8("horizontalSlider_99"));
        horizontalSlider_99->setCursor(QCursor(Qt::PointingHandCursor));
        horizontalSlider_99->setMaximum(10);
        horizontalSlider_99->setOrientation(Qt::Horizontal);

        horizontalLayout_9->addWidget(horizontalSlider_99);

        spinBox_9 = new QSpinBox(tab);
        spinBox_9->setObjectName(QString::fromUtf8("spinBox_9"));
        spinBox_9->setMaximum(10);

        horizontalLayout_9->addWidget(spinBox_9);


        gridLayout_2->addLayout(horizontalLayout_9, 14, 0, 1, 1);

        horizontalLayout_6 = new QHBoxLayout();
        horizontalLayout_6->setObjectName(QString::fromUtf8("horizontalLayout_6"));
        horizontalSlider_100 = new QSlider(tab);
        horizontalSlider_100->setObjectName(QString::fromUtf8("horizontalSlider_100"));
        horizontalSlider_100->setCursor(QCursor(Qt::PointingHandCursor));
        horizontalSlider_100->setMaximum(10);
        horizontalSlider_100->setOrientation(Qt::Horizontal);

        horizontalLayout_6->addWidget(horizontalSlider_100);

        spinBox_6 = new QSpinBox(tab);
        spinBox_6->setObjectName(QString::fromUtf8("spinBox_6"));
        spinBox_6->setMaximum(10);

        horizontalLayout_6->addWidget(spinBox_6);


        gridLayout_2->addLayout(horizontalLayout_6, 15, 0, 1, 1);


        gridLayout_3->addLayout(gridLayout_2, 0, 3, 1, 1);


        gridLayout_4->addLayout(gridLayout_3, 4, 0, 1, 2);

        gridLayout_5 = new QGridLayout();
        gridLayout_5->setObjectName(QString::fromUtf8("gridLayout_5"));
        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_5->addItem(horizontalSpacer, 0, 1, 1, 1);

        pushButton_2 = new QPushButton(tab);
        pushButton_2->setObjectName(QString::fromUtf8("pushButton_2"));
        QFont font4;
        font4.setBold(true);
        font4.setWeight(75);
        pushButton_2->setFont(font4);
        pushButton_2->setCursor(QCursor(Qt::PointingHandCursor));

        gridLayout_5->addWidget(pushButton_2, 0, 0, 1, 1);


        gridLayout_4->addLayout(gridLayout_5, 5, 0, 1, 2);

        label_50 = new QLabel(tab);
        label_50->setObjectName(QString::fromUtf8("label_50"));
        QFont font5;
        font5.setPointSize(26);
        font5.setBold(true);
        font5.setWeight(75);
        label_50->setFont(font5);
        label_50->setAlignment(Qt::AlignBottom|Qt::AlignLeading|Qt::AlignLeft);

        gridLayout_4->addWidget(label_50, 0, 0, 1, 1);


        gridLayout_15->addLayout(gridLayout_4, 0, 0, 1, 1);

        tabWidget->addTab(tab, QString());
        tab_2 = new QWidget();
        tab_2->setObjectName(QString::fromUtf8("tab_2"));
        gridLayout_6 = new QGridLayout(tab_2);
        gridLayout_6->setObjectName(QString::fromUtf8("gridLayout_6"));
        splitter = new QSplitter(tab_2);
        splitter->setObjectName(QString::fromUtf8("splitter"));
        splitter->setOrientation(Qt::Vertical);
        label_4 = new QLabel(splitter);
        label_4->setObjectName(QString::fromUtf8("label_4"));
        label_4->setFont(font5);
        label_4->setAlignment(Qt::AlignCenter);
        splitter->addWidget(label_4);
        layoutWidget = new QWidget(splitter);
        layoutWidget->setObjectName(QString::fromUtf8("layoutWidget"));
        gridLayout = new QGridLayout(layoutWidget);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        gridLayout->setContentsMargins(0, 0, 0, 0);
        gridLayout_11 = new QGridLayout();
        gridLayout_11->setObjectName(QString::fromUtf8("gridLayout_11"));
        checkBox_18 = new QCheckBox(layoutWidget);
        checkBox_18->setObjectName(QString::fromUtf8("checkBox_18"));
        QSizePolicy sizePolicy(QSizePolicy::Minimum, QSizePolicy::Minimum);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(checkBox_18->sizePolicy().hasHeightForWidth());
        checkBox_18->setSizePolicy(sizePolicy);

        gridLayout_11->addWidget(checkBox_18, 1, 0, 1, 1);

        checkBox_19 = new QCheckBox(layoutWidget);
        checkBox_19->setObjectName(QString::fromUtf8("checkBox_19"));
        sizePolicy.setHeightForWidth(checkBox_19->sizePolicy().hasHeightForWidth());
        checkBox_19->setSizePolicy(sizePolicy);

        gridLayout_11->addWidget(checkBox_19, 2, 0, 1, 1);

        checkBox_20 = new QCheckBox(layoutWidget);
        checkBox_20->setObjectName(QString::fromUtf8("checkBox_20"));
        sizePolicy.setHeightForWidth(checkBox_20->sizePolicy().hasHeightForWidth());
        checkBox_20->setSizePolicy(sizePolicy);

        gridLayout_11->addWidget(checkBox_20, 3, 0, 1, 1);

        checkBox_21 = new QCheckBox(layoutWidget);
        checkBox_21->setObjectName(QString::fromUtf8("checkBox_21"));
        sizePolicy.setHeightForWidth(checkBox_21->sizePolicy().hasHeightForWidth());
        checkBox_21->setSizePolicy(sizePolicy);

        gridLayout_11->addWidget(checkBox_21, 4, 0, 1, 1);

        checkBox_22 = new QCheckBox(layoutWidget);
        checkBox_22->setObjectName(QString::fromUtf8("checkBox_22"));
        sizePolicy.setHeightForWidth(checkBox_22->sizePolicy().hasHeightForWidth());
        checkBox_22->setSizePolicy(sizePolicy);

        gridLayout_11->addWidget(checkBox_22, 5, 0, 1, 1);

        checkBox_23 = new QCheckBox(layoutWidget);
        checkBox_23->setObjectName(QString::fromUtf8("checkBox_23"));
        sizePolicy.setHeightForWidth(checkBox_23->sizePolicy().hasHeightForWidth());
        checkBox_23->setSizePolicy(sizePolicy);

        gridLayout_11->addWidget(checkBox_23, 6, 0, 1, 1);

        checkBox_24 = new QCheckBox(layoutWidget);
        checkBox_24->setObjectName(QString::fromUtf8("checkBox_24"));
        sizePolicy.setHeightForWidth(checkBox_24->sizePolicy().hasHeightForWidth());
        checkBox_24->setSizePolicy(sizePolicy);

        gridLayout_11->addWidget(checkBox_24, 7, 0, 1, 1);

        checkBox_25 = new QCheckBox(layoutWidget);
        checkBox_25->setObjectName(QString::fromUtf8("checkBox_25"));
        sizePolicy.setHeightForWidth(checkBox_25->sizePolicy().hasHeightForWidth());
        checkBox_25->setSizePolicy(sizePolicy);

        gridLayout_11->addWidget(checkBox_25, 8, 0, 1, 1);

        checkBox_26 = new QCheckBox(layoutWidget);
        checkBox_26->setObjectName(QString::fromUtf8("checkBox_26"));
        sizePolicy.setHeightForWidth(checkBox_26->sizePolicy().hasHeightForWidth());
        checkBox_26->setSizePolicy(sizePolicy);

        gridLayout_11->addWidget(checkBox_26, 9, 0, 1, 1);

        checkBox_27 = new QCheckBox(layoutWidget);
        checkBox_27->setObjectName(QString::fromUtf8("checkBox_27"));
        sizePolicy.setHeightForWidth(checkBox_27->sizePolicy().hasHeightForWidth());
        checkBox_27->setSizePolicy(sizePolicy);

        gridLayout_11->addWidget(checkBox_27, 10, 0, 1, 1);

        checkBox_28 = new QCheckBox(layoutWidget);
        checkBox_28->setObjectName(QString::fromUtf8("checkBox_28"));
        sizePolicy.setHeightForWidth(checkBox_28->sizePolicy().hasHeightForWidth());
        checkBox_28->setSizePolicy(sizePolicy);

        gridLayout_11->addWidget(checkBox_28, 11, 0, 1, 1);

        checkBox_29 = new QCheckBox(layoutWidget);
        checkBox_29->setObjectName(QString::fromUtf8("checkBox_29"));
        sizePolicy.setHeightForWidth(checkBox_29->sizePolicy().hasHeightForWidth());
        checkBox_29->setSizePolicy(sizePolicy);

        gridLayout_11->addWidget(checkBox_29, 12, 0, 1, 1);

        checkBox_30 = new QCheckBox(layoutWidget);
        checkBox_30->setObjectName(QString::fromUtf8("checkBox_30"));
        sizePolicy.setHeightForWidth(checkBox_30->sizePolicy().hasHeightForWidth());
        checkBox_30->setSizePolicy(sizePolicy);

        gridLayout_11->addWidget(checkBox_30, 13, 0, 1, 1);

        checkBox_31 = new QCheckBox(layoutWidget);
        checkBox_31->setObjectName(QString::fromUtf8("checkBox_31"));
        sizePolicy.setHeightForWidth(checkBox_31->sizePolicy().hasHeightForWidth());
        checkBox_31->setSizePolicy(sizePolicy);

        gridLayout_11->addWidget(checkBox_31, 14, 0, 1, 1);

        checkBox_32 = new QCheckBox(layoutWidget);
        checkBox_32->setObjectName(QString::fromUtf8("checkBox_32"));
        sizePolicy.setHeightForWidth(checkBox_32->sizePolicy().hasHeightForWidth());
        checkBox_32->setSizePolicy(sizePolicy);

        gridLayout_11->addWidget(checkBox_32, 15, 0, 1, 1);

        checkBox_17 = new QCheckBox(layoutWidget);
        checkBox_17->setObjectName(QString::fromUtf8("checkBox_17"));
        sizePolicy.setHeightForWidth(checkBox_17->sizePolicy().hasHeightForWidth());
        checkBox_17->setSizePolicy(sizePolicy);
        QFont font6;
        font6.setFamily(QString::fromUtf8("Copperplate Gothic Light"));
        font6.setPointSize(14);
        checkBox_17->setFont(font6);

        gridLayout_11->addWidget(checkBox_17, 0, 0, 1, 1);


        gridLayout->addLayout(gridLayout_11, 1, 0, 1, 1);

        gridLayout_14 = new QGridLayout();
        gridLayout_14->setObjectName(QString::fromUtf8("gridLayout_14"));
        label_132 = new QLabel(layoutWidget);
        label_132->setObjectName(QString::fromUtf8("label_132"));
        QFont font7;
        font7.setFamily(QString::fromUtf8("Copperplate Gothic Light"));
        font7.setPointSize(12);
        label_132->setFont(font7);
        label_132->setAlignment(Qt::AlignCenter);

        gridLayout_14->addWidget(label_132, 0, 0, 1, 1);

        label_133 = new QLabel(layoutWidget);
        label_133->setObjectName(QString::fromUtf8("label_133"));
        label_133->setFont(font2);
        label_133->setAlignment(Qt::AlignCenter);

        gridLayout_14->addWidget(label_133, 1, 0, 1, 1);

        label_134 = new QLabel(layoutWidget);
        label_134->setObjectName(QString::fromUtf8("label_134"));
        label_134->setFont(font2);
        label_134->setAlignment(Qt::AlignCenter);

        gridLayout_14->addWidget(label_134, 2, 0, 1, 1);

        label_135 = new QLabel(layoutWidget);
        label_135->setObjectName(QString::fromUtf8("label_135"));
        label_135->setFont(font2);
        label_135->setAlignment(Qt::AlignCenter);

        gridLayout_14->addWidget(label_135, 3, 0, 1, 1);

        label_136 = new QLabel(layoutWidget);
        label_136->setObjectName(QString::fromUtf8("label_136"));
        label_136->setFont(font2);
        label_136->setAlignment(Qt::AlignCenter);

        gridLayout_14->addWidget(label_136, 4, 0, 1, 1);

        label_137 = new QLabel(layoutWidget);
        label_137->setObjectName(QString::fromUtf8("label_137"));
        label_137->setFont(font2);
        label_137->setAlignment(Qt::AlignCenter);

        gridLayout_14->addWidget(label_137, 5, 0, 1, 1);

        label_138 = new QLabel(layoutWidget);
        label_138->setObjectName(QString::fromUtf8("label_138"));
        label_138->setFont(font2);
        label_138->setAlignment(Qt::AlignCenter);

        gridLayout_14->addWidget(label_138, 6, 0, 1, 1);

        label_139 = new QLabel(layoutWidget);
        label_139->setObjectName(QString::fromUtf8("label_139"));
        label_139->setFont(font2);
        label_139->setAlignment(Qt::AlignCenter);

        gridLayout_14->addWidget(label_139, 7, 0, 1, 1);

        label_140 = new QLabel(layoutWidget);
        label_140->setObjectName(QString::fromUtf8("label_140"));
        label_140->setFont(font2);
        label_140->setAlignment(Qt::AlignCenter);

        gridLayout_14->addWidget(label_140, 8, 0, 1, 1);

        label_141 = new QLabel(layoutWidget);
        label_141->setObjectName(QString::fromUtf8("label_141"));
        label_141->setFont(font2);
        label_141->setAlignment(Qt::AlignCenter);

        gridLayout_14->addWidget(label_141, 9, 0, 1, 1);

        label_142 = new QLabel(layoutWidget);
        label_142->setObjectName(QString::fromUtf8("label_142"));
        label_142->setFont(font2);
        label_142->setAlignment(Qt::AlignCenter);

        gridLayout_14->addWidget(label_142, 10, 0, 1, 1);

        label_143 = new QLabel(layoutWidget);
        label_143->setObjectName(QString::fromUtf8("label_143"));
        label_143->setFont(font2);
        label_143->setAlignment(Qt::AlignCenter);

        gridLayout_14->addWidget(label_143, 11, 0, 1, 1);

        label_144 = new QLabel(layoutWidget);
        label_144->setObjectName(QString::fromUtf8("label_144"));
        label_144->setFont(font2);
        label_144->setAlignment(Qt::AlignCenter);

        gridLayout_14->addWidget(label_144, 12, 0, 1, 1);

        label_145 = new QLabel(layoutWidget);
        label_145->setObjectName(QString::fromUtf8("label_145"));
        label_145->setFont(font2);
        label_145->setAlignment(Qt::AlignCenter);

        gridLayout_14->addWidget(label_145, 13, 0, 1, 1);

        label_146 = new QLabel(layoutWidget);
        label_146->setObjectName(QString::fromUtf8("label_146"));
        label_146->setFont(font2);
        label_146->setAlignment(Qt::AlignCenter);

        gridLayout_14->addWidget(label_146, 14, 0, 1, 1);

        label_147 = new QLabel(layoutWidget);
        label_147->setObjectName(QString::fromUtf8("label_147"));
        label_147->setFont(font2);
        label_147->setAlignment(Qt::AlignCenter);

        gridLayout_14->addWidget(label_147, 15, 0, 1, 1);


        gridLayout->addLayout(gridLayout_14, 1, 1, 1, 1);

        gridLayout_12 = new QGridLayout();
        gridLayout_12->setObjectName(QString::fromUtf8("gridLayout_12"));
        label_100 = new QLabel(layoutWidget);
        label_100->setObjectName(QString::fromUtf8("label_100"));
        label_100->setFont(font7);
        label_100->setAlignment(Qt::AlignCenter);

        gridLayout_12->addWidget(label_100, 0, 0, 1, 1);

        label_101 = new QLabel(layoutWidget);
        label_101->setObjectName(QString::fromUtf8("label_101"));
        label_101->setFont(font2);
        label_101->setAlignment(Qt::AlignCenter);

        gridLayout_12->addWidget(label_101, 1, 0, 1, 1);

        label_102 = new QLabel(layoutWidget);
        label_102->setObjectName(QString::fromUtf8("label_102"));
        label_102->setFont(font2);
        label_102->setAlignment(Qt::AlignCenter);

        gridLayout_12->addWidget(label_102, 2, 0, 1, 1);

        label_103 = new QLabel(layoutWidget);
        label_103->setObjectName(QString::fromUtf8("label_103"));
        label_103->setFont(font2);
        label_103->setAlignment(Qt::AlignCenter);

        gridLayout_12->addWidget(label_103, 3, 0, 1, 1);

        label_104 = new QLabel(layoutWidget);
        label_104->setObjectName(QString::fromUtf8("label_104"));
        label_104->setFont(font2);
        label_104->setAlignment(Qt::AlignCenter);

        gridLayout_12->addWidget(label_104, 4, 0, 1, 1);

        label_105 = new QLabel(layoutWidget);
        label_105->setObjectName(QString::fromUtf8("label_105"));
        label_105->setFont(font2);
        label_105->setAlignment(Qt::AlignCenter);

        gridLayout_12->addWidget(label_105, 5, 0, 1, 1);

        label_106 = new QLabel(layoutWidget);
        label_106->setObjectName(QString::fromUtf8("label_106"));
        label_106->setFont(font2);
        label_106->setAlignment(Qt::AlignCenter);

        gridLayout_12->addWidget(label_106, 6, 0, 1, 1);

        label_107 = new QLabel(layoutWidget);
        label_107->setObjectName(QString::fromUtf8("label_107"));
        label_107->setFont(font2);
        label_107->setAlignment(Qt::AlignCenter);

        gridLayout_12->addWidget(label_107, 7, 0, 1, 1);

        label_108 = new QLabel(layoutWidget);
        label_108->setObjectName(QString::fromUtf8("label_108"));
        label_108->setFont(font2);
        label_108->setAlignment(Qt::AlignCenter);

        gridLayout_12->addWidget(label_108, 8, 0, 1, 1);

        label_109 = new QLabel(layoutWidget);
        label_109->setObjectName(QString::fromUtf8("label_109"));
        label_109->setFont(font2);
        label_109->setAlignment(Qt::AlignCenter);

        gridLayout_12->addWidget(label_109, 9, 0, 1, 1);

        label_110 = new QLabel(layoutWidget);
        label_110->setObjectName(QString::fromUtf8("label_110"));
        label_110->setFont(font2);
        label_110->setAlignment(Qt::AlignCenter);

        gridLayout_12->addWidget(label_110, 10, 0, 1, 1);

        label_111 = new QLabel(layoutWidget);
        label_111->setObjectName(QString::fromUtf8("label_111"));
        label_111->setFont(font2);
        label_111->setAlignment(Qt::AlignCenter);

        gridLayout_12->addWidget(label_111, 11, 0, 1, 1);

        label_112 = new QLabel(layoutWidget);
        label_112->setObjectName(QString::fromUtf8("label_112"));
        label_112->setFont(font2);
        label_112->setAlignment(Qt::AlignCenter);

        gridLayout_12->addWidget(label_112, 12, 0, 1, 1);

        label_113 = new QLabel(layoutWidget);
        label_113->setObjectName(QString::fromUtf8("label_113"));
        label_113->setFont(font2);
        label_113->setAlignment(Qt::AlignCenter);

        gridLayout_12->addWidget(label_113, 13, 0, 1, 1);

        label_114 = new QLabel(layoutWidget);
        label_114->setObjectName(QString::fromUtf8("label_114"));
        label_114->setFont(font2);
        label_114->setAlignment(Qt::AlignCenter);

        gridLayout_12->addWidget(label_114, 14, 0, 1, 1);

        label_115 = new QLabel(layoutWidget);
        label_115->setObjectName(QString::fromUtf8("label_115"));
        label_115->setFont(font2);
        label_115->setAlignment(Qt::AlignCenter);

        gridLayout_12->addWidget(label_115, 15, 0, 1, 1);


        gridLayout->addLayout(gridLayout_12, 1, 2, 1, 1);

        gridLayout_13 = new QGridLayout();
        gridLayout_13->setObjectName(QString::fromUtf8("gridLayout_13"));
        label_116 = new QLabel(layoutWidget);
        label_116->setObjectName(QString::fromUtf8("label_116"));
        label_116->setFont(font7);
        label_116->setAlignment(Qt::AlignCenter);

        gridLayout_13->addWidget(label_116, 0, 0, 1, 1);

        label_117 = new QLabel(layoutWidget);
        label_117->setObjectName(QString::fromUtf8("label_117"));
        label_117->setFont(font2);
        label_117->setAlignment(Qt::AlignCenter);

        gridLayout_13->addWidget(label_117, 1, 0, 1, 1);

        label_118 = new QLabel(layoutWidget);
        label_118->setObjectName(QString::fromUtf8("label_118"));
        label_118->setFont(font2);
        label_118->setAlignment(Qt::AlignCenter);

        gridLayout_13->addWidget(label_118, 2, 0, 1, 1);

        label_119 = new QLabel(layoutWidget);
        label_119->setObjectName(QString::fromUtf8("label_119"));
        label_119->setFont(font2);
        label_119->setAlignment(Qt::AlignCenter);

        gridLayout_13->addWidget(label_119, 3, 0, 1, 1);

        label_120 = new QLabel(layoutWidget);
        label_120->setObjectName(QString::fromUtf8("label_120"));
        label_120->setFont(font2);
        label_120->setAlignment(Qt::AlignCenter);

        gridLayout_13->addWidget(label_120, 4, 0, 1, 1);

        label_121 = new QLabel(layoutWidget);
        label_121->setObjectName(QString::fromUtf8("label_121"));
        label_121->setFont(font2);
        label_121->setAlignment(Qt::AlignCenter);

        gridLayout_13->addWidget(label_121, 5, 0, 1, 1);

        label_122 = new QLabel(layoutWidget);
        label_122->setObjectName(QString::fromUtf8("label_122"));
        label_122->setFont(font2);
        label_122->setAlignment(Qt::AlignCenter);

        gridLayout_13->addWidget(label_122, 6, 0, 1, 1);

        label_123 = new QLabel(layoutWidget);
        label_123->setObjectName(QString::fromUtf8("label_123"));
        label_123->setFont(font2);
        label_123->setAlignment(Qt::AlignCenter);

        gridLayout_13->addWidget(label_123, 7, 0, 1, 1);

        label_124 = new QLabel(layoutWidget);
        label_124->setObjectName(QString::fromUtf8("label_124"));
        label_124->setFont(font2);
        label_124->setAlignment(Qt::AlignCenter);

        gridLayout_13->addWidget(label_124, 8, 0, 1, 1);

        label_125 = new QLabel(layoutWidget);
        label_125->setObjectName(QString::fromUtf8("label_125"));
        label_125->setFont(font2);
        label_125->setAlignment(Qt::AlignCenter);

        gridLayout_13->addWidget(label_125, 9, 0, 1, 1);

        label_126 = new QLabel(layoutWidget);
        label_126->setObjectName(QString::fromUtf8("label_126"));
        label_126->setFont(font2);
        label_126->setAlignment(Qt::AlignCenter);

        gridLayout_13->addWidget(label_126, 10, 0, 1, 1);

        label_127 = new QLabel(layoutWidget);
        label_127->setObjectName(QString::fromUtf8("label_127"));
        label_127->setFont(font2);
        label_127->setAlignment(Qt::AlignCenter);

        gridLayout_13->addWidget(label_127, 11, 0, 1, 1);

        label_128 = new QLabel(layoutWidget);
        label_128->setObjectName(QString::fromUtf8("label_128"));
        label_128->setFont(font2);
        label_128->setAlignment(Qt::AlignCenter);

        gridLayout_13->addWidget(label_128, 12, 0, 1, 1);

        label_129 = new QLabel(layoutWidget);
        label_129->setObjectName(QString::fromUtf8("label_129"));
        label_129->setFont(font2);
        label_129->setAlignment(Qt::AlignCenter);

        gridLayout_13->addWidget(label_129, 13, 0, 1, 1);

        label_130 = new QLabel(layoutWidget);
        label_130->setObjectName(QString::fromUtf8("label_130"));
        label_130->setFont(font2);
        label_130->setAlignment(Qt::AlignCenter);

        gridLayout_13->addWidget(label_130, 14, 0, 1, 1);

        label_131 = new QLabel(layoutWidget);
        label_131->setObjectName(QString::fromUtf8("label_131"));
        label_131->setFont(font2);
        label_131->setAlignment(Qt::AlignCenter);

        gridLayout_13->addWidget(label_131, 15, 0, 1, 1);


        gridLayout->addLayout(gridLayout_13, 1, 3, 1, 1);

        verticalSpacer_3 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::MinimumExpanding);

        gridLayout->addItem(verticalSpacer_3, 0, 1, 1, 1);

        splitter->addWidget(layoutWidget);

        gridLayout_6->addWidget(splitter, 0, 0, 1, 1);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Minimum);

        gridLayout_6->addItem(verticalSpacer, 1, 0, 1, 1);

        gridLayout_8 = new QGridLayout();
        gridLayout_8->setObjectName(QString::fromUtf8("gridLayout_8"));
        horizontalSpacer_5 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_8->addItem(horizontalSpacer_5, 0, 0, 1, 1);


        gridLayout_6->addLayout(gridLayout_8, 2, 0, 1, 1);

        tabWidget->addTab(tab_2, QString());
        splitter_2->addWidget(tabWidget);

        gridLayout_17->addWidget(splitter_2, 0, 0, 1, 1);


        retranslateUi(Squad);
        QObject::connect(horizontalSlider_86, SIGNAL(valueChanged(int)), spinBox, SLOT(setValue(int)));
        QObject::connect(spinBox, SIGNAL(valueChanged(int)), horizontalSlider_86, SLOT(setValue(int)));
        QObject::connect(horizontalSlider_87, SIGNAL(valueChanged(int)), spinBox_2, SLOT(setValue(int)));
        QObject::connect(spinBox_2, SIGNAL(valueChanged(int)), horizontalSlider_87, SLOT(setValue(int)));
        QObject::connect(horizontalSlider_88, SIGNAL(valueChanged(int)), spinBox_3, SLOT(setValue(int)));
        QObject::connect(spinBox_3, SIGNAL(valueChanged(int)), horizontalSlider_88, SLOT(setValue(int)));
        QObject::connect(horizontalSlider_89, SIGNAL(valueChanged(int)), spinBox_4, SLOT(setValue(int)));
        QObject::connect(spinBox_4, SIGNAL(valueChanged(int)), horizontalSlider_89, SLOT(setValue(int)));
        QObject::connect(horizontalSlider_90, SIGNAL(valueChanged(int)), spinBox_5, SLOT(setValue(int)));
        QObject::connect(spinBox_5, SIGNAL(valueChanged(int)), horizontalSlider_90, SLOT(setValue(int)));
        QObject::connect(horizontalSlider_91, SIGNAL(valueChanged(int)), spinBox_13, SLOT(setValue(int)));
        QObject::connect(spinBox_13, SIGNAL(valueChanged(int)), horizontalSlider_91, SLOT(setValue(int)));
        QObject::connect(horizontalSlider_92, SIGNAL(valueChanged(int)), spinBox_12, SLOT(setValue(int)));
        QObject::connect(spinBox_12, SIGNAL(valueChanged(int)), horizontalSlider_92, SLOT(setValue(int)));
        QObject::connect(horizontalSlider_93, SIGNAL(valueChanged(int)), spinBox_15, SLOT(setValue(int)));
        QObject::connect(spinBox_15, SIGNAL(valueChanged(int)), horizontalSlider_93, SLOT(setValue(int)));
        QObject::connect(horizontalSlider_94, SIGNAL(valueChanged(int)), spinBox_14, SLOT(setValue(int)));
        QObject::connect(spinBox_14, SIGNAL(valueChanged(int)), horizontalSlider_94, SLOT(setValue(int)));
        QObject::connect(horizontalSlider_95, SIGNAL(valueChanged(int)), spinBox_11, SLOT(setValue(int)));
        QObject::connect(spinBox_11, SIGNAL(valueChanged(int)), horizontalSlider_95, SLOT(setValue(int)));
        QObject::connect(horizontalSlider_96, SIGNAL(valueChanged(int)), spinBox_8, SLOT(setValue(int)));
        QObject::connect(spinBox_8, SIGNAL(valueChanged(int)), horizontalSlider_96, SLOT(setValue(int)));
        QObject::connect(horizontalSlider_97, SIGNAL(valueChanged(int)), spinBox_7, SLOT(setValue(int)));
        QObject::connect(spinBox_7, SIGNAL(valueChanged(int)), horizontalSlider_97, SLOT(setValue(int)));
        QObject::connect(horizontalSlider_98, SIGNAL(valueChanged(int)), spinBox_10, SLOT(setValue(int)));
        QObject::connect(spinBox_10, SIGNAL(valueChanged(int)), horizontalSlider_98, SLOT(setValue(int)));
        QObject::connect(horizontalSlider_99, SIGNAL(valueChanged(int)), spinBox_9, SLOT(setValue(int)));
        QObject::connect(spinBox_9, SIGNAL(valueChanged(int)), horizontalSlider_99, SLOT(setValue(int)));
        QObject::connect(horizontalSlider_100, SIGNAL(valueChanged(int)), spinBox_6, SLOT(setValue(int)));
        QObject::connect(spinBox_6, SIGNAL(valueChanged(int)), horizontalSlider_100, SLOT(setValue(int)));

        tabWidget->setCurrentIndex(0);


        QMetaObject::connectSlotsByName(Squad);
    } // setupUi

    void retranslateUi(QDialog *Squad)
    {
        Squad->setWindowTitle(QApplication::translate("Squad", "Dialog", 0, QApplication::UnicodeUTF8));
        label_2->setText(QApplication::translate("Squad", "Expenditure", 0, QApplication::UnicodeUTF8));
        label_3->setText(QApplication::translate("Squad", "Remaining Balance", 0, QApplication::UnicodeUTF8));
        label_84->setText(QApplication::translate("Squad", "NAME", 0, QApplication::UnicodeUTF8));
        label_85->setText(QApplication::translate("Squad", "TextLabel", 0, QApplication::UnicodeUTF8));
        label_86->setText(QApplication::translate("Squad", "TextLabel", 0, QApplication::UnicodeUTF8));
        label_87->setText(QApplication::translate("Squad", "TextLabel", 0, QApplication::UnicodeUTF8));
        label_88->setText(QApplication::translate("Squad", "TextLabel", 0, QApplication::UnicodeUTF8));
        label_89->setText(QApplication::translate("Squad", "TextLabel", 0, QApplication::UnicodeUTF8));
        label_90->setText(QApplication::translate("Squad", "TextLabel", 0, QApplication::UnicodeUTF8));
        label_91->setText(QApplication::translate("Squad", "TextLabel", 0, QApplication::UnicodeUTF8));
        label_92->setText(QApplication::translate("Squad", "TextLabel", 0, QApplication::UnicodeUTF8));
        label_93->setText(QApplication::translate("Squad", "TextLabel", 0, QApplication::UnicodeUTF8));
        label_94->setText(QApplication::translate("Squad", "TextLabel", 0, QApplication::UnicodeUTF8));
        label_95->setText(QApplication::translate("Squad", "TextLabel", 0, QApplication::UnicodeUTF8));
        label_96->setText(QApplication::translate("Squad", "TextLabel", 0, QApplication::UnicodeUTF8));
        label_97->setText(QApplication::translate("Squad", "TextLabel", 0, QApplication::UnicodeUTF8));
        label_98->setText(QApplication::translate("Squad", "TextLabel", 0, QApplication::UnicodeUTF8));
        label_99->setText(QApplication::translate("Squad", "TextLabel", 0, QApplication::UnicodeUTF8));
        label_51->setText(QApplication::translate("Squad", "OVERALL", 0, QApplication::UnicodeUTF8));
        label_52->setText(QApplication::translate("Squad", "TextLabel", 0, QApplication::UnicodeUTF8));
        label_53->setText(QApplication::translate("Squad", "TextLabel", 0, QApplication::UnicodeUTF8));
        label_54->setText(QApplication::translate("Squad", "TextLabel", 0, QApplication::UnicodeUTF8));
        label_55->setText(QApplication::translate("Squad", "TextLabel", 0, QApplication::UnicodeUTF8));
        label_56->setText(QApplication::translate("Squad", "TextLabel", 0, QApplication::UnicodeUTF8));
        label_57->setText(QApplication::translate("Squad", "TextLabel", 0, QApplication::UnicodeUTF8));
        label_58->setText(QApplication::translate("Squad", "TextLabel", 0, QApplication::UnicodeUTF8));
        label_59->setText(QApplication::translate("Squad", "TextLabel", 0, QApplication::UnicodeUTF8));
        label_60->setText(QApplication::translate("Squad", "TextLabel", 0, QApplication::UnicodeUTF8));
        label_61->setText(QApplication::translate("Squad", "TextLabel", 0, QApplication::UnicodeUTF8));
        label_62->setText(QApplication::translate("Squad", "TextLabel", 0, QApplication::UnicodeUTF8));
        label_63->setText(QApplication::translate("Squad", "TextLabel", 0, QApplication::UnicodeUTF8));
        label_64->setText(QApplication::translate("Squad", "TextLabel", 0, QApplication::UnicodeUTF8));
        label_65->setText(QApplication::translate("Squad", "TextLabel", 0, QApplication::UnicodeUTF8));
        label_66->setText(QApplication::translate("Squad", "TextLabel", 0, QApplication::UnicodeUTF8));
        label_68->setText(QApplication::translate("Squad", "POSITION", 0, QApplication::UnicodeUTF8));
        label_69->setText(QApplication::translate("Squad", "TextLabel", 0, QApplication::UnicodeUTF8));
        label_70->setText(QApplication::translate("Squad", "TextLabel", 0, QApplication::UnicodeUTF8));
        label_71->setText(QApplication::translate("Squad", "TextLabel", 0, QApplication::UnicodeUTF8));
        label_72->setText(QApplication::translate("Squad", "TextLabel", 0, QApplication::UnicodeUTF8));
        label_73->setText(QApplication::translate("Squad", "TextLabel", 0, QApplication::UnicodeUTF8));
        label_74->setText(QApplication::translate("Squad", "TextLabel", 0, QApplication::UnicodeUTF8));
        label_75->setText(QApplication::translate("Squad", "TextLabel", 0, QApplication::UnicodeUTF8));
        label_76->setText(QApplication::translate("Squad", "TextLabel", 0, QApplication::UnicodeUTF8));
        label_77->setText(QApplication::translate("Squad", "TextLabel", 0, QApplication::UnicodeUTF8));
        label_78->setText(QApplication::translate("Squad", "TextLabel", 0, QApplication::UnicodeUTF8));
        label_79->setText(QApplication::translate("Squad", "TextLabel", 0, QApplication::UnicodeUTF8));
        label_80->setText(QApplication::translate("Squad", "TextLabel", 0, QApplication::UnicodeUTF8));
        label_81->setText(QApplication::translate("Squad", "TextLabel", 0, QApplication::UnicodeUTF8));
        label_82->setText(QApplication::translate("Squad", "TextLabel", 0, QApplication::UnicodeUTF8));
        label_83->setText(QApplication::translate("Squad", "TextLabel", 0, QApplication::UnicodeUTF8));
        label_5->setText(QApplication::translate("Squad", "TRAINING", 0, QApplication::UnicodeUTF8));
        pushButton_2->setText(QApplication::translate("Squad", "Back", 0, QApplication::UnicodeUTF8));
        label_50->setText(QApplication::translate("Squad", "TextLabel", 0, QApplication::UnicodeUTF8));
        tabWidget->setTabText(tabWidget->indexOf(tab), QApplication::translate("Squad", "Training", 0, QApplication::UnicodeUTF8));
        label_4->setText(QApplication::translate("Squad", "TextLabel", 0, QApplication::UnicodeUTF8));
        checkBox_18->setText(QApplication::translate("Squad", "1", 0, QApplication::UnicodeUTF8));
        checkBox_19->setText(QApplication::translate("Squad", "2", 0, QApplication::UnicodeUTF8));
        checkBox_20->setText(QApplication::translate("Squad", "3", 0, QApplication::UnicodeUTF8));
        checkBox_21->setText(QApplication::translate("Squad", "4", 0, QApplication::UnicodeUTF8));
        checkBox_22->setText(QApplication::translate("Squad", "5", 0, QApplication::UnicodeUTF8));
        checkBox_23->setText(QApplication::translate("Squad", "6", 0, QApplication::UnicodeUTF8));
        checkBox_24->setText(QApplication::translate("Squad", "7", 0, QApplication::UnicodeUTF8));
        checkBox_25->setText(QApplication::translate("Squad", "8", 0, QApplication::UnicodeUTF8));
        checkBox_26->setText(QApplication::translate("Squad", "9", 0, QApplication::UnicodeUTF8));
        checkBox_27->setText(QApplication::translate("Squad", "10", 0, QApplication::UnicodeUTF8));
        checkBox_28->setText(QApplication::translate("Squad", "11", 0, QApplication::UnicodeUTF8));
        checkBox_29->setText(QApplication::translate("Squad", "12", 0, QApplication::UnicodeUTF8));
        checkBox_30->setText(QApplication::translate("Squad", "13", 0, QApplication::UnicodeUTF8));
        checkBox_31->setText(QApplication::translate("Squad", "14", 0, QApplication::UnicodeUTF8));
        checkBox_32->setText(QApplication::translate("Squad", "15", 0, QApplication::UnicodeUTF8));
        checkBox_17->setText(QApplication::translate("Squad", "SELECT", 0, QApplication::UnicodeUTF8));
        label_132->setText(QApplication::translate("Squad", "NAME", 0, QApplication::UnicodeUTF8));
        label_133->setText(QApplication::translate("Squad", "TextLabel", 0, QApplication::UnicodeUTF8));
        label_134->setText(QApplication::translate("Squad", "TextLabel", 0, QApplication::UnicodeUTF8));
        label_135->setText(QApplication::translate("Squad", "TextLabel", 0, QApplication::UnicodeUTF8));
        label_136->setText(QApplication::translate("Squad", "TextLabel", 0, QApplication::UnicodeUTF8));
        label_137->setText(QApplication::translate("Squad", "TextLabel", 0, QApplication::UnicodeUTF8));
        label_138->setText(QApplication::translate("Squad", "TextLabel", 0, QApplication::UnicodeUTF8));
        label_139->setText(QApplication::translate("Squad", "TextLabel", 0, QApplication::UnicodeUTF8));
        label_140->setText(QApplication::translate("Squad", "TextLabel", 0, QApplication::UnicodeUTF8));
        label_141->setText(QApplication::translate("Squad", "TextLabel", 0, QApplication::UnicodeUTF8));
        label_142->setText(QApplication::translate("Squad", "TextLabel", 0, QApplication::UnicodeUTF8));
        label_143->setText(QApplication::translate("Squad", "TextLabel", 0, QApplication::UnicodeUTF8));
        label_144->setText(QApplication::translate("Squad", "TextLabel", 0, QApplication::UnicodeUTF8));
        label_145->setText(QApplication::translate("Squad", "TextLabel", 0, QApplication::UnicodeUTF8));
        label_146->setText(QApplication::translate("Squad", "TextLabel", 0, QApplication::UnicodeUTF8));
        label_147->setText(QApplication::translate("Squad", "TextLabel", 0, QApplication::UnicodeUTF8));
        label_100->setText(QApplication::translate("Squad", "OVERALL", 0, QApplication::UnicodeUTF8));
        label_101->setText(QApplication::translate("Squad", "TextLabel", 0, QApplication::UnicodeUTF8));
        label_102->setText(QApplication::translate("Squad", "TextLabel", 0, QApplication::UnicodeUTF8));
        label_103->setText(QApplication::translate("Squad", "TextLabel", 0, QApplication::UnicodeUTF8));
        label_104->setText(QApplication::translate("Squad", "TextLabel", 0, QApplication::UnicodeUTF8));
        label_105->setText(QApplication::translate("Squad", "TextLabel", 0, QApplication::UnicodeUTF8));
        label_106->setText(QApplication::translate("Squad", "TextLabel", 0, QApplication::UnicodeUTF8));
        label_107->setText(QApplication::translate("Squad", "TextLabel", 0, QApplication::UnicodeUTF8));
        label_108->setText(QApplication::translate("Squad", "TextLabel", 0, QApplication::UnicodeUTF8));
        label_109->setText(QApplication::translate("Squad", "TextLabel", 0, QApplication::UnicodeUTF8));
        label_110->setText(QApplication::translate("Squad", "TextLabel", 0, QApplication::UnicodeUTF8));
        label_111->setText(QApplication::translate("Squad", "TextLabel", 0, QApplication::UnicodeUTF8));
        label_112->setText(QApplication::translate("Squad", "TextLabel", 0, QApplication::UnicodeUTF8));
        label_113->setText(QApplication::translate("Squad", "TextLabel", 0, QApplication::UnicodeUTF8));
        label_114->setText(QApplication::translate("Squad", "TextLabel", 0, QApplication::UnicodeUTF8));
        label_115->setText(QApplication::translate("Squad", "TextLabel", 0, QApplication::UnicodeUTF8));
        label_116->setText(QApplication::translate("Squad", "POSITION", 0, QApplication::UnicodeUTF8));
        label_117->setText(QApplication::translate("Squad", "TextLabel", 0, QApplication::UnicodeUTF8));
        label_118->setText(QApplication::translate("Squad", "TextLabel", 0, QApplication::UnicodeUTF8));
        label_119->setText(QApplication::translate("Squad", "TextLabel", 0, QApplication::UnicodeUTF8));
        label_120->setText(QApplication::translate("Squad", "TextLabel", 0, QApplication::UnicodeUTF8));
        label_121->setText(QApplication::translate("Squad", "TextLabel", 0, QApplication::UnicodeUTF8));
        label_122->setText(QApplication::translate("Squad", "TextLabel", 0, QApplication::UnicodeUTF8));
        label_123->setText(QApplication::translate("Squad", "TextLabel", 0, QApplication::UnicodeUTF8));
        label_124->setText(QApplication::translate("Squad", "TextLabel", 0, QApplication::UnicodeUTF8));
        label_125->setText(QApplication::translate("Squad", "TextLabel", 0, QApplication::UnicodeUTF8));
        label_126->setText(QApplication::translate("Squad", "TextLabel", 0, QApplication::UnicodeUTF8));
        label_127->setText(QApplication::translate("Squad", "TextLabel", 0, QApplication::UnicodeUTF8));
        label_128->setText(QApplication::translate("Squad", "TextLabel", 0, QApplication::UnicodeUTF8));
        label_129->setText(QApplication::translate("Squad", "TextLabel", 0, QApplication::UnicodeUTF8));
        label_130->setText(QApplication::translate("Squad", "TextLabel", 0, QApplication::UnicodeUTF8));
        label_131->setText(QApplication::translate("Squad", "TextLabel", 0, QApplication::UnicodeUTF8));
        tabWidget->setTabText(tabWidget->indexOf(tab_2), QApplication::translate("Squad", "Squad", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class Squad: public Ui_Squad {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_SQUAD_H
