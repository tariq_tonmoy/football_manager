/********************************************************************************
** Form generated from reading UI file 'stadium.ui'
**
** Created: Thu Apr 19 02:12:48 2012
**      by: Qt User Interface Compiler version 4.6.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_STADIUM_H
#define UI_STADIUM_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QDialog>
#include <QtGui/QFormLayout>
#include <QtGui/QGridLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QLineEdit>
#include <QtGui/QPushButton>
#include <QtGui/QSlider>
#include <QtGui/QSpacerItem>
#include <QtGui/QSpinBox>
#include <QtGui/QSplitter>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_Stadium
{
public:
    QGridLayout *gridLayout_7;
    QLabel *label_5;
    QGridLayout *gridLayout_6;
    QSplitter *splitter;
    QWidget *layoutWidget;
    QGridLayout *gridLayout_3;
    QFormLayout *formLayout_4;
    QLineEdit *lineEdit_4;
    QLabel *label_8;
    QFormLayout *formLayout_5;
    QLineEdit *lineEdit_5;
    QLabel *label_9;
    QWidget *layoutWidget1;
    QGridLayout *gridLayout_2;
    QFormLayout *formLayout;
    QLabel *label_3;
    QLineEdit *lineEdit;
    QFormLayout *formLayout_2;
    QLineEdit *lineEdit_2;
    QLabel *label_4;
    QFormLayout *formLayout_3;
    QLineEdit *lineEdit_3;
    QLabel *label_7;
    QGridLayout *gridLayout_5;
    QGridLayout *gridLayout;
    QLabel *label_6;
    QSlider *verticalSlider;
    QLabel *label;
    QGridLayout *gridLayout_4;
    QLabel *label_2;
    QSpinBox *spinBox_2;
    QSlider *verticalSlider_2;
    QPushButton *pushButton;
    QSpacerItem *horizontalSpacer;

    void setupUi(QDialog *Stadium)
    {
        if (Stadium->objectName().isEmpty())
            Stadium->setObjectName(QString::fromUtf8("Stadium"));
        Stadium->resize(606, 455);
        gridLayout_7 = new QGridLayout(Stadium);
        gridLayout_7->setObjectName(QString::fromUtf8("gridLayout_7"));
        label_5 = new QLabel(Stadium);
        label_5->setObjectName(QString::fromUtf8("label_5"));
        QFont font;
        font.setFamily(QString::fromUtf8("Stencil"));
        font.setPointSize(26);
        font.setBold(false);
        font.setWeight(50);
        label_5->setFont(font);
        label_5->setFrameShape(QFrame::WinPanel);
        label_5->setAlignment(Qt::AlignCenter);

        gridLayout_7->addWidget(label_5, 0, 0, 1, 2);

        gridLayout_6 = new QGridLayout();
        gridLayout_6->setObjectName(QString::fromUtf8("gridLayout_6"));
        splitter = new QSplitter(Stadium);
        splitter->setObjectName(QString::fromUtf8("splitter"));
        splitter->setOrientation(Qt::Horizontal);
        layoutWidget = new QWidget(splitter);
        layoutWidget->setObjectName(QString::fromUtf8("layoutWidget"));
        gridLayout_3 = new QGridLayout(layoutWidget);
        gridLayout_3->setObjectName(QString::fromUtf8("gridLayout_3"));
        gridLayout_3->setContentsMargins(0, 0, 0, 0);
        formLayout_4 = new QFormLayout();
        formLayout_4->setObjectName(QString::fromUtf8("formLayout_4"));
        lineEdit_4 = new QLineEdit(layoutWidget);
        lineEdit_4->setObjectName(QString::fromUtf8("lineEdit_4"));

        formLayout_4->setWidget(0, QFormLayout::FieldRole, lineEdit_4);

        label_8 = new QLabel(layoutWidget);
        label_8->setObjectName(QString::fromUtf8("label_8"));
        QFont font1;
        font1.setFamily(QString::fromUtf8("Copperplate Gothic Light"));
        font1.setPointSize(10);
        font1.setBold(false);
        font1.setItalic(true);
        font1.setWeight(50);
        label_8->setFont(font1);
        label_8->setFrameShape(QFrame::Panel);
        label_8->setFrameShadow(QFrame::Raised);

        formLayout_4->setWidget(0, QFormLayout::LabelRole, label_8);


        gridLayout_3->addLayout(formLayout_4, 0, 0, 1, 1);

        formLayout_5 = new QFormLayout();
        formLayout_5->setObjectName(QString::fromUtf8("formLayout_5"));
        lineEdit_5 = new QLineEdit(layoutWidget);
        lineEdit_5->setObjectName(QString::fromUtf8("lineEdit_5"));

        formLayout_5->setWidget(0, QFormLayout::FieldRole, lineEdit_5);

        label_9 = new QLabel(layoutWidget);
        label_9->setObjectName(QString::fromUtf8("label_9"));
        label_9->setFont(font1);
        label_9->setFrameShape(QFrame::Panel);
        label_9->setFrameShadow(QFrame::Raised);

        formLayout_5->setWidget(0, QFormLayout::LabelRole, label_9);


        gridLayout_3->addLayout(formLayout_5, 1, 0, 1, 1);

        splitter->addWidget(layoutWidget);
        layoutWidget1 = new QWidget(splitter);
        layoutWidget1->setObjectName(QString::fromUtf8("layoutWidget1"));
        gridLayout_2 = new QGridLayout(layoutWidget1);
        gridLayout_2->setObjectName(QString::fromUtf8("gridLayout_2"));
        gridLayout_2->setContentsMargins(0, 0, 0, 0);
        formLayout = new QFormLayout();
        formLayout->setObjectName(QString::fromUtf8("formLayout"));
        formLayout->setFieldGrowthPolicy(QFormLayout::AllNonFixedFieldsGrow);
        label_3 = new QLabel(layoutWidget1);
        label_3->setObjectName(QString::fromUtf8("label_3"));
        label_3->setFont(font1);
        label_3->setFrameShape(QFrame::Panel);
        label_3->setFrameShadow(QFrame::Raised);

        formLayout->setWidget(0, QFormLayout::LabelRole, label_3);

        lineEdit = new QLineEdit(layoutWidget1);
        lineEdit->setObjectName(QString::fromUtf8("lineEdit"));

        formLayout->setWidget(0, QFormLayout::FieldRole, lineEdit);


        gridLayout_2->addLayout(formLayout, 0, 0, 1, 1);

        formLayout_2 = new QFormLayout();
        formLayout_2->setObjectName(QString::fromUtf8("formLayout_2"));
        lineEdit_2 = new QLineEdit(layoutWidget1);
        lineEdit_2->setObjectName(QString::fromUtf8("lineEdit_2"));

        formLayout_2->setWidget(0, QFormLayout::FieldRole, lineEdit_2);

        label_4 = new QLabel(layoutWidget1);
        label_4->setObjectName(QString::fromUtf8("label_4"));
        label_4->setFont(font1);
        label_4->setFrameShape(QFrame::Panel);
        label_4->setFrameShadow(QFrame::Raised);

        formLayout_2->setWidget(0, QFormLayout::LabelRole, label_4);


        gridLayout_2->addLayout(formLayout_2, 1, 0, 1, 1);

        formLayout_3 = new QFormLayout();
        formLayout_3->setObjectName(QString::fromUtf8("formLayout_3"));
        lineEdit_3 = new QLineEdit(layoutWidget1);
        lineEdit_3->setObjectName(QString::fromUtf8("lineEdit_3"));

        formLayout_3->setWidget(0, QFormLayout::FieldRole, lineEdit_3);

        label_7 = new QLabel(layoutWidget1);
        label_7->setObjectName(QString::fromUtf8("label_7"));
        label_7->setFont(font1);
        label_7->setFrameShape(QFrame::Panel);
        label_7->setFrameShadow(QFrame::Raised);

        formLayout_3->setWidget(0, QFormLayout::LabelRole, label_7);


        gridLayout_2->addLayout(formLayout_3, 2, 0, 1, 1);

        splitter->addWidget(layoutWidget1);

        gridLayout_6->addWidget(splitter, 0, 0, 1, 1);

        gridLayout_5 = new QGridLayout();
        gridLayout_5->setObjectName(QString::fromUtf8("gridLayout_5"));
        gridLayout = new QGridLayout();
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        label_6 = new QLabel(Stadium);
        label_6->setObjectName(QString::fromUtf8("label_6"));
        QFont font2;
        font2.setFamily(QString::fromUtf8("Copperplate Gothic Light"));
        font2.setPointSize(10);
        font2.setBold(false);
        font2.setWeight(50);
        label_6->setFont(font2);

        gridLayout->addWidget(label_6, 1, 0, 1, 1);

        verticalSlider = new QSlider(Stadium);
        verticalSlider->setObjectName(QString::fromUtf8("verticalSlider"));
        verticalSlider->setCursor(QCursor(Qt::PointingHandCursor));
        verticalSlider->setMinimum(-3);
        verticalSlider->setMaximum(3);
        verticalSlider->setValue(0);
        verticalSlider->setOrientation(Qt::Vertical);

        gridLayout->addWidget(verticalSlider, 2, 0, 1, 1);

        label = new QLabel(Stadium);
        label->setObjectName(QString::fromUtf8("label"));
        QFont font3;
        font3.setFamily(QString::fromUtf8("Century Gothic"));
        font3.setPointSize(16);
        font3.setBold(true);
        font3.setWeight(75);
        label->setFont(font3);
        label->setFrameShape(QFrame::Box);
        label->setFrameShadow(QFrame::Raised);

        gridLayout->addWidget(label, 0, 0, 1, 1);


        gridLayout_5->addLayout(gridLayout, 0, 0, 1, 1);

        gridLayout_4 = new QGridLayout();
        gridLayout_4->setObjectName(QString::fromUtf8("gridLayout_4"));
        label_2 = new QLabel(Stadium);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        label_2->setFont(font3);
        label_2->setFrameShape(QFrame::Box);
        label_2->setFrameShadow(QFrame::Raised);

        gridLayout_4->addWidget(label_2, 0, 0, 1, 1);

        spinBox_2 = new QSpinBox(Stadium);
        spinBox_2->setObjectName(QString::fromUtf8("spinBox_2"));
        QFont font4;
        font4.setFamily(QString::fromUtf8("Copperplate Gothic Bold"));
        spinBox_2->setFont(font4);
        spinBox_2->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);
        spinBox_2->setMaximum(50);
        spinBox_2->setSingleStep(10);

        gridLayout_4->addWidget(spinBox_2, 1, 0, 1, 1);

        verticalSlider_2 = new QSlider(Stadium);
        verticalSlider_2->setObjectName(QString::fromUtf8("verticalSlider_2"));
        verticalSlider_2->setCursor(QCursor(Qt::PointingHandCursor));
        verticalSlider_2->setOrientation(Qt::Vertical);

        gridLayout_4->addWidget(verticalSlider_2, 2, 0, 1, 1);


        gridLayout_5->addLayout(gridLayout_4, 0, 1, 1, 1);


        gridLayout_6->addLayout(gridLayout_5, 1, 0, 1, 1);


        gridLayout_7->addLayout(gridLayout_6, 1, 0, 1, 2);

        pushButton = new QPushButton(Stadium);
        pushButton->setObjectName(QString::fromUtf8("pushButton"));
        QFont font5;
        font5.setFamily(QString::fromUtf8("Century Gothic"));
        font5.setBold(true);
        font5.setWeight(75);
        pushButton->setFont(font5);
        pushButton->setCursor(QCursor(Qt::PointingHandCursor));

        gridLayout_7->addWidget(pushButton, 2, 0, 1, 1);

        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_7->addItem(horizontalSpacer, 2, 1, 1, 1);


        retranslateUi(Stadium);

        QMetaObject::connectSlotsByName(Stadium);
    } // setupUi

    void retranslateUi(QDialog *Stadium)
    {
        Stadium->setWindowTitle(QApplication::translate("Stadium", "Dialog", 0, QApplication::UnicodeUTF8));
        label_5->setText(QApplication::translate("Stadium", "STADIUM", 0, QApplication::UnicodeUTF8));
        label_8->setText(QApplication::translate("Stadium", "Ticket Price", 0, QApplication::UnicodeUTF8));
        label_9->setText(QApplication::translate("Stadium", "Popularity", 0, QApplication::UnicodeUTF8));
        label_3->setText(QApplication::translate("Stadium", "CURRENT BALANCE", 0, QApplication::UnicodeUTF8));
        label_4->setText(QApplication::translate("Stadium", "EXPENDITURE", 0, QApplication::UnicodeUTF8));
        label_7->setText(QApplication::translate("Stadium", "Stadium Capacity", 0, QApplication::UnicodeUTF8));
        label_6->setText(QApplication::translate("Stadium", "TextLabel", 0, QApplication::UnicodeUTF8));
        label->setText(QApplication::translate("Stadium", "TICKET PRICE", 0, QApplication::UnicodeUTF8));
        label_2->setText(QApplication::translate("Stadium", "STADIUM UPGRADE(%)", 0, QApplication::UnicodeUTF8));
        pushButton->setText(QApplication::translate("Stadium", "Back", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class Stadium: public Ui_Stadium {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_STADIUM_H
