/********************************************************************************
** Form generated from reading UI file 'transfer.ui'
**
** Created: Thu Apr 19 02:12:48 2012
**      by: Qt User Interface Compiler version 4.6.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_TRANSFER_H
#define UI_TRANSFER_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QCheckBox>
#include <QtGui/QDialog>
#include <QtGui/QGridLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QLineEdit>
#include <QtGui/QPushButton>
#include <QtGui/QSpacerItem>
#include <QtGui/QSplitter>
#include <QtGui/QTabWidget>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_Transfer
{
public:
    QGridLayout *gridLayout_11;
    QGridLayout *gridLayout_10;
    QVBoxLayout *verticalLayout;
    QLabel *label;
    QSpacerItem *verticalSpacer_3;
    QGridLayout *gridLayout_3;
    QGridLayout *gridLayout_2;
    QGridLayout *gridLayout_8;
    QGridLayout *gridLayout_6;
    QLabel *label_57;
    QLabel *label_58;
    QSpacerItem *horizontalSpacer;
    QGridLayout *gridLayout_7;
    QLineEdit *lineEdit;
    QLineEdit *lineEdit_2;
    QTabWidget *tabWidget;
    QWidget *tab;
    QGridLayout *gridLayout_9;
    QSplitter *splitter_2;
    QWidget *layoutWidget;
    QGridLayout *gridLayout_5;
    QCheckBox *checkBox;
    QCheckBox *checkBox_b1;
    QCheckBox *checkBox_b2;
    QCheckBox *checkBox_b3;
    QCheckBox *checkBox_b4;
    QCheckBox *checkBox_b5;
    QCheckBox *checkBox_b6;
    QCheckBox *checkBox_b7;
    QCheckBox *checkBox_b8;
    QCheckBox *checkBox_b9;
    QCheckBox *checkBox_b10;
    QWidget *layoutWidget1;
    QGridLayout *gridLayout_4;
    QLabel *label_2;
    QLabel *label_3;
    QLabel *label_4;
    QLabel *label_5;
    QLabel *label_6;
    QLabel *label_pn1;
    QLabel *label_o1;
    QLabel *label_p1;
    QLabel *label_t1;
    QLabel *label_pr1;
    QLabel *label_pn2;
    QLabel *label_o2;
    QLabel *label_p2;
    QLabel *label_t2;
    QLabel *label_pr2;
    QLabel *label_pn3;
    QLabel *label_o3;
    QLabel *label_p3;
    QLabel *label_t3;
    QLabel *label_pr3;
    QLabel *label_pn4;
    QLabel *label_o4;
    QLabel *label_p4;
    QLabel *label_t4;
    QLabel *label_pr4;
    QLabel *label_pn5;
    QLabel *label_o5;
    QLabel *label_p5;
    QLabel *label_t5;
    QLabel *label_pr5;
    QLabel *label_pn6;
    QLabel *label_o6;
    QLabel *label_p6;
    QLabel *label_t6;
    QLabel *label_pr6;
    QLabel *label_pn7;
    QLabel *label_o7;
    QLabel *label_p7;
    QLabel *label_t7;
    QLabel *label_pr7;
    QLabel *label_pn8;
    QLabel *label_o8;
    QLabel *label_p8;
    QLabel *label_t8;
    QLabel *label_pr8;
    QLabel *label_pn9;
    QLabel *label_o9;
    QLabel *label_p9;
    QLabel *label_t9;
    QLabel *label_pr9;
    QLabel *label_pn10;
    QLabel *label_o10;
    QLabel *label_p10;
    QLabel *label_t10;
    QLabel *label_pr10;
    QWidget *tab_2;
    QGridLayout *gridLayout_14;
    QSplitter *splitter_3;
    QWidget *layoutWidget_2;
    QGridLayout *gridLayout_12;
    QCheckBox *checkBox_12;
    QCheckBox *checkBox_s1;
    QCheckBox *checkBox_s2;
    QCheckBox *checkBox_s3;
    QCheckBox *checkBox_s4;
    QCheckBox *checkBox_s5;
    QCheckBox *checkBox_s6;
    QCheckBox *checkBox_s7;
    QCheckBox *checkBox_s8;
    QCheckBox *checkBox_s9;
    QCheckBox *checkBox_s10;
    QWidget *layoutWidget_3;
    QGridLayout *gridLayout_13;
    QLabel *label_59;
    QLabel *label_60;
    QLabel *label_61;
    QLabel *label_63;
    QLabel *label_o11;
    QLabel *label_p11;
    QLabel *label_pr11;
    QLabel *label_pn12;
    QLabel *label_o12;
    QLabel *label_p12;
    QLabel *label_pr12;
    QLabel *label_pn13;
    QLabel *label_o13;
    QLabel *label_p13;
    QLabel *label_pr13;
    QLabel *label_pn14;
    QLabel *label_o14;
    QLabel *label_p14;
    QLabel *label_pr14;
    QLabel *label_pn15;
    QLabel *label_o15;
    QLabel *label_p15;
    QLabel *label_pr15;
    QLabel *label_pn16;
    QLabel *label_o16;
    QLabel *label_p16;
    QLabel *label_pr16;
    QLabel *label_pn17;
    QLabel *label_o17;
    QLabel *label_p17;
    QLabel *label_pr17;
    QLabel *label_pn18;
    QLabel *label_o18;
    QLabel *label_p18;
    QLabel *label_pr18;
    QLabel *label_pn19;
    QLabel *label_o19;
    QLabel *label_p19;
    QLabel *label_pr19;
    QLabel *label_pn20;
    QLabel *label_o20;
    QLabel *label_p20;
    QLabel *label_pr20;
    QLabel *label_pn11;
    QGridLayout *gridLayout;
    QPushButton *pushButton;
    QPushButton *pushButton_2;
    QPushButton *pushButton_4;
    QSpacerItem *verticalSpacer_4;

    void setupUi(QDialog *Transfer)
    {
        if (Transfer->objectName().isEmpty())
            Transfer->setObjectName(QString::fromUtf8("Transfer"));
        Transfer->resize(755, 480);
        gridLayout_11 = new QGridLayout(Transfer);
        gridLayout_11->setObjectName(QString::fromUtf8("gridLayout_11"));
        gridLayout_10 = new QGridLayout();
        gridLayout_10->setObjectName(QString::fromUtf8("gridLayout_10"));
        verticalLayout = new QVBoxLayout();
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        label = new QLabel(Transfer);
        label->setObjectName(QString::fromUtf8("label"));
        QFont font;
        font.setFamily(QString::fromUtf8("Stencil"));
        font.setPointSize(22);
        font.setBold(false);
        font.setWeight(50);
        label->setFont(font);
        label->setAlignment(Qt::AlignCenter);

        verticalLayout->addWidget(label);

        verticalSpacer_3 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Maximum);

        verticalLayout->addItem(verticalSpacer_3);


        gridLayout_10->addLayout(verticalLayout, 0, 0, 1, 1);

        gridLayout_3 = new QGridLayout();
        gridLayout_3->setObjectName(QString::fromUtf8("gridLayout_3"));
        gridLayout_2 = new QGridLayout();
        gridLayout_2->setObjectName(QString::fromUtf8("gridLayout_2"));
        gridLayout_8 = new QGridLayout();
        gridLayout_8->setObjectName(QString::fromUtf8("gridLayout_8"));
        gridLayout_6 = new QGridLayout();
        gridLayout_6->setObjectName(QString::fromUtf8("gridLayout_6"));
        label_57 = new QLabel(Transfer);
        label_57->setObjectName(QString::fromUtf8("label_57"));
        QFont font1;
        font1.setFamily(QString::fromUtf8("Copperplate Gothic Light"));
        font1.setPointSize(12);
        label_57->setFont(font1);
        label_57->setAlignment(Qt::AlignCenter);

        gridLayout_6->addWidget(label_57, 0, 1, 1, 1);

        label_58 = new QLabel(Transfer);
        label_58->setObjectName(QString::fromUtf8("label_58"));
        label_58->setFont(font1);
        label_58->setAlignment(Qt::AlignCenter);

        gridLayout_6->addWidget(label_58, 1, 1, 1, 1);

        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_6->addItem(horizontalSpacer, 0, 0, 1, 1);


        gridLayout_8->addLayout(gridLayout_6, 0, 0, 1, 1);

        gridLayout_7 = new QGridLayout();
        gridLayout_7->setObjectName(QString::fromUtf8("gridLayout_7"));
        lineEdit = new QLineEdit(Transfer);
        lineEdit->setObjectName(QString::fromUtf8("lineEdit"));
        QFont font2;
        font2.setFamily(QString::fromUtf8("Segoe UI"));
        font2.setPointSize(12);
        font2.setBold(true);
        font2.setWeight(75);
        lineEdit->setFont(font2);
        lineEdit->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout_7->addWidget(lineEdit, 0, 0, 1, 1);

        lineEdit_2 = new QLineEdit(Transfer);
        lineEdit_2->setObjectName(QString::fromUtf8("lineEdit_2"));
        lineEdit_2->setFont(font2);
        lineEdit_2->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout_7->addWidget(lineEdit_2, 1, 0, 1, 1);


        gridLayout_8->addLayout(gridLayout_7, 0, 1, 1, 1);


        gridLayout_2->addLayout(gridLayout_8, 0, 0, 1, 1);

        tabWidget = new QTabWidget(Transfer);
        tabWidget->setObjectName(QString::fromUtf8("tabWidget"));
        QFont font3;
        font3.setFamily(QString::fromUtf8("Copperplate Gothic Light"));
        font3.setPointSize(10);
        font3.setBold(true);
        font3.setWeight(75);
        tabWidget->setFont(font3);
        tab = new QWidget();
        tab->setObjectName(QString::fromUtf8("tab"));
        gridLayout_9 = new QGridLayout(tab);
        gridLayout_9->setObjectName(QString::fromUtf8("gridLayout_9"));
        splitter_2 = new QSplitter(tab);
        splitter_2->setObjectName(QString::fromUtf8("splitter_2"));
        splitter_2->setOrientation(Qt::Horizontal);
        layoutWidget = new QWidget(splitter_2);
        layoutWidget->setObjectName(QString::fromUtf8("layoutWidget"));
        gridLayout_5 = new QGridLayout(layoutWidget);
        gridLayout_5->setObjectName(QString::fromUtf8("gridLayout_5"));
        gridLayout_5->setContentsMargins(0, 0, 0, 0);
        checkBox = new QCheckBox(layoutWidget);
        checkBox->setObjectName(QString::fromUtf8("checkBox"));
        QFont font4;
        font4.setFamily(QString::fromUtf8("Copperplate Gothic Light"));
        font4.setBold(true);
        font4.setWeight(75);
        checkBox->setFont(font4);

        gridLayout_5->addWidget(checkBox, 0, 0, 1, 1);

        checkBox_b1 = new QCheckBox(layoutWidget);
        checkBox_b1->setObjectName(QString::fromUtf8("checkBox_b1"));
        QFont font5;
        font5.setFamily(QString::fromUtf8("Copperplate Gothic Light"));
        font5.setPointSize(12);
        font5.setBold(false);
        font5.setWeight(50);
        checkBox_b1->setFont(font5);

        gridLayout_5->addWidget(checkBox_b1, 1, 0, 1, 1);

        checkBox_b2 = new QCheckBox(layoutWidget);
        checkBox_b2->setObjectName(QString::fromUtf8("checkBox_b2"));
        checkBox_b2->setFont(font5);

        gridLayout_5->addWidget(checkBox_b2, 2, 0, 1, 1);

        checkBox_b3 = new QCheckBox(layoutWidget);
        checkBox_b3->setObjectName(QString::fromUtf8("checkBox_b3"));
        checkBox_b3->setFont(font5);

        gridLayout_5->addWidget(checkBox_b3, 3, 0, 1, 1);

        checkBox_b4 = new QCheckBox(layoutWidget);
        checkBox_b4->setObjectName(QString::fromUtf8("checkBox_b4"));
        checkBox_b4->setFont(font5);

        gridLayout_5->addWidget(checkBox_b4, 4, 0, 1, 1);

        checkBox_b5 = new QCheckBox(layoutWidget);
        checkBox_b5->setObjectName(QString::fromUtf8("checkBox_b5"));
        checkBox_b5->setFont(font5);

        gridLayout_5->addWidget(checkBox_b5, 5, 0, 1, 1);

        checkBox_b6 = new QCheckBox(layoutWidget);
        checkBox_b6->setObjectName(QString::fromUtf8("checkBox_b6"));
        checkBox_b6->setFont(font5);

        gridLayout_5->addWidget(checkBox_b6, 6, 0, 1, 1);

        checkBox_b7 = new QCheckBox(layoutWidget);
        checkBox_b7->setObjectName(QString::fromUtf8("checkBox_b7"));
        checkBox_b7->setFont(font5);

        gridLayout_5->addWidget(checkBox_b7, 7, 0, 1, 1);

        checkBox_b8 = new QCheckBox(layoutWidget);
        checkBox_b8->setObjectName(QString::fromUtf8("checkBox_b8"));
        checkBox_b8->setFont(font5);

        gridLayout_5->addWidget(checkBox_b8, 8, 0, 1, 1);

        checkBox_b9 = new QCheckBox(layoutWidget);
        checkBox_b9->setObjectName(QString::fromUtf8("checkBox_b9"));
        checkBox_b9->setFont(font5);

        gridLayout_5->addWidget(checkBox_b9, 9, 0, 1, 1);

        checkBox_b10 = new QCheckBox(layoutWidget);
        checkBox_b10->setObjectName(QString::fromUtf8("checkBox_b10"));
        checkBox_b10->setFont(font5);

        gridLayout_5->addWidget(checkBox_b10, 10, 0, 1, 1);

        splitter_2->addWidget(layoutWidget);
        layoutWidget1 = new QWidget(splitter_2);
        layoutWidget1->setObjectName(QString::fromUtf8("layoutWidget1"));
        gridLayout_4 = new QGridLayout(layoutWidget1);
        gridLayout_4->setObjectName(QString::fromUtf8("gridLayout_4"));
        gridLayout_4->setContentsMargins(0, 0, 0, 0);
        label_2 = new QLabel(layoutWidget1);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        QFont font6;
        font6.setFamily(QString::fromUtf8("Copperplate Gothic Light"));
        font6.setPointSize(8);
        font6.setBold(true);
        font6.setWeight(75);
        label_2->setFont(font6);
        label_2->setFrameShape(QFrame::Box);
        label_2->setFrameShadow(QFrame::Raised);
        label_2->setAlignment(Qt::AlignCenter);

        gridLayout_4->addWidget(label_2, 0, 0, 1, 1);

        label_3 = new QLabel(layoutWidget1);
        label_3->setObjectName(QString::fromUtf8("label_3"));
        label_3->setFont(font6);
        label_3->setFrameShape(QFrame::Box);
        label_3->setFrameShadow(QFrame::Raised);
        label_3->setAlignment(Qt::AlignCenter);

        gridLayout_4->addWidget(label_3, 0, 1, 1, 1);

        label_4 = new QLabel(layoutWidget1);
        label_4->setObjectName(QString::fromUtf8("label_4"));
        label_4->setFont(font6);
        label_4->setFrameShape(QFrame::Box);
        label_4->setFrameShadow(QFrame::Raised);
        label_4->setAlignment(Qt::AlignCenter);

        gridLayout_4->addWidget(label_4, 0, 2, 1, 1);

        label_5 = new QLabel(layoutWidget1);
        label_5->setObjectName(QString::fromUtf8("label_5"));
        label_5->setFont(font6);
        label_5->setFrameShape(QFrame::Box);
        label_5->setFrameShadow(QFrame::Raised);
        label_5->setAlignment(Qt::AlignCenter);

        gridLayout_4->addWidget(label_5, 0, 3, 1, 1);

        label_6 = new QLabel(layoutWidget1);
        label_6->setObjectName(QString::fromUtf8("label_6"));
        label_6->setFont(font6);
        label_6->setFrameShape(QFrame::Box);
        label_6->setFrameShadow(QFrame::Raised);
        label_6->setAlignment(Qt::AlignCenter);

        gridLayout_4->addWidget(label_6, 0, 4, 1, 1);

        label_pn1 = new QLabel(layoutWidget1);
        label_pn1->setObjectName(QString::fromUtf8("label_pn1"));
        label_pn1->setFont(font5);
        label_pn1->setAlignment(Qt::AlignCenter);

        gridLayout_4->addWidget(label_pn1, 1, 0, 1, 1);

        label_o1 = new QLabel(layoutWidget1);
        label_o1->setObjectName(QString::fromUtf8("label_o1"));
        label_o1->setFont(font5);
        label_o1->setAlignment(Qt::AlignCenter);

        gridLayout_4->addWidget(label_o1, 1, 1, 1, 1);

        label_p1 = new QLabel(layoutWidget1);
        label_p1->setObjectName(QString::fromUtf8("label_p1"));
        label_p1->setFont(font5);
        label_p1->setAlignment(Qt::AlignCenter);

        gridLayout_4->addWidget(label_p1, 1, 2, 1, 1);

        label_t1 = new QLabel(layoutWidget1);
        label_t1->setObjectName(QString::fromUtf8("label_t1"));
        label_t1->setFont(font5);
        label_t1->setAlignment(Qt::AlignCenter);

        gridLayout_4->addWidget(label_t1, 1, 3, 1, 1);

        label_pr1 = new QLabel(layoutWidget1);
        label_pr1->setObjectName(QString::fromUtf8("label_pr1"));
        label_pr1->setFont(font5);
        label_pr1->setAlignment(Qt::AlignCenter);

        gridLayout_4->addWidget(label_pr1, 1, 4, 1, 1);

        label_pn2 = new QLabel(layoutWidget1);
        label_pn2->setObjectName(QString::fromUtf8("label_pn2"));
        label_pn2->setFont(font5);
        label_pn2->setAlignment(Qt::AlignCenter);

        gridLayout_4->addWidget(label_pn2, 2, 0, 1, 1);

        label_o2 = new QLabel(layoutWidget1);
        label_o2->setObjectName(QString::fromUtf8("label_o2"));
        label_o2->setFont(font5);
        label_o2->setAlignment(Qt::AlignCenter);

        gridLayout_4->addWidget(label_o2, 2, 1, 1, 1);

        label_p2 = new QLabel(layoutWidget1);
        label_p2->setObjectName(QString::fromUtf8("label_p2"));
        label_p2->setFont(font5);
        label_p2->setAlignment(Qt::AlignCenter);

        gridLayout_4->addWidget(label_p2, 2, 2, 1, 1);

        label_t2 = new QLabel(layoutWidget1);
        label_t2->setObjectName(QString::fromUtf8("label_t2"));
        label_t2->setFont(font5);
        label_t2->setAlignment(Qt::AlignCenter);

        gridLayout_4->addWidget(label_t2, 2, 3, 1, 1);

        label_pr2 = new QLabel(layoutWidget1);
        label_pr2->setObjectName(QString::fromUtf8("label_pr2"));
        label_pr2->setFont(font5);
        label_pr2->setAlignment(Qt::AlignCenter);

        gridLayout_4->addWidget(label_pr2, 2, 4, 1, 1);

        label_pn3 = new QLabel(layoutWidget1);
        label_pn3->setObjectName(QString::fromUtf8("label_pn3"));
        label_pn3->setFont(font5);
        label_pn3->setAlignment(Qt::AlignCenter);

        gridLayout_4->addWidget(label_pn3, 3, 0, 1, 1);

        label_o3 = new QLabel(layoutWidget1);
        label_o3->setObjectName(QString::fromUtf8("label_o3"));
        label_o3->setFont(font5);
        label_o3->setAlignment(Qt::AlignCenter);

        gridLayout_4->addWidget(label_o3, 3, 1, 1, 1);

        label_p3 = new QLabel(layoutWidget1);
        label_p3->setObjectName(QString::fromUtf8("label_p3"));
        label_p3->setFont(font5);
        label_p3->setAlignment(Qt::AlignCenter);

        gridLayout_4->addWidget(label_p3, 3, 2, 1, 1);

        label_t3 = new QLabel(layoutWidget1);
        label_t3->setObjectName(QString::fromUtf8("label_t3"));
        label_t3->setFont(font5);
        label_t3->setAlignment(Qt::AlignCenter);

        gridLayout_4->addWidget(label_t3, 3, 3, 1, 1);

        label_pr3 = new QLabel(layoutWidget1);
        label_pr3->setObjectName(QString::fromUtf8("label_pr3"));
        label_pr3->setFont(font5);
        label_pr3->setAlignment(Qt::AlignCenter);

        gridLayout_4->addWidget(label_pr3, 3, 4, 1, 1);

        label_pn4 = new QLabel(layoutWidget1);
        label_pn4->setObjectName(QString::fromUtf8("label_pn4"));
        label_pn4->setFont(font5);
        label_pn4->setAlignment(Qt::AlignCenter);

        gridLayout_4->addWidget(label_pn4, 4, 0, 1, 1);

        label_o4 = new QLabel(layoutWidget1);
        label_o4->setObjectName(QString::fromUtf8("label_o4"));
        label_o4->setFont(font5);
        label_o4->setAlignment(Qt::AlignCenter);

        gridLayout_4->addWidget(label_o4, 4, 1, 1, 1);

        label_p4 = new QLabel(layoutWidget1);
        label_p4->setObjectName(QString::fromUtf8("label_p4"));
        label_p4->setFont(font5);
        label_p4->setAlignment(Qt::AlignCenter);

        gridLayout_4->addWidget(label_p4, 4, 2, 1, 1);

        label_t4 = new QLabel(layoutWidget1);
        label_t4->setObjectName(QString::fromUtf8("label_t4"));
        label_t4->setFont(font5);
        label_t4->setAlignment(Qt::AlignCenter);

        gridLayout_4->addWidget(label_t4, 4, 3, 1, 1);

        label_pr4 = new QLabel(layoutWidget1);
        label_pr4->setObjectName(QString::fromUtf8("label_pr4"));
        label_pr4->setFont(font5);
        label_pr4->setAlignment(Qt::AlignCenter);

        gridLayout_4->addWidget(label_pr4, 4, 4, 1, 1);

        label_pn5 = new QLabel(layoutWidget1);
        label_pn5->setObjectName(QString::fromUtf8("label_pn5"));
        label_pn5->setFont(font5);
        label_pn5->setAlignment(Qt::AlignCenter);

        gridLayout_4->addWidget(label_pn5, 5, 0, 1, 1);

        label_o5 = new QLabel(layoutWidget1);
        label_o5->setObjectName(QString::fromUtf8("label_o5"));
        label_o5->setFont(font5);
        label_o5->setAlignment(Qt::AlignCenter);

        gridLayout_4->addWidget(label_o5, 5, 1, 1, 1);

        label_p5 = new QLabel(layoutWidget1);
        label_p5->setObjectName(QString::fromUtf8("label_p5"));
        label_p5->setFont(font5);
        label_p5->setAlignment(Qt::AlignCenter);

        gridLayout_4->addWidget(label_p5, 5, 2, 1, 1);

        label_t5 = new QLabel(layoutWidget1);
        label_t5->setObjectName(QString::fromUtf8("label_t5"));
        label_t5->setFont(font5);
        label_t5->setAlignment(Qt::AlignCenter);

        gridLayout_4->addWidget(label_t5, 5, 3, 1, 1);

        label_pr5 = new QLabel(layoutWidget1);
        label_pr5->setObjectName(QString::fromUtf8("label_pr5"));
        label_pr5->setFont(font5);
        label_pr5->setAlignment(Qt::AlignCenter);

        gridLayout_4->addWidget(label_pr5, 5, 4, 1, 1);

        label_pn6 = new QLabel(layoutWidget1);
        label_pn6->setObjectName(QString::fromUtf8("label_pn6"));
        label_pn6->setFont(font5);
        label_pn6->setAlignment(Qt::AlignCenter);

        gridLayout_4->addWidget(label_pn6, 6, 0, 1, 1);

        label_o6 = new QLabel(layoutWidget1);
        label_o6->setObjectName(QString::fromUtf8("label_o6"));
        label_o6->setFont(font5);
        label_o6->setAlignment(Qt::AlignCenter);

        gridLayout_4->addWidget(label_o6, 6, 1, 1, 1);

        label_p6 = new QLabel(layoutWidget1);
        label_p6->setObjectName(QString::fromUtf8("label_p6"));
        label_p6->setFont(font5);
        label_p6->setAlignment(Qt::AlignCenter);

        gridLayout_4->addWidget(label_p6, 6, 2, 1, 1);

        label_t6 = new QLabel(layoutWidget1);
        label_t6->setObjectName(QString::fromUtf8("label_t6"));
        label_t6->setFont(font5);
        label_t6->setAlignment(Qt::AlignCenter);

        gridLayout_4->addWidget(label_t6, 6, 3, 1, 1);

        label_pr6 = new QLabel(layoutWidget1);
        label_pr6->setObjectName(QString::fromUtf8("label_pr6"));
        label_pr6->setFont(font5);
        label_pr6->setAlignment(Qt::AlignCenter);

        gridLayout_4->addWidget(label_pr6, 6, 4, 1, 1);

        label_pn7 = new QLabel(layoutWidget1);
        label_pn7->setObjectName(QString::fromUtf8("label_pn7"));
        label_pn7->setFont(font5);
        label_pn7->setAlignment(Qt::AlignCenter);

        gridLayout_4->addWidget(label_pn7, 7, 0, 1, 1);

        label_o7 = new QLabel(layoutWidget1);
        label_o7->setObjectName(QString::fromUtf8("label_o7"));
        label_o7->setFont(font5);
        label_o7->setAlignment(Qt::AlignCenter);

        gridLayout_4->addWidget(label_o7, 7, 1, 1, 1);

        label_p7 = new QLabel(layoutWidget1);
        label_p7->setObjectName(QString::fromUtf8("label_p7"));
        label_p7->setFont(font5);
        label_p7->setAlignment(Qt::AlignCenter);

        gridLayout_4->addWidget(label_p7, 7, 2, 1, 1);

        label_t7 = new QLabel(layoutWidget1);
        label_t7->setObjectName(QString::fromUtf8("label_t7"));
        label_t7->setFont(font5);
        label_t7->setAlignment(Qt::AlignCenter);

        gridLayout_4->addWidget(label_t7, 7, 3, 1, 1);

        label_pr7 = new QLabel(layoutWidget1);
        label_pr7->setObjectName(QString::fromUtf8("label_pr7"));
        label_pr7->setFont(font5);
        label_pr7->setAlignment(Qt::AlignCenter);

        gridLayout_4->addWidget(label_pr7, 7, 4, 1, 1);

        label_pn8 = new QLabel(layoutWidget1);
        label_pn8->setObjectName(QString::fromUtf8("label_pn8"));
        label_pn8->setFont(font5);
        label_pn8->setAlignment(Qt::AlignCenter);

        gridLayout_4->addWidget(label_pn8, 8, 0, 1, 1);

        label_o8 = new QLabel(layoutWidget1);
        label_o8->setObjectName(QString::fromUtf8("label_o8"));
        label_o8->setFont(font5);
        label_o8->setAlignment(Qt::AlignCenter);

        gridLayout_4->addWidget(label_o8, 8, 1, 1, 1);

        label_p8 = new QLabel(layoutWidget1);
        label_p8->setObjectName(QString::fromUtf8("label_p8"));
        label_p8->setFont(font5);
        label_p8->setAlignment(Qt::AlignCenter);

        gridLayout_4->addWidget(label_p8, 8, 2, 1, 1);

        label_t8 = new QLabel(layoutWidget1);
        label_t8->setObjectName(QString::fromUtf8("label_t8"));
        label_t8->setFont(font5);
        label_t8->setAlignment(Qt::AlignCenter);

        gridLayout_4->addWidget(label_t8, 8, 3, 1, 1);

        label_pr8 = new QLabel(layoutWidget1);
        label_pr8->setObjectName(QString::fromUtf8("label_pr8"));
        label_pr8->setFont(font5);
        label_pr8->setAlignment(Qt::AlignCenter);

        gridLayout_4->addWidget(label_pr8, 8, 4, 1, 1);

        label_pn9 = new QLabel(layoutWidget1);
        label_pn9->setObjectName(QString::fromUtf8("label_pn9"));
        label_pn9->setFont(font5);
        label_pn9->setAlignment(Qt::AlignCenter);

        gridLayout_4->addWidget(label_pn9, 9, 0, 1, 1);

        label_o9 = new QLabel(layoutWidget1);
        label_o9->setObjectName(QString::fromUtf8("label_o9"));
        label_o9->setFont(font5);
        label_o9->setAlignment(Qt::AlignCenter);

        gridLayout_4->addWidget(label_o9, 9, 1, 1, 1);

        label_p9 = new QLabel(layoutWidget1);
        label_p9->setObjectName(QString::fromUtf8("label_p9"));
        label_p9->setFont(font5);
        label_p9->setAlignment(Qt::AlignCenter);

        gridLayout_4->addWidget(label_p9, 9, 2, 1, 1);

        label_t9 = new QLabel(layoutWidget1);
        label_t9->setObjectName(QString::fromUtf8("label_t9"));
        label_t9->setFont(font5);
        label_t9->setAlignment(Qt::AlignCenter);

        gridLayout_4->addWidget(label_t9, 9, 3, 1, 1);

        label_pr9 = new QLabel(layoutWidget1);
        label_pr9->setObjectName(QString::fromUtf8("label_pr9"));
        label_pr9->setFont(font5);
        label_pr9->setAlignment(Qt::AlignCenter);

        gridLayout_4->addWidget(label_pr9, 9, 4, 1, 1);

        label_pn10 = new QLabel(layoutWidget1);
        label_pn10->setObjectName(QString::fromUtf8("label_pn10"));
        label_pn10->setFont(font5);
        label_pn10->setAlignment(Qt::AlignCenter);

        gridLayout_4->addWidget(label_pn10, 10, 0, 1, 1);

        label_o10 = new QLabel(layoutWidget1);
        label_o10->setObjectName(QString::fromUtf8("label_o10"));
        label_o10->setFont(font5);
        label_o10->setAlignment(Qt::AlignCenter);

        gridLayout_4->addWidget(label_o10, 10, 1, 1, 1);

        label_p10 = new QLabel(layoutWidget1);
        label_p10->setObjectName(QString::fromUtf8("label_p10"));
        label_p10->setFont(font5);
        label_p10->setAlignment(Qt::AlignCenter);

        gridLayout_4->addWidget(label_p10, 10, 2, 1, 1);

        label_t10 = new QLabel(layoutWidget1);
        label_t10->setObjectName(QString::fromUtf8("label_t10"));
        label_t10->setFont(font5);
        label_t10->setAlignment(Qt::AlignCenter);

        gridLayout_4->addWidget(label_t10, 10, 3, 1, 1);

        label_pr10 = new QLabel(layoutWidget1);
        label_pr10->setObjectName(QString::fromUtf8("label_pr10"));
        label_pr10->setFont(font5);
        label_pr10->setAlignment(Qt::AlignCenter);

        gridLayout_4->addWidget(label_pr10, 10, 4, 1, 1);

        splitter_2->addWidget(layoutWidget1);

        gridLayout_9->addWidget(splitter_2, 0, 0, 1, 1);

        tabWidget->addTab(tab, QString());
        tab_2 = new QWidget();
        tab_2->setObjectName(QString::fromUtf8("tab_2"));
        gridLayout_14 = new QGridLayout(tab_2);
        gridLayout_14->setObjectName(QString::fromUtf8("gridLayout_14"));
        splitter_3 = new QSplitter(tab_2);
        splitter_3->setObjectName(QString::fromUtf8("splitter_3"));
        splitter_3->setOrientation(Qt::Horizontal);
        layoutWidget_2 = new QWidget(splitter_3);
        layoutWidget_2->setObjectName(QString::fromUtf8("layoutWidget_2"));
        gridLayout_12 = new QGridLayout(layoutWidget_2);
        gridLayout_12->setObjectName(QString::fromUtf8("gridLayout_12"));
        gridLayout_12->setContentsMargins(0, 0, 0, 0);
        checkBox_12 = new QCheckBox(layoutWidget_2);
        checkBox_12->setObjectName(QString::fromUtf8("checkBox_12"));
        checkBox_12->setFont(font4);

        gridLayout_12->addWidget(checkBox_12, 0, 0, 1, 1);

        checkBox_s1 = new QCheckBox(layoutWidget_2);
        checkBox_s1->setObjectName(QString::fromUtf8("checkBox_s1"));
        checkBox_s1->setFont(font5);

        gridLayout_12->addWidget(checkBox_s1, 1, 0, 1, 1);

        checkBox_s2 = new QCheckBox(layoutWidget_2);
        checkBox_s2->setObjectName(QString::fromUtf8("checkBox_s2"));
        checkBox_s2->setFont(font5);

        gridLayout_12->addWidget(checkBox_s2, 2, 0, 1, 1);

        checkBox_s3 = new QCheckBox(layoutWidget_2);
        checkBox_s3->setObjectName(QString::fromUtf8("checkBox_s3"));
        checkBox_s3->setFont(font5);

        gridLayout_12->addWidget(checkBox_s3, 3, 0, 1, 1);

        checkBox_s4 = new QCheckBox(layoutWidget_2);
        checkBox_s4->setObjectName(QString::fromUtf8("checkBox_s4"));
        checkBox_s4->setFont(font5);

        gridLayout_12->addWidget(checkBox_s4, 4, 0, 1, 1);

        checkBox_s5 = new QCheckBox(layoutWidget_2);
        checkBox_s5->setObjectName(QString::fromUtf8("checkBox_s5"));
        checkBox_s5->setFont(font5);

        gridLayout_12->addWidget(checkBox_s5, 5, 0, 1, 1);

        checkBox_s6 = new QCheckBox(layoutWidget_2);
        checkBox_s6->setObjectName(QString::fromUtf8("checkBox_s6"));
        checkBox_s6->setFont(font5);

        gridLayout_12->addWidget(checkBox_s6, 6, 0, 1, 1);

        checkBox_s7 = new QCheckBox(layoutWidget_2);
        checkBox_s7->setObjectName(QString::fromUtf8("checkBox_s7"));
        checkBox_s7->setFont(font5);

        gridLayout_12->addWidget(checkBox_s7, 7, 0, 1, 1);

        checkBox_s8 = new QCheckBox(layoutWidget_2);
        checkBox_s8->setObjectName(QString::fromUtf8("checkBox_s8"));
        checkBox_s8->setFont(font5);

        gridLayout_12->addWidget(checkBox_s8, 8, 0, 1, 1);

        checkBox_s9 = new QCheckBox(layoutWidget_2);
        checkBox_s9->setObjectName(QString::fromUtf8("checkBox_s9"));
        checkBox_s9->setFont(font5);

        gridLayout_12->addWidget(checkBox_s9, 9, 0, 1, 1);

        checkBox_s10 = new QCheckBox(layoutWidget_2);
        checkBox_s10->setObjectName(QString::fromUtf8("checkBox_s10"));
        checkBox_s10->setFont(font5);

        gridLayout_12->addWidget(checkBox_s10, 10, 0, 1, 1);

        splitter_3->addWidget(layoutWidget_2);
        layoutWidget_3 = new QWidget(splitter_3);
        layoutWidget_3->setObjectName(QString::fromUtf8("layoutWidget_3"));
        gridLayout_13 = new QGridLayout(layoutWidget_3);
        gridLayout_13->setObjectName(QString::fromUtf8("gridLayout_13"));
        gridLayout_13->setContentsMargins(0, 0, 0, 0);
        label_59 = new QLabel(layoutWidget_3);
        label_59->setObjectName(QString::fromUtf8("label_59"));
        label_59->setFont(font6);
        label_59->setFrameShape(QFrame::Box);
        label_59->setFrameShadow(QFrame::Raised);
        label_59->setAlignment(Qt::AlignCenter);

        gridLayout_13->addWidget(label_59, 0, 0, 1, 1);

        label_60 = new QLabel(layoutWidget_3);
        label_60->setObjectName(QString::fromUtf8("label_60"));
        label_60->setFont(font6);
        label_60->setFrameShape(QFrame::Box);
        label_60->setFrameShadow(QFrame::Raised);
        label_60->setAlignment(Qt::AlignCenter);

        gridLayout_13->addWidget(label_60, 0, 1, 1, 1);

        label_61 = new QLabel(layoutWidget_3);
        label_61->setObjectName(QString::fromUtf8("label_61"));
        label_61->setFont(font6);
        label_61->setFrameShape(QFrame::Box);
        label_61->setFrameShadow(QFrame::Raised);
        label_61->setAlignment(Qt::AlignCenter);

        gridLayout_13->addWidget(label_61, 0, 2, 1, 1);

        label_63 = new QLabel(layoutWidget_3);
        label_63->setObjectName(QString::fromUtf8("label_63"));
        label_63->setFont(font6);
        label_63->setFrameShape(QFrame::Box);
        label_63->setFrameShadow(QFrame::Raised);
        label_63->setAlignment(Qt::AlignCenter);

        gridLayout_13->addWidget(label_63, 0, 3, 1, 1);

        label_o11 = new QLabel(layoutWidget_3);
        label_o11->setObjectName(QString::fromUtf8("label_o11"));
        label_o11->setFont(font5);
        label_o11->setAlignment(Qt::AlignCenter);

        gridLayout_13->addWidget(label_o11, 1, 1, 1, 1);

        label_p11 = new QLabel(layoutWidget_3);
        label_p11->setObjectName(QString::fromUtf8("label_p11"));
        label_p11->setFont(font5);
        label_p11->setAlignment(Qt::AlignCenter);

        gridLayout_13->addWidget(label_p11, 1, 2, 1, 1);

        label_pr11 = new QLabel(layoutWidget_3);
        label_pr11->setObjectName(QString::fromUtf8("label_pr11"));
        label_pr11->setFont(font5);
        label_pr11->setAlignment(Qt::AlignCenter);

        gridLayout_13->addWidget(label_pr11, 1, 3, 1, 1);

        label_pn12 = new QLabel(layoutWidget_3);
        label_pn12->setObjectName(QString::fromUtf8("label_pn12"));
        label_pn12->setFont(font5);
        label_pn12->setAlignment(Qt::AlignCenter);

        gridLayout_13->addWidget(label_pn12, 2, 0, 1, 1);

        label_o12 = new QLabel(layoutWidget_3);
        label_o12->setObjectName(QString::fromUtf8("label_o12"));
        label_o12->setFont(font5);
        label_o12->setAlignment(Qt::AlignCenter);

        gridLayout_13->addWidget(label_o12, 2, 1, 1, 1);

        label_p12 = new QLabel(layoutWidget_3);
        label_p12->setObjectName(QString::fromUtf8("label_p12"));
        label_p12->setFont(font5);
        label_p12->setAlignment(Qt::AlignCenter);

        gridLayout_13->addWidget(label_p12, 2, 2, 1, 1);

        label_pr12 = new QLabel(layoutWidget_3);
        label_pr12->setObjectName(QString::fromUtf8("label_pr12"));
        label_pr12->setFont(font5);
        label_pr12->setAlignment(Qt::AlignCenter);

        gridLayout_13->addWidget(label_pr12, 2, 3, 1, 1);

        label_pn13 = new QLabel(layoutWidget_3);
        label_pn13->setObjectName(QString::fromUtf8("label_pn13"));
        label_pn13->setFont(font5);
        label_pn13->setAlignment(Qt::AlignCenter);

        gridLayout_13->addWidget(label_pn13, 3, 0, 1, 1);

        label_o13 = new QLabel(layoutWidget_3);
        label_o13->setObjectName(QString::fromUtf8("label_o13"));
        label_o13->setFont(font5);
        label_o13->setAlignment(Qt::AlignCenter);

        gridLayout_13->addWidget(label_o13, 3, 1, 1, 1);

        label_p13 = new QLabel(layoutWidget_3);
        label_p13->setObjectName(QString::fromUtf8("label_p13"));
        label_p13->setFont(font5);
        label_p13->setAlignment(Qt::AlignCenter);

        gridLayout_13->addWidget(label_p13, 3, 2, 1, 1);

        label_pr13 = new QLabel(layoutWidget_3);
        label_pr13->setObjectName(QString::fromUtf8("label_pr13"));
        label_pr13->setFont(font5);
        label_pr13->setAlignment(Qt::AlignCenter);

        gridLayout_13->addWidget(label_pr13, 3, 3, 1, 1);

        label_pn14 = new QLabel(layoutWidget_3);
        label_pn14->setObjectName(QString::fromUtf8("label_pn14"));
        label_pn14->setFont(font5);
        label_pn14->setAlignment(Qt::AlignCenter);

        gridLayout_13->addWidget(label_pn14, 4, 0, 1, 1);

        label_o14 = new QLabel(layoutWidget_3);
        label_o14->setObjectName(QString::fromUtf8("label_o14"));
        label_o14->setFont(font5);
        label_o14->setAlignment(Qt::AlignCenter);

        gridLayout_13->addWidget(label_o14, 4, 1, 1, 1);

        label_p14 = new QLabel(layoutWidget_3);
        label_p14->setObjectName(QString::fromUtf8("label_p14"));
        label_p14->setFont(font5);
        label_p14->setAlignment(Qt::AlignCenter);

        gridLayout_13->addWidget(label_p14, 4, 2, 1, 1);

        label_pr14 = new QLabel(layoutWidget_3);
        label_pr14->setObjectName(QString::fromUtf8("label_pr14"));
        label_pr14->setFont(font5);
        label_pr14->setAlignment(Qt::AlignCenter);

        gridLayout_13->addWidget(label_pr14, 4, 3, 1, 1);

        label_pn15 = new QLabel(layoutWidget_3);
        label_pn15->setObjectName(QString::fromUtf8("label_pn15"));
        label_pn15->setFont(font5);
        label_pn15->setAlignment(Qt::AlignCenter);

        gridLayout_13->addWidget(label_pn15, 5, 0, 1, 1);

        label_o15 = new QLabel(layoutWidget_3);
        label_o15->setObjectName(QString::fromUtf8("label_o15"));
        label_o15->setFont(font5);
        label_o15->setAlignment(Qt::AlignCenter);

        gridLayout_13->addWidget(label_o15, 5, 1, 1, 1);

        label_p15 = new QLabel(layoutWidget_3);
        label_p15->setObjectName(QString::fromUtf8("label_p15"));
        label_p15->setFont(font5);
        label_p15->setAlignment(Qt::AlignCenter);

        gridLayout_13->addWidget(label_p15, 5, 2, 1, 1);

        label_pr15 = new QLabel(layoutWidget_3);
        label_pr15->setObjectName(QString::fromUtf8("label_pr15"));
        label_pr15->setFont(font5);
        label_pr15->setAlignment(Qt::AlignCenter);

        gridLayout_13->addWidget(label_pr15, 5, 3, 1, 1);

        label_pn16 = new QLabel(layoutWidget_3);
        label_pn16->setObjectName(QString::fromUtf8("label_pn16"));
        label_pn16->setFont(font5);
        label_pn16->setAlignment(Qt::AlignCenter);

        gridLayout_13->addWidget(label_pn16, 6, 0, 1, 1);

        label_o16 = new QLabel(layoutWidget_3);
        label_o16->setObjectName(QString::fromUtf8("label_o16"));
        label_o16->setFont(font5);
        label_o16->setAlignment(Qt::AlignCenter);

        gridLayout_13->addWidget(label_o16, 6, 1, 1, 1);

        label_p16 = new QLabel(layoutWidget_3);
        label_p16->setObjectName(QString::fromUtf8("label_p16"));
        label_p16->setFont(font5);
        label_p16->setAlignment(Qt::AlignCenter);

        gridLayout_13->addWidget(label_p16, 6, 2, 1, 1);

        label_pr16 = new QLabel(layoutWidget_3);
        label_pr16->setObjectName(QString::fromUtf8("label_pr16"));
        label_pr16->setFont(font5);
        label_pr16->setAlignment(Qt::AlignCenter);

        gridLayout_13->addWidget(label_pr16, 6, 3, 1, 1);

        label_pn17 = new QLabel(layoutWidget_3);
        label_pn17->setObjectName(QString::fromUtf8("label_pn17"));
        label_pn17->setFont(font5);
        label_pn17->setAlignment(Qt::AlignCenter);

        gridLayout_13->addWidget(label_pn17, 7, 0, 1, 1);

        label_o17 = new QLabel(layoutWidget_3);
        label_o17->setObjectName(QString::fromUtf8("label_o17"));
        label_o17->setFont(font5);
        label_o17->setAlignment(Qt::AlignCenter);

        gridLayout_13->addWidget(label_o17, 7, 1, 1, 1);

        label_p17 = new QLabel(layoutWidget_3);
        label_p17->setObjectName(QString::fromUtf8("label_p17"));
        label_p17->setFont(font5);
        label_p17->setAlignment(Qt::AlignCenter);

        gridLayout_13->addWidget(label_p17, 7, 2, 1, 1);

        label_pr17 = new QLabel(layoutWidget_3);
        label_pr17->setObjectName(QString::fromUtf8("label_pr17"));
        label_pr17->setFont(font5);
        label_pr17->setAlignment(Qt::AlignCenter);

        gridLayout_13->addWidget(label_pr17, 7, 3, 1, 1);

        label_pn18 = new QLabel(layoutWidget_3);
        label_pn18->setObjectName(QString::fromUtf8("label_pn18"));
        label_pn18->setFont(font5);
        label_pn18->setAlignment(Qt::AlignCenter);

        gridLayout_13->addWidget(label_pn18, 8, 0, 1, 1);

        label_o18 = new QLabel(layoutWidget_3);
        label_o18->setObjectName(QString::fromUtf8("label_o18"));
        label_o18->setFont(font5);
        label_o18->setAlignment(Qt::AlignCenter);

        gridLayout_13->addWidget(label_o18, 8, 1, 1, 1);

        label_p18 = new QLabel(layoutWidget_3);
        label_p18->setObjectName(QString::fromUtf8("label_p18"));
        label_p18->setFont(font5);
        label_p18->setAlignment(Qt::AlignCenter);

        gridLayout_13->addWidget(label_p18, 8, 2, 1, 1);

        label_pr18 = new QLabel(layoutWidget_3);
        label_pr18->setObjectName(QString::fromUtf8("label_pr18"));
        label_pr18->setFont(font5);
        label_pr18->setAlignment(Qt::AlignCenter);

        gridLayout_13->addWidget(label_pr18, 8, 3, 1, 1);

        label_pn19 = new QLabel(layoutWidget_3);
        label_pn19->setObjectName(QString::fromUtf8("label_pn19"));
        label_pn19->setFont(font5);
        label_pn19->setAlignment(Qt::AlignCenter);

        gridLayout_13->addWidget(label_pn19, 9, 0, 1, 1);

        label_o19 = new QLabel(layoutWidget_3);
        label_o19->setObjectName(QString::fromUtf8("label_o19"));
        label_o19->setFont(font5);
        label_o19->setAlignment(Qt::AlignCenter);

        gridLayout_13->addWidget(label_o19, 9, 1, 1, 1);

        label_p19 = new QLabel(layoutWidget_3);
        label_p19->setObjectName(QString::fromUtf8("label_p19"));
        label_p19->setFont(font5);
        label_p19->setAlignment(Qt::AlignCenter);

        gridLayout_13->addWidget(label_p19, 9, 2, 1, 1);

        label_pr19 = new QLabel(layoutWidget_3);
        label_pr19->setObjectName(QString::fromUtf8("label_pr19"));
        label_pr19->setFont(font5);
        label_pr19->setAlignment(Qt::AlignCenter);

        gridLayout_13->addWidget(label_pr19, 9, 3, 1, 1);

        label_pn20 = new QLabel(layoutWidget_3);
        label_pn20->setObjectName(QString::fromUtf8("label_pn20"));
        label_pn20->setFont(font5);
        label_pn20->setAlignment(Qt::AlignCenter);

        gridLayout_13->addWidget(label_pn20, 10, 0, 1, 1);

        label_o20 = new QLabel(layoutWidget_3);
        label_o20->setObjectName(QString::fromUtf8("label_o20"));
        label_o20->setFont(font5);
        label_o20->setAlignment(Qt::AlignCenter);

        gridLayout_13->addWidget(label_o20, 10, 1, 1, 1);

        label_p20 = new QLabel(layoutWidget_3);
        label_p20->setObjectName(QString::fromUtf8("label_p20"));
        label_p20->setFont(font5);
        label_p20->setAlignment(Qt::AlignCenter);

        gridLayout_13->addWidget(label_p20, 10, 2, 1, 1);

        label_pr20 = new QLabel(layoutWidget_3);
        label_pr20->setObjectName(QString::fromUtf8("label_pr20"));
        label_pr20->setFont(font5);
        label_pr20->setAlignment(Qt::AlignCenter);

        gridLayout_13->addWidget(label_pr20, 10, 3, 1, 1);

        label_pn11 = new QLabel(layoutWidget_3);
        label_pn11->setObjectName(QString::fromUtf8("label_pn11"));
        label_pn11->setFont(font5);
        label_pn11->setAlignment(Qt::AlignCenter);

        gridLayout_13->addWidget(label_pn11, 1, 0, 1, 1);

        splitter_3->addWidget(layoutWidget_3);

        gridLayout_14->addWidget(splitter_3, 1, 0, 1, 1);

        tabWidget->addTab(tab_2, QString());

        gridLayout_2->addWidget(tabWidget, 1, 0, 1, 1);


        gridLayout_3->addLayout(gridLayout_2, 0, 1, 1, 1);

        gridLayout = new QGridLayout();
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        pushButton = new QPushButton(Transfer);
        pushButton->setObjectName(QString::fromUtf8("pushButton"));
        QFont font7;
        font7.setFamily(QString::fromUtf8("Century Gothic"));
        font7.setBold(true);
        font7.setWeight(75);
        pushButton->setFont(font7);
        pushButton->setCursor(QCursor(Qt::PointingHandCursor));

        gridLayout->addWidget(pushButton, 0, 0, 1, 1);

        pushButton_2 = new QPushButton(Transfer);
        pushButton_2->setObjectName(QString::fromUtf8("pushButton_2"));
        pushButton_2->setFont(font7);
        pushButton_2->setCursor(QCursor(Qt::PointingHandCursor));

        gridLayout->addWidget(pushButton_2, 1, 0, 1, 1);

        pushButton_4 = new QPushButton(Transfer);
        pushButton_4->setObjectName(QString::fromUtf8("pushButton_4"));
        pushButton_4->setFont(font7);
        pushButton_4->setCursor(QCursor(Qt::PointingHandCursor));

        gridLayout->addWidget(pushButton_4, 2, 0, 1, 1);

        verticalSpacer_4 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Maximum);

        gridLayout->addItem(verticalSpacer_4, 3, 0, 1, 1);


        gridLayout_3->addLayout(gridLayout, 0, 0, 1, 1);


        gridLayout_10->addLayout(gridLayout_3, 1, 0, 1, 1);


        gridLayout_11->addLayout(gridLayout_10, 0, 0, 1, 1);


        retranslateUi(Transfer);

        tabWidget->setCurrentIndex(1);


        QMetaObject::connectSlotsByName(Transfer);
    } // setupUi

    void retranslateUi(QDialog *Transfer)
    {
        Transfer->setWindowTitle(QApplication::translate("Transfer", "Dialog", 0, QApplication::UnicodeUTF8));
        label->setText(QApplication::translate("Transfer", "TRANSFER WINDOW", 0, QApplication::UnicodeUTF8));
        label_57->setText(QApplication::translate("Transfer", "Current money", 0, QApplication::UnicodeUTF8));
        label_58->setText(QApplication::translate("Transfer", "Expenditure", 0, QApplication::UnicodeUTF8));
        checkBox->setText(QApplication::translate("Transfer", "SELECT", 0, QApplication::UnicodeUTF8));
        checkBox_b1->setText(QApplication::translate("Transfer", "1", 0, QApplication::UnicodeUTF8));
        checkBox_b2->setText(QApplication::translate("Transfer", "2", 0, QApplication::UnicodeUTF8));
        checkBox_b3->setText(QApplication::translate("Transfer", "3", 0, QApplication::UnicodeUTF8));
        checkBox_b4->setText(QApplication::translate("Transfer", "4", 0, QApplication::UnicodeUTF8));
        checkBox_b5->setText(QApplication::translate("Transfer", "5", 0, QApplication::UnicodeUTF8));
        checkBox_b6->setText(QApplication::translate("Transfer", "6", 0, QApplication::UnicodeUTF8));
        checkBox_b7->setText(QApplication::translate("Transfer", "7", 0, QApplication::UnicodeUTF8));
        checkBox_b8->setText(QApplication::translate("Transfer", "8", 0, QApplication::UnicodeUTF8));
        checkBox_b9->setText(QApplication::translate("Transfer", "9", 0, QApplication::UnicodeUTF8));
        checkBox_b10->setText(QApplication::translate("Transfer", "10", 0, QApplication::UnicodeUTF8));
        label_2->setText(QApplication::translate("Transfer", "PLAYER NAME", 0, QApplication::UnicodeUTF8));
        label_3->setText(QApplication::translate("Transfer", "OVERALL", 0, QApplication::UnicodeUTF8));
        label_4->setText(QApplication::translate("Transfer", "POSITION", 0, QApplication::UnicodeUTF8));
        label_5->setText(QApplication::translate("Transfer", "TEAM", 0, QApplication::UnicodeUTF8));
        label_6->setText(QApplication::translate("Transfer", "PRICE", 0, QApplication::UnicodeUTF8));
        label_pn1->setText(QApplication::translate("Transfer", "- - - ", 0, QApplication::UnicodeUTF8));
        label_o1->setText(QApplication::translate("Transfer", "- - - ", 0, QApplication::UnicodeUTF8));
        label_p1->setText(QApplication::translate("Transfer", "- - - ", 0, QApplication::UnicodeUTF8));
        label_t1->setText(QApplication::translate("Transfer", "- - - ", 0, QApplication::UnicodeUTF8));
        label_pr1->setText(QApplication::translate("Transfer", "- - - ", 0, QApplication::UnicodeUTF8));
        label_pn2->setText(QApplication::translate("Transfer", "- - - ", 0, QApplication::UnicodeUTF8));
        label_o2->setText(QApplication::translate("Transfer", "- - - ", 0, QApplication::UnicodeUTF8));
        label_p2->setText(QApplication::translate("Transfer", "- - - ", 0, QApplication::UnicodeUTF8));
        label_t2->setText(QApplication::translate("Transfer", "- - - ", 0, QApplication::UnicodeUTF8));
        label_pr2->setText(QApplication::translate("Transfer", "- - - ", 0, QApplication::UnicodeUTF8));
        label_pn3->setText(QApplication::translate("Transfer", "- - - ", 0, QApplication::UnicodeUTF8));
        label_o3->setText(QApplication::translate("Transfer", "- - - ", 0, QApplication::UnicodeUTF8));
        label_p3->setText(QApplication::translate("Transfer", "- - - ", 0, QApplication::UnicodeUTF8));
        label_t3->setText(QApplication::translate("Transfer", "- - - ", 0, QApplication::UnicodeUTF8));
        label_pr3->setText(QApplication::translate("Transfer", "- - - ", 0, QApplication::UnicodeUTF8));
        label_pn4->setText(QApplication::translate("Transfer", "- - - ", 0, QApplication::UnicodeUTF8));
        label_o4->setText(QApplication::translate("Transfer", "- - - ", 0, QApplication::UnicodeUTF8));
        label_p4->setText(QApplication::translate("Transfer", "- - - ", 0, QApplication::UnicodeUTF8));
        label_t4->setText(QApplication::translate("Transfer", "- - - ", 0, QApplication::UnicodeUTF8));
        label_pr4->setText(QApplication::translate("Transfer", "- - - ", 0, QApplication::UnicodeUTF8));
        label_pn5->setText(QApplication::translate("Transfer", "- - - ", 0, QApplication::UnicodeUTF8));
        label_o5->setText(QApplication::translate("Transfer", "- - - ", 0, QApplication::UnicodeUTF8));
        label_p5->setText(QApplication::translate("Transfer", "- - - ", 0, QApplication::UnicodeUTF8));
        label_t5->setText(QApplication::translate("Transfer", "- - - ", 0, QApplication::UnicodeUTF8));
        label_pr5->setText(QApplication::translate("Transfer", "- - - ", 0, QApplication::UnicodeUTF8));
        label_pn6->setText(QApplication::translate("Transfer", "- - - ", 0, QApplication::UnicodeUTF8));
        label_o6->setText(QApplication::translate("Transfer", "- - - ", 0, QApplication::UnicodeUTF8));
        label_p6->setText(QApplication::translate("Transfer", "- - - ", 0, QApplication::UnicodeUTF8));
        label_t6->setText(QApplication::translate("Transfer", "- - - ", 0, QApplication::UnicodeUTF8));
        label_pr6->setText(QApplication::translate("Transfer", "- - - ", 0, QApplication::UnicodeUTF8));
        label_pn7->setText(QApplication::translate("Transfer", "- - - ", 0, QApplication::UnicodeUTF8));
        label_o7->setText(QApplication::translate("Transfer", "- - - ", 0, QApplication::UnicodeUTF8));
        label_p7->setText(QApplication::translate("Transfer", "- - - ", 0, QApplication::UnicodeUTF8));
        label_t7->setText(QApplication::translate("Transfer", "- - - ", 0, QApplication::UnicodeUTF8));
        label_pr7->setText(QApplication::translate("Transfer", "- - - ", 0, QApplication::UnicodeUTF8));
        label_pn8->setText(QApplication::translate("Transfer", "- - - ", 0, QApplication::UnicodeUTF8));
        label_o8->setText(QApplication::translate("Transfer", "- - - ", 0, QApplication::UnicodeUTF8));
        label_p8->setText(QApplication::translate("Transfer", "- - - ", 0, QApplication::UnicodeUTF8));
        label_t8->setText(QApplication::translate("Transfer", "- - - ", 0, QApplication::UnicodeUTF8));
        label_pr8->setText(QApplication::translate("Transfer", "- - - ", 0, QApplication::UnicodeUTF8));
        label_pn9->setText(QApplication::translate("Transfer", "- - - ", 0, QApplication::UnicodeUTF8));
        label_o9->setText(QApplication::translate("Transfer", "- - - ", 0, QApplication::UnicodeUTF8));
        label_p9->setText(QApplication::translate("Transfer", "- - - ", 0, QApplication::UnicodeUTF8));
        label_t9->setText(QApplication::translate("Transfer", "- - - ", 0, QApplication::UnicodeUTF8));
        label_pr9->setText(QApplication::translate("Transfer", "- - - ", 0, QApplication::UnicodeUTF8));
        label_pn10->setText(QApplication::translate("Transfer", "- - - ", 0, QApplication::UnicodeUTF8));
        label_o10->setText(QApplication::translate("Transfer", "- - - ", 0, QApplication::UnicodeUTF8));
        label_p10->setText(QApplication::translate("Transfer", "- - - ", 0, QApplication::UnicodeUTF8));
        label_t10->setText(QApplication::translate("Transfer", "- - - ", 0, QApplication::UnicodeUTF8));
        label_pr10->setText(QApplication::translate("Transfer", "- - - ", 0, QApplication::UnicodeUTF8));
        tabWidget->setTabText(tabWidget->indexOf(tab), QApplication::translate("Transfer", "BUY WINDOW", 0, QApplication::UnicodeUTF8));
        checkBox_12->setText(QApplication::translate("Transfer", "SELECT", 0, QApplication::UnicodeUTF8));
        checkBox_s1->setText(QApplication::translate("Transfer", "1", 0, QApplication::UnicodeUTF8));
        checkBox_s2->setText(QApplication::translate("Transfer", "2", 0, QApplication::UnicodeUTF8));
        checkBox_s3->setText(QApplication::translate("Transfer", "3", 0, QApplication::UnicodeUTF8));
        checkBox_s4->setText(QApplication::translate("Transfer", "4", 0, QApplication::UnicodeUTF8));
        checkBox_s5->setText(QApplication::translate("Transfer", "5", 0, QApplication::UnicodeUTF8));
        checkBox_s6->setText(QApplication::translate("Transfer", "6", 0, QApplication::UnicodeUTF8));
        checkBox_s7->setText(QApplication::translate("Transfer", "7", 0, QApplication::UnicodeUTF8));
        checkBox_s8->setText(QApplication::translate("Transfer", "8", 0, QApplication::UnicodeUTF8));
        checkBox_s9->setText(QApplication::translate("Transfer", "9", 0, QApplication::UnicodeUTF8));
        checkBox_s10->setText(QApplication::translate("Transfer", "10", 0, QApplication::UnicodeUTF8));
        label_59->setText(QApplication::translate("Transfer", "PLAYER NAME", 0, QApplication::UnicodeUTF8));
        label_60->setText(QApplication::translate("Transfer", "OVERALL", 0, QApplication::UnicodeUTF8));
        label_61->setText(QApplication::translate("Transfer", "POSITION", 0, QApplication::UnicodeUTF8));
        label_63->setText(QApplication::translate("Transfer", "PRICE", 0, QApplication::UnicodeUTF8));
        label_o11->setText(QApplication::translate("Transfer", "- - -", 0, QApplication::UnicodeUTF8));
        label_p11->setText(QApplication::translate("Transfer", "- - -", 0, QApplication::UnicodeUTF8));
        label_pr11->setText(QApplication::translate("Transfer", "- - -", 0, QApplication::UnicodeUTF8));
        label_pn12->setText(QApplication::translate("Transfer", "- - -", 0, QApplication::UnicodeUTF8));
        label_o12->setText(QApplication::translate("Transfer", "- - -", 0, QApplication::UnicodeUTF8));
        label_p12->setText(QApplication::translate("Transfer", "- - -", 0, QApplication::UnicodeUTF8));
        label_pr12->setText(QApplication::translate("Transfer", "- - -", 0, QApplication::UnicodeUTF8));
        label_pn13->setText(QApplication::translate("Transfer", "- - -", 0, QApplication::UnicodeUTF8));
        label_o13->setText(QApplication::translate("Transfer", "- - -", 0, QApplication::UnicodeUTF8));
        label_p13->setText(QApplication::translate("Transfer", "- - -", 0, QApplication::UnicodeUTF8));
        label_pr13->setText(QApplication::translate("Transfer", "- - -", 0, QApplication::UnicodeUTF8));
        label_pn14->setText(QApplication::translate("Transfer", "- - -", 0, QApplication::UnicodeUTF8));
        label_o14->setText(QApplication::translate("Transfer", "- - -", 0, QApplication::UnicodeUTF8));
        label_p14->setText(QApplication::translate("Transfer", "- - -", 0, QApplication::UnicodeUTF8));
        label_pr14->setText(QApplication::translate("Transfer", "- - -", 0, QApplication::UnicodeUTF8));
        label_pn15->setText(QApplication::translate("Transfer", "- - -", 0, QApplication::UnicodeUTF8));
        label_o15->setText(QApplication::translate("Transfer", "- - -", 0, QApplication::UnicodeUTF8));
        label_p15->setText(QApplication::translate("Transfer", "- - -", 0, QApplication::UnicodeUTF8));
        label_pr15->setText(QApplication::translate("Transfer", "- - -", 0, QApplication::UnicodeUTF8));
        label_pn16->setText(QApplication::translate("Transfer", "- - -", 0, QApplication::UnicodeUTF8));
        label_o16->setText(QApplication::translate("Transfer", "- - -", 0, QApplication::UnicodeUTF8));
        label_p16->setText(QApplication::translate("Transfer", "- - -", 0, QApplication::UnicodeUTF8));
        label_pr16->setText(QApplication::translate("Transfer", "- - -", 0, QApplication::UnicodeUTF8));
        label_pn17->setText(QApplication::translate("Transfer", "- - -", 0, QApplication::UnicodeUTF8));
        label_o17->setText(QApplication::translate("Transfer", "- - -", 0, QApplication::UnicodeUTF8));
        label_p17->setText(QApplication::translate("Transfer", "- - -", 0, QApplication::UnicodeUTF8));
        label_pr17->setText(QApplication::translate("Transfer", "- - -", 0, QApplication::UnicodeUTF8));
        label_pn18->setText(QApplication::translate("Transfer", "- - -", 0, QApplication::UnicodeUTF8));
        label_o18->setText(QApplication::translate("Transfer", "- - -", 0, QApplication::UnicodeUTF8));
        label_p18->setText(QApplication::translate("Transfer", "- - -", 0, QApplication::UnicodeUTF8));
        label_pr18->setText(QApplication::translate("Transfer", "- - -", 0, QApplication::UnicodeUTF8));
        label_pn19->setText(QApplication::translate("Transfer", "- - -", 0, QApplication::UnicodeUTF8));
        label_o19->setText(QApplication::translate("Transfer", "- - -", 0, QApplication::UnicodeUTF8));
        label_p19->setText(QApplication::translate("Transfer", "- - -", 0, QApplication::UnicodeUTF8));
        label_pr19->setText(QApplication::translate("Transfer", "- - -", 0, QApplication::UnicodeUTF8));
        label_pn20->setText(QApplication::translate("Transfer", "- - -", 0, QApplication::UnicodeUTF8));
        label_o20->setText(QApplication::translate("Transfer", "- - -", 0, QApplication::UnicodeUTF8));
        label_p20->setText(QApplication::translate("Transfer", "- - -", 0, QApplication::UnicodeUTF8));
        label_pr20->setText(QApplication::translate("Transfer", "- - -", 0, QApplication::UnicodeUTF8));
        label_pn11->setText(QApplication::translate("Transfer", "- - -", 0, QApplication::UnicodeUTF8));
        tabWidget->setTabText(tabWidget->indexOf(tab_2), QApplication::translate("Transfer", "SELL WINDOW", 0, QApplication::UnicodeUTF8));
        pushButton->setText(QApplication::translate("Transfer", "BUY PLAYER", 0, QApplication::UnicodeUTF8));
        pushButton_2->setText(QApplication::translate("Transfer", "SELL PLAYER", 0, QApplication::UnicodeUTF8));
        pushButton_4->setText(QApplication::translate("Transfer", "BACK", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class Transfer: public Ui_Transfer {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_TRANSFER_H
