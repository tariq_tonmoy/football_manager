#include "Encoder_1.h"

#include<iostream>
#include<string.h>
using namespace std;

char Encoder_1::encode(char ch){
    unsigned temp;
    EncUnion u;
    u.c=ch;

    temp=u.e.a;
    u.e.a=u.e.b;
    u.e.b=temp;

    temp=u.e.c;
    u.e.c=u.e.d;
    u.e.d=temp;

    temp=u.e.e;
    u.e.e=u.e.f;
    u.e.f=temp;

    temp=u.e.g;
    u.e.g=u.e.h;
    u.e.h=temp;

    return u.c;
}